﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.WebUtilities;

using IOTCell.BL.ClientControllerProvider;
using IOTCell.BL.Diagnostics;
using IOTCell.Framework.Models;
using IOTCell.BL.ViewModels;
using IOTCell.Framework.Enums;
using IOTCell.Custom.FloLive.BL.Configuration;

//using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class CustomBLProvider : IClientControllerProvider
    {
        #region Members
        private IConfiguration configuration;
        private IActivityEntryLogger logger;
        private IClientAppSettings clientAppSettings;
        #endregion

        #region Methods
        #region Init
        void IClientControllerProvider.Init(IConfiguration configuration, IActivityEntryLogger logger, NameValueCollection settings)
        {
            this.configuration = configuration;
            this.clientAppSettings = new ClientAppSettings(this.configuration);
            this.logger = logger;
        }
        #endregion

        #region ActivateService
        async Task<ResponseDetails> IClientControllerProvider.ActivateService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string token;

            //get authorization token
            ResponseDetails clientToken = await this.GetToken(customerViewModel);

            //check token
            if (clientToken.StatusCode == "OK")
            {
                TokenResultBody tokenResultBody = (TokenResultBody)clientToken.ResultResponse;
                token = tokenResultBody.content[0].Token.ToString();
            }
            else
            {
                responseDetails.StatusCode = clientToken.StatusCode.ToString();
                responseDetails.Message = clientToken.Message[0].ToString();

                return responseDetails;

            }

            //JW Added code to dettach the plan first, then activate for flo live  (Zhao)
            var requestUri = this.clientAppSettings.FloLiveBasePath + this.clientAppSettings.FloLiveAPIDetachOffer.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue);

            using (var client = new HttpClient())
            {

                using var request = new HttpRequestMessage(HttpMethod.Delete, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                await this.logger.AddEntryWithFlush("Change service - Detach offer - Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Change service - Detach offer - Client API", "Receive API Response");

                string resultBody = string.Empty;


                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;
            }


            requestUri = this.clientAppSettings.FloLiveBasePath + this.clientAppSettings.FloLiveAPIAttachOffer.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue).Replace("{id}", customerViewModel.PlanCode);

            using (var client = new HttpClient())
            {

                using var request = new HttpRequestMessage(HttpMethod.Post, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                await this.logger.AddEntryWithFlush("Post Activation Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Post Activation Client API", "Receive API Response");

                string resultBody = string.Empty;


                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion

        #region GetUsage
        async Task<ResponseDetails> IClientControllerProvider.GetUsage(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string token;

            //get authorization token
            ResponseDetails clientToken = await this.GetToken(customerViewModel);

            //check token
            if (clientToken.StatusCode == "OK")
            {
                TokenResultBody tokenResultBody = (TokenResultBody)clientToken.ResultResponse;
                token = tokenResultBody.content[0].Token.ToString();
            }
            else
            {
                responseDetails.StatusCode = clientToken.StatusCode.ToString();
                responseDetails.Message = clientToken.Message[0].ToString();

                return responseDetails;

            }





            string clientAPIGetUsageURL = this.clientAppSettings.FloLiveAPIGetUsage;

            UsageBody usageBody = new UsageBody
            {
                FromDate = customerViewModel.FromDate,
                //Service = "DATA",
                ToDate = customerViewModel.ToDate,
                Unit = "KB"
            };
            string jsonString = usageBody.ToString();

            var requestUri = this.clientAppSettings.FloLiveBasePath + clientAPIGetUsageURL.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue);

            using (var client = new HttpClient())
            {

               

                using var request = new HttpRequestMessage(HttpMethod.Post, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                //add token in header???
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Receive API Response");

                string resultBody = string.Empty;


                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                UsageResultFloLiveBody usageResultBody = JsonSerializer.Deserialize<UsageResultFloLiveBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (usageResultBody.ErrorMessage != null)
                responseDetails.Message = usageResultBody.ErrorMessage;
                responseDetails.ResultResponse = usageResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region GetCalculatedUsage
        async Task<ResponseDetails> IClientControllerProvider.GetCalculatedUsage(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIGetUsageURL = this.clientAppSettings.FloLiveAPIGetUsage + "/" + customerViewModel.SimIdentifierValue;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
                { "fromDate", DateTime.MinValue.ToString("yyyy-MM-dd") },
                { "toDate", DateTime.Now.ToString("yyyy-MM-dd") },
                { "filter", "Month"},
                { "type", customerViewModel.SimIdentifier},
                { "MVNO", clientAPIMVNO }
            };

            var requestUri = QueryHelpers.AddQueryString(clientAPIGetUsageURL, parameters);

            using (var client = new HttpClient())
            {
                var authenticationString = this.clientAppSettings.FloLiveIBSUsr + ":" + this.clientAppSettings.FloLiveIBSPwd;

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                UsageMonthResultBody usageMonthResultBody = JsonSerializer.Deserialize<UsageMonthResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = usageMonthResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = usageMonthResultBody;
            }

            return responseDetails;
        }
        #endregion



        #region GetTotalUsage
        async Task<ResponseDetails> IClientControllerProvider.GetTotalUsage(CustomerViewModel customerViewModel)
        {

            string userName = this.clientAppSettings.GetEncryptedValue("claudiu.daraban@itsp-inc.com");
            string pwd = this.clientAppSettings.GetEncryptedValue("Itsp1234!");
            //string uatUser = this.clientAppSettings.GetEncryptedValue("h3ixc3l@r");

            



            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIGetTotalUsageURL = this.clientAppSettings.FloLiveAPIGetTotalUsage;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
                { "fromDate", DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd") },
                { "toDate", DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd") },
                { "filter", "Day"},
                { "MVNO", clientAPIMVNO }
            };

            var requestUri = QueryHelpers.AddQueryString(clientAPIGetTotalUsageURL, parameters);

            using (var client = new HttpClient())
            {
                var authenticationString = this.clientAppSettings.FloLiveIBSUsr + ":" + this.clientAppSettings.FloLiveIBSPwd;

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Total Usage Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Total Usage Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                TotalUsageResultBody totalUsageResultBody = JsonSerializer.Deserialize<TotalUsageResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = totalUsageResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = totalUsageResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region TerminateService
        async Task<ResponseDetails> IClientControllerProvider.TerminateService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string token;

            //get authorization token
            ResponseDetails clientToken = await this.GetToken(customerViewModel);

            //check token
            if (clientToken.StatusCode == "OK")
            {
                TokenResultBody tokenResultBody = (TokenResultBody)clientToken.ResultResponse;
                token = tokenResultBody.content[0].Token.ToString();
            }
            else
            {
                responseDetails.StatusCode = clientToken.StatusCode.ToString();
                responseDetails.Message = clientToken.Message[0].ToString();

                return responseDetails;

            }

            var requestUri = this.clientAppSettings.FloLiveBasePath + this.clientAppSettings.FloLiveAPIDetachOffer.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue);

            using (var client = new HttpClient())
            {

                using var request = new HttpRequestMessage(HttpMethod.Delete, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                await this.logger.AddEntryWithFlush("Change service - Detach offer - Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Change service - Detach offer - Client API", "Receive API Response");

                string resultBody = string.Empty;


                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region SuspendService
        async Task<ResponseDetails> IClientControllerProvider.SuspendService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string token;

            //get authorization token
            ResponseDetails clientToken = await this.GetToken(customerViewModel);

            //check token
            if (clientToken.StatusCode == "OK")
            {
                TokenResultBody tokenResultBody = (TokenResultBody)clientToken.ResultResponse;
                token = tokenResultBody.content[0].Token.ToString();
            }
            else
            {
                responseDetails.StatusCode = clientToken.StatusCode.ToString();
                responseDetails.Message = clientToken.Message[0].ToString();

                return responseDetails;

            }





            string clientAPIChangeStatusURL = this.clientAppSettings.FloLiveAPIChangeStatus;

            SubscriberStatus subscriberStatus = new SubscriberStatus
            {
                Status = "SUSPEND",
            };
            string jsonString = subscriberStatus.ToString();

            var requestUri = this.clientAppSettings.FloLiveBasePath + clientAPIChangeStatusURL.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue);

            using (var client = new HttpClient())
            {
                using var request = new HttpRequestMessage(HttpMethod.Put, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Suspend service Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Suspend service Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options2 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options2);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;

            }

            return responseDetails;
        }
        #endregion


        #region ResumeService
        async Task<ResponseDetails> IClientControllerProvider.ResumeService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string token;

            //get authorization token
            ResponseDetails clientToken = await this.GetToken(customerViewModel);

            //check token
            if (clientToken.StatusCode == "OK")
            {
                TokenResultBody tokenResultBody = (TokenResultBody)clientToken.ResultResponse;
                token = tokenResultBody.content[0].Token.ToString();
            }
            else
            {
                responseDetails.StatusCode = clientToken.StatusCode.ToString();
                responseDetails.Message = clientToken.Message[0].ToString();

                return responseDetails;

            }

            string clientAPIChangeStatusURL = this.clientAppSettings.FloLiveAPIChangeStatus;

            SubscriberStatus subscriberStatus = new SubscriberStatus
            {
                Status = "ACTIVE",
            };

            string jsonString = subscriberStatus.ToString();

            var requestUri = this.clientAppSettings.FloLiveBasePath + clientAPIChangeStatusURL.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue);

            using (var client = new HttpClient())
            {
                using var request = new HttpRequestMessage(HttpMethod.Put, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Resume service Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Resume service Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options2 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options2);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;

            }

            return responseDetails;
        }
        #endregion


        #region ChangeServicePlan
        async Task<ResponseDetails> IClientControllerProvider.ChangeServicePlan(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string token;

            //get authorization token
            ResponseDetails clientToken = await this.GetToken(customerViewModel);

            //check token
            if (clientToken.StatusCode == "OK")
            {
                TokenResultBody tokenResultBody = (TokenResultBody)clientToken.ResultResponse;
                token = tokenResultBody.content[0].Token.ToString();
            }
            else
            {
                responseDetails.StatusCode = clientToken.StatusCode.ToString();
                responseDetails.Message = clientToken.Message[0].ToString();

                return responseDetails;

            }

            var requestUri = this.clientAppSettings.FloLiveBasePath + this.clientAppSettings.FloLiveAPIDetachOffer.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue);

            using (var client = new HttpClient())
            {

                using var request = new HttpRequestMessage(HttpMethod.Delete, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                await this.logger.AddEntryWithFlush("Change service - Detach offer - Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Change service - Detach offer - Client API", "Receive API Response");

                string resultBody = string.Empty;


                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;
            }


            requestUri = this.clientAppSettings.FloLiveBasePath + this.clientAppSettings.FloLiveAPIAttachOffer.Replace("{type}", customerViewModel.SimIdentifier.ToLower()).Replace("{value}", customerViewModel.SimIdentifierValue).Replace("{id}", customerViewModel.PlanCode);

            using (var client = new HttpClient())
            {

                using var request = new HttpRequestMessage(HttpMethod.Post, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                await this.logger.AddEntryWithFlush("Change Service - attach offer - Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Change Service - attach offer - Client API", "Receive API Response");

                string resultBody = string.Empty;


                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ResultFloLiveBody activationResultBody = JsonSerializer.Deserialize<ResultFloLiveBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                if (activationResultBody.ErrorMessage != null)
                    responseDetails.Message = activationResultBody.ErrorMessage.ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;

        }
        #endregion



        #region GetAvailableServicePlans
        async Task<ResponseDetails> IClientControllerProvider.GetAvailableServicePlans(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIProductsURL = this.clientAppSettings.FloLiveAPIProducts;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
                { "MVNO", clientAPIMVNO }
            };

            var requestUri = QueryHelpers.AddQueryString(clientAPIProductsURL, parameters);
            

            using (var client = new HttpClient())
            {
                var authenticationString = this.clientAppSettings.FloLiveIBSUsr + ":" + this.clientAppSettings.FloLiveIBSPwd;

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Available Service Plans Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Available Service Plans Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ServicePlanResultBody servicePlanResultBody = JsonSerializer.Deserialize<ServicePlanResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = servicePlanResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = servicePlanResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region GetToken
        async Task<ResponseDetails> GetToken(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIToken = this.clientAppSettings.FloLiveBasePath + this.clientAppSettings.FloLiveAPIPostToken;

            Token token = new Token
            {
                Username = this.clientAppSettings.FloLiveIBSUsr,
                Password = this.clientAppSettings.FloLiveIBSPwd
            };

            string jsonString = token.ToString();



            using (var client = new HttpClient())
            {

                using var request = new HttpRequestMessage(HttpMethod.Post, clientAPIToken);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Get Token Client API", "Send API Request");

               

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Total Usage Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                TokenResultBody tokenResultBody = JsonSerializer.Deserialize<TokenResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.ResultResponse = tokenResultBody;
                if (tokenResultBody.ErrorMessage != null)
                    responseDetails.Message = tokenResultBody.ErrorMessage.ToString();

            }

            return responseDetails;
        }
        #endregion



        #endregion
    }
}