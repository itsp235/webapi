﻿using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class Characteristic
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}