﻿using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class RootChangePlanOld
    {
        [JsonProperty("Plan")]
        public string Plan { get; set; }
    }
}