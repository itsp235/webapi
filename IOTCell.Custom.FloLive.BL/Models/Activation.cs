﻿using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class Activation
    {
        [JsonProperty("service")]
        public Service Service { get; set; }
    }
}