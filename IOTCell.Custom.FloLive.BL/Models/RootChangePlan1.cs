﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class RootChangePlan1
    {
        [JsonProperty("Characteristic")]
        public List<Characteristic> Characteristics { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }


    }
}