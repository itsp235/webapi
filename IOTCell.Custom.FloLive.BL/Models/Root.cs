﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class Root
    {
        [JsonProperty("activations")]
        public List<Activation> Activations { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}