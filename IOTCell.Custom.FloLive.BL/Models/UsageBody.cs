﻿using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class UsageBody
    {
        [JsonProperty("fromDate")]
        public string FromDate { get; set; }
        [JsonProperty("service")]
        public string Service { get; set; }
        
        [JsonProperty("toDate")]
        public string ToDate { get; set; }
        [JsonProperty("unit")]
        public string Unit { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

    }
}