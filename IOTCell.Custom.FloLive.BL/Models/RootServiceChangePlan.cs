﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class RootServiceChangePlan
    {
        [JsonProperty("service")]
        public Service Service { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

    }


}
