﻿using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class SubscriberStatus
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

    }
}