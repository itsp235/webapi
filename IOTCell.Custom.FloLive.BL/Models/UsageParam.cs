﻿using Newtonsoft.Json;

namespace IOTCell.Custom.FloLive.BL
{
    public class UsageParam
    {
        [JsonProperty("iccid")]
        public string ICCID { get; set; }
        [JsonProperty("eid")]
        public string EID { get; set; }
        [JsonProperty("fromDate")]
        public string FromDate { get; set; }
        [JsonProperty("toDate")]
        public string ToDate { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}