﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using IOTCell.Framework.Common;

namespace IOTCell.Custom.FloLive.BL.Configuration
{
    public class ClientAppSettings : IClientAppSettings
    {
        #region Members
        private readonly IConfiguration configuration;
        #endregion

        #region Properties

        #region FloLiveBasePath
        string IClientAppSettings.FloLiveBasePath
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveBasePath"]); }
        }
        #endregion

        #region FloLiveAPIPostToken
        string IClientAppSettings.FloLiveAPIPostToken
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIPostToken"]); }
        }
        #endregion


        #region FloLiveAPIAttachOffer
        string IClientAppSettings.FloLiveAPIAttachOffer
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIAttachOffer"]); }
        }
        #endregion

        #region FloLiveAPIAttachOffer
        string IClientAppSettings.FloLiveAPIDetachOffer
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIDetachOffer"]); }
        }
        #endregion


        #region FloLiveAPIGetUsage
        string IClientAppSettings.FloLiveAPIGetUsage
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIGetUsage"]); }
        }
        #endregion


        #region FloLiveAPIGetTotalUsage
        string IClientAppSettings.FloLiveAPIGetTotalUsage
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIGetTotalUsage"]); }
        }
        #endregion

        #region FloLiveAPIChangeStatus
        string IClientAppSettings.FloLiveAPIChangeStatus
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIChangeStatus"]); }
        }

        #region FloLiveAPIProducts
        string IClientAppSettings.FloLiveAPIProducts
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIProducts"]); }
        }
        #endregion



        #endregion



        #region FloLiveAPIPlanType
        string IClientAppSettings.FloLiveAPIPlanType
        {
            get { return this.GetSettingAsString(this.configuration["FloLiveAppSettings:FloLiveAPIPlanType"]); }
        }
        #endregion

        #region FloLiveIBSUsr
        string IClientAppSettings.FloLiveIBSUsr
        {
            get { return this.GetSettingAsString(this.PrivateGetDecryptedValue(this.configuration["FloLiveAppSettings:FloLiveIBSUsr"])); }
        }
        #endregion

        #region FloLiveIBSPwd
        string IClientAppSettings.FloLiveIBSPwd
        {
            get { return this.GetSettingAsString(this.PrivateGetDecryptedValue(this.configuration["FloLiveAppSettings:FloLiveIBSPwd"])); }
        }
        #endregion


        #endregion

        #region Constructor
        public ClientAppSettings(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        #endregion

        #region Methods
        #region GetSetting
        private object GetSetting(string value, Type type, object defaultValue)
        {
            bool exists = (value != null && value.Length > 0);
            if (exists)
            {
                object result = Converter.ConvertToType(type, value, string.Empty, defaultValue);
                return result;
            }
            else
            {
                return defaultValue;
            }
        }
        #endregion

        #region GetSettingAsString
        private string GetSettingAsString(string value)
        {
            return (string)this.GetSetting(value, typeof(string), string.Empty);
        }
        #endregion

        #region GetSettingAsBool
        private bool GetSettingAsBool(string key)
        {
            return (bool)this.GetSetting(key, typeof(bool), false);
        }

        private bool GetSettingAsBool(string key, bool defaultValue)
        {
            return (bool)this.GetSetting(key, typeof(bool), defaultValue);
        }
        #endregion

        #region GetSettingAsDouble
        private double GetSettingAsDouble(string key)
        {
            return (double)this.GetSetting(key, typeof(double), 0F);
        }
        #endregion

        #region GetSettingAsInt32
        private int GetSettingAsInt32(string key)
        {
            return (int)this.GetSetting(key, typeof(int), 0);
        }
        #endregion

        #region GetSettingAsLong
        private long GetSettingAsLong(string key)
        {
            return (long)this.GetSetting(key, typeof(long), 0);
        }
        #endregion

        #region GetSettingAsTimeSpan
        private TimeSpan GetSettingAsTimeSpan(string key)
        {
            string valueAsString = (string)this.GetSetting(key, typeof(string), TimeSpan.Zero.ToString());
            return TimeSpan.Parse(valueAsString);
        }
        #endregion

        #region GetEncryptedValue
        string IClientAppSettings.GetEncryptedValue(string value)
        {
            return EncryptionHelper.Encrypt(value,
                "rESThELWebaPiForioT",
                "ITSPhEl02032022IoT%ReSt-!^#%Web",
                "SHA512",
                2,
                "ItSp2021IoT*<@>Z",
                256);
        }
        #endregion

        #region PrivateGetDecryptedValue
        private string PrivateGetDecryptedValue(string value)
        {
            return EncryptionHelper.Decrypt(value,
                "rESThELWebaPiForioT",
                "ITSPhEl02032022IoT%ReSt-!^#%Web",
                "SHA512",
                2,
                "ItSp2021IoT*<@>Z",
                256);
        }
        #endregion

        #region GetDecryptedValue
        string IClientAppSettings.GetDecryptedValue(string value)
        {
            return this.PrivateGetDecryptedValue(value);
        }
        #endregion

        #endregion
    }
}