﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace IOTCell.Custom.FloLive.BL.Configuration
{
    public interface IClientAppSettings
    {
        string FloLiveBasePath { get; }
        string FloLiveAPIPostToken { get; }
        string FloLiveAPIAttachOffer { get; }
        string FloLiveAPIDetachOffer { get; }
        string FloLiveAPIGetUsage { get; }
        string FloLiveAPIGetTotalUsage { get; }
        string FloLiveAPIChangeStatus { get; }
        string FloLiveAPIProducts { get; }
        string FloLiveAPIPlanType { get; }
        string FloLiveIBSUsr { get; }
        string FloLiveIBSPwd { get; }

        string GetEncryptedValue(string value);
        string GetDecryptedValue(string value);
    }
}