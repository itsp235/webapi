﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public abstract class NullableStructBase
    {
		#region Properties
		private object baseValue;
        protected object BaseValue
        {
            get
            {
                return baseValue;
            }
            set
            {
                baseValue = value;
            }
        }

        public bool HasValue
        {
            get
            {
                if (this.BaseValue == null)
                {
                    return false;
                }
                
				return true;
            }
        }
		#endregion

		#region Methods
		#region SetNull
		public void SetNull()
        {
            this.BaseValue = null;
        }
		#endregion
		
		#region GetHashCode
		public override int GetHashCode()
        {
            if (!this.HasValue)
            {
                return this.ValueType.GetHashCode() ^ false.GetHashCode();
            }
            
			return this.ValueType.GetHashCode() ^ this.BaseValue.GetHashCode();
        }
		#endregion
		
		#region GetValue
		public object GetValue()
		{
			return this.BaseValue;
		}
		#endregion
		
		#region SetValue
		public void SetValue(object val)
		{
			this.BaseValue = val;
		}
		#endregion
		
		#region Clear
		public void Clear()
		{
			this.SetNull();
		}
		#endregion
		
		#region ToString
		public override string ToString()
		{
			if (this.HasValue)
			{
				object val = this.GetValue();
				return val.ToString();
			}
			
			return "[No Value]";
		}
		#endregion

		#region IsCorrectType
		protected bool IsCorrectType(NullableStructBase obj)
        {
            if (obj is null)
            {
                return true;
            }
            
			Type type = obj.GetType();
            return (type == this.ValueType);
        }
		#endregion

		#region AreEqual
		public static bool AreEqual(NullableStructBase a, NullableStructBase b)
        {
            if (a is null)
            {
                if (b is null)
                {
                    return true;
                }
                return b.Equals(a);
            }
            else if (b is null)
            {
                return !a.HasValue;
            }
            else
            {
                if (a.ValueType != b.ValueType)
                {
                    throw new Exception(string.Format("Cannot compare type {0} to type {1}.", b.ValueType.FullName, a.ValueType.FullName));
                }

                if (a.HasValue)
                {
                    if (b.HasValue)
                    {
                        return a.BaseValue.Equals(b.BaseValue);
                    }
                    return false;
                }
                else // a.HasValue == false
                {
                    return !b.HasValue;
                }
            }
        }
		#endregion
        
		#region Equals
		protected virtual bool Equals(NullableStructBase obj)
        {
            return NullableStructBase.AreEqual(this, obj);
        }
        
		public override bool Equals(object obj)
        {
            if (obj is NullableStructBase objNullableStructBase)
            {
                return this.Equals((NullableStructBase)objNullableStructBase);
            }
            else if (obj is bool objBool)
            {
                return this.Equals((bool)objBool);
            }
            else if (obj is null)
            {
                return NullableStructBase.AreEqual(this, null);
            }
            
			throw new Exception(string.Format("Cannot compare NullableBoolean to object of type {0}.", obj.GetType().FullName));
        }
		#endregion

		#region ValueType
		public abstract Type ValueType {get;}
		#endregion

		#region NullableStructBase
		public abstract NullableStructBase GetNewInstance();
		#endregion
		#endregion
    }
}