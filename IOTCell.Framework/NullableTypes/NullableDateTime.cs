﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public class NullableDateTime
        : NullableStructBase
    {
		#region Properties
		public DateTime Value
        {
            get
            {
                return (DateTime)this.BaseValue;
            }
            set
            {
                this.BaseValue = value;
            }
        }
		#endregion
        
		#region Operators
        public static explicit operator DateTime(NullableDateTime o)
        {
            if (!o.HasValue)
            {
                throw new Exception("Cannot convert null DateTimeo an DateTime.");
            }
           
			return o.Value;
        }
        public static implicit operator NullableDateTime(DateTime val)
        {
            return new NullableDateTime(val);
        }

		public static bool operator ==(NullableDateTime a, NullableDateTime b)
		{
            return NullableStructBase.AreEqual(a, b);
        }
		public static bool operator !=(NullableDateTime a, NullableDateTime b)
		{
            return !NullableStructBase.AreEqual(a, b);
        }

        public static bool operator >=(NullableDateTime a, NullableDateTime b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value >= b.Value);
        }
        
		public static bool operator > (NullableDateTime a, NullableDateTime b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value > b.Value);
        }
        
		public static bool operator <= (NullableDateTime a, NullableDateTime b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
           
			return (a.Value <= b.Value);
        }

        public static bool operator < (NullableDateTime a, NullableDateTime b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }

            return (a.Value < b.Value);
        }
        #endregion

		#region Constructors
		public NullableDateTime()
        {
        }

        public NullableDateTime(DateTime val)
        {
            this.Value = val;
        }
		#endregion

		#region Methods
		#region GetNewInstance
		public override NullableStructBase GetNewInstance()
        {
            return new NullableDateTime();
        }
		#endregion

		#region GetValueOrDefault
        public DateTime GetValueOrDefault()
        {
            if (this.HasValue)
            {
                return this.Value;
            }
            
			return DateTime.MinValue;
        }
		#endregion
        
		#region Equals
		public virtual bool Equals(NullableDateTime obj)
        {
            return base.Equals(obj);
        }
        
		public virtual bool Equals(DateTime obj)
        {
            NullableDateTime nullable = new NullableDateTime(obj);
            return this.Equals(nullable);
        }
		
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		#endregion
        
		#region ValueType
		public override Type ValueType
        {
            get 
			{ 
				return typeof(DateTime); 
			}
        }
		#endregion

		#region GetHashCode
		public override int GetHashCode()
		{	 
			return base.GetHashCode(); 
		}
		#endregion
		#endregion
    }
}