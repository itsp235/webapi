﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public class NullableInt32
        : NullableStructBase
    {
		#region Properties
		public int Value
        {
            get
            {
                return (int)this.BaseValue;
            }
            set
            {
                this.BaseValue = value;
            }
        }
		#endregion

        #region Operators
        public static explicit operator int(NullableInt32 o)
        {
            if (!o.HasValue)
            {
                throw new Exception("Cannot convert null into an int.");
            }
            
			return o.Value;
        }
        
		public static implicit operator NullableInt32(int val)
        {
            return new NullableInt32(val);
        }

        public static NullableInt32 operator +(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt32();
            }

            NullableInt32 result = new NullableInt32()
            {
                Value = a.Value + b.Value
            };

            return result;
        }
        
		public static NullableInt32 operator -(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt32();
            }

            NullableInt32 result = new NullableInt32()
            {
                Value = a.Value - b.Value
            };

            return result;
        }
        
		public static NullableInt32 operator *(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt32();
            }

            NullableInt32 result = new NullableInt32()
            {
                Value = a.Value * b.Value
            };

            return result;
        }
        
		public static NullableInt32 operator /(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt32();
            }

            NullableInt32 result = new NullableInt32()
            {
                Value = a.Value / b.Value
            };

            return result;
        }

		public static bool operator ==(NullableInt32 a, NullableInt32 b)
		{
			return NullableStructBase.AreEqual(a, b);
		}

		public static bool operator !=(NullableInt32 a, NullableInt32 b)
		{
            return !NullableStructBase.AreEqual(a, b);
        }

        public static bool operator >=(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value >= b.Value);
        }
        
		public static bool operator >(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value > b.Value);
        }
        
		public static bool operator <=(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value <= b.Value);
        }
        
		public static bool operator <(NullableInt32 a, NullableInt32 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value < b.Value);
        }
        #endregion

		#region Constructors
		public NullableInt32()
        {
        }

        public NullableInt32(int val)
        {
            this.Value = val;
        }
		#endregion

		#region Methods
		#region GetNewInstance
		public override NullableStructBase GetNewInstance()
        {
            return new NullableInt32();
        }
		#endregion
		
		#region GetValueOrDefault
        public int GetValueOrDefault()
        {
            if (this.HasValue)
            {
                return this.Value;
            }
            
			return 0;
        }
		#endregion

		#region Equals
		public virtual bool Equals(NullableInt32 obj)
        {
            return base.Equals(obj);
        }
        
		public virtual bool Equals(int obj)
        {
            NullableInt32 nullable = new NullableInt32(obj);
            return this.Equals(nullable);
        }

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		#endregion
        
		#region ValueType
		public override Type ValueType
        {
            get 
			{ 
				return typeof(int); 
			}
        }
		#endregion

		#region GetHashCode
		public override int GetHashCode()
		{	 
			return base.GetHashCode(); 
		}
		#endregion
		#endregion
    }
}