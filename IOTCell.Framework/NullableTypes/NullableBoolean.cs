﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public class NullableBoolean
        : NullableStructBase
    {
		#region Properties
		public bool Value
        {
            get
            {
                return (bool)this.BaseValue;
            }
            set
            {
                this.BaseValue = value;
            }
        }
		#endregion

        #region Operators
        public static explicit operator bool(NullableBoolean o)
        {
            if (!o.HasValue)
            {
                throw new Exception("Cannot convert null boolo an bool.");
            }
           
			return o.Value;
        }

        public static implicit operator NullableBoolean(bool val)
        {
            return new NullableBoolean(val);
        }

		public static bool operator == (NullableBoolean a, NullableBoolean b)
		{
            return NullableStructBase.AreEqual(a, b);
		}

		public static bool operator != (NullableBoolean a, NullableBoolean b)
		{
            return !NullableStructBase.AreEqual(a, b);
		}
        #endregion

		#region Constructors
		public NullableBoolean()
		{
		}
        
		public NullableBoolean(bool val)
		{
			this.Value = val;
		}
		#endregion

		#region Methods
		#region GetNewInstance
		public override NullableStructBase GetNewInstance()
        {
            return new NullableBoolean();
        }
		#endregion
        
		#region GetValueOrDefault
		public bool GetValueOrDefault()
        {
            if (this.HasValue)
            {
                return this.Value;
            }
            
			return false;
        }
		#endregion

		#region Equals
		public virtual bool Equals(NullableBoolean obj)
        {
            return NullableStructBase.AreEqual(this, obj);
        }
        
		public virtual bool Equals(bool obj)
        {
            NullableBoolean nullable = new NullableBoolean(obj);
            return NullableStructBase.AreEqual(this, nullable);
        }
        
		public override bool Equals(object obj)
        {
            if (obj is NullableBoolean objNullableBoolean)
            {
                return this.Equals((NullableBoolean)objNullableBoolean);
            }
            else if (obj is bool objBool)
            {
                return this.Equals((bool)objBool);
            }
            else if (obj is null)
            {
                return NullableStructBase.AreEqual(this, null);
            }
            
			throw new Exception(string.Format("Cannot compare NullableBoolean to object of type {0}.", obj.GetType().FullName));
        }
		#endregion
        
		#region ValueType
		public override Type ValueType
        {
            get 
			{ 
				return typeof(bool); 
			}
        }
		#endregion
			
		#region GetHashCode
		public override int GetHashCode()
		{	 
			return base.GetHashCode(); 
		}
		#endregion
		#endregion
    }
}