﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public class NullableDecimal
        : NullableStructBase
    {
		#region Properties
		public decimal Value
        {
            get
            {
                return (decimal)this.BaseValue;
            }
            set
            {
                this.BaseValue = value;
            }
        }
		#endregion
        
		#region Operators
        public static explicit operator decimal(NullableDecimal o)
        {
            if (!o.HasValue)
            {
                throw new Exception("Cannot convert null decimalo an decimal.");
            }

            return o.Value;
        }
        public static implicit operator NullableDecimal(decimal val)
        {
            return new NullableDecimal(val);
        }

        public static NullableDecimal operator +(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableDecimal();
            }

            NullableDecimal result = new NullableDecimal()
            {
                Value = a.Value + b.Value
            };

            return result;
        }
        public static NullableDecimal operator -(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableDecimal();
            }

            NullableDecimal result = new NullableDecimal()
            {
                Value = a.Value - b.Value
            };

            return result;
        }
        public static NullableDecimal operator *(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableDecimal();
            }

            NullableDecimal result = new NullableDecimal()
            {
                Value = a.Value * b.Value
            };

            return result;
        }
        public static NullableDecimal operator /(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableDecimal();
            }

            NullableDecimal result = new NullableDecimal()
            {
                Value = a.Value / b.Value
            };

            return result;
        }

		public static bool operator ==(NullableDecimal a, NullableDecimal b)
		{
            return NullableDecimal.AreEqual(a, b);
        }
		public static bool operator !=(NullableDecimal a, NullableDecimal b)
		{
            return !NullableDecimal.AreEqual(a, b);
        }
        public static bool operator >=(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }

            return (a.Value >= b.Value);
        }
        public static bool operator >(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }

            return (a.Value > b.Value);
        }
        public static bool operator <=(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }

            return (a.Value <= b.Value);
        }
        public static bool operator <(NullableDecimal a, NullableDecimal b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }

            return (a.Value < b.Value);
        }

        #endregion

		#region Constructors
		public NullableDecimal()
        {
        }
        
		public NullableDecimal(decimal val)
        {
            this.Value = val;
        }
		#endregion

		#region Methods
		#region GetNewInstance
		public override NullableStructBase GetNewInstance()
        {
            return new NullableDecimal();
        }
		#endregion

		#region GetValueOrDefault
        public decimal GetValueOrDefault()
        {
            if (this.HasValue)
            {
                return this.Value;
            }
            
			return 0;
        }
		#endregion

		#region Equals
        public virtual bool Equals(NullableDecimal obj)
        {
            return base.Equals(obj);
        }
       
		public virtual bool Equals(int obj)
        {
            NullableDecimal nullable = new NullableDecimal(obj);
            return this.Equals(nullable);
        }

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		#endregion
		
		#region ValueType
		public override Type ValueType
		{
			get 
			{ 
				return typeof(decimal); 
			}
		}
		#endregion

		#region GetHashCode
		public override int GetHashCode()
		{	 
			return base.GetHashCode(); 
		}
		#endregion
		#endregion
    }
}