﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public class NullableHelper
    {
		#region Methods
		#region IsNullable
		public static bool IsNullable(Type type)
        {
            bool isNullable = (type.BaseType == typeof(NullableStructBase));
            return isNullable;
        }
		#endregion

		#region CreateNullableInstance
		public static NullableStructBase CreateNullableInstance(Type type)
        {
            bool isNullable = NullableHelper.IsNullable(type);
            if (isNullable)
            {
                NullableStructBase instance = (NullableStructBase)Activator.CreateInstance(type);
                if (instance == null)
                {
                    throw new Exception(string.Format("Unable to create instance of nullable type {0}.", type.FullName));
                }

				return instance;
            }
            
			throw new NotImplementedException(string.Format("Cannot create a nullable instance of type {0}.", type.FullName));
        }
		#endregion
		#endregion
    }
}