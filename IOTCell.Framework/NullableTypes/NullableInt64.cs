﻿using System;

namespace IOTCell.Framework.NullableTypes
{
    public class NullableInt64
        : NullableStructBase
    {
		#region Properties
		public long Value
        {
            get
            {
                return (long)this.BaseValue;
            }
            set
            {
                this.BaseValue = value;
            }
        }
		#endregion

        #region Operators
        public static explicit operator long(NullableInt64 o)
        {
            if (!o.HasValue)
            {
                throw new Exception("Cannot convert null longo an long.");
            }
            
			return o.Value;
        }

        public static implicit operator NullableInt64(long val)
        {
            return new NullableInt64(val);
        }

        public static NullableInt64 operator +(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt64();
            }

            NullableInt64 result = new NullableInt64()
            {
                Value = a.Value + b.Value
            };

            return result;
        }
        
		public static NullableInt64 operator -(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt64();
            }

            NullableInt64 result = new NullableInt64()
            {
                Value = a.Value - b.Value
            };

            return result;
        }
        
		public static NullableInt64 operator *(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt64();
            }

            NullableInt64 result = new NullableInt64()
            {
                Value = a.Value * b.Value
            };
            
            return result;
        }
        
		public static NullableInt64 operator /(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return new NullableInt64();
            }

            NullableInt64 result = new NullableInt64()
            {
                Value = a.Value / b.Value
            };

            return result;
        }

        public static bool operator ==(NullableInt64 a, NullableInt64 b)
        {
            return NullableInt64.AreEqual(a, b);
        }
        
		public static bool operator !=(NullableInt64 a, NullableInt64 b)
        {
            return !NullableInt64.AreEqual(a, b);
        }

        public static bool operator >=(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }

            return (a.Value >= b.Value);
        }
        
		public static bool operator >(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
            
			return (a.Value > b.Value);
        }
        
		public static bool operator <=(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
        
			return (a.Value <= b.Value);
        }
        
		public static bool operator <(NullableInt64 a, NullableInt64 b)
        {
            if (a == null || !a.HasValue || b == null || !b.HasValue)
            {
                return false;
            }
        
			return (a.Value < b.Value);
        }
        #endregion

		#region Constructors
		public NullableInt64()
        {
        }

        public NullableInt64(long val)
        {
            this.Value = val;
        }
		#endregion

		#region Methods
		#region GetNewInstance
		public override NullableStructBase GetNewInstance()
        {
            return new NullableInt64();
        }
		#endregion

		#region GetValueOrDefault
		public long GetValueOrDefault()
        {
            if (this.HasValue)
            {
                return this.Value;
            }
            
			return 0;
        }
		#endregion

		#region Equals
		public virtual bool Equals(NullableInt64 obj)
        {
            return base.Equals(obj);
        }

        public virtual bool Equals(long obj)
        {
            NullableInt64 nullable = new NullableInt64(obj);
            return this.Equals(nullable);
        }

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		#endregion

		#region ValueType
		public override Type ValueType
        {
            get
			{ 
				return typeof(long); 
			}
        }
		#endregion

		#region GetHashCode
		public override int GetHashCode()
		{	 
			return base.GetHashCode(); 
		}
		#endregion
		#endregion
    }
}