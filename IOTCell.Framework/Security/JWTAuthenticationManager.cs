﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

using IOTCell.Framework.Configuration;
using IOTCell.Framework.Models;

namespace IOTCell.Framework.Security
{
    public class JWTAuthenticationManager : IJWTAuthenticationManager
    {
        #region Members
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        private readonly string tokenSecretKey;
        #endregion

        #region Constructor
        public JWTAuthenticationManager(IConfiguration configuration, string tokenSecretKey)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
            this.tokenSecretKey = tokenSecretKey;
        }
        #endregion

        #region Methods
        #region Authenticate
        TokenDetails IJWTAuthenticationManager.Authenticate(AuthenticationDetails authenticationDetails)
        {
            var claims = new Claim[] {
                new Claim(JwtRegisteredClaimNames.Sub, authenticationDetails.UserName)
             };

            var secretBytes = Encoding.UTF8.GetBytes(this.tokenSecretKey);
            var key = new SymmetricSecurityKey(secretBytes);
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);

            var token = new JwtSecurityToken(
                this.appSettings.JWTIssuer,
                this.appSettings.JWTAudience,
                claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials);

            var accessToken = new JwtSecurityTokenHandler().WriteToken(token);

            TokenDetails tokenDetails = new TokenDetails()
            {
                AccessToken = accessToken
            };

            return tokenDetails;
        }
        #endregion
        #endregion
    }
}