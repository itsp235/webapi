﻿using IOTCell.Framework.Models;

namespace IOTCell.Framework.Security
{
    public interface IJWTAuthenticationManager
    {
        public TokenDetails Authenticate(AuthenticationDetails authenticationDetails);
    }
}