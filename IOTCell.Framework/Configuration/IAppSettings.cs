﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace IOTCell.Framework.Configuration
{
    public interface IAppSettings
    {
        string ConnectionString { get; }
        string JWTIssuer { get; }
        string JWTAudience { get; }
        string TokenSecretKey { get; }
        string CustomCorrelationIDHeaderName { get; }
        string LogType { get; }
        bool LogAPIActivity { get; }
        bool LogActivity { get; }
        bool LogError { get; }
        bool MultiCarrier { get; }
        List<Dictionary<string, string>> ActivityParametersToMaskList { get; }
        string GetCorrelationID(IHeaderDictionary headers, string customCorrelationIDHeaderName);
        string GetEncryptedValue(string value);
        string GetDecryptedValue(string value);
        string GetEncodedValue(string value);
        string GetDecodedValue(string value);

    }
}