﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using IOTCell.Framework.Common;

namespace IOTCell.Framework.Configuration
{
    public class AppSettings : IAppSettings
    {
        #region Members
        private readonly IConfiguration configuration;
        #endregion

        #region Properties
        #region ConnectionString
        string IAppSettings.ConnectionString
        {
            //get { return this.GetSettingAsString(this.configuration.GetConnectionString("iot_cell1Database")); }
            get { return this.GetSettingAsString(this.configuration.GetConnectionString("iot_cell1Database1"))
                    + this.PrivateGetDecryptedValue(this.GetSettingAsString(this.configuration.GetConnectionString("iot_cell1DatabaseUser")))
                    + this.GetSettingAsString(this.configuration.GetConnectionString("iot_cell1Database2"))
                    + this.PrivateGetDecryptedValue(this.GetSettingAsString(this.configuration.GetConnectionString("iot_cell1DatabasePwd")))
                    + this.GetSettingAsString(this.configuration.GetConnectionString("iot_cell1Database3")); }
        }
        #endregion

        #region JWTIssuer
        string IAppSettings.JWTIssuer
        {
            get { return this.GetSettingAsString(this.configuration["AppSettings:JWTIssuer"]); }
        }
        #endregion

        #region JWTAudience
        string IAppSettings.JWTAudience
        {
            get { return this.GetSettingAsString(this.configuration["AppSettings:JWTAudience"]); }
        }
        #endregion

        #region TokenSecretKey
        string IAppSettings.TokenSecretKey
        {
            get { return EncryptionHelper.Encrypt("mYq3t6w9z$C&F)J@NcRfUjXnZr4u7x!A%D*G-KaPdSgVkYp3s5v8y/B?E(H+MbQe", "rESTpAyWebaPiForghT", "ITSPp@y05152020Bri%ReSt-!^#%Web", "SHA512", 2, "ItSp2020Pay*+&%I", 256); }
        }
        #endregion

        #region CustomCorrelationIDHeaderName
        string IAppSettings.CustomCorrelationIDHeaderName
        {
            get { return "Custom-Correlation-ID"; }
        }
        #endregion

        #region LogType
        string IAppSettings.LogType
        {
            get { return this.GetSettingAsString(this.configuration["AppSettings:LogType"]); }
        }
        #endregion

        #region LogAPIActivity
        bool IAppSettings.LogAPIActivity
        {
            get { return this.GetSettingAsBool(this.configuration["AppSettings:LogAPIActivity"]); }
        }
        #endregion

        #region LogActivity
        bool IAppSettings.LogActivity
        {
            get { return this.GetSettingAsBool(this.configuration["AppSettings:LogActivity"]); }
        }
        #endregion

        #region LogError
        bool IAppSettings.LogError
        {
            get { return this.GetSettingAsBool(this.configuration["AppSettings:LogError"]); }
        }
        #endregion

        #region MultiCarrier
        bool IAppSettings.MultiCarrier
        {
            get { return this.GetSettingAsBool(this.configuration["AppSettings:MultiCarrier"]); }
        }
        #endregion



        #region ActivityParametersToMaskList
        List<Dictionary<string, string>> IAppSettings.ActivityParametersToMaskList
        {
            get
            {
                var activityParametersToMaskList = new List<Dictionary<string, string>>();
                this.configuration.GetSection("AppSettings").GetSection("ActivityParametersToMask").Bind(activityParametersToMaskList);
                return activityParametersToMaskList;
            }
        }
        #endregion
        #endregion

        #region Constructor
        public AppSettings(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        #endregion

        #region Methods
        #region GetSetting
        private object GetSetting(string value, Type type, object defaultValue)
        {
            bool exists = (value != null && value.Length > 0);
            if (exists)
            {
                object result = Converter.ConvertToType(type, value, string.Empty, defaultValue);
                return result;
            }
            else
            {
                return defaultValue;
            }
        }
        #endregion

        #region GetSettingAsString
        private string GetSettingAsString(string value)
        {
            return (string)this.GetSetting(value, typeof(string), string.Empty);
        }
        #endregion

        #region GetSettingAsBool
        private bool GetSettingAsBool(string key)
        {
            return (bool)this.GetSetting(key, typeof(bool), false);
        }

        private bool GetSettingAsBool(string key, bool defaultValue)
        {
            return (bool)this.GetSetting(key, typeof(bool), defaultValue);
        }
        #endregion

        #region GetSettingAsDouble
        private double GetSettingAsDouble(string key)
        {
            return (double)this.GetSetting(key, typeof(double), 0F);
        }
        #endregion

        #region GetSettingAsInt32
        private int GetSettingAsInt32(string key)
        {
            return (int)this.GetSetting(key, typeof(int), 0);
        }
        #endregion

        #region GetSettingAsLong
        private long GetSettingAsLong(string key)
        {
            return (long)this.GetSetting(key, typeof(long), 0);
        }
        #endregion

        #region GetSettingAsTimeSpan
        private TimeSpan GetSettingAsTimeSpan(string key)
        {
            string valueAsString = (string)this.GetSetting(key, typeof(string), TimeSpan.Zero.ToString());
            return TimeSpan.Parse(valueAsString);
        }
        #endregion

        #region GetCorrelationID
        string IAppSettings.GetCorrelationID(IHeaderDictionary headers, string customCorrelationIDHeaderName)
        {
            if (headers.ContainsKey(customCorrelationIDHeaderName))
            {
                return headers[customCorrelationIDHeaderName];
            }

            return null;
        }
        #endregion

        #region GetEncryptedValue
        string IAppSettings.GetEncryptedValue(string value)
        {
            return EncryptionHelper.Encrypt(value,
                "rESTpAyWebaPiForghT",
                "ITSPC@s01202021IOT%ReSt)&@#%Api",
                "SHA512",
                2,
                "ItSp2021ioT+=^!z",
                256);
        }
        #endregion


        #region PrivateGetDecryptedValue
        private string PrivateGetDecryptedValue(string value)
        {
            return EncryptionHelper.Decrypt(value,
                "rESTpAyWebaPiForghT",
                "ITSPC@s01202021IOT%ReSt)&@#%Api",
                "SHA512",
                2,
                "ItSp2021ioT+=^!z",
                256);
        }
        #endregion



        #region GetDecryptedValue
        string IAppSettings.GetDecryptedValue(string value)
        {
            return this.PrivateGetDecryptedValue(value);
        }
        #endregion

        #region GetDecodedValue
        string IAppSettings.GetDecodedValue(string value)
        {
            if (value == null)
                return null;
            else
            { 
                var base64EncodedBytes = System.Convert.FromBase64String(value);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
        }
        #endregion

        #region GetEncodedValue
        string IAppSettings.GetEncodedValue(string value)
        {
            if (value == null)
                return null;
            else
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(value);
                return System.Convert.ToBase64String(plainTextBytes);
            }
        }

        #endregion


        #endregion
    }
}