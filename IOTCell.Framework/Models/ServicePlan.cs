﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ServicePlan
    {
        /// <summary>
        /// Plan code
        /// </summary>
        /// <example>2GB</example>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Plan description
        /// </summary>
        /// <example>2GB quota per card</example>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        /// <example>Active</example>
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}