﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class UsageMonth
    {
        /// <summary>
        /// From Date
        /// </summary>
        /// <example>2021-01-01</example>
        [JsonProperty("fromDate")]
        public string FromDate { get; set; }

        /// <summary>
        /// To Date
        /// </summary>
        /// <example>2021-01-31</example>
        [JsonProperty("toDate")]
        public string ToDate { get; set; }


        /// <summary>
        /// Volume per month (in KB)
        /// </summary>
        /// <example>12345.12</example>
        [JsonProperty("volume")]
        public decimal Volume { get; set; }

        /// <summary>
        /// Plan
        /// </summary>
        /// <example>2GB</example>
        [JsonProperty("plan")]
        public string Plan { get; set; }
    }
}