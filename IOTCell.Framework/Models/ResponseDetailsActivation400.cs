﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsActivation400
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>400</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Company code is empty</example>
        public string Message { get; set; }

    }
}
