﻿
using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class UsageMonthResultBody
    {
        [JsonProperty("resultCode")]
        public int ResultCode { get; set; }
        [JsonProperty("resultType")]
        public string ResultType { get; set; }
        [JsonProperty("Messages")]
        public List<string> Messages { get; set; }
        [JsonProperty("usages")]
        public List<UsageMonth> Usages { get; set; }
    }
}