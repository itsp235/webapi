﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsUsage404
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>404</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Company not found</example>
        public string Message { get; set; }

    }
}
