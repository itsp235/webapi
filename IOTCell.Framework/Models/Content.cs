﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class Content
    {
        [JsonProperty("customerId")]
        public string CustomerId { get; set; }
        [JsonProperty("refreshToken")]
        public string RefreshToken { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("validityTime")]
        public int ValidityTime { get; set; }
    }
}