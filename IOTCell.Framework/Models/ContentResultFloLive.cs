﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ContentResultFloLive
    {
        [JsonProperty("requestId")]
        public string RequestId { get; set; }
    }
}