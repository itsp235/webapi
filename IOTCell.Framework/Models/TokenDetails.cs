﻿namespace IOTCell.Framework.Models
{
    public class TokenDetails
    {
        public string AccessToken { get; set; }
    }
}