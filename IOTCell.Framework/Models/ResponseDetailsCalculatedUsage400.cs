﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsCalculatedUsage400
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>400</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Wrong SIM identifier (valid values EID or ICCID)</example>
        public string Message { get; set; }


        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
