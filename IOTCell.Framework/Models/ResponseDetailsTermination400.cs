﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsTermination400
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>400</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>SIM identifier (EID or ICCID) is empty</example>
        public string Message { get; set; }

    }
}
