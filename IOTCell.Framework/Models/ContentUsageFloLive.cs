﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ContentUsageFloLive
    {
        [JsonProperty("customerId")]
        public string CustomerId { get; set; }
        [JsonProperty("customerName")]
        public string CustomerName { get; set; }
        [JsonProperty("quantity")]
        public decimal Quantity { get; set; }
        [JsonProperty("subscriberId")]
        public string SubscriberId { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("unit")]
        public string Unit { get; set; }
    }
}