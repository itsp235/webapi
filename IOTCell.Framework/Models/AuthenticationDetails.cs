﻿namespace IOTCell.Framework.Models
{
    public class AuthenticationDetails
    {
        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        public string CompanyCode { get; set; }
        /// <summary>
        /// User name
        /// </summary>
        /// <example>exampleUser1!</example>
        public string UserName { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        /// <example>examplePassword1!</example>
        public string Password { get; set; }
    }
}