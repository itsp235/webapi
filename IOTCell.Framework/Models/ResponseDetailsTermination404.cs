﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsTermination404
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>404</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>This SIM was not found in the inventory database</example>
        public string Message { get; set; }

    }
}
