﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetails
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public object ResultResponse { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}