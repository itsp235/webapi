﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class IOTChangePlanResult
    {

        /// <summary>
        /// Status
        /// </summary>
        /// <example>Plan changed</example>
        [JsonProperty("status")]
        public string Status { get; set; }

        /// <summary>
        /// Effective date (activation date)
        /// </summary>
        /// <example>4/15/2020, 9:25:16 AM</example>
        [JsonProperty("effectiveDate")]
        public string EffectiveDate { get; set; }



        /// <summary>
        /// ICCID
        /// </summary>
        /// <example>8931080219101218961F</example>
        [JsonProperty("iccid")]
        public string ICCID { get; set; }


        /// <summary>
        /// EID
        /// </summary>
        /// <example>89001570010894932000523494032015</example>
        [JsonProperty("eid")]
        public string EID { get; set; }

        /// <summary>
        /// Plan
        /// </summary>
        /// <example>2GB</example>
        [JsonProperty("plan")]
        public string Plan { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Request performed successfully.</example>
        [JsonProperty("message")]
        public string Message { get; set; }


        /// <summary>
        /// RequestID from carrier's APIs
        /// </summary>
        /// <example>87bc4f9a-1f9c-4e27-9b45-d922d051a230</example>
        [JsonProperty("requestID")]
        public string RequestID { get; set; }


    }
}