﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class IOTServicePlanResult
    {
        /// <summary>
        /// Message
        /// </summary>
        /// <example>Request performed successfully.</example>
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("servicePlans")]
        public List<ServicePlan> ServicePlans { get; set; }

    }
}