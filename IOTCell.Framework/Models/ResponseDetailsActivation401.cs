﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsActivation401
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>401</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Unauthorized</example>
        public string Message { get; set; }

    }
}
