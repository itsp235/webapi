﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseToken
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public TokenDetails TokenDetails { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}