﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class TotalUsage
    {
        /// <summary>
        /// Date
        /// </summary>
        /// <example>2021-01-01</example>
        [JsonProperty("date")]
        public string Date { get; set; }

        /// <summary>
        /// Volume per day (in KB)
        /// </summary>
        /// <example>12345.12</example>
        [JsonProperty("totalVolume")]
        public decimal TotalVolume { get; set; }
    }
}