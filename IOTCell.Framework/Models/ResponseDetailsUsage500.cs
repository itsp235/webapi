﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsUsage500
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>500</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Operation failed.</example>
        public string Message { get; set; }

    }
}
