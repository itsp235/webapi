﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class TokenResultBody
    {
        [JsonProperty("content")]
        public List<Content> content { get; set; }

        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("pageable")]
        public Pageable Pageable { get; set; }


    }
}