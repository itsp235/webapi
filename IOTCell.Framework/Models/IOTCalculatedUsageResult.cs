﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class IOTCalculatedUsageResult
    {

        /// <summary>
        /// Total volume of data used in current month (in KB)
        /// </summary>
        /// <example>1234.61</example>
        [JsonProperty("totalVolumeCurrentMonth")]
        public decimal TotalVolumeCurrentMonth { get; set; }

        /// <summary>
        /// Total volume of data used in last 3 full months (in KB)
        /// </summary>
        /// <example>3211.63</example>
        [JsonProperty("totalVolumeLast3Months")]
        public decimal TotalVolumeLast3Months { get; set; }

        /// <summary>
        /// Total volume of data used in last 6 full months (in KB)
        /// </summary>
        /// <example>6012.63</example>
        [JsonProperty("totalVolumeLast6Months")]
        public decimal TotalVolumeLast6Months { get; set; }

        /// <summary>
        /// Total volume of data used in last 12 full months (in KB)
        /// </summary>
        /// <example>12345.63</example>
        [JsonProperty("totalVolumeLast12Months")]
        public decimal TotalVolumeLast12Months { get; set; }

        /// <summary>
        /// Total volume of data used in SIM's lifetime (in KB)
        /// </summary>
        /// <example>22345.63</example>
        [JsonProperty("totalVolumeLifetime")]
        public decimal TotalVolumeLifetime { get; set; }



        /// <summary>
        /// Message
        /// </summary>
        /// <example>Request performed successfully.</example>
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("usages")]
        public List<UsageMonth> Usages { get; set; }

    }
}