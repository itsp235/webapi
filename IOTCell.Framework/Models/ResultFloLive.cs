﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class ResultFloLiveBody
    {
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
        [JsonProperty("content")]
        public List<ContentResultFloLive> Content { get; set; }
        [JsonProperty("pageable")]
        public Pageable Pageable { get; set; }

    }
}