﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsActivationNoDB200
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>200</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Request performed successfully.</example>
        public string Message { get; set; }

        //        public IOTActivationResultNoDB IotActivationResultNoDB { get; set; }
        public IOTActivationResultNoDB ResultResponse { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}
