﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class IOTTotalUsageResult
    {
        /// <summary>
        /// Start Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-01</example>
        [JsonProperty("fromDate")]
        public string FromDate { get; set; }

        /// <summary>
        /// to Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-31</example>
        [JsonProperty("toDate")]
        public string ToDate { get; set; }



        /// <summary>
        /// Total volume of data used (in KB)
        /// </summary>
        /// <example>1234567.68</example>
        [JsonProperty("totalVolumePerPeriod")]
        public decimal TotalVolumePerPeriod { get; set; }



        /// <summary>
        /// Message
        /// </summary>
        /// <example>Request performed successfully.</example>
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("totalUsages")]
        public List<TotalUsage> TotalUsages { get; set; }

    }
}