﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class IOTUsageResult
    {
        /// <summary>
        /// ICCID
        /// </summary>
        /// <example>8931080219101218961F</example>
        [JsonProperty("iccid")]
        public string ICCID { get; set; }


        /// <summary>
        /// EID
        /// </summary>
        /// <example>89001570010894932000523494032015</example>
        [JsonProperty("eid")]
        public string EID { get; set; }

        /// <summary>
        /// Start Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-01</example>
        [JsonProperty("fromDate")]
        public string FromDate { get; set; }

        /// <summary>
        /// to Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-31</example>
        [JsonProperty("toDate")]
        public string ToDate { get; set; }

        /// <summary>
        /// Plan
        /// </summary>
        /// <example>2GB</example>
        [JsonProperty("plan")]
        public string Plan { get; set; }



        /// <summary>
        /// Total volume of data used (in KB)
        /// </summary>
        /// <example>1234567.68</example>
        [JsonProperty("totalVolume")]
        public decimal TotalVolume { get; set; }



        /// <summary>
        /// Message
        /// </summary>
        /// <example>Request performed successfully.</example>
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("usages")]
        public List<Usage> Usages { get; set; }

    }
}