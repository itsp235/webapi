﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IOTCell.Framework.Models
{
    public class ActivationResultBody
    {
        [JsonProperty("resultType")]
        public string ResultType { get; set; }
        [JsonProperty("messages")]
        public List<string> Messages { get; set; }
        [JsonProperty("result")]
        public Result Result { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("eid")]
        public string EID { get; set; }
        [JsonProperty("plan")]
        public string Plan { get; set; }
        [JsonProperty("iccid")]
        public string ICCID { get; set; }
        [JsonProperty("effectiveDate")]
        public string EffectiveDate { get; set; }
    }
}