﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class ResponseDetailsActivation404
    {
        /// <summary>
        /// Status code
        /// </summary>
        /// <example>404</example>

        public string StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <example>Company not found or wrong company code</example>
        public string Message { get; set; }

    }
}
