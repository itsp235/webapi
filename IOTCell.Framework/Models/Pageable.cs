﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class Pageable
    {
        [JsonProperty("page")]
        public int Page { get; set; }
        [JsonProperty("size")]
        public int Size { get; set; }
        [JsonProperty("totalElements")]
        public string TotalElements { get; set; }
        [JsonProperty("totalPages")]
        public string TotalPages { get; set; }
    }
}