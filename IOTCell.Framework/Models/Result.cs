﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class Result
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("eid")]
        public string EID { get; set; }
        [JsonProperty("plan")]
        public string Plan { get; set; }
        [JsonProperty("iccid")]
        public string ICCID { get; set; }
        [JsonProperty("effectiveDate")]
        public string EffectiveDate { get; set; }
    }
}