﻿using Newtonsoft.Json;

namespace IOTCell.Framework.Models
{
    public class Usage
    {
        /// <summary>
        /// Date
        /// </summary>
        /// <example>2021-01-01</example>
        [JsonProperty("date")]
        public string Date { get; set; }

        /// <summary>
        /// Volume per day (in KB)
        /// </summary>
        /// <example>12345.12</example>
        [JsonProperty("volume")]
        public decimal Volume { get; set; }

        /// <summary>
        /// Plan
        /// </summary>
        /// <example>2GB</example>
        [JsonProperty("plan")]
        public string Plan { get; set; }
    }
}