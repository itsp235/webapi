﻿using IOTCell.Framework.Models;

namespace IOTCell.Framework.Common
{
    public static class CommonHelper
    {
        #region Methods
        #region GetResponseDetails
        public static ResponseDetails GetResponseDetails(string statusCode, string message)
        {
            return new ResponseDetails()
            {
                StatusCode = statusCode,
                Message = message
            };
        }

        public static ResponseDetails GetResponseDetails(string statusCode, string message, object result)
        {
            return new ResponseDetails()
            {
                StatusCode = statusCode,
                Message = message,
                ResultResponse = result
            };
        }
        #endregion
        #endregion
    }
}