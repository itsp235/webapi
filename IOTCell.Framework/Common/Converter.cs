﻿using System;

using IOTCell.Framework.NullableTypes;

namespace IOTCell.Framework.Common
{
	public class Converter
	{
		#region Constructor
		private Converter()
		{
		}
		#endregion

		#region Methods
		#region ConvertToType
		public static object ConvertToType(Type newType, object valueToConvert, string format)
		{
			return Converter.ConvertToType(newType, valueToConvert, format, null);
		}

		public static object ConvertToType(Type newType, object valueToConvert, string format, object defaultValue)
		{
			bool isNewTypeNullable = NullableHelper.IsNullable(newType);

			// Return defaultValue if valueToConvert is null.
			if (valueToConvert == null || Convert.IsDBNull(valueToConvert) || (valueToConvert == DBNull.Value))
			{
				if (isNewTypeNullable && (defaultValue == null))
				{
					// Return an instance of the nullable type without a Value.
					NullableStructBase nullInstance = NullableHelper.CreateNullableInstance(newType);
					return nullInstance;
				}
				else
				{
					return defaultValue;
				}
			}

			Type oldType = valueToConvert.GetType();
			bool isOldTypeNullable = NullableHelper.IsNullable(oldType);

			if (isNewTypeNullable)
			{
				if (isOldTypeNullable)
				{
					// Convert the value to the desired raw type, and create an instance of the nullable equivalent.
					// Create an instance of the new, nullable type (A).
					NullableStructBase instance = NullableHelper.CreateNullableInstance(newType);

					// Get the raw type (B) of type A.
					Type rawType = instance.ValueType;

					// Convert the (nullable) value to type B.
					object newValue = Converter.ConvertToType(rawType, valueToConvert, format);

					// Assign this value to the instance of A.
					instance.SetValue(newValue);

					return instance;
				}
				else
				{
					// Only the new type is nullable.
					// NOTE: valueToConvert is not null.
					// Create an instance of the new, nullable type containing the converted value.

					// Create a new instance of the new, nullable type (A).
					NullableStructBase instance = NullableHelper.CreateNullableInstance(newType);

					// Determine the raw value type (B) of type A.
					Type rawType = instance.ValueType;

					// Convert the value to type B.
					object newValue = Converter.ConvertToType(rawType, valueToConvert, format);

					// Assign this value to the instance of A.
					instance.SetValue(newValue);

					return instance;
				}
			}
			else if (isOldTypeNullable)
			{
				// Convert the value to its non-nullable equivalent, and then convert that to the new type.
				// Cast the value to nullable.
				NullableStructBase nullableValue = (NullableStructBase)valueToConvert;

				// Get the raw value to convert. NOTE: It is not null.
				object rawValue = nullableValue.GetValue();

				// Convert the value.
				object newValue = Converter.ConvertToType(newType, rawValue, format);

				return newValue;
			}

			if (newType.BaseType == typeof(NullableStructBase))
			{
				NullableStructBase instance = (NullableStructBase)Activator.CreateInstance(newType);
				newType = instance.ValueType;
			}

			if (valueToConvert is NullableStructBase)
			{
				NullableStructBase nullableStruct = valueToConvert as NullableStructBase;
				if (nullableStruct.HasValue)
				{
					valueToConvert = nullableStruct.GetValue();
				}
				else
				{
					valueToConvert = null;
				}
			}

			// If both types are the same, return the value without converting.
			if (newType == oldType)
			{
				return valueToConvert;
			}

			if (newType.IsEnum)
			{
				if (valueToConvert is string)
				{
					string valueToConvertAsString = valueToConvert as string;
					return Enum.Parse(newType, valueToConvertAsString, true);
				}
				else if (valueToConvert is sbyte
					|| valueToConvert is Int16
					|| valueToConvert is Int32
					|| valueToConvert is Int64
					|| valueToConvert is Byte
					|| valueToConvert is UInt16
					|| valueToConvert is UInt32
					|| valueToConvert is UInt64)
				{
					if (!Enum.IsDefined(newType, valueToConvert))
					{
						throw new Exception(string.Format("{0} is not a valid value for enumeration type {1}.", valueToConvert, newType.FullName));
					}
                    
					return Enum.ToObject(newType, valueToConvert);
				}
                
				throw new Exception(string.Format("{0} is not of a data type that can be converted to an enumeration ({1}).", valueToConvert, newType.FullName));
			}
			else if (valueToConvert is string)
			{
				string valueToConvertAsString = valueToConvert as string;
				if (valueToConvertAsString.Length == 0 && newType != typeof(string))
				{
					// Cannot parse an empty string into anything other than a string, so return defaultValue.
					return defaultValue;
				}
                
				if (newType == typeof(DateTime))
				{
					DateTime valueAsDateTime;
					if (format != null && format.Length > 0)
					{
						valueAsDateTime = DateTime.ParseExact(valueToConvertAsString, format, System.Globalization.CultureInfo.InvariantCulture);
					}
					else
					{
						valueAsDateTime = DateTime.Parse(valueToConvertAsString);
					}
					
					return valueAsDateTime;
				}

				if (newType == typeof(TimeSpan))
				{
					TimeSpan valueAsTimeSpan = TimeSpan.Parse(valueToConvertAsString);
					return valueAsTimeSpan;
				}

				if (newType == typeof(bool))
				{
					string trimmedStringValue = valueToConvertAsString.Trim();
					if (trimmedStringValue.Equals("0"))
					{
						return false;
					}
					else if (trimmedStringValue.Equals("1"))
					{
						return true;
					}
				}
			}
			else if (valueToConvert is DateTime valueToConvertDateTime)
			{
				if (newType == typeof(string))
				{
					DateTime valueToConvertAsDateTime = (DateTime)valueToConvertDateTime;
					string formattedDateString;
					if (format == "JJJ")
					{
						int days = valueToConvertAsDateTime.DayOfYear;
						formattedDateString = days.ToString().PadLeft(3, '0');
					}
					else if (format == "y")
					{
						formattedDateString = valueToConvertAsDateTime.Year.ToString().Substring(3, 1);
					}
					else
					{
						formattedDateString = valueToConvertAsDateTime.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
					}

					return formattedDateString;
				}
			}
			else if (valueToConvert is decimal valueToConvertDecimal)
			{
				if (newType == typeof(string))
				{
					decimal valueAsNumber = (decimal)valueToConvertDecimal;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}
			else if (valueToConvert is int valueToConvertInt)
			{
				if (newType == typeof(string))
				{
					int valueAsNumber = (int)valueToConvertInt;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}
			else if (valueToConvert is short valueToConvertShort)
			{
				if (newType == typeof(string))
				{
					short valueAsNumber = (short)valueToConvertShort;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}
			else if (valueToConvert is byte valueToConvertByte)
			{
				if (newType == typeof(string))
				{
					byte valueAsNumber = (byte)valueToConvertByte;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}
			else if (valueToConvert is long valueToConvertLong)
			{
				if (newType == typeof(string))
				{
					long valueAsNumber = (long)valueToConvertLong;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}
			else if (valueToConvert is float valueToConvertFloat)
			{
				if (newType == typeof(string))
				{
					float valueAsNumber = (float)valueToConvertFloat;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}
			else if (valueToConvert is double valueToConvertDouble)
			{
				if (newType == typeof(string))
				{
					double valueAsNumber = (double)valueToConvertDouble;
					return valueAsNumber.ToString(format, System.Globalization.CultureInfo.InvariantCulture);
				}
			}

			object convertedValue = Convert.ChangeType(valueToConvert, newType, System.Globalization.CultureInfo.InvariantCulture);
			return convertedValue;
		}
		#endregion
		#endregion
	}
}