﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

using IOTCell.Framework.Configuration;
using IOTCell.Framework.Diagnostics;

namespace IOTCell.Framework.Common
{
    public class APILogHelper
    {
        #region Properties
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        #endregion

        #region Constructor
        public APILogHelper(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
        }
        #endregion

        #region Methods
        #region LogAPIActivity
        public async Task LogAPIActivity(APIActivity apiActivity)
        {           
                string activityLogMessage = this.LogActivityMessageBuilder(apiActivity);

                string activityLogFilePath = this.configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
                string activityLogFileName = "IOTCell.WebAPI.ActivityLog_" + DateTime.Now.ToString("yyyyMMdd") + ".log";

                activityLogFilePath = Path.Combine(activityLogFilePath, "Logs");
                if (!Directory.Exists(activityLogFilePath))
                {
                    Directory.CreateDirectory(activityLogFilePath);
                }

                activityLogFilePath = Path.Combine(activityLogFilePath, "ActivityLog");
                if (!Directory.Exists(activityLogFilePath))
                {
                    Directory.CreateDirectory(activityLogFilePath);
                }

            try
            {
                activityLogMessage = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + Environment.NewLine + activityLogMessage + "\r\n\r\n";

                byte[] encodedMessage = Encoding.Unicode.GetBytes(activityLogMessage);

                activityLogFilePath = Path.Combine(activityLogFilePath, activityLogFileName);

                using (FileStream sourceStream = new FileStream(activityLogFilePath,
                    FileMode.Append, FileAccess.Write, FileShare.None,
                    bufferSize: 4096, useAsync: true))
                {
                    await sourceStream.WriteAsync(encodedMessage, 0, encodedMessage.Length);
                };
            }
            catch (Exception exception)
            {
                StackTrace stackTrace = new StackTrace(exception, true);

                await this.LogError(new APIError
                {
                    CorrelationID = apiActivity.CorrelationID,
                    ErrorDateTime = DateTime.Now,
                    MethodName = exception.TargetSite.ToString(),
                    Message = exception.Message,
                    StackTrace = stackTrace.ToString(),
                    LineNumber = stackTrace.GetFrame(0).GetFileLineNumber(),
                    ColumnNumber = stackTrace.GetFrame(0).GetFileColumnNumber(),
                    FileName = stackTrace.GetFrame(0).GetFileName()
                });
            }
        }
        #endregion

        #region LogError
        public async Task LogError(ValidationException validationException, string correlationID)
        {
            StackTrace stackTrace = new StackTrace(validationException, true);

            string message = validationException.Message;

            if (validationException.InnerException != null)
            {
                message += Environment.NewLine + "Inner Exception is {0}" + validationException.InnerException;
            }
            
            await this.LogError(new APIError
            {
                CorrelationID = correlationID,
                ErrorDateTime = DateTime.Now,
                MethodName = validationException.TargetSite.ToString(),
                Message = message,
                StackTrace = stackTrace.ToString(),
                LineNumber = stackTrace.GetFrame(0).GetFileLineNumber(),
                ColumnNumber = stackTrace.GetFrame(0).GetFileColumnNumber(),
                FileName = stackTrace.GetFrame(0).GetFileName()
            });
        }

        public async Task LogError(Exception exception, string correlationID)
        {
            APIError apiError = this.GetAPIErrorFromException(exception, correlationID);
            await this.LogError(apiError);
        }

        private async Task LogError(APIError apiError)
        {
            string errorLogMessage = this.LogErrorMessageBuilder(apiError);

            string errorLogFilePath = this.configuration.GetValue<string>(WebHostDefaults.ContentRootKey);
            string errorLogFileName = "IOTCell.WebAPI.ErrorLog_" + DateTime.Now.ToString("yyyyMMdd") + ".log";

            errorLogFilePath = Path.Combine(errorLogFilePath, "Logs");
            if (!Directory.Exists(errorLogFilePath))
            {
                Directory.CreateDirectory(errorLogFilePath);
            }

            errorLogFilePath = Path.Combine(errorLogFilePath, "ErrorLog");
            if (!Directory.Exists(errorLogFilePath))
            {
                Directory.CreateDirectory(errorLogFilePath);
            }

            try
            {
                errorLogMessage = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + Environment.NewLine + errorLogMessage + "\r\n\r\n";

                await File.AppendAllTextAsync(Path.Combine(errorLogFilePath, errorLogFileName), errorLogMessage);
            }
            catch
            {
                // Do nothing
            }
        }
        #endregion

        #region GetAPIErrorFromValidationException
        public APIError GetAPIErrorFromValidationException(ValidationException validationException, string correlationID)
        {
            StackTrace stackTrace = new StackTrace(validationException, true);

            string message = validationException.Message;

            if (validationException.InnerException != null)
            {
                message += Environment.NewLine + "Inner Exception is {0}" + validationException.InnerException;
            }

            return new APIError()
            {
                CorrelationID = correlationID,
                ErrorDateTime = DateTime.Now,
                MethodName = validationException.TargetSite.ToString(),
                Message = message,
                StackTrace = stackTrace.ToString(),
                LineNumber = stackTrace.GetFrame(0).GetFileLineNumber(),
                ColumnNumber = stackTrace.GetFrame(0).GetFileColumnNumber(),
                FileName = stackTrace.GetFrame(0).GetFileName()
            };
        }
        #endregion

        #region GetAPIErrorFromException
        public APIError GetAPIErrorFromException(Exception exception, string correlationID)
        {
            StackTrace stackTrace = new StackTrace(exception, true);

            string message = exception.Message;

            if (exception.InnerException != null)
            {
                message += Environment.NewLine + "Inner Exception is {0}" + exception.InnerException;
            }

            return new APIError()
            {
                CorrelationID = correlationID,
                ErrorDateTime = DateTime.Now,
                MethodName = exception.TargetSite.ToString(),
                Message = message,
                StackTrace = stackTrace.ToString(),
                LineNumber = stackTrace.GetFrame(0).GetFileLineNumber(),
                ColumnNumber = stackTrace.GetFrame(0).GetFileColumnNumber(),
                FileName = stackTrace.GetFrame(0).GetFileName()
            };
        }
        #endregion

        #region LogActivityMessageBuilder
        private string LogActivityMessageBuilder(APIActivity apiActivity)
        {
            string activityLogMessage =
                "CorrelationID: " + apiActivity.CorrelationID + Environment.NewLine +
                "RequestTime: " + apiActivity.RequestTime + Environment.NewLine +
                "ResponseMillis: " + apiActivity.ResponseMilliseconds + Environment.NewLine +
                "StatusCode: " + apiActivity.StatusCode + Environment.NewLine +
                "Url: " + apiActivity.Url + Environment.NewLine +
                "Headers: " + apiActivity.HeadersText +
                "Method: " + apiActivity.Method + Environment.NewLine +
                "Path: " + apiActivity.Path + Environment.NewLine +
                "QueryString: " + apiActivity.QueryString + Environment.NewLine +
                "RequestBody: " + apiActivity.RequestBody + Environment.NewLine +
                "ResponseBody: " + apiActivity.ResponseBody;

            return activityLogMessage;
        }
        #endregion

        #region LogErrorMessageBuilder
        private string LogErrorMessageBuilder(APIError apiError)
        {
            string errorLogMessage =
                "CorrelationID: " + apiError.CorrelationID + Environment.NewLine +
                "Error Date Time: " + apiError.ErrorDateTime + Environment.NewLine +
                "Method Name: " + apiError.MethodName + Environment.NewLine +
                "Message: " + apiError.Message + Environment.NewLine +
                "Stack Trace: " + apiError.StackTrace +
                "Line Number: " + apiError.LineNumber + Environment.NewLine +
                "Column Number: " + apiError.ColumnNumber + Environment.NewLine +
                "File Name: " + apiError.FileName;

            return errorLogMessage;
        }
        #endregion
        #endregion
    }
}