﻿using System;

namespace IOTCell.Framework.Diagnostics
{
    public class APIError
    {
        public string CorrelationID { get; set; }
        public DateTime ErrorDateTime { get; set; }
        public string MethodName { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public int LineNumber { get; set; }
        public int ColumnNumber { get; set; }
        public string FileName { get; set; }
    }
}