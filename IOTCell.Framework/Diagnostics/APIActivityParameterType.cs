﻿namespace IOTCell.Framework.Diagnostics
{
    public enum APIActivityParameterType
    {
        All,
        AllExceptHeaders,
        Header,
        Url,
        QueryString,
        QueryStringAndUrl,
        RequestBody,
        ResponseBody
    }
}