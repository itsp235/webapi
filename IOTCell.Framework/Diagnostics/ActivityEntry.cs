using System;

namespace IOTCell.Framework.Diagnostics
{
	public class ActivityEntry
	{
		public string EntryType { get; set; }
		public string CorrelationID { get; set; }
		public string ActivityText { get; set; }
		public DateTime ActivityDate { get; set; }
	}
}