﻿using System;

using Microsoft.AspNetCore.Http;

namespace IOTCell.Framework.Diagnostics
{
    public class APIActivity
    {
        public string CorrelationID { get; set; }
        public DateTime RequestTime { get; set; }
        public long ResponseMilliseconds { get; set; }
        public int StatusCode { get; set; }
        public string Url { get; set; }
        public IHeaderDictionary Headers { get; set; }
        public string HeadersText { get; set; }
        public string Method { get; set; }
        public string Path { get; set; }
        public string QueryString { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
    }
}