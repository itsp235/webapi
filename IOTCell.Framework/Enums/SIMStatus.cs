﻿
namespace IOTCell.Framework.Enums
{
	public enum SIMStatus
	{
		Active = 1,
		PlanChanged = 2,
		Suspended_Inactive = 0,
		Cancelled_Inactive = -1
	}
}