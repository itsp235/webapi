﻿namespace IOTCell.Framework.Enums
{
	public enum OrderStatus
	{
		Active = 1,
		Inactive = 0,
		PlanChanged = -1
	}
}