﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using IOTCell.BL;
using IOTCell.Framework.Common;
using IOTCell.Framework.Configuration;
using IOTCell.Framework.Diagnostics;
using IOTCell.Framework.Enums;
using IOTCell.Framework.Models;

namespace IOTCell.WebAPI.Middleware.Extensions
{
    public class ExceptionLogMiddleware
    {
        #region Members
        private readonly RequestDelegate next;
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        #endregion

        #region Constructor
        public ExceptionLogMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            this.next = next;
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
        }
        #endregion

        #region Methods
        #region InvokeAsync
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                if (!httpContext.Response.HasStarted)
                {
                    httpContext.Response.ContentType = "application/json";
                    await this.next(httpContext);
                }
            }
            // For Entity Framework. If using Validator.ValidateObject to validate data,  it will throw an ValidationException if validation fails. 
            // DbEntityValidationException is no longer available in Entity Framework Core 3.1
            catch (ValidationException validationException)
            {
                if (appSettings.LogError)
                {
                    string correlationID = this.appSettings.GetCorrelationID(httpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);

                    APILogHelper apiLogHelper = new APILogHelper(this.configuration);

                    // Log the Error
                    LogType logType = (LogType)Enum.Parse(typeof(LogType), this.appSettings.LogType, true);
                    switch (logType)
                    {
                        case LogType.File:
                            await apiLogHelper.LogError(validationException, correlationID);
                            break;
                        case LogType.Database:
                            APIError apiError = apiLogHelper.GetAPIErrorFromValidationException(validationException, correlationID);
                            IBLProvider blProvider = new BLProvider(this.configuration, correlationID);
                            await blProvider.LogError(apiError);
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Invalid Log Type is configured"));
                    }
                }

                await HandleExceptionAsync(httpContext);
            }
            catch (Exception exception)
            {
                if (appSettings.LogError)
                {
                    string correlationID = this.appSettings.GetCorrelationID(httpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);

                    APILogHelper apiLogHelper = new APILogHelper(this.configuration);

                    // Log the Error
                    LogType logType = (LogType)Enum.Parse(typeof(LogType), this.appSettings.LogType, true);
                    switch (logType)
                    {
                        case LogType.File:
                            await apiLogHelper.LogError(exception, correlationID);
                            break;
                        case LogType.Database:
                            APIError apiError = apiLogHelper.GetAPIErrorFromException(exception, correlationID);
                            IBLProvider blProvider = new BLProvider(this.configuration, correlationID);
                            await blProvider.LogError(apiError);
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Invalid Log Type is configured"));
                    }
                }

                await HandleExceptionAsync(httpContext);
            }
        }
        #endregion

        #region HandleExceptionAsync
        private Task HandleExceptionAsync(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(new ResponseDetails()
            {
                StatusCode = context.Response.StatusCode.ToString(),
                Message = "Internal Server Error. Please have administrator check logs."
            }.ToString());
        }
        #endregion
        #endregion
    }
}