﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using IOTCell.BL;
using IOTCell.Framework.Common;
using IOTCell.Framework.Configuration;
using IOTCell.Framework.Enums;
using IOTCell.Framework.Diagnostics;

namespace IOTCell.WebAPI.Middleware
{
    public class ActivityLogMiddleware
    {
        #region Members
        private readonly RequestDelegate next;
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        private readonly RecyclableMemoryStreamManager recyclableMemoryStreamManager;
        private readonly string maskedMessage = "Masked By App";
        #endregion

        #region Constructor
        public ActivityLogMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            this.next = next;
            this.configuration = configuration; ;
            this.appSettings = new AppSettings(this.configuration);
            this.recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
        }
        #endregion

        #region Methods
        #region InvokeAsync
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                var request = httpContext.Request;

                if (request.Path.Value.ToLower().Contains("/api/") &&
                    !request.Path.Value.ToLower().Contains("/api/error"))
                {
                    // Correlation ID
                    string correlationID = Guid.NewGuid().ToString();
                    httpContext.Response.Headers.Add(this.appSettings.CustomCorrelationIDHeaderName, correlationID);

                    var stopWatch = Stopwatch.StartNew();
                    var requestTime = DateTime.Now;

                    // Read the request body
                    var requestBodyContent = await ReadRequestBody(request);

                    var originalBodyStream = httpContext.Response.Body;

                    // Recycle the memory stream to avoid any memory leaks
                    await using var responseBody = this.recyclableMemoryStreamManager.GetStream();

                    var response = httpContext.Response;
                    response.Body = responseBody;

                    if (!httpContext.Response.HasStarted)
                    {
                        await this.next(httpContext);
                    }

                    stopWatch.Stop();

                    // Read the response body
                    string responseBodyContent = null;
                    responseBodyContent = await ReadResponseBody(response);
                    await responseBody.CopyToAsync(originalBodyStream);

                    correlationID = this.appSettings.GetCorrelationID(httpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);

                    await SafeActivityLog(new APIActivity
                    {
                        CorrelationID = correlationID,
                        RequestTime = requestTime,
                        ResponseMilliseconds = stopWatch.ElapsedMilliseconds,
                        StatusCode = response.StatusCode,
                        Url = request.GetEncodedUrl(),
                        Headers = request.Headers,
                        Method = request.Method,
                        Path = request.Path,
                        QueryString = request.QueryString.ToString(),
                        RequestBody = requestBodyContent,
                        ResponseBody = responseBodyContent
                    }); ;
                }
                else
                {
                    if (!httpContext.Response.HasStarted)
                    {
                        await this.next(httpContext);
                    }
                }
            }
            catch (Exception exception)
            {
                if (appSettings.LogError)
                {
                    string correlationID = this.appSettings.GetCorrelationID(httpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);

                    APILogHelper apiLogHelper = new APILogHelper(this.configuration);

                    // Log the Error
                    LogType logType = (LogType)Enum.Parse(typeof(LogType), appSettings.LogType, true);
                    switch (logType)
                    {
                        case LogType.File:
                            await apiLogHelper.LogError(exception, correlationID);
                            break;
                        case LogType.Database:
                            APIError apiError = apiLogHelper.GetAPIErrorFromException(exception, correlationID);
                            IBLProvider blProvider = new BLProvider(this.configuration, correlationID);
                            await blProvider.LogError(apiError);
                            break;
                        default:
                            throw new NotImplementedException(string.Format("Invalid Log Type is configured"));
                    }
                }

                if (!httpContext.Response.HasStarted)
                {
                    await this.next(httpContext);
                }
            }
        }
        #endregion

        #region ReadRequestBody
        private async Task<string> ReadRequestBody(HttpRequest request)
        {
            request.EnableBuffering();

            //var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            //await request.Body.ReadAsync(buffer, 0, buffer.Length);
            //var bodyAsText = Encoding.UTF8.GetString(buffer);
            //request.Body.Seek(0, SeekOrigin.Begin);

            //return bodyAsText;

            // Recycle the memory stream to avoid any memory leaks
            await using var requestStream = this.recyclableMemoryStreamManager.GetStream();
            await request.Body.CopyToAsync(requestStream);
            string requestBody = this.ReadStreamInBlocks(requestStream);
            request.Body.Position = 0;

            return requestBody;
        }
        #endregion

        #region ReadResponseBody
        private async Task<string> ReadResponseBody(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            var responseBody = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return responseBody;
        }
        #endregion

        #region ReadStreamInBlocks
        private string ReadStreamInBlocks(Stream stream)
        {
            const int readBlockBufferLength = 4096;

            stream.Seek(0, SeekOrigin.Begin);

            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);

            var readBlock = new char[readBlockBufferLength];
            int readBlockLength;

            do
            {
                readBlockLength = reader.ReadBlock(readBlock, 0, readBlockBufferLength);
                textWriter.Write(readBlock, 0, readBlockLength);
            } while (readBlockLength > 0);

            return textWriter.ToString();
        }
        #endregion

        #region SafeActivityLog
        private async Task SafeActivityLog(APIActivity apiActivity)
        {
            if (this.appSettings.LogAPIActivity)
            {
                APILogHelper apiLogHelper = new APILogHelper(this.configuration);

                // Mask the Activity
                apiActivity = this.MaskActivityLog(apiActivity);

                // Log the Activity
                LogType logType = (LogType)Enum.Parse(typeof(LogType), appSettings.LogType, true);
                switch (logType)
                {
                    case LogType.File:
                        await apiLogHelper.LogAPIActivity(apiActivity);
                        break;
                    case LogType.Database:
                        IBLProvider blProvider = new BLProvider(this.configuration, apiActivity.CorrelationID);
                        await blProvider.LogAPIActivity(apiActivity);
                        break;
                    default:
                        throw new NotImplementedException(string.Format("Invalid Log Type is configured"));
                }
            }
        }
        #endregion

        #region MaskActivityLog
        private APIActivity MaskActivityLog(APIActivity apiActivity)
        {
            apiActivity.HeadersText = this.MaskHeader(apiActivity.Headers, string.Empty, string.Empty).ToString();

            var activityParametersToMaskList = new List<Dictionary<string, string>>();
            activityParametersToMaskList = this.appSettings.ActivityParametersToMaskList;

            var filteredList = from a in activityParametersToMaskList
                                where a["APIPath"].ToLower() == apiActivity.Path.ToLower()
                                select a;
           
            foreach (Dictionary<string, string> list in filteredList)
            {
                string apiPath = list["APIPath"].ToLower();
                string parameterType = list["ParameterType"].ToLower();
                string parameterName = list["ParameterName"].ToLower();

                if (apiPath == apiActivity.Path.ToLower())
                {
                    if (parameterType == APIActivityParameterType.All.ToString().ToLower())
                    {
                        apiActivity.Url = this.maskedMessage;
                        apiActivity.QueryString = this.maskedMessage;
                        apiActivity.RequestBody = this.maskedMessage;
                        apiActivity.ResponseBody = this.maskedMessage;

                        apiActivity.HeadersText = this.MaskHeader(apiActivity.Headers, parameterType, parameterName).ToString();
                    }

                    if (parameterType == APIActivityParameterType.AllExceptHeaders.ToString().ToLower())
                    {
                        apiActivity.Url = this.maskedMessage;
                        apiActivity.QueryString = this.maskedMessage;
                        apiActivity.RequestBody = this.maskedMessage;
                        apiActivity.ResponseBody = this.maskedMessage;
                    }

                    if (parameterType == APIActivityParameterType.Header.ToString().ToLower())
                    {
                        apiActivity.HeadersText = this.MaskHeader(apiActivity.Headers, parameterType, parameterName).ToString();
                    }

                    if (parameterType == APIActivityParameterType.Url.ToString().ToLower())
                    {
                        if (string.IsNullOrEmpty(parameterName))
                        {
                            apiActivity.Url = this.maskedMessage;
                        }
                        else
                        {
                            if (apiActivity.Url.Length > 0)
                            {
                                apiActivity = this.MaskQueryString(apiActivity, parameterName);
                            }
                        }
                    }

                    if (parameterType == APIActivityParameterType.QueryString.ToString().ToLower())
                    {
                        if (string.IsNullOrEmpty(parameterName))
                        {
                            apiActivity.QueryString = this.maskedMessage;
                        }
                        else
                        {
                            if (apiActivity.QueryString.Length > 0)
                            {
                                apiActivity = this.MaskQueryString(apiActivity, parameterName);
                            }
                        }
                    }

                    if (parameterType == APIActivityParameterType.QueryStringAndUrl.ToString().ToLower())
                    {
                        if (string.IsNullOrEmpty(parameterName))
                        {
                            apiActivity.QueryString = this.maskedMessage;
                            apiActivity.Url = this.maskedMessage;
                        }
                        else
                        {
                            if (apiActivity.QueryString.Length > 0)
                            {
                                apiActivity = this.MaskQueryString(apiActivity, parameterName);
                            }
                        }
                    }

                    if (parameterType == APIActivityParameterType.RequestBody.ToString().ToLower())
                    {
                        if (string.IsNullOrEmpty(parameterName))
                        {
                            apiActivity.RequestBody = this.maskedMessage;
                        }
                        else
                        {
                            if (apiActivity.RequestBody.Length > 0)
                            {
                                apiActivity.RequestBody = this.MaskJsonString(apiActivity.RequestBody, parameterName);
                            }
                        }
                    }

                    if (parameterType == APIActivityParameterType.ResponseBody.ToString().ToLower())
                    {
                        if (string.IsNullOrEmpty(parameterName))
                        {
                            apiActivity.ResponseBody = this.maskedMessage;
                        }
                        else
                        {
                            if (apiActivity.ResponseBody.Length > 0)
                            {
                                apiActivity.ResponseBody = this.MaskJsonString(apiActivity.ResponseBody, parameterName);
                            }
                        }
                    }
                }
            }

            return apiActivity;
        }
        #endregion

        #region MaskHeader
        private StringBuilder MaskHeader(IHeaderDictionary headers, string parameterType, string parameterName)
        {
            // Used to determine the first request header. Used to add tabbing to headers in log file
            Boolean firstRequestHeader = false;

            var resquestHeaders = new StringBuilder();

            foreach (var header in headers)
            {
                string tabCharacter = string.Empty;
                if (!firstRequestHeader)
                {
                    firstRequestHeader = true;
                    tabCharacter = string.Empty;
                }
                else
                {
                    tabCharacter = "\t";
                }

                if (parameterType == APIActivityParameterType.All.ToString().ToLower())
                {
                    resquestHeaders.Append($"{tabCharacter + header.Key}: {string.Join(", ", this.maskedMessage)}{Environment.NewLine}");
                }
                else
                {
                    if (parameterName.Trim().Length > 0)
                    {
                        if (header.ToString().ToLower().IndexOf(parameterName) >= 0)
                        {
                            resquestHeaders.Append($"{tabCharacter + header.Key}: {string.Join(", ", this.maskedMessage)}{Environment.NewLine}");
                        }
                        else
                        {
                            resquestHeaders.Append($"{tabCharacter + header.Key}: {string.Join(", ", header.Value)}{Environment.NewLine}");
                        }
                    }
                    else
                    {
                        resquestHeaders.Append($"{tabCharacter + header.Key}: {string.Join(", ", header.Value)}{Environment.NewLine}");
                    }
                }
            }

            return resquestHeaders;
        }
        #endregion

        #region MaskQueryString
        private APIActivity MaskQueryString(APIActivity apiActivity, string parameterName)
        {
            string[] queryStringList = apiActivity.QueryString.Replace("?", string.Empty).Split("&");
            foreach (string queryStringParameterNameAndValue in queryStringList)
            {
                string[] parameterList = queryStringParameterNameAndValue.Split("=");
                if (parameterList.Length > 0)
                {
                    if (parameterList[0].ToLower().IndexOf(parameterName) >= 0)
                    {
                        apiActivity.Url = apiActivity.Url.Replace(parameterList[0] + "=" + parameterList[1], parameterList[0] + "=" + this.maskedMessage);
                        apiActivity.QueryString = apiActivity.QueryString.Replace(parameterList[0] + "=" + parameterList[1], parameterList[0] + "=" + this.maskedMessage);

                        return apiActivity;
                    }
                }
            }

            return apiActivity;
        }
        #endregion

        #region MaskJsonString
        private string MaskJsonString(string jsonString, string parameterName)
        {
            ExpandoObjectConverter expandoObjectConverter = new ExpandoObjectConverter();

            var jsonToken = JToken.Parse(jsonString);

            if (jsonToken.Type == JTokenType.Array)
            {
                dynamic inputJsonExpandoObject = JsonConvert.DeserializeObject<List<ExpandoObject>>(jsonString, expandoObjectConverter);
                List<ExpandoObject> outputJsonExpandoObject = this.MaskExpandoObjectListByKey(inputJsonExpandoObject, parameterName);
                return JsonConvert.SerializeObject(outputJsonExpandoObject);
            }
            else
            {
                dynamic inputJsonExpandoObject = JsonConvert.DeserializeObject<ExpandoObject>(jsonString, expandoObjectConverter);
                ExpandoObject outputJsonExpandoObject = this.MaskExpandoObjectByKey(inputJsonExpandoObject, parameterName);
                return JsonConvert.SerializeObject(outputJsonExpandoObject);
            }
        }
        #endregion

        #region MaskExpandoObjectListByKey
        private List<ExpandoObject> MaskExpandoObjectListByKey(List<ExpandoObject> expandoObjectList, string key)
        {
            List<ExpandoObject> newExpandoObjectList = new List<ExpandoObject>();

            foreach (var expandoObject in expandoObjectList)
            {
                ExpandoObject newExpandoObject = new ExpandoObject();

                foreach (var keyValuePair in expandoObject)
                {
                    string value = keyValuePair.Value.ToString();

                    if (keyValuePair.Key.ToLower().Trim() == key.ToLower().Trim())
                    {
                        value = this.maskedMessage;
                    }

                    Type type = keyValuePair.Value.GetType();
                    if (type == typeof(ExpandoObject))
                    {
                        ExpandoObject editedExpandoObject = MaskExpandoObjectByKey((ExpandoObject)keyValuePair.Value, key);
                        newExpandoObject.TryAdd(keyValuePair.Key, editedExpandoObject);
                    }
                    else if (type == typeof(List<object>))
                    {
                        List<object> objectList = (List<object>)keyValuePair.Value;
                        if (objectList.Count > 0)
                        {
                            if (objectList[0].GetType() == typeof(object))
                            {
                                List<object> editedExpandoObject = MaskObjectListByKey((List<object>)keyValuePair.Value, key);
                                newExpandoObject.TryAdd(keyValuePair.Key, editedExpandoObject);
                            }
                            else
                            {
                                newExpandoObject.TryAdd(keyValuePair.Key, objectList);
                            }
                        }
                    }
                    else
                    {
                        newExpandoObject.TryAdd(keyValuePair.Key, value);
                    }
                }

                newExpandoObjectList.Add(newExpandoObject);
            }

            return newExpandoObjectList;
        }
        #endregion

        #region MaskExpandoObjectByKey
        private ExpandoObject MaskExpandoObjectByKey(ExpandoObject expandoObject, string key)
        {
            ExpandoObject newExpandoObject = new ExpandoObject();

            foreach (var keyValuePair in expandoObject)
            {
                if (keyValuePair.Value != null)
                {
                    string value = keyValuePair.Value.ToString();

                    if (keyValuePair.Key.ToLower().Trim() == key.ToLower().Trim())
                    {
                        value = this.maskedMessage;
                    }

                    Type type = keyValuePair.Value.GetType();
                    if (type == typeof(ExpandoObject))
                    {
                        ExpandoObject editedExpandoObject = MaskExpandoObjectByKey((ExpandoObject)keyValuePair.Value, key);
                        newExpandoObject.TryAdd(keyValuePair.Key, editedExpandoObject);
                    }
                    else if (type == typeof(List<object>))
                    {
                        List<object> objectList = (List<object>)keyValuePair.Value;
                        if (objectList.Count > 0)
                        {
                            if (objectList[0].GetType() == typeof(object))
                            {
                                List<object> editedExpandoObject = MaskObjectListByKey((List<object>)keyValuePair.Value, key);
                                newExpandoObject.TryAdd(keyValuePair.Key, editedExpandoObject);
                            }
                            else
                            {
                                newExpandoObject.TryAdd(keyValuePair.Key, objectList);
                            }
                        }
                    }
                    else
                    {
                        newExpandoObject.TryAdd(keyValuePair.Key, value);
                    }
                }
            }

            return newExpandoObject;
        }
        #endregion

        #region MaskObjectListByKey
        private List<object> MaskObjectListByKey(List<object> objectList, string key)
        {
            List<object> newObjectList = new List<object>();

            foreach (var obj in objectList)
            {
                ExpandoObject newExpandoObject = new ExpandoObject();

                ExpandoObject expandoObject = (ExpandoObject)obj;
                foreach (var keyValuePair in expandoObject)
                {
                    string value = keyValuePair.Value.ToString();

                    if (keyValuePair.Key.ToLower().Trim() == key.ToLower().Trim())
                    {
                        value = this.maskedMessage;
                    }

                    Type type = keyValuePair.Value.GetType();
                    if (type == typeof(ExpandoObject))
                    {
                        ExpandoObject editedExpandoObject = MaskExpandoObjectByKey((ExpandoObject)keyValuePair.Value, key);
                        newExpandoObject.TryAdd(keyValuePair.Key, editedExpandoObject);
                    }
                    else if (type == typeof(List<object>))
                    {
                        List<object> valueObjectList = (List<object>)keyValuePair.Value;
                        if (valueObjectList.Count > 0)
                        {
                            if (valueObjectList[0].GetType() == typeof(object))
                            {
                                List<object> editedExpandoObject = MaskObjectListByKey((List<object>)keyValuePair.Value, key);
                                newExpandoObject.TryAdd(keyValuePair.Key, editedExpandoObject);
                            }
                            else
                            {
                                newExpandoObject.TryAdd(keyValuePair.Key, objectList);
                            }
                        }
                    }
                    else
                    {
                        newExpandoObject.TryAdd(keyValuePair.Key, value);
                    }
                }

                newObjectList.Add(newExpandoObject);
            }

            return newObjectList;
        }
        #endregion
        #endregion
    }
}