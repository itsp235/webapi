using System;
using System.Text;
using System.IO;
using System.Reflection;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

using IOTCell.Framework.Configuration;
using IOTCell.Framework.Security;
using IOTCell.WebAPI.Middleware;
using IOTCell.WebAPI.Middleware.Extensions;
using System.Collections.Generic;

namespace IOTCell.WebAPI
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(option => option.EnableEndpointRouting = false);

            services.AddCors(o => o.AddPolicy("IOTCellPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));



            var tokenSecretKey = this.appSettings.TokenSecretKey;

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.RequireHttpsMetadata = false;
                jwtBearerOptions.SaveToken = true;

                // This is to allow the token to be passed in the URL with query string parameter
                //jwtBearerOptions.Events = new JwtBearerEvents()
                //{
                //    OnMessageReceived = context =>
                //    {
                //        if (context.Request.Query.ContainsKey("AccessToken"))
                //        {
                //            context.Token = context.Request.Query["AccessToken"];
                //        }

                //        return Task.CompletedTask;
                //    }
                //};

                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = this.appSettings.JWTIssuer,
                    ValidAudience = this.appSettings.JWTAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSecretKey)),
                    //ClockSkew = TimeSpan.FromMinutes(5) // 5 minute tolerance for the expiration date
                };
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            //services.AddSwaggerGen();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "IOTCell APIs",
                    Description = "APIs for managing SIM/ESIM in IOTCell system (activating, plans, usage etc)",
//                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "API support",
                        Email = "john.smith@itsp-inc.com",
                        Url = new Uri("https://www.itsp-inc.com/web/itsp"),
                    }

                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Scheme = "bearer",
                    Description = "Please key token into the field below"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "IOTCell.Framework.xml"));
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "IOTCell.BL.xml"));
                c.EnableAnnotations();


            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();            
            }
            else
            {
                //app.UseExceptionHandler("/api/Error"); // Calls the ErrorController to LogError. This was replaced with ExceptionLogMiddleware
            }

            app.UseMiddleware<ActivityLogMiddleware>();
            app.UseMiddleware<ExceptionLogMiddleware>();
            app.UseStatusCodePagesWithReExecute("/api/Error/{0}");

            app.UseAuthentication();
            app.UseMvc();
 //           app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            //app.UseSwaggerUI(c =>
            //{
            //    c.ConfigObject = new Swashbuckle.AspNetCore.SwaggerUI.ConfigObject
            //    {
            //        ShowCommonExtensions = true
            //    };
            //});

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "IOTCell API V1");
                //c.RoutePrefix = "v1";
                //var basePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                //c.SwaggerEndpoint($"{basePath}/swagger/{c.RoutePrefix}/swagger.json", "IOTCell API V1");
                c.DefaultModelsExpandDepth(-1);
            });

            //app.UseSwaggerUI(c => {
            //    c.DefaultModelsExpandDepth(-1);
            //});

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}" } };
                    //swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"https://192.168.2.23:44315/IOTCellAPI/swagger/index.html" } };
                
                });
            });


            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}