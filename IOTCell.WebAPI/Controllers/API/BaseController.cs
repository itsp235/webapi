﻿using Microsoft.AspNetCore.Mvc;

using IOTCell.Framework.Models;

namespace IOTCell.WebAPI.Controllers.API
{
    public class BaseController : ControllerBase
    {
        #region Members
        #endregion

        #region Constructor
        public BaseController()
        {
        }
        #endregion

        #region Methods
        #region ChooseResponse
        [NonAction]
        public IActionResult ChooseResponse(ResponseDetails responseDetails)
        {
            IActionResult actionResult = responseDetails.StatusCode switch
            {
                "200" => StatusCode(200, responseDetails), // Ok
                "401" => StatusCode(401, responseDetails), // Unauthroized
                "404" => StatusCode(404, responseDetails), // NotFound
                _ => StatusCode(500, responseDetails),
            };

            return actionResult;
        }
        #endregion
        #endregion
    }
}