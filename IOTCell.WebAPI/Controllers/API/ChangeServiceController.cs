﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

using IOTCell.BL.Diagnostics;
using IOTCell.BL.Providers;
using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;
using IOTCell.Framework.Configuration;

using Swashbuckle.AspNetCore.Annotations;

namespace IOTCell.WebAPI.Controllers.API

    
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class ChangeServiceController : BaseController
    {
        #region Members
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        private IActivityEntryLogger logger;
        #endregion

        #region Constructor
        public ChangeServiceController(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
        }
        #endregion

        #region Methods

        #region TerminateService

        /// <summary>
        /// Terminates/cancels existing service (SIM/ESIM)
        /// </summary>
        /// <param name="simViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the effective date for termination</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when the reseller info, SIM etc was not found for this activation</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpPatch("TerminateService")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsTermination200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsTermination400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsTermination401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsTermination404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsTermination500))]


        public async Task<IActionResult> TerminateService([FromBody, SwaggerRequestBody("Terminate service parameters", Required = true)] SIMViewModel simViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
            
            IChangeProvider changeProvider = new ChangeProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simViewModel);

            ResponseDetails responseDetails = await changeProvider.TerminateService(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion


        #region SuspendService

        /// <summary>
        /// Suspends existing service (SIM/ESIM)
        /// </summary>
        /// <param name="simViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the effective date for suspension</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when the reseller info, SIM etc was not found for this activation</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpPatch("SuspendService")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsSuspend200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsTermination400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsTermination401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsTermination404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsTermination500))]
        

        public async Task<IActionResult> SuspendService([FromBody, SwaggerRequestBody("Suspend service parameters", Required = true)] SIMViewModel simViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);

            IChangeProvider changeProvider = new ChangeProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simViewModel);

            ResponseDetails responseDetails = await changeProvider.SuspendService(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion


        #region ResumeService

        /// <summary>
        /// Resumes suspended service (SIM/ESIM)
        /// </summary>
        /// <param name="simViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the effective date for resuming service</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when the reseller info, SIM etc was not found for this activation</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpPatch("ResumeService")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsResume200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsTermination400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsTermination401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsTermination404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsTermination500))]
        

        public async Task<IActionResult> ResumeService([FromBody, SwaggerRequestBody("Resume service parameters", Required = true)] SIMViewModel simViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);

            IChangeProvider changeProvider = new ChangeProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simViewModel);

            ResponseDetails responseDetails = await changeProvider.ResumeService(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion


        #region ChangePlan

        /// <summary>
        /// Changes service plan
        /// </summary>
        /// <param name="simPlanChangeViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the effective date for service plan change</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when the reseller info, SIM etc was not found for this activation</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpPatch("ChangeServicePlan")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsChangePlan200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsTermination400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsTermination401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsTermination404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsTermination500))]
        

        public async Task<IActionResult> ChangeServicePlan([FromBody, SwaggerRequestBody("Change service plan parameters", Required = true)] SIMPlanChangeViewModel simPlanChangeViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);

            IChangeProvider changeProvider = new ChangeProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simPlanChangeViewModel);

            ResponseDetails responseDetails = await changeProvider.ChangeServicePlan(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion



        #region GetAvailableServicePlans
        /// <summary>
        /// Get available service plans from carrier
        /// </summary>
        /// <param name="resellerCarrierViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the usage for the period of time specified</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when company was not found</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpGet("GetAvailableServicePlans")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsServicePlan200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsServicePlan400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsUsage401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsUsage404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsUsage500))]

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetAvailableServicePlans([FromQuery] ResellerCarrierViewModel resellerCarrierViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
            IUsageProvider usageProvider = new UsageProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(resellerCarrierViewModel);

            ResponseDetails responseDetails = await usageProvider.GetAvailableServicePlans(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion


        #endregion
    }
}