﻿using System;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using IOTCell.Framework.Models;

namespace IOTCell.WebAPI.Controllers.API
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class ErrorController : ControllerBase
    {
        #region Constructor
        public ErrorController()
        {
        }
        #endregion

        #region Methods
        #region HttpStatusCodeHandler
        [AllowAnonymous]
        [Route("/api/error/{statusCode}")]
        [ApiExplorerSettings(IgnoreApi = true)]
                public IActionResult HttpStatusCodeHandler([FromBody] int statusCode)
//      public IActionResult HttpStatusCodeHandler([FromForm] int statusCode)
        {
            IActionResult result = statusCode switch
            {
                401 =>
                    Unauthorized(new ResponseDetails()
                    {
                        StatusCode = statusCode.ToString(),
                        Message = "Oops! You are not authorized to view this resource page."
                    }.ToString()),
                404 => 
                    NotFound(new ResponseDetails()
                    {
                        StatusCode = statusCode.ToString(),
                        Message = "Oops! We can't seem to find the resource page that you're looking for."
                    }.ToString()),
                405 =>
                     BadRequest(new ResponseDetails()
                     {
                         StatusCode = statusCode.ToString(),
                         Message = "Oops! Method not allowed."
                     }.ToString()),
                _ =>
                    throw new NotImplementedException("Status code " + statusCode)
            };

            return result;
        }
        #endregion
        #endregion
    }
}