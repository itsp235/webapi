﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;

using Microsoft.AspNetCore.Authentication.JwtBearer;

using IOTCell.BL.Diagnostics;
using IOTCell.BL.Providers;
using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;
using IOTCell.Framework.Configuration;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace IOTCell.WebAPI.Controllers.API
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class UsageController : BaseController
    {
        #region Members
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        private IActivityEntryLogger logger;
        #endregion

        #region Constructor
        public UsageController(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
        }
        #endregion

        #region Methods

        #region GetUsage
        /// <summary>
        /// Get usage for SIM/ESIM
        /// </summary>
        /// <param name="simUsageViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the usage for the period of time specified</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when SIM was not found</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      

        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpGet("GetUsage")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsUsage200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsUsage400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsUsage401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsUsage404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsUsage500))]

        public async Task<IActionResult> GetUsage([FromQuery] SIMUsageViewModel simUsageViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
            IUsageProvider usageProvider = new UsageProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simUsageViewModel);

            ResponseDetails responseDetails = await usageProvider.GetUsage(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion


//        #region GetTotalUsageForReseller
//        /// <summary>
//        /// Get total usage for Reseller
//        /// </summary>
//        /// <param name="resellerUsageViewModel"></param>
//        /// <returns>A newly created ResponseDetailsActivation</returns>
//        /// <response code="200">Returns the usage for the period of time specified</response>
//        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
//        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
//        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
////        [Authorize]
//        [EnableCors("IOTCellPolicy")]
//        [HttpGet("GetTotalUsageForReseller")]
//        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsTotalUsage200))]
//        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsUsage400))]
//        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsUsage401))]
//        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsUsage500))]


//        //        [ApiExplorerSettings(IgnoreApi = true)]
//        public async Task<IActionResult> GetTotalUsageForReseller([FromQuery] ResellerUsageViewModel resellerUsageViewModel)
//        {
//            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
//            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
//            IUsageProvider usageProvider = new UsageProvider(this.configuration, this.logger);

//            CustomerViewModel customerViewModel = new CustomerViewModel();
//            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(resellerUsageViewModel);

//            ResponseDetails responseDetails = await usageProvider.GetTotalUsage(customerViewModel);

//            return this.ChooseResponse(responseDetails);
//        }
//        #endregion


//        #region GetCalculatedUsage
//        /// <summary>
//        /// Get usage for SIM/ESIM for current month, 3, 6, 12 months, lifetime
//        /// </summary>
//        /// <param name="simUsageTotalViewModel"></param>
//        /// <returns>A newly created ResponseDetailsActivation</returns>
//        /// <response code="200">Returns the usage for this SIM/ESIM</response>
//        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
//        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
//        /// <response code="404">Occurs when SIM was not found</response> 
//        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
////        [Authorize]
//        [EnableCors("IOTCellPolicy")]
//        [HttpGet("GetCalculatedUsage")]
//        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsCalculatedUsage200))]
//        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsCalculatedUsage400))]
//        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsUsage401))]
//        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsUsage404))]
//        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsUsage500))]

////        [ApiExplorerSettings(IgnoreApi = true)]
//        public async Task<IActionResult> GetCalculatedUsage([FromQuery] SIMUsageTotalViewModel simUsageTotalViewModel)
//        {
//            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
//            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
//            IUsageProvider usageProvider = new UsageProvider(this.configuration, this.logger);

//            CustomerViewModel customerViewModel = new CustomerViewModel();
//            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simUsageTotalViewModel);

//            ResponseDetails responseDetails = await usageProvider.GetCalculatedUsage(customerViewModel);

//            return this.ChooseResponse(responseDetails);
//        }
//        #endregion



        #endregion
    }
}