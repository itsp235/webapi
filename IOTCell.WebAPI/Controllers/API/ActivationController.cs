﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

using IOTCell.BL.Diagnostics;
using IOTCell.BL.Providers;
using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;
using IOTCell.Framework.Configuration;

using Swashbuckle.AspNetCore.Annotations;



namespace IOTCell.WebAPI.Controllers.API
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/[controller]")]
    public class ActivationController : BaseController
    {
        #region Members
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        private IActivityEntryLogger logger;
        #endregion

        #region Constructor
        public ActivationController(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
        }
        #endregion

        #region Methods

        //#region ActivateNewCustomer

        ///// <summary>
        ///// Activates service (SIM/ESIM) for new customer.
        ///// </summary>
        ///// <param name="customerSIMPlanViewModel"></param>
        ///// <returns>A newly created ResponseDetailsActivation</returns>
        ///// <response code="200">Returns the account number for the newly created customer</response>
        ///// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        ///// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        ///// <response code="404">Occurs when the reseller info, plan, SIM etc was not found for this activation</response> 
        ///// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        //[EnableCors("IOTCellPolicy")]
        //[HttpPost("ActivateNewCustomer")]
        //[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsActivation200))]
        //[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsActivation400))]
        //[ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsActivation401))]
        //[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsActivation404))]
        //[ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsActivation500))]
        //public async Task<IActionResult> ActivateNewCustomer([FromBody, SwaggerRequestBody("Activation parameters for new customer", Required = true)] CustomerSIMPlanViewModel customerSIMPlanViewModel)
        //{
        //    string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
        //    this.logger = new ActivityEntryLogger(this.configuration, correlationID);
        //    IActivationProvider activationProvider = new ActivationProvider(this.configuration, this.logger);

        //    CustomerViewModel customerViewModel = new CustomerViewModel();
        //    customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(customerSIMPlanViewModel);

        //    ResponseDetails responseDetails = await activationProvider.ActivateNewCustomer(customerViewModel);

        //    return this.ChooseResponse(responseDetails);
        //}
        //#endregion

        #region ActivateDevice

        /// <summary>
        /// Activates service (SIM/ESIM) for existing customer.
        /// </summary>
        /// <param name="customerAccountViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Returns the account number</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when the reseller info, plan, SIM etc was not found for this activation</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpPost("ActivateDevice")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsActivation200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsActivation400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsActivation401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsActivation404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsActivation500))]


        public async Task<IActionResult> ActivateDevice([FromBody, SwaggerRequestBody("Activation parameters for existing customer", Required = true)] CustomerAccountViewModel customerAccountViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
            IActivationProvider activationProvider = new ActivationProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(customerAccountViewModel);

            ResponseDetails responseDetails = await activationProvider.ActivateDevice(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion

        #region ActivateServiceNoDB

        /// <summary>
        /// Activates service (SIM/ESIM) - no database changes
        /// </summary>
        /// <param name="simPlanChangeViewModel"></param>
        /// <returns>A newly created ResponseDetailsActivation</returns>
        /// <response code="200">Activation successful - don't forget to apply all the required changes to the database</response>
        /// <response code="400">Occurs when one of the mandatory fields is empty or invalid</response> 
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 
        /// <response code="404">Occurs when the reseller info, plan, SIM etc was not found for this activation</response> 
        /// <response code="500">Occurs when there's an error with the third party APIs or with the internal systems, database etc</response>      
        [Authorize]
        [EnableCors("IOTCellPolicy")]
        [HttpPost("ActivateServiceNoDB")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseDetailsActivationNoDB200))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ResponseDetailsActivation400))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseDetailsActivation401))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ResponseDetailsActivation404))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ResponseDetailsActivation500))]

        

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> ActivateServiceNoDB([FromBody, SwaggerRequestBody("Activation parameters for service (SIM/ESIM)", Required = true)] SIMPlanChangeViewModel simPlanChangeViewModel)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);
            IActivationProvider activationProvider = new ActivationProvider(this.configuration, this.logger);

            CustomerViewModel customerViewModel = new CustomerViewModel();
            customerViewModel = customerViewModel.ConvertViewModelToCustomerViewModel(simPlanChangeViewModel);

            ResponseDetails responseDetails = await activationProvider.ActivateServiceNoDB(customerViewModel);

            return this.ChooseResponse(responseDetails);
        }
        #endregion


        #endregion
    }
}