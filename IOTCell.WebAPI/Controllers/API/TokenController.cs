﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Configuration;

using IOTCell.BL.Diagnostics;
using IOTCell.BL.Providers;
using IOTCell.Framework.Models;
using IOTCell.Framework.Configuration;
using Swashbuckle.AspNetCore.Annotations;

namespace IOTCell.WebAPI.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class TokenController : BaseController
    {
        #region Members
        private readonly IConfiguration configuration;
        private readonly IAppSettings appSettings;
        private IActivityEntryLogger logger;
        #endregion

        #region Constructor
        public TokenController(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration); 
        }
        #endregion

        #region Methods
        #region Authenticate
        /// <summary>
        /// Creates an access token 
        /// </summary>
        /// <param name="authenticationDetails"></param>
        /// <returns>A newly created TokenDetails</returns>
        /// <response code="200">Returns the access token</response>
        /// <response code="401">Occurs when the request lacks valid authentication credentials (Unauthorized)</response> 

        [EnableCors("IOTCellPolicy")]
        [HttpPost("authenticate")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseToken))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ResponseToken))]
//        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> Authenticate([FromBody, SwaggerRequestBody("Authentication parameters", Required = true)] AuthenticationDetails authenticationDetails)
        {
            string correlationID = this.appSettings.GetCorrelationID(HttpContext.Response.Headers, this.appSettings.CustomCorrelationIDHeaderName);
            this.logger = new ActivityEntryLogger(this.configuration, correlationID);

            ITokenProvider tokenProvider = new TokenProvider(this.configuration, this.logger);
            
            ResponseDetails responseDetails = await tokenProvider.Authenticate(authenticationDetails);

            return this.ChooseResponse(responseDetails);
        }
        #endregion
        #endregion
    }
}