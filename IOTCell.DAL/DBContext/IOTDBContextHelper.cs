﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using IOTCell.Framework.Configuration;

namespace IOTCell.DAL.DBContext
{
    public static class IOTDBContextHelper
    {
        public static DbContextOptions<IOTDBContext> GetDbContextOptions(IConfiguration configuration)
        {
            IAppSettings appSettings = new AppSettings(configuration);
            DbContextOptions<IOTDBContext> result = new DbContextOptionsBuilder<IOTDBContext>().UseMySQL(appSettings.ConnectionString).Options;
            return result;
        }
    }
}