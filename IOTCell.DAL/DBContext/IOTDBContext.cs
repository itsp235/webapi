﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using IOTCell.DAL.Models;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.DBContext
{
    public partial class IOTDBContext : DbContext
    {
        public IOTDBContext()
        {
        }

        public IOTDBContext(DbContextOptions<IOTDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Act> Act { get; set; }
        public virtual DbSet<Models.Action> Action { get; set; }
        public virtual DbSet<Actionalert> Actionalert { get; set; }
        public virtual DbSet<ActionalertTemplate> ActionalertTemplate { get; set; }
        public virtual DbSet<Actionlog> Actionlog { get; set; }
        public virtual DbSet<Actionrule> Actionrule { get; set; }
        public virtual DbSet<ActionruleTemplate> ActionruleTemplate { get; set; }
        public virtual DbSet<ActionruleTemplateType> ActionruleTemplateType { get; set; }
        public virtual DbSet<ActivityEntryLog> ActivityEntryLog { get; set; }
        public virtual DbSet<ActivityReport> ActivityReport { get; set; }
        public virtual DbSet<Address> Address { get; set; }
        public virtual DbSet<Analysis> Analysis { get; set; }
        public virtual DbSet<ApiActivityLog> ApiActivityLog { get; set; }
        public virtual DbSet<Apikey> Apikey { get; set; }
        public virtual DbSet<Asset> Asset { get; set; }
        public virtual DbSet<AssetAttribute> AssetAttribute { get; set; }
        public virtual DbSet<Assetclass> Assetclass { get; set; }
        public virtual DbSet<Assetdevicetype> Assetdevicetype { get; set; }
        public virtual DbSet<Models.Attribute> Attribute { get; set; }
        public virtual DbSet<AttributeMeta> AttributeMeta { get; set; }
        public virtual DbSet<BillingView> BillingView { get; set; }
        public virtual DbSet<CalculateAttribute> CalculateAttribute { get; set; }
        public virtual DbSet<Carrier> Carrier { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompanyDevicetype> CompanyDevicetype { get; set; }
        public virtual DbSet<Config> Config { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<ContentOption> ContentOption { get; set; }
        public virtual DbSet<ContentPermission> ContentPermission { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerExtension> CustomerExtension { get; set; }
        public virtual DbSet<DashboardLayout> DashboardLayout { get; set; }
        public virtual DbSet<DashboardTemplate> DashboardTemplate { get; set; }
        public virtual DbSet<DashboardTemplateEntry> DashboardTemplateEntry { get; set; }
        public virtual DbSet<Decodedmessage> Decodedmessage { get; set; }
        public virtual DbSet<Device> Device { get; set; }
        public virtual DbSet<DeviceMaster> DeviceMaster { get; set; }
        public virtual DbSet<Devicelifecycle> Devicelifecycle { get; set; }
        public virtual DbSet<Devicetype> Devicetype { get; set; }
        public virtual DbSet<Division> Division { get; set; }
        public virtual DbSet<ErrorLog> ErrorLog { get; set; }
        public virtual DbSet<ExceptionLog> ExceptionLog { get; set; }
        public virtual DbSet<Geofence> Geofence { get; set; }
        public virtual DbSet<HibernateSequence> HibernateSequence { get; set; }
        public virtual DbSet<Icon> Icon { get; set; }
        public virtual DbSet<Icons> Icons { get; set; }
        public virtual DbSet<Industry> Industry { get; set; }
        public virtual DbSet<Inventory> Inventory { get; set; }
        public virtual DbSet<InventoryItemView> InventoryItemView { get; set; }
        public virtual DbSet<InventoryMaster> InventoryMaster { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<InvoiceListView> InvoiceListView { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemPayFrequency> ItemPayFrequency { get; set; }
        public virtual DbSet<ItemRate> ItemRate { get; set; }
        public virtual DbSet<ItemType> ItemType { get; set; }
        public virtual DbSet<Itspgroup> Itspgroup { get; set; }
        public virtual DbSet<Lob> Lob { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderAddress> OrderAddress { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<OrderLog> OrderLog { get; set; }
        public virtual DbSet<Paymentterm> Paymentterm { get; set; }
        public virtual DbSet<PlanUsage> PlanUsage { get; set; }
        public virtual DbSet<PlanUsageHistory> PlanUsageHistory { get; set; }
        public virtual DbSet<Promote> Promote { get; set; }
        public virtual DbSet<PurchaseLog> PurchaseLog { get; set; }
        public virtual DbSet<Refund> Refund { get; set; }
        public virtual DbSet<Report> Report { get; set; }
        public virtual DbSet<Reportlog> Reportlog { get; set; }
        public virtual DbSet<Reportrule> Reportrule { get; set; }
        public virtual DbSet<Reseller> Reseller { get; set; }
        public virtual DbSet<ResellerExtension> ResellerExtension { get; set; }
        public virtual DbSet<Resellerinventoryitemview> Resellerinventoryitemview { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        public virtual DbSet<ShippingMethod> ShippingMethod { get; set; }
        public virtual DbSet<ShippingStatus> ShippingStatus { get; set; }
        public virtual DbSet<SidebarOption> SidebarOption { get; set; }
        public virtual DbSet<SidebarPermission> SidebarPermission { get; set; }
        public virtual DbSet<SimUsage> SimUsage { get; set; }
        public virtual DbSet<SimUsageClientCustomerView> SimUsageClientCustomerView { get; set; }
        public virtual DbSet<SimUsageClientView> SimUsageClientView { get; set; }
        public virtual DbSet<SimUsageResellerView> SimUsageResellerView { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<Tax> Tax { get; set; }
        public virtual DbSet<Template> Template { get; set; }
        public virtual DbSet<Themeattribute> Themeattribute { get; set; }
        public virtual DbSet<Transaction> Transaction { get; set; }
        public virtual DbSet<Typegroup> Typegroup { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserAsset> UserAsset { get; set; }
        public virtual DbSet<UserAssetclass> UserAssetclass { get; set; }
        public virtual DbSet<UserDevicetypes> UserDevicetypes { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("account");

                entity.HasIndex(e => e.AccountId)
                    .HasName("ACCOUNT_ID")
                    .IsUnique();

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_id_idx");

                entity.Property(e => e.AccountId).HasColumnName("ACCOUNT_ID");

                entity.Property(e => e.AccountName)
                    .HasColumnName("ACCOUNT_NAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AccountType)
                    .HasColumnName("ACCOUNT_TYPE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CardMask)
                    .HasColumnName("CARD_MASK")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("COMPANY_CODE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("CREATEDATE")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CurrentBalance)
                    .HasColumnName("CURRENT_BALANCE")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CurrentBalanceTax)
                    .HasColumnName("CURRENT_BALANCE_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CustomerId).HasColumnName("CUSTOMER_ID");

                entity.Property(e => e.DefaultPayment)
                    .HasColumnName("DEFAULT_PAYMENT")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasComment("Y-Default payment method; N - No");

                entity.Property(e => e.Expdate)
                    .HasColumnName("EXPDATE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IsNotification)
                    .HasColumnName("IS_NOTIFICATION")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IssuerId)
                    .HasColumnName("ISSUER_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverdueBalance)
                    .HasColumnName("OVERDUE_BALANCE")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OverdueBalanceTax)
                    .HasColumnName("OVERDUE_BALANCE_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("PAYMENT_METHOD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.Property(e => e.TokenCode)
                    .HasColumnName("TOKEN_CODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_id");
            });

            modelBuilder.Entity<Act>(entity =>
            {
                entity.ToTable("act");

                entity.Property(e => e.Actid).HasColumnName("actid");

                entity.Property(e => e.Assetclassid).HasColumnName("assetclassid");

                entity.Property(e => e.Assetclassname)
                    .HasColumnName("assetclassname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.Assetname)
                    .HasColumnName("assetname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Colname)
                    .HasColumnName("colname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Deviceid)
                    .HasColumnName("deviceid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicename)
                    .HasColumnName("devicename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Operation)
                    .HasColumnName("operation")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Models.Action>(entity =>
            {
                entity.ToTable("action");

                entity.Property(e => e.Actionid).HasColumnName("actionid");

                entity.Property(e => e.Action1)
                    .HasColumnName("action")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'CHECK (action IN (''Alert'',''Stat'',''Report'')),'")
                    .HasComment("01:Alert\\n02:Statistic");

                entity.Property(e => e.Actiondescription)
                    .HasColumnName("actiondescription")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Actionname)
                    .HasColumnName("actionname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Actiontime).HasColumnName("actiontime");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Createtime)
                    .HasColumnName("createtime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Ruleid).HasColumnName("ruleid");

                entity.Property(e => e.Rulename)
                    .HasColumnName("rulename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Triggertype)
                    .HasColumnName("triggertype")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Weekday)
                    .HasColumnName("weekday")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Actionalert>(entity =>
            {
                entity.ToTable("actionalert");

                entity.Property(e => e.Actionalertid).HasColumnName("actionalertid");

                entity.Property(e => e.Actionid).HasColumnName("actionid");

                entity.Property(e => e.Alerttype)
                    .HasColumnName("alerttype")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Contactid)
                    .HasColumnName("contactid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Content)
                    .HasColumnName("content")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Destination)
                    .HasColumnName("destination")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Endtime).HasColumnName("endtime");

                entity.Property(e => e.NotificationTemplateid)
                    .HasColumnName("notification_templateid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Starttime).HasColumnName("starttime");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Weekday)
                    .HasColumnName("weekday")
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ActionalertTemplate>(entity =>
            {
                entity.ToTable("actionalert_template");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Actionid).HasColumnName("actionid");

                entity.Property(e => e.Companycode)
                    .HasColumnName("companycode")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Companyname)
                    .HasColumnName("companyname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Contactid)
                    .HasColumnName("contactid")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Content)
                    .HasColumnName("content")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Createdtime)
                    .HasColumnName("createdtime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Destination)
                    .HasColumnName("destination")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Endtime).HasColumnName("endtime");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Owner)
                    .HasColumnName("owner")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Settinggroup)
                    .HasColumnName("settinggroup")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Starttime).HasColumnName("starttime");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Weekday)
                    .HasColumnName("weekday")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Actionlog>(entity =>
            {
                entity.ToTable("actionlog");

                entity.Property(e => e.Actionlogid).HasColumnName("actionlogid");

                entity.Property(e => e.Acktime).HasColumnName("acktime");

                entity.Property(e => e.Actionid).HasColumnName("actionid");

                entity.Property(e => e.Alertmsg)
                    .HasColumnName("alertmsg")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Alerttime)
                    .HasColumnName("alerttime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Alerttype)
                    .HasColumnName("alerttype")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Assetclassid).HasColumnName("assetclassid");

                entity.Property(e => e.Assetclassname)
                    .HasColumnName("assetclassname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.Assetname)
                    .HasColumnName("assetname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Attributetype)
                    .HasColumnName("attributetype")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Cleartime).HasColumnName("cleartime");

                entity.Property(e => e.Sendstatus)
                    .HasColumnName("sendstatus")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Severity)
                    .HasColumnName("severity")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Actionrule>(entity =>
            {
                entity.ToTable("actionrule");

                entity.Property(e => e.Actionruleid).HasColumnName("actionruleid");

                entity.Property(e => e.Actionid)
                    .HasColumnName("actionid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Assetclassid)
                    .HasColumnName("assetclassid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Assetclassname)
                    .HasColumnName("assetclassname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Assetid)
                    .HasColumnName("assetid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Assetname)
                    .HasColumnName("assetname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Attributeid)
                    .HasColumnName("attributeid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Attributename)
                    .HasColumnName("attributename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Geofenceid)
                    .HasColumnName("geofenceid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Geofencename)
                    .HasColumnName("geofencename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Groupid)
                    .HasColumnName("groupid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Groupindex)
                    .HasColumnName("groupindex")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Grouprelation)
                    .HasColumnName("grouprelation")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Operator)
                    .HasColumnName("operator")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Relation)
                    .HasColumnName("relation")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Templateid)
                    .HasColumnName("templateid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<ActionruleTemplate>(entity =>
            {
                entity.HasKey(e => e.Templateid)
                    .HasName("PRIMARY");

                entity.ToTable("actionrule_template");

                entity.Property(e => e.Templateid).HasColumnName("templateid");

                entity.Property(e => e.Attributeid).HasColumnName("attributeid");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Geofenceid).HasColumnName("geofenceid");

                entity.Property(e => e.Operator).HasColumnName("operator");

                entity.Property(e => e.Typeid).HasColumnName("typeid");

                entity.Property(e => e.Typename)
                    .HasColumnName("typename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<ActionruleTemplateType>(entity =>
            {
                entity.HasKey(e => e.Typeid)
                    .HasName("PRIMARY");

                entity.ToTable("actionrule_template_type");

                entity.Property(e => e.Typeid).HasColumnName("typeid");

                entity.Property(e => e.Typedescription)
                    .HasColumnName("typedescription")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Typename)
                    .HasColumnName("typename")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ActivityEntryLog>(entity =>
            {
                entity.ToTable("activity_entry_log");

                entity.Property(e => e.ActivityEntryLogId).HasColumnName("activity_entry_log_id");

                entity.Property(e => e.ActivityDate).HasColumnName("activity_date");

                entity.Property(e => e.ActvityText)
                    .HasColumnName("actvity_text")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CorrelationId)
                    .HasColumnName("correlation_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EntryType)
                    .HasColumnName("entry_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ActivityReport>(entity =>
            {
                entity.HasKey(e => e.ActivityId)
                    .HasName("PRIMARY");

                entity.ToTable("activity_report");

                entity.Property(e => e.ActivityId).HasColumnName("ACTIVITY_ID");

                entity.Property(e => e.Activity)
                    .IsRequired()
                    .HasColumnName("ACTIVITY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId).HasColumnName("COMPANY_ID");

                entity.Property(e => e.IdentifierType)
                    .IsRequired()
                    .HasColumnName("IDENTIFIER_TYPE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.IdentifierValue)
                    .IsRequired()
                    .HasColumnName("IDENTIFIER_VALUE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.InventoryId).HasColumnName("INVENTORY_ID");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("UPDATE_DATE")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("address");

                entity.HasComment("contains all addresses (Company, Reseller, customer, shipping, billing etc)");

                entity.HasIndex(e => e.AddressId)
                    .HasName("address_id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.CompanyId)
                    .HasName("address_company_company_id_idx");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("address_customer_customer_id_idx");

                entity.HasIndex(e => e.ResellerId)
                    .HasName("address_reseller_reseller_id_idx");

                entity.Property(e => e.AddressId).HasColumnName("address_id");

                entity.Property(e => e.AddressLine1)
                    .HasColumnName("ADDRESS_LINE1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine2)
                    .HasColumnName("ADDRESS_LINE2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AddressType)
                    .HasColumnName("ADDRESS_TYPE")
                    .HasMaxLength(1)
                    .IsFixedLength()
                    .HasComment("'S' for shipping, 'B' for Billing");

                entity.Property(e => e.Aptno)
                    .HasColumnName("aptno")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("CITY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId).HasColumnName("company_id");

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.IsStillUsed).HasComment("0 if the address is not valid anymore, is not in use anymore (won't be visible in the address selection) or 1 if the address is valid and in use");

                entity.Property(e => e.ResellerId).HasColumnName("reseller_id");

                entity.Property(e => e.State)
                    .HasColumnName("STATE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Streetname)
                    .HasColumnName("streetname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Streetno)
                    .HasColumnName("streetno")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("address_company_company_id");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("address_customer_customer_id");

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.Address)
                    .HasForeignKey(d => d.ResellerId)
                    .HasConstraintName("address_reseller_reseller_id");
            });

            modelBuilder.Entity<Analysis>(entity =>
            {
                entity.ToTable("analysis");

                entity.Property(e => e.Analysisid).HasColumnName("analysisid");

                entity.Property(e => e.Actid).HasColumnName("actid");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ApiActivityLog>(entity =>
            {
                entity.ToTable("api_activity_log");

                entity.Property(e => e.ApiActivityLogId).HasColumnName("api_activity_log_id");

                entity.Property(e => e.CorrelationId)
                    .HasColumnName("correlation_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Headers).HasColumnName("headers");

                entity.Property(e => e.Method)
                    .HasColumnName("method")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Path)
                    .HasColumnName("path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.QueryString).HasColumnName("query_string");

                entity.Property(e => e.RequestBody).HasColumnName("request_body");

                entity.Property(e => e.RequestTime).HasColumnName("request_time");

                entity.Property(e => e.ResponseBody).HasColumnName("response_body");

                entity.Property(e => e.ResponseMilliseconds).HasColumnName("response_milliseconds");

                entity.Property(e => e.StatusCode).HasColumnName("status_code");

                entity.Property(e => e.Url).HasColumnName("url");
            });

            modelBuilder.Entity<Apikey>(entity =>
            {
                entity.ToTable("apikey");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Apikey1).HasColumnName("apikey");

                entity.Property(e => e.Checktype).HasColumnName("checktype");

                entity.Property(e => e.Checkvalue).HasColumnName("checkvalue");

                entity.Property(e => e.Createdby).HasColumnName("createdby");

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Endpoint).HasColumnName("endpoint");

                entity.Property(e => e.Expiretime).HasColumnName("expiretime");
            });

            modelBuilder.Entity<Asset>(entity =>
            {
                entity.ToTable("asset");

                entity.HasIndex(e => e.Assetclass)
                    .HasName("assetclass_idx");

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.Assetclass)
                    .HasColumnName("assetclass")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Assetname)
                    .HasColumnName("assetname")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Modifiedby)
                    .HasColumnName("modifiedby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Modifiedtime).HasColumnName("modifiedtime");

                entity.Property(e => e.Parentasset).HasColumnName("parentasset");

                entity.HasOne(d => d.AssetclassNavigation)
                    .WithMany(p => p.Asset)
                    .HasForeignKey(d => d.Assetclass)
                    .HasConstraintName("assetclass");
            });

            modelBuilder.Entity<AssetAttribute>(entity =>
            {
                entity.ToTable("asset_attribute");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.Attmetaid).HasColumnName("attmetaid");

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnName("create_time");

                entity.Property(e => e.LastmodifiedBy)
                    .HasColumnName("lastmodified_by")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LastmodifiedTime).HasColumnName("lastmodified_time");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Assetclass>(entity =>
            {
                entity.ToTable("assetclass");

                entity.HasIndex(e => e.Iconid)
                    .HasName("iconid_idx");

                entity.Property(e => e.Assetclassid).HasColumnName("assetclassid");

                entity.Property(e => e.Classname)
                    .HasColumnName("classname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Iconid)
                    .HasColumnName("iconid")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Modifiedby)
                    .HasColumnName("modifiedby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Modifiedtime).HasColumnName("modifiedtime");

                entity.Property(e => e.Parentclass).HasColumnName("parentclass");

                entity.HasOne(d => d.Icon)
                    .WithMany(p => p.Assetclass)
                    .HasForeignKey(d => d.Iconid)
                    .HasConstraintName("iconid");
            });

            modelBuilder.Entity<Assetdevicetype>(entity =>
            {
                entity.ToTable("assetdevicetype");

                entity.HasIndex(e => e.Assetclassid)
                    .HasName("assetclassid_idx");

                entity.HasIndex(e => e.Devicetypeid)
                    .HasName("devicetypeid_idx");

                entity.Property(e => e.Assetdevicetypeid).HasColumnName("assetdevicetypeid");

                entity.Property(e => e.Assetclassid).HasColumnName("assetclassid");

                entity.Property(e => e.Assetclassname)
                    .HasColumnName("assetclassname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.Assetclass)
                    .WithMany(p => p.Assetdevicetype)
                    .HasForeignKey(d => d.Assetclassid)
                    .HasConstraintName("assetclassid");

                entity.HasOne(d => d.Devicetype)
                    .WithMany(p => p.Assetdevicetype)
                    .HasForeignKey(d => d.Devicetypeid)
                    .HasConstraintName("devicetypeid");
            });

            modelBuilder.Entity<Models.Attribute>(entity =>
            {
                entity.ToTable("attribute");

                entity.Property(e => e.Attributeid).HasColumnName("attributeid");

                entity.Property(e => e.Attributename)
                    .HasColumnName("attributename")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Attributetype)
                    .HasColumnName("attributetype")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'admin'");

                entity.Property(e => e.Createdtime)
                    .HasColumnName("createdtime")
                    .HasDefaultValueSql("'2019-10-20 10:01:01'");

                entity.Property(e => e.Devicetype)
                    .HasColumnName("devicetype")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Displayname)
                    .HasColumnName("displayname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Editable)
                    .HasColumnName("editable")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Messagetype)
                    .HasColumnName("messagetype")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Useradded)
                    .HasColumnName("useradded")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<AttributeMeta>(entity =>
            {
                entity.ToTable("attribute_meta");

                entity.HasIndex(e => e.Assetclass)
                    .HasName("assetclass_idx");

                entity.Property(e => e.Attributemetaid).HasColumnName("attributemetaid");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Assetclass).HasColumnName("assetclass");

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnName("create_time");

                entity.Property(e => e.LastmodifiedBy)
                    .HasColumnName("lastmodified_by")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LastmodifiedTime).HasColumnName("lastmodified_time");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Source)
                    .HasColumnName("source")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.AssetclassNavigation)
                    .WithMany(p => p.AttributeMeta)
                    .HasForeignKey(d => d.Assetclass)
                    .HasConstraintName("attribute_meta_assetclass");
            });

            modelBuilder.Entity<BillingView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("billing_view");

                entity.Property(e => e.AccountId).HasColumnName("ACCOUNT_ID");

                entity.Property(e => e.ActiveDate).HasColumnName("ACTIVE_DATE");

                entity.Property(e => e.Billcycle).HasColumnName("BILLCYCLE");

                entity.Property(e => e.BillingEndDt)
                    .HasColumnName("BILLING_END_DT")
                    .HasColumnType("date");

                entity.Property(e => e.BillingStartDt)
                    .HasColumnName("BILLING_START_DT")
                    .HasColumnType("date");

                entity.Property(e => e.BillingType)
                    .IsRequired()
                    .HasColumnName("BILLING_TYPE")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.CompanyId).HasColumnName("COMPANY_ID");

                entity.Property(e => e.CustomerId).HasColumnName("CUSTOMER_ID");

                entity.Property(e => e.DataUsageWhenPlanChanged).HasColumnType("decimal(20,2)");

                entity.Property(e => e.InventoryId).HasColumnName("INVENTORY_ID");

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.ItemPrice)
                    .HasColumnName("ITEM_PRICE")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.NextBillingDt)
                    .HasColumnName("NEXT_BILLING_DT")
                    .HasColumnType("date");

                entity.Property(e => e.OrderId).HasColumnName("ORDER_ID");

                entity.Property(e => e.OrderitemId).HasColumnName("ORDERITEM_ID");

                entity.Property(e => e.PayFreq)
                    .HasColumnName("PAY_FREQ")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PromoteId).HasColumnName("PROMOTE_ID");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.SmsusageWhenPlanChanged)
                    .HasColumnName("SMSUsageWhenPlanChanged")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.UsageHistoryId).HasColumnName("USAGE_HISTORY_ID");

                entity.Property(e => e.VoiceUsageWhenPlanChanged).HasColumnType("decimal(20,2)");
            });

            modelBuilder.Entity<CalculateAttribute>(entity =>
            {
                entity.ToTable("calculate_attribute");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Assetclassid).HasColumnName("assetclassid");

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Createdtime)
                    .HasColumnName("createdtime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Displayname)
                    .HasColumnName("displayname")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Expression)
                    .HasColumnName("expression")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Vars)
                    .HasColumnName("vars")
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Carrier>(entity =>
            {
                entity.ToTable("carrier");

                entity.HasIndex(e => e.CarrierId)
                    .HasName("CarrierID_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.CarrierId).HasColumnName("CarrierID");

                entity.Property(e => e.CarrierAssembly)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierCode)
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.CarrierName)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UsesGetCalculatedUsageMethod).HasColumnName("Uses_GetCalculatedUsage_method");

                entity.Property(e => e.UsesGetTotalUsageForResellerMethod).HasColumnName("Uses_GetTotalUsageForReseller_method");

                entity.Property(e => e.UsesTerminateServiceMethod).HasColumnName("Uses_TerminateService_method");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.ToTable("company");

                entity.HasIndex(e => e.CompanyCode)
                    .HasName("company_code_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.IndustryId)
                    .HasName("industryID_idx");

                entity.Property(e => e.CompanyId).HasColumnName("company_id");

                entity.Property(e => e.Addressline1)
                    .HasColumnName("addressline1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Addressline2)
                    .HasColumnName("addressline2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("company_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasColumnName("company_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyNameFr)
                    .HasColumnName("COMPANY_NAME_FR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactFirstName)
                    .HasColumnName("contact_first_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ContactLastName)
                    .HasColumnName("contact_last_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Createuser)
                    .HasColumnName("createuser")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.IndustryId).HasColumnName("industryID");

                entity.Property(e => e.Language)
                    .HasColumnName("language")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.MainBusinessArea)
                    .HasColumnName("main_business_area")
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Postcode)
                    .HasColumnName("postcode")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .HasColumnName("province")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.Company)
                    .HasForeignKey(d => d.IndustryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("industryID");
            });

            modelBuilder.Entity<CompanyDevicetype>(entity =>
            {
                entity.ToTable("company_devicetype");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Companycode)
                    .HasColumnName("companycode")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Companyname)
                    .HasColumnName("companyname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnName("create_time");

                entity.Property(e => e.Creator)
                    .HasColumnName("creator")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Config>(entity =>
            {
                entity.ToTable("config");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Appname)
                    .HasColumnName("appname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Companycode)
                    .HasColumnName("companycode")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Displayname)
                    .HasColumnName("displayname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Itemname)
                    .IsRequired()
                    .HasColumnName("itemname")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Itemvalue)
                    .IsRequired()
                    .HasColumnName("itemvalue")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Modulename)
                    .HasColumnName("modulename")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RangeHigh)
                    .HasColumnName("range_high")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RangeLow)
                    .HasColumnName("range_low")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Settinggroup)
                    .HasColumnName("settinggroup")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Unit)
                    .HasColumnName("unit")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("contact");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Companycode)
                    .HasColumnName("companycode")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Emailenable)
                    .HasColumnName("emailenable")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Owner)
                    .HasColumnName("owner")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Phoneenable)
                    .HasColumnName("phoneenable")
                    .HasDefaultValueSql("'1'");
            });

            modelBuilder.Entity<ContentOption>(entity =>
            {
                entity.ToTable("content_option");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SidebarOptionId).HasColumnName("SIDEBAR_OPTION_ID");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("TITLE")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContentPermission>(entity =>
            {
                entity.ToTable("content_permission");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("COMPANY_CODE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ContentOptionId).HasColumnName("CONTENT_OPTION_ID");

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.ToTable("country");

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.Country1)
                    .HasColumnName("country")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("customer");

                entity.HasIndex(e => e.Accountnumber)
                    .HasName("ACCOUNTNUMBER_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.CarrierId)
                    .HasName("carrierid_idx");

                entity.HasIndex(e => e.CountryId)
                    .HasName("country_id_idx");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("CUSTOMER_ID")
                    .IsUnique();

                entity.HasIndex(e => e.PaymentTermId)
                    .HasName("paymenttermid_idx");

                entity.HasIndex(e => e.ResellerId)
                    .HasName("reseller_id_idx");

                entity.Property(e => e.CustomerId).HasColumnName("CUSTOMER_ID");

                entity.Property(e => e.Accountnumber)
                    .HasColumnName("ACCOUNTNUMBER")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine1)
                    .HasColumnName("ADDRESS_LINE1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine2)
                    .HasColumnName("ADDRESS_LINE2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BirthDate).HasColumnName("BIRTH_DATE");

                entity.Property(e => e.BusinessName)
                    .HasColumnName("Business_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BusinessTaxId)
                    .HasColumnName("Business_Tax_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierId).HasColumnName("CarrierID");

                entity.Property(e => e.City)
                    .HasColumnName("CITY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(256)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("COMPANY_CODE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.Createdate)
                    .HasColumnName("CREATEDATE")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Createuser)
                    .HasColumnName("CREATEUSER")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCreditClass)
                    .HasColumnName("CUSTOMER_CREDIT_CLASS")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerNo)
                    .HasColumnName("CUSTOMER_NO")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerType)
                    .HasColumnName("CUSTOMER_TYPE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.DivisionId).HasColumnName("DivisionID");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("FIRST_NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Language)
                    .HasColumnName("LANGUAGE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("LAST_NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Lobid).HasColumnName("LOBID");

                entity.Property(e => e.MiddleName)
                    .HasColumnName("MIDDLE_NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mvno)
                    .HasColumnName("mvno")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("PASSWORD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentTermId).HasColumnName("PaymentTermID");

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.Resellercount).HasColumnName("RESELLERCOUNT");

                entity.Property(e => e.ShippingAddressLine1)
                    .HasColumnName("ShippingADDRESS_LINE1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddressLine2)
                    .HasColumnName("ShippingADDRESS_LINE2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCity)
                    .HasColumnName("ShippingCITY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCountry)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingState)
                    .HasColumnName("ShippingSTATE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingZipcode)
                    .HasColumnName("ShippingZIPCODE")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("STATE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("STATUS")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.Property(e => e.Updatedate).HasColumnName("UPDATEDATE");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(9)
                    .IsUnicode(false);

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("country_id");

                entity.HasOne(d => d.PaymentTerm)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.PaymentTermId)
                    .HasConstraintName("paymenttermid");

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.ResellerId)
                    .HasConstraintName("reseller_id");
            });

            modelBuilder.Entity<CustomerExtension>(entity =>
            {
                entity.ToTable("customer_extension");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customer_extension_customer_id_idx");

                entity.Property(e => e.CustomerExtensionId).HasColumnName("customer_extension_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.CustomerPassword)
                    .HasColumnName("customer_password")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerUserName)
                    .HasColumnName("customer_user_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerExtension)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_extension_customer_id");
            });

            modelBuilder.Entity<DashboardLayout>(entity =>
            {
                entity.HasKey(e => e.Layoutid)
                    .HasName("PRIMARY");

                entity.ToTable("dashboard_layout");

                entity.Property(e => e.Layoutid).HasColumnName("layoutid");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Display)
                    .HasColumnName("display")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Layoutname)
                    .HasColumnName("layoutname")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'System'");

                entity.Property(e => e.Locked)
                    .HasColumnName("locked")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter)
                    .HasColumnName("parameter")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Selected).HasColumnName("selected");

                entity.Property(e => e.Templateid).HasColumnName("templateid");

                entity.Property(e => e.Templatename)
                    .HasColumnName("templatename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Updateby)
                    .HasColumnName("updateby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Updatetime).HasColumnName("updatetime");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Width).HasColumnName("width");
            });

            modelBuilder.Entity<DashboardTemplate>(entity =>
            {
                entity.ToTable("dashboard_template");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DashboardTemplateEntry>(entity =>
            {
                entity.ToTable("dashboard_template_entry");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdtime).HasColumnName("createdtime");

                entity.Property(e => e.Display)
                    .HasColumnName("display")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Locked)
                    .HasColumnName("locked")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Parameter)
                    .HasColumnName("parameter")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Position)
                    .HasColumnName("position")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Templateid).HasColumnName("templateid");

                entity.Property(e => e.Templatename)
                    .HasColumnName("templatename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Width).HasColumnName("width");
            });

            modelBuilder.Entity<Decodedmessage>(entity =>
            {
                entity.HasKey(e => e.Decodedmsgid)
                    .HasName("PRIMARY");

                entity.ToTable("decodedmessage");

                entity.HasIndex(e => e.Dataattribute)
                    .HasName("dataattribute_idx");

                entity.HasIndex(e => e.Deviceid)
                    .HasName("deviceid_idx");

                entity.HasIndex(e => e.Devicetype)
                    .HasName("devicetype_idx");

                entity.HasIndex(e => e.Msgid)
                    .HasName("msgid_idx");

                entity.HasIndex(e => e.Timestamp)
                    .HasName("timestamp_idx");

                entity.HasIndex(e => new { e.Deviceid, e.Dataattribute })
                    .HasName("indexdataattribute1");

                entity.Property(e => e.Decodedmsgid).HasColumnName("decodedmsgid");

                entity.Property(e => e.Attributeid).HasColumnName("attributeid");

                entity.Property(e => e.Dataattribute)
                    .HasColumnName("dataattribute")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Datavalue)
                    .HasColumnName("datavalue")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Deviceid)
                    .HasColumnName("deviceid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetype)
                    .HasColumnName("devicetype")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Messagetype)
                    .HasColumnName("messagetype")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Msgid)
                    .HasColumnName("msgid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .HasColumnName("timestamp")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");
            });

            modelBuilder.Entity<Device>(entity =>
            {
                entity.ToTable("device");

                entity.Property(e => e.Deviceid)
                    .HasColumnName("deviceid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Activable)
                    .HasColumnName("activable")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Activationtime)
                    .HasColumnName("activationtime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Assetid)
                    .HasColumnName("assetid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Automaticrenewal)
                    .HasColumnName("automaticrenewal")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Automaticrenewalstatus)
                    .HasColumnName("automaticrenewalstatus")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Batteryexpiredate).HasColumnName("batteryexpiredate");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Comstate)
                    .HasColumnName("comstate")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Creationtime)
                    .HasColumnName("creationtime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Devicegroup)
                    .HasColumnName("devicegroup")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Devicename)
                    .HasColumnName("devicename")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Devicenamealias)
                    .HasColumnName("devicenamealias")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetype)
                    .HasColumnName("devicetype")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Geofence)
                    .HasColumnName("geofence")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Lasteditedby)
                    .HasColumnName("lasteditedby")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lasteditiontime)
                    .HasColumnName("lasteditiontime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Lastmessagetime)
                    .HasColumnName("lastmessagetime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Lastseqnumber)
                    .HasColumnName("lastseqnumber")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Locationlat)
                    .HasColumnName("locationlat")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Locationlng)
                    .HasColumnName("locationlng")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Lqi)
                    .HasColumnName("lqi")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Modemcertificate)
                    .HasColumnName("modemcertificate")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Prototype)
                    .HasColumnName("prototype")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.State)
                    .HasColumnName("state")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DeviceMaster>(entity =>
            {
                entity.HasKey(e => e.DeviceId)
                    .HasName("PRIMARY");

                entity.ToTable("device_master");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("DEVICE_ID")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasColumnName("COMPANY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateTime).HasColumnName("CREATE_TIME");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CUSTOMER_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DeviceStatus)
                    .HasColumnName("DEVICE_STATUS")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Devicename)
                    .HasColumnName("DEVICENAME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetype)
                    .HasColumnName("DEVICETYPE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.MacAddress)
                    .HasColumnName("MAC_ADDRESS")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mfg)
                    .HasColumnName("MFG")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MfgSerialNum)
                    .HasColumnName("MFG_SERIAL_NUM")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MfgSku)
                    .IsRequired()
                    .HasColumnName("MFG_SKU")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.NetworkSupported)
                    .HasColumnName("NETWORK_SUPPORTED")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OldDeviceId)
                    .HasColumnName("OLD_DEVICE_ID")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.Refurbished)
                    .HasColumnName("REFURBISHED")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialStock)
                    .HasColumnName("SPECIAL_STOCK")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Upc)
                    .IsRequired()
                    .HasColumnName("UPC")
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateTime).HasColumnName("UPDATE_TIME");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("UPDATED_BY")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Devicelifecycle>(entity =>
            {
                entity.ToTable("devicelifecycle");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Createby).HasColumnName("createby");

                entity.Property(e => e.Createtime).HasColumnName("createtime");

                entity.Property(e => e.Deviceid).HasColumnName("deviceid");

                entity.Property(e => e.Devicename).HasColumnName("devicename");

                entity.Property(e => e.Optime).HasColumnName("optime");

                entity.Property(e => e.Optype).HasColumnName("optype");

                entity.Property(e => e.Updateby).HasColumnName("updateby");

                entity.Property(e => e.Updatetime).HasColumnName("updatetime");
            });

            modelBuilder.Entity<Devicetype>(entity =>
            {
                entity.ToTable("devicetype");

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Automaticrenewal)
                    .HasColumnName("automaticrenewal")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Creationtime)
                    .HasColumnName("creationtime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypenamealias)
                    .HasColumnName("devicetypenamealias")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Downlinkdatastring)
                    .HasColumnName("downlinkdatastring")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Downlinkmode)
                    .HasColumnName("downlinkmode")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Geofence)
                    .HasColumnName("geofence")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Itspgroup)
                    .HasColumnName("itspgroup")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Keepalive)
                    .HasColumnName("keepalive")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Lasteditedby)
                    .HasColumnName("lasteditedby")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lasteditiontime)
                    .HasColumnName("lasteditiontime")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");

                entity.Property(e => e.Payloadtype)
                    .HasColumnName("payloadtype")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<Division>(entity =>
            {
                entity.ToTable("division");

                entity.HasIndex(e => e.CountryId)
                    .HasName("countryID_idx");

                entity.HasIndex(e => e.LobId)
                    .HasName("lobID_idx");

                entity.Property(e => e.DivisionId).HasColumnName("divisionID");

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.DivisionName)
                    .HasColumnName("divisionName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LobId).HasColumnName("lobID");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Division)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("countryID");

                entity.HasOne(d => d.Lob)
                    .WithMany(p => p.Division)
                    .HasForeignKey(d => d.LobId)
                    .HasConstraintName("lobID");
            });

            modelBuilder.Entity<ErrorLog>(entity =>
            {
                entity.ToTable("error_log");

                entity.Property(e => e.ErrorLogId).HasColumnName("error_log_id");

                entity.Property(e => e.ColumnNumber).HasColumnName("column_number");

                entity.Property(e => e.CorrelationId)
                    .HasColumnName("correlation_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorDateTime).HasColumnName("error_date_time");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LineNumber).HasColumnName("line_number");

                entity.Property(e => e.Message).HasColumnName("message");

                entity.Property(e => e.MethodName)
                    .HasColumnName("method_name")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.StackTrace).HasColumnName("stack_trace");
            });

            modelBuilder.Entity<ExceptionLog>(entity =>
            {
                entity.ToTable("exception_log");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnName("create_date");

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionName)
                    .HasColumnName("function_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectName)
                    .HasColumnName("project_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Geofence>(entity =>
            {
                entity.HasKey(e => e.Fenceid)
                    .HasName("PRIMARY");

                entity.ToTable("geofence");

                entity.Property(e => e.Fenceid).HasColumnName("fenceid");

                entity.Property(e => e.Companycode)
                    .HasColumnName("companycode")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Fencename)
                    .HasColumnName("fencename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Geodata)
                    .HasColumnName("geodata")
                    .HasMaxLength(16200)
                    .IsUnicode(false);

                entity.Property(e => e.Shape).HasColumnName("shape");
            });

            modelBuilder.Entity<HibernateSequence>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("hibernate_sequence");

                entity.Property(e => e.NextVal).HasColumnName("next_val");
            });

            modelBuilder.Entity<Icon>(entity =>
            {
                entity.ToTable("icon");

                entity.Property(e => e.Iconid).HasColumnName("iconid");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Iconname)
                    .HasColumnName("iconname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Iconscale)
                    .HasColumnName("iconscale")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Iconsvg)
                    .HasColumnName("iconsvg")
                    .HasMaxLength(4096)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Icons>(entity =>
            {
                entity.HasKey(e => e.Iconid)
                    .HasName("PRIMARY");

                entity.ToTable("icons");

                entity.Property(e => e.Iconid).HasColumnName("iconid");

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Iconname)
                    .HasColumnName("iconname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Iconscale)
                    .HasColumnName("iconscale")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Iconsvg)
                    .HasColumnName("iconsvg")
                    .HasMaxLength(4096)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Industry>(entity =>
            {
                entity.ToTable("industry");

                entity.Property(e => e.IndustryId).HasColumnName("industryID");

                entity.Property(e => e.IndustryName)
                    .HasColumnName("industryName")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Inventory>(entity =>
            {
                entity.ToTable("inventory");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("inventory_company_id_idx");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("inventory_customer_id_idx");

                entity.HasIndex(e => e.InventoryId)
                    .HasName("INVENTORY_ID")
                    .IsUnique();

                entity.HasIndex(e => e.ItemId)
                    .HasName("inventory_item_id_idx");

                entity.HasIndex(e => e.ResellerId)
                    .HasName("inventory_reseller_id_idx");

                entity.Property(e => e.InventoryId).HasColumnName("INVENTORY_ID");

                entity.Property(e => e.ActiveDate).HasColumnName("ACTIVE_DATE");

                entity.Property(e => e.CarrierId)
                    .HasColumnName("carrierID")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CompanyId).HasColumnName("COMPANY_ID");

                entity.Property(e => e.CustomerId).HasColumnName("CUSTOMER_ID");

                entity.Property(e => e.DeviceImei)
                    .HasColumnName("deviceIMEI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Devicename)
                    .HasColumnName("devicename")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetype)
                    .HasColumnName("devicetype")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Iccid)
                    .HasColumnName("ICCID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdentifierType)
                    .HasColumnName("IDENTIFIER_TYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Unique identifier: ICCID for SIM, EID for ESIM");

                entity.Property(e => e.IdentifierValue)
                    .HasColumnName("IDENTIFIER_VALUE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ImageFileLoc)
                    .HasColumnName("IMAGE_FILE_LOC")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ImageFileLocBig)
                    .HasColumnName("IMAGE_FILE_LOC_BIG")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ImageFileName)
                    .HasColumnName("IMAGE_FILE_NAME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InactiveDate).HasColumnName("INACTIVE_DATE");

                entity.Property(e => e.ItemDescription)
                    .HasColumnName("ITEM_DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("ITEM_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemPlanId).HasColumnName("ItemPlan_ID");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.SerialNum)
                    .HasColumnName("SERIAL_NUM")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasComment("0 = inactive, 1 = active, 2 = suspended, 3 = canceled | inactive = purchased but sim card hasn't been connected to plan yet | active = plan connected to sim card | suspended = had a plan, temporarily turned off | canceled = hand a plan, permanently turned off ");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Inventory)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("inventory_company_id");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Inventory)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("inventory_customer_id");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.Inventory)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("inventory_item_id");

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.Inventory)
                    .HasForeignKey(d => d.ResellerId)
                    .HasConstraintName("inventory_reseller_id");
            });

            modelBuilder.Entity<InventoryItemView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("inventory_item_view");

                entity.Property(e => e.CarrierId)
                    .HasColumnName("carrierID")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CompanyId).HasColumnName("COMPANY_ID");

                entity.Property(e => e.Devicename)
                    .HasColumnName("devicename")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdentifierType)
                    .HasColumnName("IDENTIFIER_TYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Unique identifier: ICCID for SIM, EID for ESIM");

                entity.Property(e => e.IdentifierValue)
                    .HasColumnName("IDENTIFIER_VALUE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InventoryId).HasColumnName("INVENTORY_ID");

                entity.Property(e => e.ItemDescription)
                    .HasColumnName("ITEM_DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("ITEM_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemPlanId).HasColumnName("ItemPlan_ID");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasComment("0 = inactive, 1 = active, 2 = suspended, 3 = canceled | inactive = purchased but sim card hasn't been connected to plan yet | active = plan connected to sim card | suspended = had a plan, temporarily turned off | canceled = hand a plan, permanently turned off ");

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("UNIT_PRICE")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<InventoryMaster>(entity =>
            {
                entity.HasKey(e => e.Upc)
                    .HasName("PRIMARY");

                entity.ToTable("inventory_master");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BatteryLife)
                    .HasColumnName("BATTERY_LIFE")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Color)
                    .HasColumnName("COLOR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .IsRequired()
                    .HasColumnName("COMPANY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cost)
                    .HasColumnName("COST")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CreateDatetime)
                    .HasColumnName("CREATE_DATETIME")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DescriptionEn)
                    .HasColumnName("DESCRIPTION_EN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionFr)
                    .HasColumnName("DESCRIPTION_FR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Group)
                    .HasColumnName("GROUP")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HighFrequency)
                    .HasColumnName("HIGH_FREQUENCY")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Image)
                    .HasColumnName("IMAGE")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LowFrequency)
                    .HasColumnName("LOW_FREQUENCY")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Mfg)
                    .IsRequired()
                    .HasColumnName("MFG")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MfgSku)
                    .HasColumnName("MFG_SKU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.NetworkSupported)
                    .HasColumnName("NETWORK_SUPPORTED")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OtherLanguageDec)
                    .HasColumnName("OTHER_LANGUAGE_DEC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OurSku)
                    .HasColumnName("OUR_SKU")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("QUANTITY")
                    .HasColumnType("decimal(10,0)");

                entity.Property(e => e.Refurbrished)
                    .HasColumnName("REFURBRISHED")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.Serial)
                    .HasColumnName("SERIAL")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.SoldQuantity)
                    .HasColumnName("SOLD_QUANTITY")
                    .HasColumnType("decimal(10,0)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Specification)
                    .HasColumnName("SPECIFICATION")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Supported)
                    .HasColumnName("SUPPORTED")
                    .HasMaxLength(1)
                    .IsFixedLength();

                entity.Property(e => e.Type)
                    .HasColumnName("TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDatetime)
                    .HasColumnName("UPDATE_DATETIME")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnName("UPDATED_BY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Weight)
                    .HasColumnName("WEIGHT")
                    .HasColumnType("decimal(10,2)");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.ToTable("invoice");

                entity.HasIndex(e => e.InvoiceId)
                    .HasName("INVOICE_ID")
                    .IsUnique();

                entity.HasIndex(e => e.OrderId)
                    .HasName("invoice_order_id_idx");

                entity.HasIndex(e => e.StatusId)
                    .HasName("invoice_status_id_idx");

                entity.Property(e => e.InvoiceId).HasColumnName("INVOICE_ID");

                entity.Property(e => e.AmountAdjust)
                    .HasColumnName("Amount_Adjust")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.AmountDue)
                    .HasColumnName("Amount_Due")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.AmountPaid)
                    .HasColumnName("Amount_Paid")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDt).HasColumnName("CREATE_DT");

                entity.Property(e => e.DiscountAmt)
                    .HasColumnName("DISCOUNT_AMT")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.EndDt)
                    .HasColumnName("END_DT")
                    .HasColumnType("date");

                entity.Property(e => e.FedTax)
                    .HasColumnName("FED_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.InvAmt)
                    .HasColumnName("INV_AMT")
                    .HasColumnType("decimal(20,2)")
                    .HasComment("total including tax");

                entity.Property(e => e.InvDescrpt)
                    .HasColumnName("INV_DESCRPT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvTtl)
                    .HasColumnName("INV_TTL")
                    .HasColumnType("decimal(20,2)")
                    .HasComment("total before tax");

                entity.Property(e => e.OrderId).HasColumnName("ORDER_ID");

                entity.Property(e => e.OtItemsFlag)
                    .HasColumnName("OT_ITEMS_FLAG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PreAccBal)
                    .HasColumnName("PRE_ACC_BAL")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.ProvTax)
                    .HasColumnName("PROV_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.SalesTax)
                    .HasColumnName("SALES_TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.StartDt)
                    .HasColumnName("START_DT")
                    .HasColumnType("date");

                entity.Property(e => e.StatusId).HasColumnName("STATUS_ID");

                entity.Property(e => e.SubAmt)
                    .HasColumnName("SUB_AMT")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.Tax)
                    .HasColumnName("TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasComment("tax");

                entity.Property(e => e.TelecomTax)
                    .HasColumnName("TELECOM_TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.UpdateDt).HasColumnName("UPDATE_DT");

                entity.Property(e => e.UsageDetail)
                    .HasColumnName("USAGE_DETAIL")
                    .HasMaxLength(512)
                    .IsUnicode(false);

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_order_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Invoice)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_status_id");
            });

            modelBuilder.Entity<InvoiceListView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("invoice_list_view");

                entity.Property(e => e.AccountId).HasColumnName("ACCOUNT_ID");

                entity.Property(e => e.AccountType)
                    .HasColumnName("ACCOUNT_TYPE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Accountnumber)
                    .HasColumnName("ACCOUNTNUMBER")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasComment(@"TO DO:

Set to NN and UQ after adding fake entries");

                entity.Property(e => e.AmountAdjust)
                    .HasColumnName("Amount_Adjust")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.AmountDue)
                    .HasColumnName("Amount_Due")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.AmountPaid)
                    .HasColumnName("Amount_Paid")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.CardMask)
                    .HasColumnName("CARD_MASK")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDt).HasColumnName("CREATE_DT");

                entity.Property(e => e.Createuser)
                    .HasColumnName("CREATEUSER")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.DiscountAmt)
                    .HasColumnName("DISCOUNT_AMT")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.EndDt)
                    .HasColumnName("END_DT")
                    .HasColumnType("date");

                entity.Property(e => e.FirstName)
                    .HasColumnName("FIRST_NAME")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.InvAmt)
                    .HasColumnName("INV_AMT")
                    .HasColumnType("decimal(20,2)")
                    .HasComment("total including tax");

                entity.Property(e => e.InvDescrpt)
                    .HasColumnName("INV_DESCRPT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvTtl)
                    .HasColumnName("INV_TTL")
                    .HasColumnType("decimal(20,2)")
                    .HasComment("total before tax");

                entity.Property(e => e.InvoiceId).HasColumnName("INVOICE_ID");

                entity.Property(e => e.LastName)
                    .HasColumnName("LAST_NAME")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId).HasColumnName("ORDER_ID");

                entity.Property(e => e.OtItemsFlag)
                    .HasColumnName("OT_ITEMS_FLAG")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMethod)
                    .HasColumnName("PAYMENT_METHOD")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PreAccBal)
                    .HasColumnName("PRE_ACC_BAL")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.SalesTax)
                    .HasColumnName("SALES_TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.StartDt)
                    .HasColumnName("START_DT")
                    .HasColumnType("date");

                entity.Property(e => e.StatusDesc)
                    .HasColumnName("STATUS_DESC")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("STATUS_ID");

                entity.Property(e => e.SubAmt)
                    .HasColumnName("SUB_AMT")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.Tax)
                    .HasColumnName("TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasComment("tax");

                entity.Property(e => e.TelecomTax)
                    .HasColumnName("TELECOM_TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.UpdateDt).HasColumnName("UPDATE_DT");

                entity.Property(e => e.UsageDetail)
                    .HasColumnName("USAGE_DETAIL")
                    .HasMaxLength(512)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.ToTable("item");

                entity.HasIndex(e => e.ItemId)
                    .HasName("ITEM_ID")
                    .IsUnique();

                entity.HasIndex(e => e.ItemPayFrequencyId)
                    .HasName("item_item_pay_fequency_id_idx");

                entity.HasIndex(e => e.ItemTypeId)
                    .HasName("item_item_type_id_idx");

                entity.HasIndex(e => e.PromoteId)
                    .HasName("promote_id_idx");

                entity.HasIndex(e => e.ResellerId)
                    .HasName("reseller_id_idx");

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.AutoRenewFlag)
                    .HasColumnName("AUTO_RENEW_FLAG")
                    .HasComment("0 = the Plan doesn't not auto renew\\n1 = the plan auto renews when data goes over ");

                entity.Property(e => e.Billcycle).HasColumnName("BILLCYCLE");

                entity.Property(e => e.CarrierId).HasColumnName("CarrierID");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("COMPANY_CODE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId).HasColumnName("Company_ID");

                entity.Property(e => e.CustomerId).HasColumnName("Customer_ID");

                entity.Property(e => e.ImageFileLoc)
                    .HasColumnName("IMAGE_FILE_LOC")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ImageFileLocBig)
                    .HasColumnName("IMAGE_FILE_LOC_BIG")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ImageFileName)
                    .HasColumnName("IMAGE_FILE_NAME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ItemCode)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("this is the plan code used in third party APIs - must be unique");

                entity.Property(e => e.ItemDescription)
                    .HasColumnName("ITEM_DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemDescriptionFr)
                    .HasColumnName("ITEM_DESCRIPTION_FR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("ITEM_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemPayFrequencyId).HasColumnName("ITEM_PAY_FREQUENCY_ID");

                entity.Property(e => e.ItemPayType)
                    .HasColumnName("ITEM_PAY_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ItemStatus).HasColumnName("ITEM_STATUS");

                entity.Property(e => e.ItemTypeId).HasColumnName("ITEM_TYPE_ID");

                entity.Property(e => e.LoyaltyPoint)
                    .HasColumnName("LOYALTY_POINT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PromoteId).HasColumnName("PROMOTE_ID");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.SequenceTag).HasColumnName("SEQUENCE_TAG");

                entity.Property(e => e.SerialNum)
                    .HasColumnName("SERIAL_NUM")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.Property(e => e.Unit)
                    .HasColumnName("UNIT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UnitPrice)
                    .HasColumnName("UNIT_PRICE")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.HasOne(d => d.ItemPayFrequency)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.ItemPayFrequencyId)
                    .HasConstraintName("item_item_pay_fequency_id");

                entity.HasOne(d => d.ItemType)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.ItemTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("item_item_type_id");

                entity.HasOne(d => d.Promote)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.PromoteId)
                    .HasConstraintName("promote_id");

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.ResellerId)
                    .HasConstraintName("item_reseller_id");
            });

            modelBuilder.Entity<ItemPayFrequency>(entity =>
            {
                entity.ToTable("item_pay_frequency");

                entity.HasIndex(e => e.ItemPayFrequencyId)
                    .HasName("item_pay_frequency_ID")
                    .IsUnique();

                entity.Property(e => e.ItemPayFrequencyId).HasColumnName("item_pay_frequency_ID");

                entity.Property(e => e.DefaultValue).HasColumnName("DEFAULT_VALUE");

                entity.Property(e => e.ItemPayFrequencyName)
                    .HasColumnName("item_pay_frequency_NAME")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ItemRate>(entity =>
            {
                entity.ToTable("item_rate");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.EndPoint).HasColumnName("End_Point");

                entity.Property(e => e.ItemId).HasColumnName("item_id");

                entity.Property(e => e.ItemRateStatus)
                    .HasColumnName("ITEM_RATE_STATUS")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.Property(e => e.Rate)
                    .HasColumnName("RATE")
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.StartPoint).HasColumnName("Start_Point");
            });

            modelBuilder.Entity<ItemType>(entity =>
            {
                entity.ToTable("item_type");

                entity.HasIndex(e => e.ItemTypeId)
                    .HasName("ITEM_TYPE_ID")
                    .IsUnique();

                entity.Property(e => e.ItemTypeId).HasColumnName("ITEM_TYPE_ID");

                entity.Property(e => e.ItemTypeDescFr)
                    .HasColumnName("ITEM_TYPE_DESC_FR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemTypeDescription)
                    .IsRequired()
                    .HasColumnName("ITEM_TYPE_DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Itspgroup>(entity =>
            {
                entity.HasKey(e => e.Groupid)
                    .HasName("PRIMARY");

                entity.ToTable("itspgroup");

                entity.Property(e => e.Groupid)
                    .HasColumnName("groupid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Billable)
                    .HasColumnName("billable")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Clientemail)
                    .HasColumnName("clientemail")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Descript)
                    .HasColumnName("descript")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Groupcountry)
                    .HasColumnName("groupcountry")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Groupname)
                    .HasColumnName("groupname")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Grouptimezone)
                    .HasColumnName("grouptimezone")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Grouptype)
                    .HasColumnName("grouptype")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Itsppath)
                    .HasColumnName("itsppath")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Nameci)
                    .HasColumnName("nameci")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Technicalemail)
                    .HasColumnName("technicalemail")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Lob>(entity =>
            {
                entity.ToTable("lob");

                entity.HasIndex(e => e.CountryId)
                    .HasName("country_id_idx");

                entity.Property(e => e.LobId).HasColumnName("lobID");

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.LobName)
                    .HasColumnName("lobName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Lob)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("country_id1");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasKey(e => e.Msgid)
                    .HasName("PRIMARY");

                entity.ToTable("message");

                entity.HasIndex(e => e.Deviceid)
                    .HasName("deviceid_idx");

                entity.HasIndex(e => e.Time)
                    .HasName("timestamp_idx");

                entity.Property(e => e.Msgid).HasColumnName("msgid");

                entity.Property(e => e.Computedlocationlat)
                    .HasColumnName("computedlocationlat")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Computedlocationlng)
                    .HasColumnName("computedlocationlng")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Deviceid)
                    .HasColumnName("deviceid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Lqi)
                    .HasColumnName("lqi")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Nbframes)
                    .HasColumnName("nbframes")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Operator)
                    .HasColumnName("operator")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Rollovercounter)
                    .HasColumnName("rollovercounter")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Seqnumber)
                    .HasColumnName("seqnumber")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasDefaultValueSql("'1980-01-01 00:00:00'");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("order");

                entity.HasIndex(e => e.AccountId)
                    .HasName("order_account_id_idx");

                entity.HasIndex(e => e.OrderId)
                    .HasName("ORDER_ID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.PayFrequencyId)
                    .HasName("order_pay_frequency_id_idx");

                entity.HasIndex(e => e.StatusId)
                    .HasName("order_status_id_idx");

                entity.HasIndex(e => e.TaxId)
                    .HasName("order_tax_id_idx");

                entity.Property(e => e.OrderId).HasColumnName("ORDER_ID");

                entity.Property(e => e.AccountId).HasColumnName("ACCOUNT_ID");

                entity.Property(e => e.ActiveDate)
                    .HasColumnName("ACTIVE_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.AutoPay)
                    .HasColumnName("AUTO_PAY")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Billcycle)
                    .HasColumnName("BILLCYCLE")
                    .HasColumnType("int unsigned");

                entity.Property(e => e.BillingEndDt)
                    .HasColumnName("BILLING_END_DT")
                    .HasColumnType("date");

                entity.Property(e => e.BillingStartDt)
                    .HasColumnName("BILLING_START_DT")
                    .HasColumnType("date");

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("COMPANY_CODE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Createuser)
                    .HasColumnName("createuser")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Currency)
                    .HasColumnName("CURRENCY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId).HasColumnName("CUSTOMER_ID");

                entity.Property(e => e.DiscountAmount)
                    .HasColumnName("DISCOUNT_AMOUNT")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.InactiveDate)
                    .HasColumnName("INACTIVE_DATE")
                    .HasColumnType("date");

                entity.Property(e => e.NextBillingDt)
                    .HasColumnName("NEXT_BILLING_DT")
                    .HasColumnType("date");

                entity.Property(e => e.OrderAmount)
                    .HasColumnName("ORDER_AMOUNT")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OrderBalance)
                    .HasColumnName("ORDER_BALANCE")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.OrderDate).HasColumnName("ORDER_DATE");

                entity.Property(e => e.OrderTax)
                    .HasColumnName("ORDER_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OrderTotal)
                    .HasColumnName("ORDER_TOTAL")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.OrdrPayType)
                    .HasColumnName("ORDR_PAY_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrTtl)
                    .HasColumnName("ORDR_TTL")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OrdrTtlTax)
                    .HasColumnName("ORDR_TTL_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.OrdrType)
                    .HasColumnName("ORDR_TYPE")
                    .HasComment("PRE = PREPAID, POST=POSTPAID");

                entity.Property(e => e.PayFrequencyId).HasColumnName("PAY_FREQUENCY_ID");

                entity.Property(e => e.SalesTax)
                    .HasColumnName("SALES_TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.ShippingCharge).HasColumnName("shipping_charge");

                entity.Property(e => e.ShippingLt)
                    .HasColumnName("shipping_lt")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingMethod).HasColumnName("shipping_method");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasComment("ACTIVE=1, 0 INACTIVE=0, PLAN CHANGED = -1");

                entity.Property(e => e.StatusId).HasColumnName("STATUS_ID");

                entity.Property(e => e.TaxId).HasColumnName("TAX_ID");

                entity.Property(e => e.TelecomTax)
                    .HasColumnName("TELECOM_TAX")
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.UpdateDt).HasColumnName("UPDATE_DT");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("order_account_id");

                entity.HasOne(d => d.PayFrequency)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.PayFrequencyId)
                    .HasConstraintName("order_pay_frequency_id");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("order_status_id");

                entity.HasOne(d => d.Tax)
                    .WithMany(p => p.Order)
                    .HasForeignKey(d => d.TaxId)
                    .HasConstraintName("order_tax_id");
            });

            modelBuilder.Entity<OrderAddress>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("order_address");

                entity.HasIndex(e => new { e.TrxId, e.AddrType })
                    .HasName("TRX_ID")
                    .IsUnique();

                entity.Property(e => e.AddrType)
                    .IsRequired()
                    .HasColumnName("ADDR_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Address1)
                    .HasColumnName("ADDRESS_1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasColumnName("ADDRESS_2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("CITY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("FIRST_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("LAST_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneExt)
                    .HasColumnName("PHONE_EXT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PostalZipCode)
                    .HasColumnName("POSTAL_ZIP_CODE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceState)
                    .HasColumnName("PROVINCE_STATE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TrxId).HasColumnName("TRX_ID");
            });

            modelBuilder.Entity<OrderItem>(entity =>
            {
                entity.ToTable("order_item");

                entity.HasIndex(e => e.ItemId)
                    .HasName("order_item_item_id_idx");

                entity.HasIndex(e => e.OrderId)
                    .HasName("order_item_order_id_idx");

                entity.HasIndex(e => e.OrderitemId)
                    .HasName("ORDER_ITEM_ID")
                    .IsUnique();

                entity.HasIndex(e => e.PromoteId)
                    .HasName("promote_id_idx");

                entity.Property(e => e.OrderitemId).HasColumnName("ORDERITEM_ID");

                entity.Property(e => e.Billcycle).HasColumnName("BILLCYCLE");

                entity.Property(e => e.BillingEndDt)
                    .HasColumnName("BILLING_END_DT")
                    .HasColumnType("date");

                entity.Property(e => e.BillingStartDt)
                    .HasColumnName("BILLING_START_DT")
                    .HasColumnType("date");

                entity.Property(e => e.CostAmount)
                    .HasColumnName("COST_AMOUNT")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CostTax)
                    .HasColumnName("COST_TAX")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.Courier)
                    .HasColumnName("courier")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DataUsageWhenPlanChanged).HasColumnType("decimal(20,2)");

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.ItemPrice)
                    .HasColumnName("ITEM_PRICE")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ItemQty).HasColumnName("ITEM_QTY");

                entity.Property(e => e.Modification)
                    .HasColumnName("modification")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.NextBillingDt)
                    .HasColumnName("NEXT_BILLING_DT")
                    .HasColumnType("date");

                entity.Property(e => e.OrderId).HasColumnName("ORDER_ID");

                entity.Property(e => e.PayFreq)
                    .HasColumnName("PAY_FREQ")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PromoAmount)
                    .HasColumnName("PROMO_AMOUNT")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.PromoteId).HasColumnName("PROMOTE_ID");

                entity.Property(e => e.ShippingCharge).HasColumnName("shipping_charge");

                entity.Property(e => e.ShippingDate).HasColumnName("shipping_date");

                entity.Property(e => e.ShippingStatusId).HasColumnName("shipping_status_id");

                entity.Property(e => e.SmsusageWhenPlanChanged)
                    .HasColumnName("SMSUsageWhenPlanChanged")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.TrackingNum)
                    .HasColumnName("tracking_num")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UsageHistoryId).HasColumnName("USAGE_HISTORY_ID");

                entity.Property(e => e.VoiceUsageWhenPlanChanged).HasColumnType("decimal(20,2)");

                entity.Property(e => e.WaybillNum)
                    .HasColumnName("waybill_num")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.OrderItem)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_item_item_id");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderItem)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("order_item_order_id");

                entity.HasOne(d => d.Promote)
                    .WithMany(p => p.OrderItem)
                    .HasForeignKey(d => d.PromoteId)
                    .HasConstraintName("promote_id3");
            });

            modelBuilder.Entity<OrderLog>(entity =>
            {
                entity.ToTable("order_log");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.LogDate).HasColumnName("log_date");

                entity.Property(e => e.Operation)
                    .HasColumnName("operation")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId).HasColumnName("order_id");

                entity.Property(e => e.OrderName)
                    .HasColumnName("order_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Paymentterm>(entity =>
            {
                entity.ToTable("paymentterm");

                entity.HasIndex(e => e.PaymentTermId)
                    .HasName("PaymentTermID_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.PaymentTermName)
                    .HasName("PaymentTermName_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.PaymentTermValue)
                    .HasName("PaymentTermValue_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.PaymentTermId).HasColumnName("PaymentTermID");

                entity.Property(e => e.PaymentTermName)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PlanUsage>(entity =>
            {
                entity.ToTable("plan_usage");

                entity.HasComment("table stores hourly live data usage information. It provide a pseudo live data usage, however it is not 100% accurate ");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CarrierId).HasColumnName("carrier_id");

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.ResellerCode)
                    .IsRequired()
                    .HasColumnName("reseller_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SimIdentifierType)
                    .IsRequired()
                    .HasColumnName("sim_identifier_type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SimIdentifierValue)
                    .HasColumnName("sim_identifier_value")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SimUsage)
                    .HasColumnName("sim_usage")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<PlanUsageHistory>(entity =>
            {
                entity.ToTable("plan_usage_history");

                entity.HasComment("This table is to keep track of monthly usage history to verify data");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.ResellerCode)
                    .IsRequired()
                    .HasColumnName("reseller_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SimIdentifierType)
                    .IsRequired()
                    .HasColumnName("sim_identifier_type")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SimIdentifierValue)
                    .IsRequired()
                    .HasColumnName("sim_identifier_value")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SimUsage)
                    .HasColumnName("sim_usage")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date")
                    .HasComment("The date the information was inputed into the table");
            });

            modelBuilder.Entity<Promote>(entity =>
            {
                entity.ToTable("promote");

                entity.HasIndex(e => e.PromoteId)
                    .HasName("PROMOTE_ID_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.PromoteId).HasColumnName("PROMOTE_ID");

                entity.Property(e => e.CompanyId).HasColumnName("COMPANY_ID");

                entity.Property(e => e.CustomerId).HasColumnName("CUSTOMER_ID");

                entity.Property(e => e.EndDate).HasColumnName("END_DATE");

                entity.Property(e => e.PromoteName)
                    .HasColumnName("PROMOTE_NAME")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PromoteStatus).HasColumnName("PROMOTE_STATUS");

                entity.Property(e => e.PromoteType)
                    .IsRequired()
                    .HasColumnName("PROMOTE_TYPE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PromoteValue)
                    .HasColumnName("PROMOTE_VALUE")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.StartDate).HasColumnName("START_DATE");

                entity.Property(e => e.ValidEndDate).HasColumnName("VALID_END_DATE");

                entity.Property(e => e.ValidStartDate).HasColumnName("VALID_START_DATE");
            });

            modelBuilder.Entity<PurchaseLog>(entity =>
            {
                entity.ToTable("purchase_log");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.EndDt).HasColumnName("end_dt");

                entity.Property(e => e.ItemId).HasColumnName("item_id");

                entity.Property(e => e.StartDt).HasColumnName("start_dt");

                entity.Property(e => e.Status).HasColumnName("status");
            });

            modelBuilder.Entity<Refund>(entity =>
            {
                entity.ToTable("refund");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDt).HasColumnName("CREATE_DT");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Items)
                    .HasColumnName("ITEMS")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("list all refund item ids");

                entity.Property(e => e.OrdrId).HasColumnName("ORDR_ID");

                entity.Property(e => e.RefundAmt)
                    .HasColumnName("REFUND_AMT")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.StatId).HasColumnName("STAT_ID");

                entity.Property(e => e.TrxId).HasColumnName("TRX_ID");

                entity.Property(e => e.UpdateDt).HasColumnName("UPDATE_DT");
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity.ToTable("report");

                entity.Property(e => e.Reportid).HasColumnName("reportid");

                entity.Property(e => e.CompanyCode)
                    .HasColumnName("company_code")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Datetime).HasColumnName("datetime");

                entity.Property(e => e.Format)
                    .HasColumnName("format")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Header)
                    .HasColumnName("header")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Msgbody)
                    .HasColumnName("msgbody")
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Receiver)
                    .HasColumnName("receiver")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Reportname)
                    .HasColumnName("reportname")
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Sendcron)
                    .HasColumnName("sendcron")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Timezone)
                    .HasColumnName("timezone")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<Reportlog>(entity =>
            {
                entity.ToTable("reportlog");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Reportid)
                    .HasColumnName("reportid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Result)
                    .HasColumnName("result")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Runtime).HasColumnName("runtime");
            });

            modelBuilder.Entity<Reportrule>(entity =>
            {
                entity.HasKey(e => e.Ruleid)
                    .HasName("PRIMARY");

                entity.ToTable("reportrule");

                entity.Property(e => e.Ruleid).HasColumnName("ruleid");

                entity.Property(e => e.Assetclassid).HasColumnName("assetclassid");

                entity.Property(e => e.Assetclassname)
                    .HasColumnName("assetclassname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.Assetname)
                    .HasColumnName("assetname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Colname)
                    .HasColumnName("colname")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Deviceid)
                    .HasColumnName("deviceid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicename)
                    .HasColumnName("devicename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypename)
                    .HasColumnName("devicetypename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Operation).HasColumnName("operation");

                entity.Property(e => e.Templateid).HasColumnName("templateid");

                entity.Property(e => e.Time).HasColumnName("time");
            });

            modelBuilder.Entity<Reseller>(entity =>
            {
                entity.ToTable("reseller");

                entity.HasIndex(e => e.CarrierId)
                    .HasName("carrierid_idx");

                entity.HasIndex(e => e.CompanyId)
                    .HasName("reseller_company_id_idx");

                entity.HasIndex(e => e.ResellerAccountPrefix)
                    .HasName("reseller_account_prefix_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.ResellerCode)
                    .HasName("reseller_code_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.ResellerId).HasColumnName("reseller_id");

                entity.Property(e => e.AccountNumber)
                    .HasColumnName("account_number")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasComment(@"TO DO:

Set to NN and UQ after adding fake entries");

                entity.Property(e => e.Assembly)
                    .HasColumnName("assembly")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CarrierId)
                    .HasColumnName("CarrierID")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("company_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyId).HasColumnName("company_id");

                entity.Property(e => e.ContactFirstName)
                    .HasColumnName("contact_first_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ContactLastName)
                    .HasColumnName("contact_last_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("createdate")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Createuser)
                    .HasColumnName("createuser")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Language)
                    .HasColumnName("language")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Mvno)
                    .IsRequired()
                    .HasColumnName("mvno")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneExtension)
                    .HasColumnName("phoneExtension")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerAccountPrefix)
                    .HasColumnName("reseller_account_prefix")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerCode)
                    .IsRequired()
                    .HasColumnName("reseller_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerName)
                    .HasColumnName("reseller_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Resellerkey)
                    .HasColumnName("resellerkey")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Resellerkeycreated).HasColumnName("resellerkeycreated");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.HasOne(d => d.Carrier)
                    .WithMany(p => p.Reseller)
                    .HasForeignKey(d => d.CarrierId)
                    .HasConstraintName("carrierid1");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Reseller)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("reseller_company_id");
            });

            modelBuilder.Entity<ResellerExtension>(entity =>
            {
                entity.ToTable("reseller_extension");

                entity.HasIndex(e => e.ResellerExtensionId)
                    .HasName("reseller_extension_id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.ResellerId)
                    .HasName("reseller_extension_reseller_id_idx");

                entity.Property(e => e.ResellerExtensionId).HasColumnName("reseller_extension_id");

                entity.Property(e => e.IbMvno)
                    .HasColumnName("ib_mvno")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.IbPassword)
                    .HasColumnName("ib_password")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IbUserName)
                    .HasColumnName("ib_user_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerId).HasColumnName("reseller_id");

                entity.Property(e => e.ResellerPassword)
                    .HasColumnName("reseller_password")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerUserName)
                    .HasColumnName("reseller_user_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Reseller)
                    .WithMany(p => p.ResellerExtension)
                    .HasForeignKey(d => d.ResellerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("reseller_extension_reseller_id");
            });

            modelBuilder.Entity<Resellerinventoryitemview>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("resellerinventoryitemview");

                entity.Property(e => e.Accountnumber)
                    .HasColumnName("ACCOUNTNUMBER")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasComment(@"TO DO:

Set to NN and UQ after adding fake entries");

                entity.Property(e => e.ActiveDate).HasColumnName("ACTIVE_DATE");

                entity.Property(e => e.AddressLine1)
                    .HasColumnName("ADDRESS_LINE1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine2)
                    .HasColumnName("ADDRESS_LINE2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BusinessName)
                    .HasColumnName("Business_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BusinessTaxId)
                    .HasColumnName("Business_Tax_ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasColumnName("CITY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("company_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CUSTOMER_ID")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CustomerName)
                    .HasColumnName("customerName")
                    .HasMaxLength(41)
                    .IsUnicode(false);

                entity.Property(e => e.DeviceImei)
                    .HasColumnName("deviceIMEI")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Devicename)
                    .HasColumnName("devicename")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetype)
                    .HasColumnName("devicetype")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("FIRST_NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Iccid)
                    .HasColumnName("ICCID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdentifierType)
                    .HasColumnName("IDENTIFIER_TYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("Unique identifier: ICCID for SIM, EID for ESIM");

                entity.Property(e => e.IdentifierValue)
                    .HasColumnName("IDENTIFIER_VALUE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InventoryId).HasColumnName("INVENTORY_ID");

                entity.Property(e => e.ItemDescription)
                    .HasColumnName("ITEM_DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("ITEM_ID");

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("ITEM_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemPlanId).HasColumnName("itemPlan_ID");

                entity.Property(e => e.LastName)
                    .HasColumnName("LAST_NAME")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PlanCode)
                    .HasColumnName("planCode")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasComment("this is the plan code used in third party APIs - must be unique");

                entity.Property(e => e.PlanDescription)
                    .HasColumnName("planDescription")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlanId).HasColumnName("planId");

                entity.Property(e => e.PlanName)
                    .IsRequired()
                    .HasColumnName("planName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerCode)
                    .IsRequired()
                    .HasColumnName("reseller_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.ResellerName)
                    .HasColumnName("reseller_name")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("STATE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasComment("0 = inactive, 1 = active, 2 = suspended, 3 = canceled | inactive = purchased but sim card hasn't been connected to plan yet | active = plan connected to sim card | suspended = had a plan, temporarily turned off | canceled = hand a plan, permanently turned off ");

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(9)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("role");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CompanyId).HasColumnName("companyID");

                entity.Property(e => e.CountryId).HasColumnName("countryID");

                entity.Property(e => e.CreatedOn).HasColumnName("created_on");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DivisionId).HasColumnName("divisionID");

                entity.Property(e => e.LobId).HasColumnName("lobID");

                entity.Property(e => e.ModifiedOn).HasColumnName("modified_on");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Section>(entity =>
            {
                entity.ToTable("section");

                entity.Property(e => e.Sectionid).HasColumnName("sectionid");

                entity.Property(e => e.Headsize).HasColumnName("headsize");

                entity.Property(e => e.Headstr)
                    .HasColumnName("headstr")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Reportid).HasColumnName("reportid");
            });

            modelBuilder.Entity<ShippingMethod>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("shipping_method");

                entity.HasIndex(e => e.ShippingMethodId)
                    .HasName("SHIPPING_METHOD_ID")
                    .IsUnique();

                entity.Property(e => e.ShippingMethodDesc)
                    .HasColumnName("SHIPPING_METHOD_DESC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingMethodDescFr)
                    .HasColumnName("SHIPPING_METHOD_DESC_FR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingMethodId)
                    .HasColumnName("SHIPPING_METHOD_ID")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ShippingStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("shipping_status");

                entity.HasIndex(e => e.ShippingStatId)
                    .HasName("SHIPPING_STAT_ID")
                    .IsUnique();

                entity.Property(e => e.ShippingStatDesc)
                    .HasColumnName("SHIPPING_STAT_DESC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingStatDescFr)
                    .HasColumnName("SHIPPING_STAT_DESC_FR")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingStatId)
                    .HasColumnName("SHIPPING_STAT_ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Status)
                    .HasColumnName("STATUS")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");
            });

            modelBuilder.Entity<SidebarOption>(entity =>
            {
                entity.ToTable("sidebar_option");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("TITLE")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SidebarPermission>(entity =>
            {
                entity.ToTable("sidebar_permission");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasColumnName("COMPANY_CODE")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnName("ROLE_ID");

                entity.Property(e => e.SidebarOptionId).HasColumnName("SIDEBAR_OPTION_ID");
            });

            modelBuilder.Entity<SimUsage>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PRIMARY");

                entity.ToTable("sim_usage");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.SimId)
                    .HasColumnName("simID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateTime).HasColumnName("updateTime");

                entity.Property(e => e.VolumeCurrentMonth)
                    .HasColumnName("volumeCurrentMonth")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.VolumeLast12Months)
                    .HasColumnName("volumeLast12Months")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.VolumeLast3Months)
                    .HasColumnName("volumeLast3Months")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.VolumeLast6Months)
                    .HasColumnName("volumeLast6Months")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.VolumeLifetime)
                    .HasColumnName("volumeLifetime")
                    .HasColumnType("decimal(20,2)");

                entity.HasOne(d => d.Customer)
                    .WithOne(p => p.SimUsage)
                    .HasForeignKey<SimUsage>(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_id1");
            });

            modelBuilder.Entity<SimUsageClientCustomerView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("sim_usage_client_customer_view");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.TotalCurrentMonthUsage)
                    .HasColumnName("totalCurrentMonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast12MonthUsage)
                    .HasColumnName("totalLast12MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast3MonthUsage)
                    .HasColumnName("totalLast3MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast6MonthUsage)
                    .HasColumnName("totalLast6MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLifetime)
                    .HasColumnName("totalLifetime")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.UpdateTime).HasColumnName("updateTime");
            });

            modelBuilder.Entity<SimUsageClientView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("sim_usage_client_view");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.TotalCurrentMonthUsage)
                    .HasColumnName("totalCurrentMonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast12MonthUsage)
                    .HasColumnName("totalLast12MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast3MonthUsage)
                    .HasColumnName("totalLast3MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast6MonthUsage)
                    .HasColumnName("totalLast6MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLifetime)
                    .HasColumnName("totalLifetime")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.UpdateTime).HasColumnName("updateTime");
            });

            modelBuilder.Entity<SimUsageResellerView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("sim_usage_reseller_view");

                entity.Property(e => e.ResellerId).HasColumnName("RESELLER_ID");

                entity.Property(e => e.TotalCurrentMonthUsage)
                    .HasColumnName("totalCurrentMonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast12MonthUsage)
                    .HasColumnName("totalLast12MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast3MonthUsage)
                    .HasColumnName("totalLast3MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLast6MonthUsage)
                    .HasColumnName("totalLast6MonthUsage")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.TotalLifetime)
                    .HasColumnName("totalLifetime")
                    .HasColumnType("decimal(42,2)");

                entity.Property(e => e.UpdateTime).HasColumnName("updateTime");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("status");

                entity.HasIndex(e => e.StatusId)
                    .HasName("STATUS_ID")
                    .IsUnique();

                entity.Property(e => e.StatusId).HasColumnName("STATUS_ID");

                entity.Property(e => e.Status1)
                    .HasColumnName("STATUS")
                    .HasDefaultValueSql("'1'")
                    .HasComment("1 = ACTIVE, 0 = INACTIVE");

                entity.Property(e => e.StatusDesc)
                    .HasColumnName("STATUS_DESC")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDescFr)
                    .HasColumnName("STATUS_DESC_FR")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tax>(entity =>
            {
                entity.ToTable("tax");

                entity.HasIndex(e => e.TaxId)
                    .HasName("TAX_ID")
                    .IsUnique();

                entity.Property(e => e.TaxId).HasColumnName("TAX_ID");

                entity.Property(e => e.ApplicableTaxes)
                    .HasColumnName("APPLICABLE_TAXES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate)
                    .HasColumnName("CREATEDATE")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.FederalRate)
                    .HasColumnName("FEDERAL_RATE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProvincialRate)
                    .HasColumnName("PROVINCIAL_RATE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SalesTaxes)
                    .HasColumnName("SALES_TAXES")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasColumnName("STATE")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TotalRate)
                    .HasColumnName("TOTAL_RATE")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Template>(entity =>
            {
                entity.ToTable("template");

                entity.Property(e => e.Templateid).HasColumnName("templateid");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Sectionid).HasColumnName("sectionid");

                entity.Property(e => e.Type).HasColumnName("type");
            });

            modelBuilder.Entity<Themeattribute>(entity =>
            {
                entity.ToTable("themeattribute");

                entity.Property(e => e.Themeattributeid).HasColumnName("themeattributeid");

                entity.Property(e => e.Assetclassid)
                    .HasColumnName("assetclassid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Attributeid).HasColumnName("attributeid");

                entity.Property(e => e.Createdby)
                    .HasColumnName("createdby")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'admin'");

                entity.Property(e => e.Createdtime)
                    .HasColumnName("createdtime")
                    .HasDefaultValueSql("'2019-10-05 10:00:37'");

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Display)
                    .HasColumnName("display")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Lower).HasColumnName("lower");

                entity.Property(e => e.Messagetype).HasColumnName("messagetype");

                entity.Property(e => e.Themeid)
                    .HasColumnName("themeid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Themename)
                    .HasColumnName("themename")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Upper).HasColumnName("upper");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasMaxLength(45)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'#FFFFFF'");
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.ToTable("transaction");

                entity.HasIndex(e => e.InvoiceId)
                    .HasName("transaction_invoice_id_idx");

                entity.HasIndex(e => e.OrderId)
                    .HasName("transaction_order_id_idx");

                entity.HasIndex(e => e.StatusId)
                    .HasName("transaction_status_id_idx");

                entity.HasIndex(e => e.TransactionId)
                    .HasName("TRANSACTION_ID")
                    .IsUnique();

                entity.Property(e => e.TransactionId).HasColumnName("TRANSACTION_ID");

                entity.Property(e => e.AccountId).HasColumnName("ACCOUNT_ID");

                entity.Property(e => e.AuthCode)
                    .HasColumnName("AUTH_CODE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.BankTtl)
                    .HasColumnName("BANK_TTL")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CardType)
                    .IsRequired()
                    .HasColumnName("CARD_TYPE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasColumnName("COMMENT")
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.Complete)
                    .HasColumnName("COMPLETE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.HostId)
                    .HasColumnName("HOST_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceId).HasColumnName("INVOICE_ID");

                entity.Property(e => e.IsVisaDebit)
                    .HasColumnName("IS_VISA_DEBIT")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Iso)
                    .HasColumnName("ISO")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.IssuerId)
                    .HasColumnName("ISSUER_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Msg)
                    .HasColumnName("MSG")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId).HasColumnName("ORDER_ID");

                entity.Property(e => e.ReceiptId)
                    .HasColumnName("RECEIPT_ID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RefNum)
                    .HasColumnName("REF_NUM")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RespCode)
                    .HasColumnName("RESP_CODE")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("STATUS_ID");

                entity.Property(e => e.Ticket)
                    .HasColumnName("TICKET")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeOut)
                    .HasColumnName("TIME_OUT")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionAmt)
                    .HasColumnName("TRANSACTION_AMT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionDt)
                    .HasColumnName("TRANSACTION_DT")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionNum)
                    .HasColumnName("TRANSACTION_NUM")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionTime).HasColumnName("TRANSACTION_TIME");

                entity.Property(e => e.TransactionType)
                    .HasColumnName("TRANSACTION_TYPE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDt).HasColumnName("UPDATE_DT");

                entity.HasOne(d => d.Invoice)
                    .WithMany(p => p.Transaction)
                    .HasForeignKey(d => d.InvoiceId)
                    .HasConstraintName("transaction_invoice_id");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Transaction)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("transaction_order_id");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Transaction)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("transaction_status_id");
            });

            modelBuilder.Entity<Typegroup>(entity =>
            {
                entity.HasKey(e => new { e.Devicetypeid, e.Groupid })
                    .HasName("PRIMARY");

                entity.ToTable("typegroup");

                entity.HasIndex(e => e.Groupid)
                    .HasName("groupid");

                entity.Property(e => e.Devicetypeid)
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Groupid)
                    .HasColumnName("groupid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.Devicetype)
                    .WithMany(p => p.Typegroup)
                    .HasForeignKey(d => d.Devicetypeid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("typegroup_ibfk_1");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Typegroup)
                    .HasForeignKey(d => d.Groupid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("typegroup_ibfk_2");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => e.Id)
                    .HasName("id_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("unique_username")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Businessunit)
                    .HasColumnName("businessunit")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Companycode)
                    .HasColumnName("companycode")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Createtime).HasColumnName("createtime");

                entity.Property(e => e.Creator)
                    .HasColumnName("creator")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Customerid)
                    .HasColumnName("customerid")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Department)
                    .HasColumnName("department")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Expireddate).HasColumnName("expireddate");

                entity.Property(e => e.Externalid)
                    .HasColumnName("externalid")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Network)
                    .HasColumnName("network")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Phonenum)
                    .HasColumnName("phonenum")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Resellercode)
                    .HasColumnName("resellercode")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Resetpasswordtoken)
                    .HasColumnName("resetpasswordtoken")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Usertype)
                    .HasColumnName("usertype")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAsset>(entity =>
            {
                entity.ToTable("user_asset");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Assetid).HasColumnName("assetid");

                entity.Property(e => e.CreateTime).HasColumnName("create_time");

                entity.Property(e => e.Creator)
                    .HasColumnName("creator")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("user_id")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAssetclass>(entity =>
            {
                entity.ToTable("user_assetclass");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Classid).HasColumnName("classid");

                entity.Property(e => e.CreateTime).HasColumnName("create_time");

                entity.Property(e => e.Creator)
                    .HasColumnName("creator")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("user_id")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserDevicetypes>(entity =>
            {
                entity.ToTable("user_devicetypes");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateTime).HasColumnName("create_time");

                entity.Property(e => e.Creator)
                    .HasColumnName("creator")
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Devicetypeid)
                    .IsRequired()
                    .HasColumnName("devicetypeid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("user_id")
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PRIMARY");

                entity.ToTable("user_role");

                entity.HasIndex(e => e.RoleId)
                    .HasName("FKh8ciramu9cc9q3qcqiv4ue8a6");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("role_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
