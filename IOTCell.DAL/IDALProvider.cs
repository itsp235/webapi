﻿using System.Collections.Generic;
using System.Threading.Tasks;

using IOTCell.DAL.Models;
using IOTCell.Framework.Diagnostics;

namespace IOTCell.DAL
{
    public interface IDALProvider
    {
        public Task BeginTransaction();
        public Task CommitTransaction();
        public Task RollBackTransaction();
        public Task InsertActivityEntry(ActivityEntry activityEntry);
        public Task InsertAPIActivityLog(APIActivity apiActivity);
        public Task InsertErrorLog(APIError apiError);
        public Task<IEnumerable<Company>> GetCompanies();
        public Task<Company> GetCompanyByID(int companyID);
        public Task InsertCompany(Company company);
        public Task UpdateCompany(Company company);
        public Task DeleteCompany(int companyId);
        public Task<Reseller> GetResellerByResellerCode(string resellerCode);
        public Task<ResellerExtension> GetResellerExtension(int resellerID);
        public Task<ResellerExtension> GetResellerExtensionByResellerCode(string resellerCode);
        public Task<Item> GetPlanByPlanName(string planName, int resellerID, int carrierID);
        public Task<Inventory> GetSIMFromInventory(string simIdentifier, string simIdentifierValue);
        public Task<Item> GetSIMType(long itemID);
        public Task<Customer> AddNewCustomer(Customer customer);
        public Task<Order> AddNewOrder(Order order);
        public Task<Account> AddNewAccount(Account account);
        public Task<Address> AddNewAddress(Address address);
        public Task<OrderItem> AddNewOrderItem(OrderItem orderItem);
        public long? GetLastCustomerForReseller(int resellerID);
        public Task<Inventory> UpdateInventory(Inventory inventorySIM);
        public Task<Order> UpdateOrder(Order order);
        public Task<OrderItem> UpdateOrderItem(OrderItem orderItem);
        public Task<Customer> GetCustomerByAccountNumber(string accountNumber);
        public Task<Account> GetAccountByCustomerID(long customerID);
        public Task<Account> GetAccountByResellerID(long customerID);
        public Task<OrderItem> GetOrderItemByInventoryID(long inventoryID);
        public Task<OrderItem> GetOrderItemPlanByOrderID(long orderID);
        public Task<Order> GetOrderByOrderID(long orderID);
//        public Task<Carrier> GetCarrierFromInventory(string simIdentifier, string simIdentifierValue);
        public Task<Carrier> GetCarrierFromReseller(string resellerCode);
        public Task<Carrier> GetCarrierFromInventory(int carrieriID);
    }
}