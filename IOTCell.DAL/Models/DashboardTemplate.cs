﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class DashboardTemplate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int? Status { get; set; }
        public string Username { get; set; }
        public string CompanyCode { get; set; }
        public string Createdby { get; set; }
        public DateTime? Createdtime { get; set; }
    }
}
