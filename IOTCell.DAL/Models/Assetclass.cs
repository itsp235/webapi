﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Assetclass
    {
        public Assetclass()
        {
            Asset = new HashSet<Asset>();
            Assetdevicetype = new HashSet<Assetdevicetype>();
            AttributeMeta = new HashSet<AttributeMeta>();
        }

        public int Assetclassid { get; set; }
        public string Classname { get; set; }
        public string Description { get; set; }
        public int? Iconid { get; set; }
        public string CompanyCode { get; set; }
        public string Createdby { get; set; }
        public int? Parentclass { get; set; }
        public DateTime? Createdtime { get; set; }
        public string Modifiedby { get; set; }
        public DateTime? Modifiedtime { get; set; }

        public virtual Icon Icon { get; set; }
        public virtual ICollection<Asset> Asset { get; set; }
        public virtual ICollection<Assetdevicetype> Assetdevicetype { get; set; }
        public virtual ICollection<AttributeMeta> AttributeMeta { get; set; }
    }
}
