﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Actionrule
    {
        public int Actionruleid { get; set; }
        public int? Actionid { get; set; }
        public int? Templateid { get; set; }
        public int? Assetclassid { get; set; }
        public string Assetclassname { get; set; }
        public int? Assetid { get; set; }
        public string Assetname { get; set; }
        public int? Attributeid { get; set; }
        public string Attributename { get; set; }
        public int? Geofenceid { get; set; }
        public string Geofencename { get; set; }
        public int? Operator { get; set; }
        public int? Value { get; set; }
        public int? Relation { get; set; }
        public int? Groupid { get; set; }
        public int? Groupindex { get; set; }
        public int? Grouprelation { get; set; }
    }
}
