﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Resellerinventoryitemview
    {
        public int ResellerId { get; set; }
        public string CompanyCode { get; set; }
        public string ResellerCode { get; set; }
        public string ResellerName { get; set; }
        public string Accountnumber { get; set; }
        public long? CustomerId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessTaxId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CustomerName { get; set; }
        public long InventoryId { get; set; }
        public long ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string IdentifierType { get; set; }
        public string IdentifierValue { get; set; }
        public string Iccid { get; set; }
        public int? Status { get; set; }
        public int? ItemPlanId { get; set; }
        public DateTime? ActiveDate { get; set; }
        public string Devicename { get; set; }
        public string Devicetype { get; set; }
        public string DeviceImei { get; set; }
        public long PlanId { get; set; }
        public string PlanCode { get; set; }
        public string PlanName { get; set; }
        public string PlanDescription { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
    }
}
