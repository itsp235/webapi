﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class InventoryMaster
    {
        public string Upc { get; set; }
        public string Company { get; set; }
        public string Mfg { get; set; }
        public string Group { get; set; }
        public string OurSku { get; set; }
        public string MfgSku { get; set; }
        public decimal Quantity { get; set; }
        public decimal? SoldQuantity { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionFr { get; set; }
        public string OtherLanguageDec { get; set; }
        public string Color { get; set; }
        public decimal? BatteryLife { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Cost { get; set; }
        public string Type { get; set; }
        public string Serial { get; set; }
        public string NetworkSupported { get; set; }
        public decimal? LowFrequency { get; set; }
        public decimal? HighFrequency { get; set; }
        public string Image { get; set; }
        public string Specification { get; set; }
        public DateTime? CreateDatetime { get; set; }
        public DateTime? UpdateDatetime { get; set; }
        public string Refurbrished { get; set; }
        public string UpdatedBy { get; set; }
        public string Supported { get; set; }
    }
}
