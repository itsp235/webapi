﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Item
    {
        public Item()
        {
            Inventory = new HashSet<Inventory>();
            OrderItem = new HashSet<OrderItem>();
        }

        public long ItemId { get; set; }
        public int CompanyId { get; set; }
        public int? ResellerId { get; set; }
        public long? CustomerId { get; set; }
        public string Upc { get; set; }
        public int ItemTypeId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemDescriptionFr { get; set; }
        public decimal UnitPrice { get; set; }
        public string Sku { get; set; }
        public string Unit { get; set; }
        public string LoyaltyPoint { get; set; }
        public string SerialNum { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFileLoc { get; set; }
        public string ImageFileLocBig { get; set; }
        public int? Status { get; set; }
        public string CompanyCode { get; set; }
        public int? SequenceTag { get; set; }
        public int? ItemStatus { get; set; }
        public int? PromoteId { get; set; }
        public int? ItemPayFrequencyId { get; set; }
        public string ItemPayType { get; set; }
        public int AutoRenewFlag { get; set; }
        public int Billcycle { get; set; }
        public int? PlanDataVolume { get; set; }
        public int? CarrierId { get; set; }

        public virtual ItemPayFrequency ItemPayFrequency { get; set; }
        public virtual ItemType ItemType { get; set; }
        public virtual Promote Promote { get; set; }
        public virtual Reseller Reseller { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
        public virtual ICollection<OrderItem> OrderItem { get; set; }
    }
}
