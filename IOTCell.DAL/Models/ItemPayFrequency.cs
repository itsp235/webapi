﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ItemPayFrequency
    {
        public ItemPayFrequency()
        {
            Item = new HashSet<Item>();
            Order = new HashSet<Order>();
        }

        public int ItemPayFrequencyId { get; set; }
        public string ItemPayFrequencyName { get; set; }
        public int? DefaultValue { get; set; }

        public virtual ICollection<Item> Item { get; set; }
        public virtual ICollection<Order> Order { get; set; }
    }
}
