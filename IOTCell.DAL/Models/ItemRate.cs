﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ItemRate
    {
        public int Id { get; set; }
        public int? ItemId { get; set; }
        public int? StartPoint { get; set; }
        public int? EndPoint { get; set; }
        public decimal? Rate { get; set; }
        public int? ItemRateStatus { get; set; }
    }
}
