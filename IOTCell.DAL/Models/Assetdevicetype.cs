﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Assetdevicetype
    {
        public int Assetdevicetypeid { get; set; }
        public int? Assetclassid { get; set; }
        public string Assetclassname { get; set; }
        public string Devicetypeid { get; set; }
        public string Devicetypename { get; set; }

        public virtual Assetclass Assetclass { get; set; }
        public virtual Devicetype Devicetype { get; set; }
    }
}
