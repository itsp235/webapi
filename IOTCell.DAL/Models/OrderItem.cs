﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class OrderItem
    {
        public long OrderitemId { get; set; }
        public long OrderId { get; set; }
        public long ItemId { get; set; }
        public long ItemQty { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal CostAmount { get; set; }
        public decimal CostTax { get; set; }
        public int? PromoteId { get; set; }
        public decimal PromoAmount { get; set; }
        public int Status { get; set; }
        public DateTime? ShippingDate { get; set; }
        public string Courier { get; set; }
        public double? ShippingCharge { get; set; }
        public int? ShippingStatusId { get; set; }
        public string TrackingNum { get; set; }
        public string WaybillNum { get; set; }
        public string PayFreq { get; set; }
        public long? InventoryId { get; set; }
        public int? Billcycle { get; set; }
        public DateTime? BillingStartDt { get; set; }
        public DateTime? BillingEndDt { get; set; }
        public DateTime? NextBillingDt { get; set; }
        public decimal? DataUsageWhenPlanChanged { get; set; }
        public decimal? SmsusageWhenPlanChanged { get; set; }
        public decimal? VoiceUsageWhenPlanChanged { get; set; }
        public string Modification { get; set; }
        public int? UsageHistoryId { get; set; }

        public virtual Item Item { get; set; }
        public virtual Order Order { get; set; }
        public virtual Promote Promote { get; set; }
    }
}
