﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Invoice
    {
        public Invoice()
        {
            Transaction = new HashSet<Transaction>();
        }

        public long InvoiceId { get; set; }
        public long OrderId { get; set; }
        public int StatusId { get; set; }
        public decimal InvTtl { get; set; }
        public decimal? TelecomTax { get; set; }
        public decimal? SalesTax { get; set; }
        public decimal Tax { get; set; }
        public decimal? ProvTax { get; set; }
        public decimal? FedTax { get; set; }
        public decimal DiscountAmt { get; set; }
        public decimal SubAmt { get; set; }
        public decimal PreAccBal { get; set; }
        public decimal InvAmt { get; set; }
        public decimal AmountDue { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal? AmountAdjust { get; set; }
        public DateTime? CreateDt { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDt { get; set; }
        public string Comment { get; set; }
        public string InvDescrpt { get; set; }
        public string UsageDetail { get; set; }
        public DateTime? UpdateDt { get; set; }
        public string OtItemsFlag { get; set; }

        public virtual Order Order { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<Transaction> Transaction { get; set; }
    }
}
