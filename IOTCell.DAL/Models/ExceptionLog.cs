﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ExceptionLog
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string Detail { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public string Comment { get; set; }
        public string FunctionName { get; set; }
    }
}
