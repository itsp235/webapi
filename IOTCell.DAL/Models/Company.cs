﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Company
    {
        public Company()
        {
            Address = new HashSet<Address>();
            Inventory = new HashSet<Inventory>();
            Reseller = new HashSet<Reseller>();
        }

        public int CompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNameFr { get; set; }
        public string Status { get; set; }
        public string Language { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Createuser { get; set; }
        public DateTime? Createdate { get; set; }
        public int IndustryId { get; set; }
        public string MainBusinessArea { get; set; }
        public string Addressline1 { get; set; }
        public string Addressline2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }

        public virtual Industry Industry { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
        public virtual ICollection<Reseller> Reseller { get; set; }
    }
}
