﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ShippingMethod
    {
        public int ShippingMethodId { get; set; }
        public string ShippingMethodDesc { get; set; }
        public string ShippingMethodDescFr { get; set; }
    }
}
