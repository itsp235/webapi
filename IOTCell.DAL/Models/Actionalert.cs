﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Actionalert
    {
        public int Actionalertid { get; set; }
        public int? Actionid { get; set; }
        public string Alerttype { get; set; }
        public string Destination { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Weekday { get; set; }
        public TimeSpan? Starttime { get; set; }
        public TimeSpan? Endtime { get; set; }
        public int? Contactid { get; set; }
        public int? NotificationTemplateid { get; set; }
    }
}
