﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Reportlog
    {
        public int Id { get; set; }
        public string Reportid { get; set; }
        public string Result { get; set; }
        public string Detail { get; set; }
        public DateTime? Runtime { get; set; }
    }
}
