﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Promote
    {
        public Promote()
        {
            Item = new HashSet<Item>();
            OrderItem = new HashSet<OrderItem>();
        }

        public int PromoteId { get; set; }
        public long CompanyId { get; set; }
        public long ResellerId { get; set; }
        public long? CustomerId { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string PromoteType { get; set; }
        public decimal PromoteValue { get; set; }
        public int PromoteStatus { get; set; }
        public DateTimeOffset? ValidStartDate { get; set; }
        public DateTime? ValidEndDate { get; set; }
        public string PromoteName { get; set; }

        public virtual ICollection<Item> Item { get; set; }
        public virtual ICollection<OrderItem> OrderItem { get; set; }
    }
}
