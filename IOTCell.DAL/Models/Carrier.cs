﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Carrier
    {
        public Carrier()
        {
            Reseller = new HashSet<Reseller>();
        }

        public int CarrierId { get; set; }
        public string CarrierCode { get; set; }
        public string CarrierName { get; set; }
        public string CarrierAssembly { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CompanyCode { get; set; }
        public byte? UsesGetTotalUsageForResellerMethod { get; set; }
        public byte? UsesGetCalculatedUsageMethod { get; set; }
        public byte? UsesTerminateServiceMethod { get; set; }

        public virtual ICollection<Reseller> Reseller { get; set; }
    }
}
