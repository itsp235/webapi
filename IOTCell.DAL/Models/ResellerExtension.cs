﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ResellerExtension
    {
        public int ResellerExtensionId { get; set; }
        public int ResellerId { get; set; }
        public string ResellerUserName { get; set; }
        public string ResellerPassword { get; set; }
        public string IbUserName { get; set; }
        public string IbPassword { get; set; }
        public string IbMvno { get; set; }

        public virtual Reseller Reseller { get; set; }
    }
}
