﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ApiActivityLog
    {
        public int ApiActivityLogId { get; set; }
        public string CorrelationId { get; set; }
        public DateTime? RequestTime { get; set; }
        public long? ResponseMilliseconds { get; set; }
        public int? StatusCode { get; set; }
        public string Url { get; set; }
        public string Headers { get; set; }
        public string Method { get; set; }
        public string Path { get; set; }
        public string QueryString { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
    }
}
