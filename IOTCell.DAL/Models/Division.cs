﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Division
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int? LobId { get; set; }
        public int? CountryId { get; set; }

        public virtual Country Country { get; set; }
        public virtual Lob Lob { get; set; }
    }
}
