﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Icons
    {
        public int Iconid { get; set; }
        public string Iconname { get; set; }
        public int? Iconscale { get; set; }
        public string Iconsvg { get; set; }
        public string Color { get; set; }
        public string Data { get; set; }
        public string Type { get; set; }
    }
}
