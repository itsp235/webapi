﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class BillingView
    {
        public long OrderitemId { get; set; }
        public long OrderId { get; set; }
        public int? PromoteId { get; set; }
        public long ItemId { get; set; }
        public int CompanyId { get; set; }
        public int? ResellerId { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? ActiveDate { get; set; }
        public long? AccountId { get; set; }
        public long? InventoryId { get; set; }
        public decimal ItemPrice { get; set; }
        public string PayFreq { get; set; }
        public int? Billcycle { get; set; }
        public DateTime? BillingStartDt { get; set; }
        public DateTime? BillingEndDt { get; set; }
        public DateTime? NextBillingDt { get; set; }
        public decimal? DataUsageWhenPlanChanged { get; set; }
        public decimal? SmsusageWhenPlanChanged { get; set; }
        public decimal? VoiceUsageWhenPlanChanged { get; set; }
        public int? UsageHistoryId { get; set; }
        public string BillingType { get; set; }
    }
}
