﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Refund
    {
        public long Id { get; set; }
        public long OrdrId { get; set; }
        public long? TrxId { get; set; }
        public decimal RefundAmt { get; set; }
        public int? StatId { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDt { get; set; }
        public DateTime? UpdateDt { get; set; }
        public string Items { get; set; }
    }
}
