﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Tax
    {
        public Tax()
        {
            Order = new HashSet<Order>();
        }

        public int TaxId { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string ApplicableTaxes { get; set; }
        public string ProvincialRate { get; set; }
        public string FederalRate { get; set; }
        public string TotalRate { get; set; }
        public string SalesTaxes { get; set; }
        public DateTime? Createdate { get; set; }

        public virtual ICollection<Order> Order { get; set; }
    }
}
