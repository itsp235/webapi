﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class OrderLog
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public string Operation { get; set; }
        public DateTime? LogDate { get; set; }
        public string OrderName { get; set; }
        public string CompanyCode { get; set; }
        public string Description { get; set; }
    }
}
