﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Role
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Value { get; set; }
        public long? CreatedOn { get; set; }
        public long? ModifiedOn { get; set; }
        public int? CountryId { get; set; }
        public int? LobId { get; set; }
        public int? DivisionId { get; set; }
        public int? CompanyId { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
