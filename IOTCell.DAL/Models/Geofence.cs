﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Geofence
    {
        public int Fenceid { get; set; }
        public string Fencename { get; set; }
        public int? Shape { get; set; }
        public string Geodata { get; set; }
        public string Companycode { get; set; }
        public string Createdby { get; set; }
    }
}
