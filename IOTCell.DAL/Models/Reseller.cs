﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Reseller
    {
        public Reseller()
        {
            Address = new HashSet<Address>();
            Customer = new HashSet<Customer>();
            Inventory = new HashSet<Inventory>();
            Item = new HashSet<Item>();
            ResellerExtension = new HashSet<ResellerExtension>();
        }

        public int ResellerId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyCode { get; set; }
        public string ResellerCode { get; set; }
        public string ResellerAccountPrefix { get; set; }
        public string AccountNumber { get; set; }
        public string ResellerName { get; set; }
        public string Mvno { get; set; }
        public string Status { get; set; }
        public string Language { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string Phone { get; set; }
        public string PhoneExtension { get; set; }
        public string Email { get; set; }
        public string Resellerkey { get; set; }
        public DateTime? Resellerkeycreated { get; set; }
        public string Assembly { get; set; }
        public string Createuser { get; set; }
        public DateTime? Createdate { get; set; }
        public int? CarrierId { get; set; }

        public virtual Carrier Carrier { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
        public virtual ICollection<Item> Item { get; set; }
        public virtual ICollection<ResellerExtension> ResellerExtension { get; set; }
    }
}
