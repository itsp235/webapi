﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Attribute
    {
        public int Attributeid { get; set; }
        public string Devicetype { get; set; }
        public string Displayname { get; set; }
        public int? Messagetype { get; set; }
        public string Attributename { get; set; }
        public string Attributetype { get; set; }
        public byte? Editable { get; set; }
        public byte? Useradded { get; set; }
        public string Createdby { get; set; }
        public DateTime? Createdtime { get; set; }
    }
}
