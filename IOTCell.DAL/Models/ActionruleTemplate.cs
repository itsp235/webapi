﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ActionruleTemplate
    {
        public int Templateid { get; set; }
        public int? Typeid { get; set; }
        public string Typename { get; set; }
        public string Description { get; set; }
        public string Devicetypeid { get; set; }
        public string Devicetypename { get; set; }
        public int? Attributeid { get; set; }
        public int? Geofenceid { get; set; }
        public int? Operator { get; set; }
        public int? Value { get; set; }
    }
}
