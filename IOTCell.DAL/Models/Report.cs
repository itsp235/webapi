﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Report
    {
        public int Reportid { get; set; }
        public string Reportname { get; set; }
        public string Timezone { get; set; }
        public int? Datetime { get; set; }
        public string Format { get; set; }
        public string Sendcron { get; set; }
        public string Subject { get; set; }
        public string Msgbody { get; set; }
        public string Receiver { get; set; }
        public string Header { get; set; }
        public string Status { get; set; }
        public string Createdby { get; set; }
        public string CompanyCode { get; set; }
    }
}
