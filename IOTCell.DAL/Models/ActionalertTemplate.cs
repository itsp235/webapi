﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ActionalertTemplate
    {
        public int Id { get; set; }
        public int? Actionid { get; set; }
        public string Name { get; set; }
        public string Contactid { get; set; }
        public string Destination { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Weekday { get; set; }
        public TimeSpan? Starttime { get; set; }
        public TimeSpan? Endtime { get; set; }
        public string Owner { get; set; }
        public string Companycode { get; set; }
        public string Companyname { get; set; }
        public DateTime? Createdtime { get; set; }
        public string Settinggroup { get; set; }
    }
}
