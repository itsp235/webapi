﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Section
    {
        public int Sectionid { get; set; }
        public string Headstr { get; set; }
        public int? Headsize { get; set; }
        public int? Reportid { get; set; }
    }
}
