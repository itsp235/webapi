﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class DeviceMaster
    {
        public string DeviceId { get; set; }
        public string Devicename { get; set; }
        public string Upc { get; set; }
        public string Mfg { get; set; }
        public string MfgSku { get; set; }
        public string MfgSerialNum { get; set; }
        public string MacAddress { get; set; }
        public string DeviceStatus { get; set; }
        public string SpecialStock { get; set; }
        public string CustomerId { get; set; }
        public string Company { get; set; }
        public string NetworkSupported { get; set; }
        public string Refurbished { get; set; }
        public string OldDeviceId { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdatedBy { get; set; }
        public string Devicetype { get; set; }
    }
}
