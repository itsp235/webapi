﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class CustomerExtension
    {
        public long CustomerExtensionId { get; set; }
        public long CustomerId { get; set; }
        public string CustomerUserName { get; set; }
        public string CustomerPassword { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
