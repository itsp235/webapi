﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Country
    {
        public Country()
        {
            Customer = new HashSet<Customer>();
            Division = new HashSet<Division>();
            Lob = new HashSet<Lob>();
        }

        public int CountryId { get; set; }
        public string Country1 { get; set; }

        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<Division> Division { get; set; }
        public virtual ICollection<Lob> Lob { get; set; }
    }
}
