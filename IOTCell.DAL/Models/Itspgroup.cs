﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Itspgroup
    {
        public Itspgroup()
        {
            Typegroup = new HashSet<Typegroup>();
        }

        public string Groupid { get; set; }
        public string Groupname { get; set; }
        public string Groupcountry { get; set; }
        public int? Grouptype { get; set; }
        public string Itsppath { get; set; }
        public byte? Billable { get; set; }
        public string Descript { get; set; }
        public string Clientemail { get; set; }
        public string Technicalemail { get; set; }
        public string Grouptimezone { get; set; }
        public string Nameci { get; set; }

        public virtual ICollection<Typegroup> Typegroup { get; set; }
    }
}
