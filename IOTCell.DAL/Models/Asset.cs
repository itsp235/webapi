﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Asset
    {
        public int Assetid { get; set; }
        public string Assetname { get; set; }
        public int? Assetclass { get; set; }
        public string Description { get; set; }
        public string CompanyCode { get; set; }
        public string Createdby { get; set; }
        public int? Parentasset { get; set; }
        public DateTime? Createdtime { get; set; }
        public string Modifiedby { get; set; }
        public DateTime? Modifiedtime { get; set; }

        public virtual Assetclass AssetclassNavigation { get; set; }
    }
}
