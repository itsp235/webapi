﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Config
    {
        public int Id { get; set; }
        public string Appname { get; set; }
        public string Modulename { get; set; }
        public string Itemname { get; set; }
        public string Displayname { get; set; }
        public string Itemvalue { get; set; }
        public string RangeLow { get; set; }
        public string RangeHigh { get; set; }
        public string Unit { get; set; }
        public string Companycode { get; set; }
        public string Settinggroup { get; set; }
    }
}
