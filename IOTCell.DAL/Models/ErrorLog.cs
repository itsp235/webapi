﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ErrorLog
    {
        public int ErrorLogId { get; set; }
        public string CorrelationId { get; set; }
        public DateTime? ErrorDateTime { get; set; }
        public string MethodName { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public int? LineNumber { get; set; }
        public int? ColumnNumber { get; set; }
        public string FileName { get; set; }
    }
}
