﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class AttributeMeta
    {
        public int Attributemetaid { get; set; }
        public int? Assetclass { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Active { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? LastmodifiedTime { get; set; }
        public string CreateBy { get; set; }
        public string LastmodifiedBy { get; set; }
        public string Source { get; set; }

        public virtual Assetclass AssetclassNavigation { get; set; }
    }
}
