﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Message
    {
        public int Msgid { get; set; }
        public string Deviceid { get; set; }
        public DateTime? Time { get; set; }
        public string Data { get; set; }
        public int? Rollovercounter { get; set; }
        public int? Seqnumber { get; set; }
        public int? Nbframes { get; set; }
        public string Operator { get; set; }
        public string Country { get; set; }
        public double? Computedlocationlat { get; set; }
        public double? Computedlocationlng { get; set; }
        public int? Lqi { get; set; }
    }
}
