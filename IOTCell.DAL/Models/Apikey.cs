﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Apikey
    {
        public int Id { get; set; }
        public string Endpoint { get; set; }
        public string Checktype { get; set; }
        public string Checkvalue { get; set; }
        public string Apikey1 { get; set; }
        public string Createdtime { get; set; }
        public string Createdby { get; set; }
        public string Expiretime { get; set; }
        public int? Active { get; set; }
    }
}
