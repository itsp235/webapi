﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Status
    {
        public Status()
        {
            Invoice = new HashSet<Invoice>();
            Order = new HashSet<Order>();
            Transaction = new HashSet<Transaction>();
        }

        public int StatusId { get; set; }
        public string StatusDesc { get; set; }
        public string StatusDescFr { get; set; }
        public int? Status1 { get; set; }

        public virtual ICollection<Invoice> Invoice { get; set; }
        public virtual ICollection<Order> Order { get; set; }
        public virtual ICollection<Transaction> Transaction { get; set; }
    }
}
