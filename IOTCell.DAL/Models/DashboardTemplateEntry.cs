﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class DashboardTemplateEntry
    {
        public int Id { get; set; }
        public int? Templateid { get; set; }
        public string Templatename { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int? Display { get; set; }
        public int? Locked { get; set; }
        public int? Width { get; set; }
        public string Position { get; set; }
        public string Parameter { get; set; }
        public string Username { get; set; }
        public string CompanyCode { get; set; }
        public string Createdby { get; set; }
        public DateTime? Createdtime { get; set; }
    }
}
