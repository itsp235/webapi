﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Address
    {
        public long AddressId { get; set; }
        public int? CompanyId { get; set; }
        public int? ResellerId { get; set; }
        public long? CustomerId { get; set; }
        public int? UserId { get; set; }
        public string Aptno { get; set; }
        public string Streetno { get; set; }
        public string Streetname { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public int? IsStillUsed { get; set; }
        public DateTime? Createdate { get; set; }

        public virtual Company Company { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Reseller Reseller { get; set; }
    }
}
