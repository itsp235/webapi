﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Decodedmessage
    {
        public int Decodedmsgid { get; set; }
        public string Deviceid { get; set; }
        public string Devicetype { get; set; }
        public string Msgid { get; set; }
        public int? Messagetype { get; set; }
        public DateTime? Timestamp { get; set; }
        public int? Attributeid { get; set; }
        public string Dataattribute { get; set; }
        public string Datavalue { get; set; }
    }
}
