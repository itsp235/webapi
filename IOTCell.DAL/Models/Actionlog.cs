﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Actionlog
    {
        public int Actionlogid { get; set; }
        public int? Actionid { get; set; }
        public string Alerttype { get; set; }
        public string Severity { get; set; }
        public DateTime? Alerttime { get; set; }
        public DateTime? Acktime { get; set; }
        public DateTime? Cleartime { get; set; }
        public int? Assetclassid { get; set; }
        public string Assetclassname { get; set; }
        public int? Assetid { get; set; }
        public string Assetname { get; set; }
        public string Attributetype { get; set; }
        public string Alertmsg { get; set; }
        public byte? Sendstatus { get; set; }
    }
}
