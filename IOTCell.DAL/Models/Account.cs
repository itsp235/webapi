﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Account
    {
        public Account()
        {
            Order = new HashSet<Order>();
        }

        public long AccountId { get; set; }
        public long? CustomerId { get; set; }
        public long? ResellerId { get; set; }
        public string AccountType { get; set; }
        public string CompanyCode { get; set; }
        public string Password { get; set; }
        public decimal? CurrentBalance { get; set; }
        public decimal? OverdueBalance { get; set; }
        public decimal? CurrentBalanceTax { get; set; }
        public decimal? OverdueBalanceTax { get; set; }
        public string PaymentMethod { get; set; }
        public DateTime? Createdate { get; set; }
        public DateTime? Updatedate { get; set; }
        public int? Status { get; set; }
        public string IsNotification { get; set; }
        public string TokenCode { get; set; }
        public string CardMask { get; set; }
        public string Expdate { get; set; }
        public string AccountName { get; set; }
        public string IssuerId { get; set; }
        public string DefaultPayment { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<Order> Order { get; set; }
    }
}
