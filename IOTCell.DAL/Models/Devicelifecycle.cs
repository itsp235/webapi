﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Devicelifecycle
    {
        public int Id { get; set; }
        public string Deviceid { get; set; }
        public string Devicename { get; set; }
        public string Optype { get; set; }
        public string Optime { get; set; }
        public string Createby { get; set; }
        public string Createtime { get; set; }
        public string Updateby { get; set; }
        public string Updatetime { get; set; }
    }
}
