﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Inventory
    {
        public long InventoryId { get; set; }
        public int CompanyId { get; set; }
        public int? ResellerId { get; set; }
        public long? CustomerId { get; set; }
        public long ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string IdentifierType { get; set; }
        public string IdentifierValue { get; set; }
        public string Iccid { get; set; }
        public string SerialNum { get; set; }
        public string ImageFileName { get; set; }
        public string ImageFileLoc { get; set; }
        public string ImageFileLocBig { get; set; }
        public int? Status { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? InactiveDate { get; set; }
        public int? ItemPlanId { get; set; }
        public string Devicename { get; set; }
        public string Devicetype { get; set; }
        public string DeviceImei { get; set; }
        public int CarrierId { get; set; }

        public virtual Company Company { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Item Item { get; set; }
        public virtual Reseller Reseller { get; set; }
    }
}
