﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class User
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Usertype { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Businessunit { get; set; }
        public string Phonenum { get; set; }
        public string Status { get; set; }
        public string Creator { get; set; }
        public DateTime? Createtime { get; set; }
        public DateTime? Expireddate { get; set; }
        public string Companycode { get; set; }
        public string Network { get; set; }
        public string Resellercode { get; set; }
        public long? Customerid { get; set; }
        public string Externalid { get; set; }
        public string Resetpasswordtoken { get; set; }
    }
}
