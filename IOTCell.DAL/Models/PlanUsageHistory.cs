﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class PlanUsageHistory
    {
        public int Id { get; set; }
        public string CompanyCode { get; set; }
        public string ResellerCode { get; set; }
        public int? CustomerId { get; set; }
        public string SimIdentifierType { get; set; }
        public string SimIdentifierValue { get; set; }
        public decimal SimUsage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
