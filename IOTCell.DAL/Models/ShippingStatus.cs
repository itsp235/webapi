﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ShippingStatus
    {
        public int ShippingStatId { get; set; }
        public string ShippingStatDesc { get; set; }
        public string ShippingStatDescFr { get; set; }
        public int? Status { get; set; }
    }
}
