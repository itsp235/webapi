﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Devicetype
    {
        public Devicetype()
        {
            Assetdevicetype = new HashSet<Assetdevicetype>();
            Typegroup = new HashSet<Typegroup>();
        }

        public string Devicetypeid { get; set; }
        public string Devicetypename { get; set; }
        public string Devicetypenamealias { get; set; }
        public string Description { get; set; }
        public byte? Automaticrenewal { get; set; }
        public int? Keepalive { get; set; }
        public int? Payloadtype { get; set; }
        public int? Downlinkmode { get; set; }
        public string Downlinkdatastring { get; set; }
        public string Itspgroup { get; set; }
        public DateTime? Creationtime { get; set; }
        public string Createdby { get; set; }
        public DateTime? Lasteditiontime { get; set; }
        public string Lasteditedby { get; set; }
        public int? Geofence { get; set; }

        public virtual ICollection<Assetdevicetype> Assetdevicetype { get; set; }
        public virtual ICollection<Typegroup> Typegroup { get; set; }
    }
}
