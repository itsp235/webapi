﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ActivityReport
    {
        public int ActivityId { get; set; }
        public int CompanyId { get; set; }
        public int? ResellerId { get; set; }
        public int InventoryId { get; set; }
        public string IdentifierType { get; set; }
        public string IdentifierValue { get; set; }
        public string Activity { get; set; }
        public string Status { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
