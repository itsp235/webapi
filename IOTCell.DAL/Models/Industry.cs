﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Industry
    {
        public Industry()
        {
            Company = new HashSet<Company>();
        }

        public int IndustryId { get; set; }
        public string IndustryName { get; set; }

        public virtual ICollection<Company> Company { get; set; }
    }
}
