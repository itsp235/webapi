﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public byte? Emailenable { get; set; }
        public byte? Phoneenable { get; set; }
        public string Owner { get; set; }
        public string Companycode { get; set; }
        public DateTime? Createdtime { get; set; }
    }
}
