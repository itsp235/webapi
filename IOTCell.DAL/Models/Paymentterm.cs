﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Paymentterm
    {
        public Paymentterm()
        {
            Customer = new HashSet<Customer>();
        }

        public int PaymentTermId { get; set; }
        public string PaymentTermName { get; set; }
        public int? PaymentTermValue { get; set; }

        public virtual ICollection<Customer> Customer { get; set; }
    }
}
