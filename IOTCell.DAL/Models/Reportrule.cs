﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Reportrule
    {
        public int Ruleid { get; set; }
        public int? Assetclassid { get; set; }
        public string Assetclassname { get; set; }
        public int? Assetid { get; set; }
        public string Assetname { get; set; }
        public string Devicetypeid { get; set; }
        public string Devicetypename { get; set; }
        public string Deviceid { get; set; }
        public string Devicename { get; set; }
        public string Colname { get; set; }
        public int? Operation { get; set; }
        public int? Time { get; set; }
        public int? Templateid { get; set; }
    }
}
