﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class ActivityEntryLog
    {
        public int ActivityEntryLogId { get; set; }
        public string CorrelationId { get; set; }
        public string EntryType { get; set; }
        public string ActvityText { get; set; }
        public DateTime? ActivityDate { get; set; }
    }
}
