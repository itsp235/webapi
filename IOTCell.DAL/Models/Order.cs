﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Order
    {
        public Order()
        {
            Invoice = new HashSet<Invoice>();
            OrderItem = new HashSet<OrderItem>();
            Transaction = new HashSet<Transaction>();
        }

        public long OrderId { get; set; }
        public long? AccountId { get; set; }
        public string CompanyCode { get; set; }
        public string Currency { get; set; }
        public string Comment { get; set; }
        public DateTimeOffset? OrderDate { get; set; }
        public decimal? OrdrTtl { get; set; }
        public decimal? OrdrTtlTax { get; set; }
        public decimal OrderAmount { get; set; }
        public decimal? TelecomTax { get; set; }
        public decimal? SalesTax { get; set; }
        public decimal OrderTax { get; set; }
        public decimal OrderTotal { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal OrderBalance { get; set; }
        public int? Billcycle { get; set; }
        public DateTime? BillingStartDt { get; set; }
        public DateTime? BillingEndDt { get; set; }
        public DateTime? NextBillingDt { get; set; }
        public int? StatusId { get; set; }
        public int? Status { get; set; }
        public DateTimeOffset? UpdateDt { get; set; }
        public int? AutoPay { get; set; }
        public int? PayFrequencyId { get; set; }
        public int? TaxId { get; set; }
        public double? ShippingCharge { get; set; }
        public string ShippingLt { get; set; }
        public int? ShippingMethod { get; set; }
        public DateTime? ActiveDate { get; set; }
        public DateTime? InactiveDate { get; set; }
        public int? OrdrType { get; set; }
        public string OrdrPayType { get; set; }
        public long? CustomerId { get; set; }
        public string Createuser { get; set; }
        public DateTime? Createdate { get; set; }

        public virtual Account Account { get; set; }
        public virtual ItemPayFrequency PayFrequency { get; set; }
        public virtual Status StatusNavigation { get; set; }
        public virtual Tax Tax { get; set; }
        public virtual ICollection<Invoice> Invoice { get; set; }
        public virtual ICollection<OrderItem> OrderItem { get; set; }
        public virtual ICollection<Transaction> Transaction { get; set; }
    }
}
