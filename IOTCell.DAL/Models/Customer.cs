﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Account = new HashSet<Account>();
            Address = new HashSet<Address>();
            CustomerExtension = new HashSet<CustomerExtension>();
            Inventory = new HashSet<Inventory>();
        }

        public long CustomerId { get; set; }
        public string CustomerNo { get; set; }
        public string CompanyCode { get; set; }
        public int? ResellerId { get; set; }
        public long? Resellercount { get; set; }
        public string Accountnumber { get; set; }
        public string BusinessName { get; set; }
        public string BusinessTaxId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Ssn { get; set; }
        public DateTime? BirthDate { get; set; }
        public string CustomerType { get; set; }
        public string CustomerCreditClass { get; set; }
        public string Password { get; set; }
        public string Language { get; set; }
        public DateTime? Createdate { get; set; }
        public string Createuser { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int? PaymentTermId { get; set; }
        public string ShippingAddressLine1 { get; set; }
        public string ShippingAddressLine2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingZipcode { get; set; }
        public string ShippingState { get; set; }
        public string ShippingCountry { get; set; }
        public string Mvno { get; set; }
        public int? CarrierId { get; set; }
        public int? CountryId { get; set; }
        public int? Lobid { get; set; }
        public int? DivisionId { get; set; }

        public virtual Country CountryNavigation { get; set; }
        public virtual Paymentterm PaymentTerm { get; set; }
        public virtual Reseller Reseller { get; set; }
        public virtual SimUsage SimUsage { get; set; }
        public virtual ICollection<Account> Account { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<CustomerExtension> CustomerExtension { get; set; }
        public virtual ICollection<Inventory> Inventory { get; set; }
    }
}
