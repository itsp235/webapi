﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Themeattribute
    {
        public int Themeattributeid { get; set; }
        public string Themename { get; set; }
        public int? Themeid { get; set; }
        public int? Attributeid { get; set; }
        public string Devicetypeid { get; set; }
        public int? Lower { get; set; }
        public int? Upper { get; set; }
        public string Value { get; set; }
        public int? Messagetype { get; set; }
        public byte? Display { get; set; }
        public int? Assetclassid { get; set; }
        public string Createdby { get; set; }
        public DateTime? Createdtime { get; set; }
    }
}
