﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Device
    {
        public string Deviceid { get; set; }
        public string Devicename { get; set; }
        public string Devicenamealias { get; set; }
        public int? State { get; set; }
        public int? Comstate { get; set; }
        public double? Locationlat { get; set; }
        public double? Locationlng { get; set; }
        public string Devicetype { get; set; }
        public string Devicetypename { get; set; }
        public string Devicegroup { get; set; }
        public int? Lqi { get; set; }
        public DateTime? Activationtime { get; set; }
        public DateTime? Creationtime { get; set; }
        public string Modemcertificate { get; set; }
        public byte? Prototype { get; set; }
        public byte? Automaticrenewal { get; set; }
        public int? Automaticrenewalstatus { get; set; }
        public string Createdby { get; set; }
        public DateTime? Lasteditiontime { get; set; }
        public string Lasteditedby { get; set; }
        public byte? Activable { get; set; }
        public int? Lastseqnumber { get; set; }
        public DateTime? Lastmessagetime { get; set; }
        public int? Geofence { get; set; }
        public int? Assetid { get; set; }
        public DateTime? Batteryexpiredate { get; set; }
        public string CompanyCode { get; set; }
        public string Status { get; set; }
    }
}
