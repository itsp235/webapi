﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Action
    {
        public int Actionid { get; set; }
        public string Actionname { get; set; }
        public string Actiondescription { get; set; }
        public int Ruleid { get; set; }
        public string Rulename { get; set; }
        public string Action1 { get; set; }
        public string Weekday { get; set; }
        public TimeSpan? Actiontime { get; set; }
        public byte? Active { get; set; }
        public DateTime? Createtime { get; set; }
        public string Createdby { get; set; }
        public string CompanyCode { get; set; }
        public byte? Triggertype { get; set; }
    }
}
