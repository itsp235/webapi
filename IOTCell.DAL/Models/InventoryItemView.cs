﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class InventoryItemView
    {
        public long InventoryId { get; set; }
        public int CompanyId { get; set; }
        public int? ResellerId { get; set; }
        public long ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string IdentifierType { get; set; }
        public string IdentifierValue { get; set; }
        public int? ItemPlanId { get; set; }
        public string Devicename { get; set; }
        public int CarrierId { get; set; }
        public int? Status { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
