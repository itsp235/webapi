﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class CalculateAttribute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Displayname { get; set; }
        public int? Assetclassid { get; set; }
        public int? Assetid { get; set; }
        public string Expression { get; set; }
        public string Vars { get; set; }
        public string Createdby { get; set; }
        public DateTime? Createdtime { get; set; }
    }
}
