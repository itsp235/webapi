﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class SimUsageClientCustomerView
    {
        public int? ResellerId { get; set; }
        public long CustomerId { get; set; }
        public decimal? TotalCurrentMonthUsage { get; set; }
        public decimal? TotalLast3MonthUsage { get; set; }
        public decimal? TotalLast6MonthUsage { get; set; }
        public decimal? TotalLast12MonthUsage { get; set; }
        public decimal? TotalLifetime { get; set; }
        public DateTime? UpdateTime { get; set; }
    }
}
