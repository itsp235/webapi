﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class AssetAttribute
    {
        public int Id { get; set; }
        public int? Assetid { get; set; }
        public int? Attmetaid { get; set; }
        public string Value { get; set; }
        public DateTime? CreateTime { get; set; }
        public DateTime? LastmodifiedTime { get; set; }
        public string CreateBy { get; set; }
        public string LastmodifiedBy { get; set; }
    }
}
