﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace IOTCell.DAL.Models
{
    public partial class Transaction
    {
        public long TransactionId { get; set; }
        public long OrderId { get; set; }
        public long? InvoiceId { get; set; }
        public int? StatusId { get; set; }
        public long? AccountId { get; set; }
        public string CardType { get; set; }
        public string TransactionNum { get; set; }
        public string TransactionAmt { get; set; }
        public string TransactionType { get; set; }
        public string TransactionDt { get; set; }
        public string RefNum { get; set; }
        public string RespCode { get; set; }
        public string AuthCode { get; set; }
        public string BankTtl { get; set; }
        public string Complete { get; set; }
        public string Iso { get; set; }
        public string Msg { get; set; }
        public string HostId { get; set; }
        public string IssuerId { get; set; }
        public string ReceiptId { get; set; }
        public string Ticket { get; set; }
        public string IsVisaDebit { get; set; }
        public string Comment { get; set; }
        public string TimeOut { get; set; }
        public TimeSpan? TransactionTime { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual Invoice Invoice { get; set; }
        public virtual Order Order { get; set; }
        public virtual Status Status { get; set; }
    }
}
