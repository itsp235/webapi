﻿ALTER TABLE `iot_cell1`.`order` 
DROP COLUMN `InventoryId`;

ALTER TABLE `iot_cell1`.`order_item` 
ADD COLUMN `InventoryId` BIGINT NULL AFTER `PAY_FREQ`;
