﻿ALTER TABLE `iot_cell1`.`reseller` 
ADD COLUMN `mvno` VARCHAR(45) NOT NULL AFTER `reseller_name`,
ADD UNIQUE INDEX `mvno_UNIQUE` (`mvno` ASC) VISIBLE;
UPDATE `iot_cell1`.`reseller` SET `mvno` = '500061' WHERE (`reseller_id` = '2');

ALTER TABLE `iot_cell1`.`user` 
DROP INDEX `username_UNIQUE` ;
;

 

ALTER TABLE iot_cell1.`user` ADD UNIQUE `unique_username_company`(`username`, `companycode`);

 


ALTER TABLE `iot_cell1`.`reseller` 
CHANGE COLUMN `reseller_code` `reseller_code` VARCHAR(10) NOT NULL ;

 


ALTER TABLE `iot_cell1`.`reseller` 
ADD COLUMN `company_code` VARCHAR(10) NOT NULL AFTER `company_id`;
ALTER TABLE `iot_cell1`.`reseller` ALTER INDEX `reseller_id_UNIQUE` INVISIBLE;

 

ALTER TABLE `iot_cell1`.`company` 
CHANGE COLUMN `company_code` `company_code` VARCHAR(10) NOT NULL ,
CHANGE COLUMN `company_name` `company_name` VARCHAR(45) NOT NULL ,
CHANGE COLUMN `status` `status` VARCHAR(10) NOT NULL COMMENT '1 = ACTIVE, 0 = INACTIVE' ;

 

ALTER TABLE `iot_cell1`.`reseller` 
CHANGE COLUMN `status` `status` VARCHAR(10) NOT NULL COMMENT '1 = ACTIVE, 0 = INACTIVE' ;

 

ALTER TABLE `iot_cell1`.`user` 
CHANGE COLUMN `status` `status` VARCHAR(10) NOT NULL ;

 

ALTER TABLE `iot_cell1`.`customer` 
CHANGE COLUMN `STATUS` `STATUS` VARCHAR(10) NOT NULL DEFAULT '1' COMMENT '1 = ACTIVE, 0 = INACTIVE' ;