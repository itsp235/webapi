﻿ALTER TABLE `iot_cell1`.`order_item`
ADD COLUMN `BILLCYCLE` INT(10) NULL AFTER `InventoryId`,
ADD COLUMN `BILLING_START_DT` DATE NULL AFTER `BILLCYCLE`,
ADD COLUMN `BILLING_END_DT` DATE NULL AFTER `BILLING_START_DT`,
ADD COLUMN `NEXT_BILLING_DT` DATE NULL AFTER `BILLING_END_DT`,
ADD COLUMN `UsageWhenPlanChanged` DECIMAL(20,2) NULL AFTER `NEXT_BILLING_DT`;

ALTER TABLE `iot_cell1`.`order`
DROP COLUMN `UsageWhenPlanChanged`,
DROP COLUMN `NEXT_BILLING_DT`,
DROP COLUMN `BILLING_END_DT`,
DROP COLUMN `BILLING_START_DT`,
DROP COLUMN `BILLCYCLE`;