﻿ALTER TABLE `iot_cell1`.`order_item`
DROP COLUMN `PAY_FREQ`;

ALTER TABLE `iot_cell1`.`transaction`
ADD COLUMN `ACCOUNT_ID` BIGINT NULL DEFAULT NULL AFTER `STATUS_ID`;

ALTER TABLE `iot_cell1`.`customer` 
ADD COLUMN `mvno` VARCHAR(45) NULL AFTER `ShippingCountry`;

CREATE TABLE `iot_cell1`.`customer_extension` (
  `customer_extension_id` BIGINT NOT NULL,
  `customer_id` BIGINT NOT NULL,
  `customer_user_name` VARCHAR(200) NULL,
  `customer_password` VARCHAR(200) NULL,
  PRIMARY KEY (`customer_extension_id`),
  INDEX `customer_extension_customer_id_idx` (`customer_id` ASC) VISIBLE,
  CONSTRAINT `customer_extension_customer_id`
    FOREIGN KEY (`customer_id`)
    REFERENCES `iot_cell1`.`customer` (`CUSTOMER_ID`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);

    INSERT INTO iot_cell1.customer_extension
(
`customer_id`,
`customer_user_name`,
`customer_password`) SELECT DISTINCT customer_id, 'aGVsaXh1c2Vy', 'ZDBORGJFa3ZVWGR5VFhoUFVtcG1VV3BEU3pVNGJFRlhVbEZPWWtwUFpXcGtXbmxHTWpjelVuY3ZZejA9' FROM iot_cell1.customer


