﻿ALTER TABLE `iot_cell1`.`user` 
ADD COLUMN `resellercode` VARCHAR(10) NULL DEFAULT NULL AFTER `network`,
ADD COLUMN `customerid` BIGINT(24) NULL DEFAULT NULL AFTER `resellercode`,
ADD COLUMN `externalid` NVARCHAR(100) NULL DEFAULT NULL AFTER `customerid`;

ALTER TABLE `iot_cell1`.`company` 
CHANGE COLUMN `status` `status` VARCHAR(10) NULL DEFAULT NULL COMMENT '1 = ACTIVE, 0 = INACTIVE' ;

ALTER TABLE `iot_cell1`.`user` 
CHANGE COLUMN `customerid` `customerid` BIGINT NULL DEFAULT 0 ;

ALTER TABLE `iot_cell1`.`customer` 
DROP COLUMN `PASSWORD`;

CREATE TABLE `reseller_extension` (
  `reseller_extension_id` int NOT NULL AUTO_INCREMENT,
  `reseller_id` int NOT NULL,
  `reseller_user_name` varchar(200) DEFAULT NULL,
  `reseller_password` varchar(200) DEFAULT NULL,
  `reseller_secret_key` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`reseller_extension_id`),
  UNIQUE KEY `reseller_extension_id_UNIQUE` (`reseller_extension_id`),
  KEY `reseller_extension_reseller_id_idx` (`reseller_id`),
  CONSTRAINT `reseller_extension_reseller_id` FOREIGN KEY (`reseller_id`) REFERENCES `reseller` (`reseller_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `iot_cell1`.`reseller_extension` (`reseller_extension_id`, `reseller_id`, `reseller_user_name`, `reseller_password`, `reseller_secret_key`) 
VALUES ('1', '2', 'TnU0U2laKzNaR3JlRTdGWGN1WnRldz09', 'd0NDbEkvUXdyTXhPUmpmUWpDSzU4bEFXUlFOYkpPZWpkWnlGMjczUncvYz0=', 'dk45TlNuWElyaFNzZitUeVJWWk9IZHA4QzFMV2NHVVd4NFpWWEhYWmMxOE5BNzc0YW9XYk9Gakk1d1IwNU03Y0c2ZVpGK0JnZ1o4eWFtNmdSTW5vM0xiTVByaUIyM1Q2YUl6UEk0My9GVUk9');
