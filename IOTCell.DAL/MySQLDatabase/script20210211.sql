﻿ALTER TABLE `iot_cell1`.`order` 
CHANGE COLUMN `STATUS` `STATUS` INT NULL DEFAULT NULL COMMENT 'ACTIVE=1, 0 INACTIVE=0, PLAN CHANGED = -1' ;

ALTER TABLE `iot_cell1`.`order` 
ADD COLUMN `UsageWhenPlanChanged` DECIMAL(20,2) NULL AFTER `createdate`;

