﻿ALTER TABLE `iot_cell1`.`inventory` 
CHANGE COLUMN `STATUS` `STATUS` INT NULL COMMENT '1 = ACTIVE, 0 = SUSPENDED/INACTIVE, -1 = CANCELLED/INACTIVE' ;

UPDATE `iot_cell1`.`item` SET `ItemCode` = '2GB' WHERE (`ITEM_ID` = '20');

ALTER TABLE `iot_cell1`.`reseller` 
ADD COLUMN `reseller_account_prefix` VARCHAR(5) NULL AFTER `reseller_code`,
ADD UNIQUE INDEX `reseller_account_prefix_UNIQUE` (`reseller_account_prefix` ASC) VISIBLE;
;

CREATE TABLE `iot_cell1`.`paymentterm` (
  `PaymentTermID` INT NOT NULL,
  `PaymentTermName` VARCHAR(45) NULL,
  `PaymentTermValue` INT NULL,
  PRIMARY KEY (`PaymentTermID`),
  UNIQUE INDEX `PaymentTermID_UNIQUE` (`PaymentTermID` ASC) VISIBLE,
  UNIQUE INDEX `PaymentTermValue_UNIQUE` (`PaymentTermValue` ASC) VISIBLE,
  UNIQUE INDEX `PaymentTermName_UNIQUE` (`PaymentTermName` ASC) VISIBLE);

INSERT INTO `iot_cell1`.`paymentterm` (`PaymentTermID`, `PaymentTermName`, `PaymentTermValue`) VALUES ('1', 'NET0', '0');
INSERT INTO `iot_cell1`.`paymentterm` (`PaymentTermID`, `PaymentTermName`, `PaymentTermValue`) VALUES ('2', 'NET30', '30');

ALTER TABLE `iot_cell1`.`customer` 
ADD COLUMN `PaymentTermID` INT NULL AFTER `EMAIL`;

ALTER TABLE `iot_cell1`.`customer` 
ADD COLUMN `ShippingADDRESS_LINE1` VARCHAR(255) NULL AFTER `PaymentTermID`,
ADD COLUMN `ShippingADDRESS_LINE2` VARCHAR(255) NULL AFTER `ShippingADDRESS_LINE1`,
ADD COLUMN `ShippingCITY` VARCHAR(45) NULL AFTER `ShippingADDRESS_LINE2`,
ADD COLUMN `ShippingZIPCODE` VARCHAR(9) NULL AFTER `ShippingCITY`,
ADD COLUMN `ShippingSTATE` VARCHAR(45) NULL AFTER `ShippingZIPCODE`,
ADD COLUMN `ShippingCountry` VARCHAR(45) NULL AFTER `ShippingSTATE`;
