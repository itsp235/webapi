﻿[Yesterday 5:29 PM] Zhao Chen
    ALTER TABLE `iot_cell1`.`item`
ADD COLUMN `AUTO_RENEW_FLAG` INT(11) NOT NULL DEFAULT 0 COMMENT '0 = the Plan doesn\'t not auto renew\n1 = the plan auto renews when data goes over ' AFTER `ITEM_PAY_TYPE`;
 
UPDATE `iot_cell1`.`item_type` SET `ITEM_TYPE_DESCRIPTION` = 'SMART SIM CARD' WHERE (`ITEM_TYPE_ID` = '3');
UPDATE `iot_cell1`.`item_type` SET `ITEM_TYPE_DESCRIPTION` = 'SIM CARD' WHERE (`ITEM_TYPE_ID` = '2');