﻿ALTER TABLE `iot_cell1`.`reseller` 
ADD COLUMN `CarrierID` INT NOT NULL AFTER `createdate`;

UPDATE `iot_cell1`.`reseller` SET `CarrierID` = '1' WHERE (`reseller_id` = '2');

ALTER TABLE `iot_cell1`.`inventory` 
DROP COLUMN `CarrierID`;