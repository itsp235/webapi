﻿ALTER TABLE `iot_cell1`.`user`
DROP INDEX `unique_username_company` ,
ADD UNIQUE INDEX `unique_username` (`username` ASC) VISIBLE;

ALTER TABLE `iot_cell1`.`item` 
ADD COLUMN `ItemCode` VARCHAR(50) NULL COMMENT 'this is the plan code used in third party APIs - must be unique' AFTER `ITEM_TYPE_ID`,
ADD UNIQUE INDEX `ItemCode_UNIQUE` (`ItemCode` ASC) VISIBLE;
;
