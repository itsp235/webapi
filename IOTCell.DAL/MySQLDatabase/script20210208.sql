﻿ALTER TABLE `iot_cell1`.`customer` 
ADD COLUMN `Business_Name` VARCHAR(100) NULL AFTER `ACCOUNTNUMBER`,
ADD COLUMN `Business_Tax_ID` VARCHAR(20) NULL AFTER `Business_Name`,
ADD COLUMN `SSN` VARCHAR(20) NULL AFTER `LAST_NAME`;

ALTER TABLE `iot_cell1`.`item` 
ADD COLUMN `Customer_ID` BIGINT NULL AFTER `BILLCYCLE`,
ADD COLUMN `UNIT_PRICE_FOR_RESELLER` DECIMAL(10,2) NULL AFTER `Customer_ID`;

CREATE TABLE `iot_cell1`.`carrier` (
  `CarrierID` INT NOT NULL,
  `CarrierCode` CHAR(3) NULL,
  `CarrierName` VARCHAR(45) NULL,
  `CarrierAssembly` VARCHAR(200) NULL,
  `CreateDate` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE INDEX `CarrierID_UNIQUE` (`CarrierID` ASC) VISIBLE,
  PRIMARY KEY (`CarrierID`))
ENGINE = InnoDB;

INSERT INTO `iot_cell1`.`carrier` (`CarrierID`, `CarrierCode`, `CarrierName`, `CarrierAssembly`) VALUES ('1', '001', 'iBasis', 'IOTCell.Custom.IBasis.BL.CustomBLProvider, IOTCell.Custom.IBasis.BL');

