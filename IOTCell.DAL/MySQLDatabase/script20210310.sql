﻿ALTER TABLE `iot_cell1`.`item`
CHANGE COLUMN `ItemCode` `ItemCode` VARCHAR(50) NULL COMMENT 'this is the plan code used in third party APIs - must be unique' ,
DROP INDEX `ItemCode_UNIQUE` ;

--I have renamed database iot_cell to iot_cell1

ALTER TABLE `iot_cell1`.`inventory` 
ADD COLUMN `devicename` VARCHAR(100) NULL AFTER `ItemPlan_ID`,
ADD COLUMN `devicetype` VARCHAR(100) NULL AFTER `devicename`,
ADD COLUMN `deviceIMEI` VARCHAR(100) NULL AFTER `devicetype`;

ALTER TABLE `iot_cell1`.`order` 
DROP COLUMN `deviceIMEI`,
DROP COLUMN `devicetype`,
DROP COLUMN `devicename`;


CREATE TABLE `iot_cell1`.`address` (
  `address_id` BIGINT NOT NULL AUTO_INCREMENT,
  `company_id` INT NULL,
  `reseller_id` INT NULL,
  `customer_id` BIGINT NULL,
  `ADDRESS_LINE1` VARCHAR(255) NULL,
  `ADDRESS_LINE2` VARCHAR(255) NULL,
  `CITY` VARCHAR(45) NULL,
  `ZIPCODE` VARCHAR(9) NULL,
  `STATE` VARCHAR(45) NULL,
  `COUNTRY` VARCHAR(45) NULL,
  `ADDRESS_TYPE` CHAR(1) NOT NULL COMMENT '\'S\' for shipping, \'B\' for Billing',
  `IsStillUsed` INT NULL COMMENT '0 if the address is not valid anymore, is not in use anymore (won\'t be visible in the address selection) or 1 if the address is valid and in use',
  `createdate` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`address_id`),
  UNIQUE INDEX `address_id_UNIQUE` (`address_id` ASC) VISIBLE)
COMMENT = 'contains all addresses (Company, Reseller, customer, shipping, billing etc)';

INSERT INTO `iot_cell1`.`address` (`company_id`, `ADDRESS_LINE1`, `ADDRESS_LINE2`, `CITY`, `ZIPCODE`, `STATE`, `COUNTRY`, `ADDRESS_TYPE`, `IsStillUsed`) VALUES ('1', '78 John Miller Way, Suite 212 ', ' ', 'Kearny', '07032', ' NJ', 'USA', 'S', '1');

INSERT INTO `iot_cell1`.`address` (`reseller_id`, `ADDRESS_LINE1`, `CITY`, `ZIPCODE`, `STATE`, `COUNTRY`, `ADDRESS_TYPE`, `IsStillUsed`) VALUES ('1', '\'12 Test Drive\'', 'New York', '10017', 'NY', 'USA', 'S', '1');

ALTER TABLE `iot_cell1`.`company` 
DROP COLUMN `country`,
DROP COLUMN `state`,
DROP COLUMN `zipcode`,
DROP COLUMN `city`,
DROP COLUMN `address_line2`,
DROP COLUMN `address_line1`;

ALTER TABLE `iot_cell1`.`reseller` 
DROP COLUMN `country`,
DROP COLUMN `state`,
DROP COLUMN `zipcode`,
DROP COLUMN `city`,
DROP COLUMN `address_line2`,
DROP COLUMN `address_line1`;

ALTER TABLE `iot_cell1`.`customer` 
DROP COLUMN `COUNTRY`,
DROP COLUMN `STATE`,
DROP COLUMN `ZIPCODE`,
DROP COLUMN `CITY`,
DROP COLUMN `ADDRESS_LINE2`,
DROP COLUMN `ADDRESS_LINE1`;
