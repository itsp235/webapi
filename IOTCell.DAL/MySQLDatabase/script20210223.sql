﻿ALTER TABLE `iot_cell1`.`inventory` 
CHANGE COLUMN `IDENTIFIER_VALUE` `IDENTIFIER_VALUE` VARCHAR(100) NULL DEFAULT NULL ;

ALTER TABLE `iot_cell1`.`inventory` 
DROP FOREIGN KEY `inventory_customer_id`;
ALTER TABLE `iot_cell1`.`inventory` 
CHANGE COLUMN `CUSTOMER_ID` `CUSTOMER_ID` BIGINT NULL DEFAULT NULL ;
ALTER TABLE `iot_cell1`.`inventory` 
ADD CONSTRAINT `inventory_customer_id`
  FOREIGN KEY (`CUSTOMER_ID`)
  REFERENCES `iot_cell1`.`customer` (`CUSTOMER_ID`);
