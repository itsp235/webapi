﻿ALTER TABLE `iot_cell1`.`inventory`
DROP FOREIGN KEY `inventory_company_id`;
ALTER TABLE `iot_cell1`.`inventory`
CHANGE COLUMN `COMPANY_ID` `COMPANY_ID` INT NULL DEFAULT NULL ;
ALTER TABLE `iot_cell1`.`inventory`
ADD CONSTRAINT `inventory_company_id`
  FOREIGN KEY (`COMPANY_ID`)
  REFERENCES `iot_cell1`.`company` (`company_id`);

  ALTER TABLE `iot_cell1`.`item` 
DROP COLUMN `ITEM_NAME`;

ALTER TABLE `iot_cell1`.`inventory` 
DROP COLUMN `ITEM_NAME`;

ALTER TABLE `iot_cell1`.`item` 
ADD COLUMN `ITEM_NAME` VARCHAR(50) NOT NULL AFTER `ItemCode`;

ALTER TABLE `iot_cell1`.`inventory` 
ADD COLUMN `ITEM_NAME` VARCHAR(50) NOT NULL AFTER `ITEM_ID`;

ALTER TABLE `iot_cell1`.`inventory` 
CHANGE COLUMN `IDENTIFIER_TYPE` `IDENTIFIER_TYPE` VARCHAR(50) NULL DEFAULT NULL COMMENT 'Unique identifier: ICCID for SIM, EID for ESIM' ;

ALTER TABLE `iot_cell11`.`carrier`
CHANGE COLUMN `CarrierID` `CarrierID` INT(11) NOT NULL AUTO_INCREMENT ; 