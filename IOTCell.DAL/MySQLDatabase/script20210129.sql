﻿ALTER TABLE `iot_cell1`.`customer` 
ADD UNIQUE INDEX `RESELLERCOUNT_UNIQUE` (`RESELLERCOUNT` ASC) VISIBLE,
ADD UNIQUE INDEX `ACCOUNTNUMBER_UNIQUE` (`ACCOUNTNUMBER` ASC) VISIBLE;

CREATE TABLE `iot_cell1`.`api_activity_log` (
  `api_activity_log_id` INT NOT NULL AUTO_INCREMENT,
  `correlation_id` VARCHAR(100) NULL,
  `request_time` DATETIME NULL,
  `response_milliseconds` BIGINT NULL,
  `status_code` INT NULL,
  `url` TEXT NULL,
  `headers` TEXT NULL,
  `method` VARCHAR(250) NULL,
  `path` VARCHAR(250) NULL,
  `query_string` TEXT NULL,
  `request_body` TEXT NULL,
  `response_body` TEXT NULL,
  PRIMARY KEY (`api_activity_log_id`));

CREATE TABLE `iot_cell1`.`error_log` (
  `error_log_id` INT NOT NULL AUTO_INCREMENT,
  `correlation_id` VARCHAR(100) NULL,
  `error_date_time` DATETIME NULL,
  `method_name` VARCHAR(1000) NULL,
  `message` TEXT NULL,
  `stack_trace` TEXT NULL,
  `line_number` INT NULL,
  `column_number` INT NULL,
  `file_name` VARCHAR(250) NULL,
  PRIMARY KEY (`error_log_id`));

CREATE TABLE `iot_cell1`.`activity_entry_log` (
  `activity_entry_log_id` INT NOT NULL AUTO_INCREMENT,
  `correlation_id` VARCHAR(100) NULL,
  `entry_type` VARCHAR(250) NULL,
  `actvity_text` VARCHAR(250) NULL,
  `activity_date` DATETIME NULL,
  PRIMARY KEY (`activity_entry_log_id`));
