CREATE DATABASE  IF NOT EXISTS `iot_cell1` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `iot_cell1`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: iot_cell1
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `ACCOUNT_ID` bigint NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` bigint NOT NULL,
  `ACCOUNT_TYPE` varchar(5) DEFAULT NULL,
  `PAYMENT_METHOD` varchar(5) DEFAULT NULL,
  `CREATEDATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATEDATE` datetime DEFAULT NULL,
  `STATUS` int DEFAULT '1' COMMENT '1 = ACTIVE, 0 = INACTIVE',
  `IS_NOTIFICATION` varchar(10) DEFAULT NULL,
  `TOKEN_CODE` varchar(50) DEFAULT NULL,
  `CARD_MASK` varchar(25) DEFAULT NULL,
  `EXPDATE` varchar(20) DEFAULT NULL,
  `ACCOUNT_NAME` varchar(255) DEFAULT NULL,
  `ISSUER_ID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_ID`),
  UNIQUE KEY `ACCOUNT_ID` (`ACCOUNT_ID`),
  KEY `customer_id_idx` (`CUSTOMER_ID`),
  CONSTRAINT `customer_id` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `customer` (`CUSTOMER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (50,30,NULL,NULL,'2021-01-26 15:13:58',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company` (
  `company_id` int NOT NULL AUTO_INCREMENT,
  `company_code` varchar(10) DEFAULT NULL,
  `company_name` varchar(45) DEFAULT NULL,
  `status` int DEFAULT NULL COMMENT '1 = ACTIVE, 0 = INACTIVE',
  `language` varchar(5) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `zipcode` varchar(15) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `contact_first_name` varchar(45) DEFAULT NULL,
  `contact_last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `createuser` varchar(45) DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`company_id`),
  UNIQUE KEY `company_id_UNIQUE` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Helix','Helix',1,'en','78 John Miller Way, Suite 212 ',NULL,'Kearny','07032',' NJ','USA','ContactF','ContactL','1231231234','test@helixwireless.co',NULL,'2021-01-12 14:29:37');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer` (
  `CUSTOMER_ID` bigint NOT NULL AUTO_INCREMENT,
  `RESELLER_ID` int DEFAULT NULL,
  `RESELLERCOUNT` bigint DEFAULT NULL,
  `ACCOUNTNUMBER` varchar(20) NOT NULL,
  `FIRST_NAME` varchar(20) DEFAULT NULL,
  `MIDDLE_NAME` varchar(20) DEFAULT NULL,
  `LAST_NAME` varchar(20) DEFAULT NULL,
  `BIRTH_DATE` datetime DEFAULT NULL,
  `PASSWORD` varchar(20) DEFAULT NULL,
  `LANGUAGE` varchar(5) DEFAULT NULL,
  `CREATEDATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `CREATEUSER` varchar(45) DEFAULT NULL,
  `UPDATEDATE` datetime DEFAULT NULL,
  `COMMENT` varchar(256) DEFAULT NULL,
  `STATUS` int DEFAULT '1' COMMENT '1 = ACTIVE, 0 = INACTIVE',
  `ADDRESS_LINE1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE2` varchar(255) DEFAULT NULL,
  `CITY` varchar(45) DEFAULT NULL,
  `ZIPCODE` varchar(9) DEFAULT NULL,
  `STATE` varchar(45) DEFAULT NULL,
  `COUNTRY` varchar(45) DEFAULT NULL,
  `PHONE` varchar(45) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CUSTOMER_ID`),
  UNIQUE KEY `CUSTOMER_ID` (`CUSTOMER_ID`),
  KEY `reseller_id_idx` (`RESELLER_ID`),
  CONSTRAINT `reseller_id` FOREIGN KEY (`RESELLER_ID`) REFERENCES `reseller` (`reseller_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (30,2,NULL,'2000000000000','Testfirst','Testmiddle','Testlast',NULL,NULL,'en','2021-01-26 15:13:23',NULL,NULL,NULL,1,'123 One St.','string','City123','123456','NY','US',NULL,NULL);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventory` (
  `INVENTORY_ID` bigint NOT NULL AUTO_INCREMENT,
  `COMPANY_ID` int NOT NULL,
  `RESELLER_ID` int NOT NULL,
  `CUSTOMER_ID` bigint NOT NULL,
  `ITEM_ID` bigint NOT NULL,
  `ITEM_NAME` varchar(50) NOT NULL,
  `ITEM_DESCRIPTION` varchar(50) DEFAULT NULL,
  `IDENTIFIER_TYPE` varchar(50) DEFAULT NULL COMMENT 'Unique identifier: ICCID for SIM, IMEI for phone',
  `IDENTIFIER_VALUE` varchar(20) DEFAULT NULL,
  `ICCID` varchar(100) DEFAULT NULL,
  `SERIAL_NUM` varchar(50) DEFAULT NULL,
  `IMAGE_FILE_NAME` varchar(100) DEFAULT NULL,
  `IMAGE_FILE_LOC` varchar(150) DEFAULT NULL,
  `IMAGE_FILE_LOC_BIG` varchar(150) DEFAULT NULL,
  `STATUS` int DEFAULT '1' COMMENT '1 = ACTIVE, 0 = INACTIVE',
  PRIMARY KEY (`INVENTORY_ID`),
  UNIQUE KEY `INVENTORY_ID` (`INVENTORY_ID`),
  KEY `inventory_company_id_idx` (`COMPANY_ID`),
  KEY `inventory_reseller_id_idx` (`RESELLER_ID`),
  KEY `inventory_customer_id_idx` (`CUSTOMER_ID`),
  KEY `inventory_item_id_idx` (`ITEM_ID`),
  CONSTRAINT `inventory_company_id` FOREIGN KEY (`COMPANY_ID`) REFERENCES `company` (`company_id`),
  CONSTRAINT `inventory_customer_id` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `customer` (`CUSTOMER_ID`),
  CONSTRAINT `inventory_item_id` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ITEM_ID`),
  CONSTRAINT `inventory_reseller_id` FOREIGN KEY (`RESELLER_ID`) REFERENCES `reseller` (`reseller_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoice` (
  `INVOICE_ID` bigint NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint NOT NULL,
  `STATUS_ID` int DEFAULT NULL,
  `INV_TTL` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'total before tax',
  `TAX` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'tax',
  `DISCOUNT_AMT` decimal(20,2) NOT NULL DEFAULT '0.00',
  `SUB_AMT` decimal(20,2) NOT NULL DEFAULT '0.00',
  `PRE_ACC_BAL` decimal(20,2) NOT NULL DEFAULT '0.00',
  `INV_AMT` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT 'total including tax',
  `CREATE_DT` datetime DEFAULT NULL,
  `START_DT` date DEFAULT NULL,
  `END_DT` date DEFAULT NULL,
  `COMMENT` varchar(2048) DEFAULT NULL,
  `INV_DESCRPT` varchar(50) DEFAULT NULL,
  `USAGE_DETAIL` varchar(512) DEFAULT NULL,
  `UPDATE_DT` datetime DEFAULT NULL,
  `OT_ITEMS_FLAG` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`INVOICE_ID`),
  UNIQUE KEY `INVOICE_ID` (`INVOICE_ID`),
  KEY `invoice_order_id_idx` (`ORDER_ID`),
  KEY `invoice_status_id_idx` (`STATUS_ID`),
  CONSTRAINT `invoice_order_id` FOREIGN KEY (`ORDER_ID`) REFERENCES `order` (`ORDER_ID`),
  CONSTRAINT `invoice_status_id` FOREIGN KEY (`STATUS_ID`) REFERENCES `status_transaction` (`STATUS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item` (
  `ITEM_ID` bigint NOT NULL AUTO_INCREMENT,
  `RESELLER_ID` int NOT NULL,
  `ITEM_TYPE_ID` int NOT NULL,
  `ITEM_NAME` varchar(50) NOT NULL,
  `ITEM_DESCRIPTION` varchar(50) DEFAULT NULL,
  `UNIT_PRICE` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SKU` varchar(50) DEFAULT NULL,
  `UNIT` varchar(20) DEFAULT NULL,
  `LOYALTY_POINT` varchar(20) DEFAULT NULL,
  `SERIAL_NUM` varchar(50) DEFAULT NULL,
  `IMAGE_FILE_NAME` varchar(100) DEFAULT NULL,
  `IMAGE_FILE_LOC` varchar(150) DEFAULT NULL,
  `IMAGE_FILE_LOC_BIG` varchar(150) DEFAULT NULL,
  `STATUS` int DEFAULT '1' COMMENT '1 = ACTIVE, 0 = INACTIVE',
  `SEQUENCE_TAG` int DEFAULT NULL,
  `PROMOTE_ID` int DEFAULT NULL,
  `ITEM_PAY_FREQUENCY_ID` int DEFAULT NULL,
  `ITEM_PAY_TYPE` varchar(20) DEFAULT NULL,
  `BILLCYCLE` int NOT NULL,
  PRIMARY KEY (`ITEM_ID`),
  UNIQUE KEY `ITEM_ID` (`ITEM_ID`),
  KEY `reseller_id_idx` (`RESELLER_ID`),
  KEY `item_reseller_id_idx` (`RESELLER_ID`),
  KEY `item_item_type_id_idx` (`ITEM_TYPE_ID`),
  KEY `item_item_pay_fequency_id_idx` (`ITEM_PAY_FREQUENCY_ID`),
  CONSTRAINT `item_item_pay_fequency_id` FOREIGN KEY (`ITEM_PAY_FREQUENCY_ID`) REFERENCES `item_pay_frequency` (`item_pay_frequency_ID`),
  CONSTRAINT `item_item_type_id` FOREIGN KEY (`ITEM_TYPE_ID`) REFERENCES `item_type` (`ITEM_TYPE_ID`),
  CONSTRAINT `item_reseller_id` FOREIGN KEY (`RESELLER_ID`) REFERENCES `reseller` (`reseller_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (19,2,2,'SIM','SIM Card',5.00,NULL,'1',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,30),(20,2,1,'2G','Helix Basics',16.00,NULL,'1',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,1,NULL,30),(21,2,3,'ESIM','ESIM',0.00,NULL,'0',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,30);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_pay_frequency`
--

DROP TABLE IF EXISTS `item_pay_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_pay_frequency` (
  `item_pay_frequency_ID` int NOT NULL AUTO_INCREMENT,
  `item_pay_frequency_NAME` varchar(45) DEFAULT NULL,
  `DEFAULT_VALUE` int DEFAULT NULL,
  PRIMARY KEY (`item_pay_frequency_ID`),
  UNIQUE KEY `item_pay_frequency_ID` (`item_pay_frequency_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_pay_frequency`
--

LOCK TABLES `item_pay_frequency` WRITE;
/*!40000 ALTER TABLE `item_pay_frequency` DISABLE KEYS */;
INSERT INTO `item_pay_frequency` VALUES (1,'Monthly',30);
/*!40000 ALTER TABLE `item_pay_frequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_type`
--

DROP TABLE IF EXISTS `item_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `item_type` (
  `ITEM_TYPE_ID` int NOT NULL AUTO_INCREMENT,
  `ITEM_TYPE_DESCRIPTION` varchar(50) NOT NULL,
  PRIMARY KEY (`ITEM_TYPE_ID`),
  UNIQUE KEY `ITEM_TYPE_ID` (`ITEM_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_type`
--

LOCK TABLES `item_type` WRITE;
/*!40000 ALTER TABLE `item_type` DISABLE KEYS */;
INSERT INTO `item_type` VALUES (1,'PLAN'),(2,'SIM'),(3,'ESIM'),(4,'PHONE');
/*!40000 ALTER TABLE `item_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `ORDER_ID` bigint NOT NULL AUTO_INCREMENT,
  `ACCOUNT_ID` bigint DEFAULT NULL,
  `CURRENCY` varchar(20) DEFAULT NULL,
  `COMMENT` varchar(500) DEFAULT NULL,
  `ORDER_DATE` timestamp NULL DEFAULT NULL,
  `ORDER_AMOUNT` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ORDER_TAX` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ORDER_TOTAL` decimal(20,2) NOT NULL DEFAULT '0.00',
  `DISCOUNT_AMOUNT` decimal(20,2) NOT NULL DEFAULT '0.00',
  `ORDER_BALANCE` decimal(20,2) NOT NULL DEFAULT '0.00',
  `BILLCYCLE` int unsigned NOT NULL DEFAULT '0',
  `BILLING_START_DT` date DEFAULT NULL,
  `BILLING_END_DT` date DEFAULT NULL,
  `NEXT_BILLING_DT` date DEFAULT NULL,
  `STATUS_ID` int DEFAULT NULL,
  `STATUS` int DEFAULT NULL COMMENT 'ACTIVE=1, 0 INACTIVE<=0',
  `UPDATE_DT` timestamp NULL DEFAULT NULL,
  `AUTO_PAY` int DEFAULT '0',
  `PAY_FREQUENCY_ID` int DEFAULT NULL,
  `TAX_ID` int DEFAULT NULL,
  `shipping_charge` double DEFAULT NULL,
  `shipping_lt` varchar(255) DEFAULT NULL,
  `shipping_method` int DEFAULT NULL,
  `ACTIVE_DATE` date DEFAULT NULL,
  `INACTIVE_DATE` date DEFAULT NULL,
  `ORDR_TYPE` int DEFAULT NULL COMMENT 'PRE = PREPAID, POST=POSTPAID',
  `ORDR_PAY_TYPE` varchar(20) DEFAULT NULL,
  `inventoryID` bigint DEFAULT NULL,
  `devicename` varchar(100) DEFAULT NULL,
  `devicetype` varchar(100) DEFAULT NULL,
  `deviceIMEI` varchar(100) DEFAULT NULL,
  `createuser` varchar(45) DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ORDER_ID`),
  UNIQUE KEY `ORDER_ID_UNIQUE` (`ORDER_ID`),
  KEY `order_account_id_idx` (`ACCOUNT_ID`),
  KEY `order_status_id_idx` (`STATUS_ID`),
  KEY `order_pay_frequency_id_idx` (`PAY_FREQUENCY_ID`),
  KEY `order_tax_id_idx` (`TAX_ID`),
  CONSTRAINT `order_account_id` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ACCOUNT_ID`),
  CONSTRAINT `order_pay_frequency_id` FOREIGN KEY (`PAY_FREQUENCY_ID`) REFERENCES `item_pay_frequency` (`item_pay_frequency_ID`),
  CONSTRAINT `order_status_id` FOREIGN KEY (`STATUS_ID`) REFERENCES `status_transaction` (`STATUS_ID`),
  CONSTRAINT `order_tax_id` FOREIGN KEY (`TAX_ID`) REFERENCES `tax` (`TAX_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (263,50,NULL,NULL,'2021-01-26 20:11:37',0.00,0.00,0.00,0.00,0.00,30,'2021-01-26','2021-02-25','2021-02-26',NULL,1,NULL,0,1,NULL,NULL,NULL,NULL,'2021-01-26',NULL,NULL,NULL,NULL,'Samsung','Samsung','987654321',NULL,'2021-01-26 15:14:11');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item` (
  `ORDERITEM_ID` bigint NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint NOT NULL,
  `ITEM_ID` bigint NOT NULL,
  `ITEM_QTY` bigint NOT NULL DEFAULT '0',
  `ITEM_PRICE` decimal(10,2) NOT NULL DEFAULT '0.00',
  `COST_AMOUNT` decimal(10,2) NOT NULL DEFAULT '0.00',
  `COST_TAX` decimal(10,2) NOT NULL DEFAULT '0.00',
  `PROMO_CODE` int NOT NULL DEFAULT '0',
  `PROMO_AMOUNT` decimal(10,2) NOT NULL DEFAULT '0.00',
  `STATUS` int NOT NULL DEFAULT '1',
  `shipping_date` datetime DEFAULT NULL,
  `courier` varchar(255) DEFAULT NULL,
  `shipping_charge` double DEFAULT NULL,
  `shipping_status_id` int DEFAULT NULL,
  `tracking_num` varchar(255) DEFAULT NULL,
  `waybill_num` varchar(255) DEFAULT NULL,
  `PAY_FREQ` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ORDERITEM_ID`),
  UNIQUE KEY `ORDER_ITEM_ID` (`ORDERITEM_ID`),
  KEY `order_item_order_id_idx` (`ORDER_ID`),
  KEY `order_item_item_id_idx` (`ITEM_ID`),
  CONSTRAINT `order_item_item_id` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ITEM_ID`),
  CONSTRAINT `order_item_order_id` FOREIGN KEY (`ORDER_ID`) REFERENCES `order` (`ORDER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (275,263,20,1,16.00,0.00,0.00,0,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(276,263,21,1,0.00,0.00,0.00,0,0.00,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reseller`
--

DROP TABLE IF EXISTS `reseller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reseller` (
  `reseller_id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `reseller_code` varchar(10) DEFAULT NULL,
  `reseller_name` varchar(45) DEFAULT NULL,
  `status` int DEFAULT NULL COMMENT '1 = ACTIVE, 0 = INACTIVE',
  `language` varchar(5) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `zipcode` varchar(15) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `contact_first_name` varchar(45) DEFAULT NULL,
  `contact_last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `resellerkey` varchar(200) DEFAULT NULL,
  `resellerkeycreated` datetime DEFAULT NULL,
  `assembly` varchar(200) DEFAULT NULL,
  `createuser` varchar(45) DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reseller_id`),
  UNIQUE KEY `reseller_id_UNIQUE` (`reseller_id`),
  UNIQUE KEY `reseller_code_UNIQUE` (`reseller_code`),
  KEY `reseller_company_id_idx` (`company_id`),
  CONSTRAINT `reseller_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reseller`
--

LOCK TABLES `reseller` WRITE;
/*!40000 ALTER TABLE `reseller` DISABLE KEYS */;
INSERT INTO `reseller` VALUES (2,1,'Helix','Helix',1,'en','12 Test Drive',NULL,'New York','10017','NY','USA','John','Smith','1231231234','email@reseller1.com','123123','2021-01-25 00:00:00','IOTCell.Custom.IBasis.BL.CustomBLProvider, IOTCell.Custom.IBasis.BL',NULL,'2021-01-12 14:30:47');
/*!40000 ALTER TABLE `reseller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_on` bigint DEFAULT NULL,
  `modified_on` bigint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'SUPERADMIN','System Admin',NULL,NULL),(2,'ADMIN','Company Admin',NULL,NULL),(3,'USER','Company User',NULL,NULL),(4,'ENDUSER','End User',NULL,NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_transaction`
--

DROP TABLE IF EXISTS `status_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_transaction` (
  `STATUS_ID` int NOT NULL AUTO_INCREMENT,
  `STATUS_DESC` varchar(30) DEFAULT NULL,
  `STATUS` int DEFAULT '1' COMMENT '1 = ACTIVE, 0 = INACTIVE',
  PRIMARY KEY (`STATUS_ID`),
  UNIQUE KEY `STATUS_ID` (`STATUS_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_transaction`
--

LOCK TABLES `status_transaction` WRITE;
/*!40000 ALTER TABLE `status_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax`
--

DROP TABLE IF EXISTS `tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tax` (
  `TAX_ID` int NOT NULL AUTO_INCREMENT,
  `COUNTRY` varchar(40) DEFAULT NULL,
  `STATE` varchar(40) DEFAULT NULL,
  `SALES_TAXES` varchar(20) DEFAULT NULL,
  `CREATEDATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`TAX_ID`),
  UNIQUE KEY `TAX_ID` (`TAX_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax`
--

LOCK TABLES `tax` WRITE;
/*!40000 ALTER TABLE `tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction` (
  `TRANSACTION_ID` bigint NOT NULL AUTO_INCREMENT,
  `ORDER_ID` bigint NOT NULL,
  `INVOICE_ID` bigint DEFAULT NULL,
  `STATUS_ID` int DEFAULT NULL,
  `CARD_TYPE` varchar(30) NOT NULL,
  `TRANSACTION_NUM` varchar(30) DEFAULT NULL,
  `TRANSACTION_AMT` varchar(20) DEFAULT NULL,
  `TRANSACTION_TYPE` varchar(20) DEFAULT NULL,
  `TRANSACTION_DT` varchar(30) DEFAULT NULL,
  `REF_NUM` varchar(20) DEFAULT NULL,
  `RESP_CODE` varchar(30) DEFAULT NULL,
  `AUTH_CODE` varchar(30) DEFAULT NULL,
  `BANK_TTL` varchar(30) DEFAULT NULL,
  `COMPLETE` varchar(30) DEFAULT NULL,
  `ISO` varchar(30) DEFAULT NULL,
  `MSG` varchar(250) DEFAULT NULL,
  `HOST_ID` varchar(30) DEFAULT NULL,
  `ISSUER_ID` varchar(30) DEFAULT NULL,
  `RECEIPT_ID` varchar(30) DEFAULT NULL,
  `TICKET` varchar(50) DEFAULT NULL,
  `IS_VISA_DEBIT` varchar(30) DEFAULT NULL,
  `COMMENT` varchar(2048) DEFAULT NULL,
  `TIME_OUT` varchar(30) DEFAULT NULL,
  `TRANSACTION_TIME` time DEFAULT NULL,
  `UPDATE_DT` datetime DEFAULT NULL,
  PRIMARY KEY (`TRANSACTION_ID`),
  UNIQUE KEY `TRANSACTION_ID` (`TRANSACTION_ID`),
  KEY `transaction_order_id_idx` (`ORDER_ID`),
  KEY `transaction_status_id_idx` (`STATUS_ID`),
  KEY `transaction_invoice_id_idx` (`INVOICE_ID`),
  CONSTRAINT `transaction_invoice_id` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice` (`INVOICE_ID`),
  CONSTRAINT `transaction_order_id` FOREIGN KEY (`ORDER_ID`) REFERENCES `order` (`ORDER_ID`),
  CONSTRAINT `transaction_status_id` FOREIGN KEY (`STATUS_ID`) REFERENCES `status_transaction` (`STATUS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `usertype` varchar(10) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `department` varchar(5) DEFAULT NULL,
  `businessunit` varchar(5) DEFAULT NULL,
  `phonenum` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `creator` varchar(45) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `expireddate` datetime DEFAULT NULL,
  `companycode` varchar(5) DEFAULT NULL,
  `network` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'test39789','$2a$10$2NIEkZJ0EX0K6HlPlebjm.cnnv.Fauvtdsow0Q0Adf5XiCC4ZrOuG','tt','test5678@itsp.ca','001','TT','5551231234','S',NULL,'2019-09-18 17:05:46',NULL,'151','test1'),(3,'test2789','$2a$10$5wFue4IbDmhgMwEGvFi4WunPn5wCE8FkUSd08KSxhXtpLrMe9tdZq','tt','test5678@itsp.ca','001','TT','5551231234','S',NULL,'2019-09-18 17:05:46',NULL,'151','test1'),(4,'test12345','$2a$10$zuc0t7xUpna4pYvjUVvdZuGBuB93dukXAtUgPk6rQ4C/GS3rVOJsu','tt','test12345@itsp-inc.ca','001','MM','5551231224','S','testq123','2019-09-18 17:05:46',NULL,'111','test21'),(5,'test123456','$2a$10$S80Eev4ItSekmG7CTkJ38OTv9m2LcYorB..BAEKgi/.zXv9dkmEIq','tt','test5678@itsp.ca','001','TT','5551231234','S',NULL,'2019-09-18 17:05:46',NULL,'171','test1'),(6,'test62347','$2a$10$m0oz4n7ctH92bt0IosSjee6AJiUYwyyfPgvV5boKzZldwLOGuuSuO','pp','liao_xinqing@yahoo.ca','001','MM','1234545567','S','test123','2019-10-17 13:45:43',NULL,'171',NULL),(7,'test1234567','$2a$10$Ju5G.2oR2D.GldgXc9Hqa.0RhguBs3S3ld5kix1BMp.bGm6OdLtgq','tt','test5678@itsp.ca','001','TT','5551231234','S',NULL,'2019-09-18 17:05:46',NULL,'121','test1'),(8,'test334567','$2a$10$U/hsJWYmzZNtBqvv4qMtN.rNx12HaUDjWbyhDrSDVKBW96Ns7v9f6','tt','test5678@itsp.ca','001','TT','5551231234','S',NULL,'2019-09-18 17:05:46',NULL,'121','test1'),(9,'test44567','$2a$10$73kqDavCaJFn1uqr.uHcVuiS9d3S4k/7NYly3fnf.m7PINbMUuN46','tt','test44567@itsp.ca','001','TT','5551231233','S',NULL,'2019-09-16 17:05:46',NULL,'121','test1'),(10,'test123','$2a$10$e9bQcL8rTY3oPwvzcRV3AepyXWLgwJtl04xSvApWP.VyDMZh22EEO','pp','liao_xinqing@yahoo.ca','001','PP','4169649052','S','test123','2019-10-18 11:02:49',NULL,'171',NULL),(11,'test123_2','$2a$10$R4m0IhkJFlI/geMs2zKhwe9TqbGvn388nnygsTwJiPq39YMGsyd9.','pp','liao_xinqing@yahoo.ca','001','PP','4169649053','S','test123','2019-10-18 11:09:26',NULL,'171',NULL),(12,'test54567','$2a$10$0Qj4L.sClnCaBvvf4ELy3.rGS543JKbnNCl0bsh8OzWXawZ0UYMFy','tt','test54567@itsp.ca','001','TT','5551231234','S',NULL,'2019-09-16 17:05:46',NULL,'121','test1'),(13,'test64567','$2a$10$UjXkMxcGsfvE.fvWVBODpefhOg8ReL1gwrHjdT4zKXMimyNDLICe2','tt','test64567@itsp.ca','001','MM','5551231234','S',NULL,'2019-09-16 17:05:46',NULL,'151','test1'),(14,'test74567','$2a$10$wRUCuvs1IZ6IJ./XDscRz.URo6WUMJMxDgOjE341qlptf1Fy4oORW','tt','test74567@itsp.ca','001','MM','5551231234','S',NULL,'2019-09-16 17:05:46',NULL,'111','test1'),(16,'test623478','$2a$10$GR2yU80e4GVi5KQ50PYVaup6ICmZdP.zbv1uxb1VZeWFv0wSVWcoe','tt','liao_xinqing@yahoo.ca','001','TT','1234567890','S','test1234567','2019-10-28 19:12:19',NULL,'121',NULL),(19,'testaa','$2a$10$9smxEs1Br3szD7o.bEzUMeX/7Ocd2fQdg0LjIpCRF3h32FxNh2yde','pp','test12345@itsp-inc.ca','001','TT','5551231224','S','test1234567','2019-10-28 20:31:40',NULL,'121',NULL),(20,'admin','$2a$10$UfxH3ppoW4EFrxD8g7.eCOUYuuaBUOEBtHmeuDZjqYFMixiV.uvni','pp','vtimpu7C0W/4f91dG+bYOg==','001','TT','3yoV3c4I9CrJrcUogqFQqg==','A','test1234567','2019-10-29 13:13:32',NULL,'121',NULL),(21,'user001','$2a$10$UfxH3ppoW4EFrxD8g7.eCOUYuuaBUOEBtHmeuDZjqYFMixiV.uvni','pp',NULL,'001','MM',NULL,'S','test12345','2019-10-29 13:26:39',NULL,'111',NULL),(22,'user002','$2a$10$72K8Sm8zcZ6lA66AU1BFr.aBXsof3cr0iO4FfE6TulKg57y.lmCU2','pp',NULL,'001','MM',NULL,'S','test12345','2019-10-29 13:49:46',NULL,'111',NULL),(23,'user003','123456','pp','test123456@itsp.ca','001','MM','5551231235','S','user001','2019-10-29 13:51:25',NULL,'111',NULL),(24,'itsp001','$2a$10$NBIOnuzlSxXoAFSf4B8RROJjXRc9UKriOG4.YbavhlB42kjJG9nfa','pp',NULL,'001','TT',NULL,'S','admin','2019-10-29 14:02:05',NULL,'121',NULL),(25,'itsp002','$2a$10$gQDn8FumqWo.T3P6HZYK5.vO2cPJ/2QGfj6Tg2knyuX6J9BH45Z2y','pp',NULL,'001','TT',NULL,'S','itsp001','2019-10-29 14:21:16',NULL,'121',NULL),(26,'user new','$2a$10$cpLv/hUUzV6yvhXGPW/0h.2Ian2HsWyt3QCoog/w60Mj0C9pHfL9G','pp','itsp@itsp-inc.com','001','MM','5555555555','S','user001','2019-10-29 16:17:09',NULL,'111',NULL),(27,'user_new_001','$2a$10$Zx9LK0b1YrglnEHxYrGKYeKxafCk5ortQs91u2cw8V8XBGfQIEF86','pp','liao_xinqing@yahoo.ca','','','4169649052','S','user001','2019-11-01 20:54:09',NULL,'111',NULL),(28,'delete_test','$2a$10$Q6JGmRZKYeTxXIfIlHyn2Op1za5gPpSgLUhlRw64BGG6WEhNI7x5G','pp','liao_xinqing@yahoo.ca','','','4169649052','S','user001','2019-11-03 20:08:46',NULL,'111',NULL),(29,'admin_01','$2a$10$AGRxfaK7kZw9XYjyZPetjuOemgv9ZH05rBeILZM3RxV8UnwwCcmmS','pp',NULL,'','',NULL,'S','user001','2019-11-04 02:05:49',NULL,'111',NULL),(30,'admin_test_002','$2a$10$XWQ6CmbzeG//8Uq.Sn44iemXqFHWRVn6g14Zd3nGVOarAVYGp87k6','pp','liao_xinqing@yahoo.ca','','','4169649052','S','user001','2019-11-04 02:21:30',NULL,'111',NULL),(31,'admin_test_003','$2a$10$UPj1MovrhGOHaswcBnQ4QeSuaVubUCrG/4xVN5wWmj.O7lUiJ6dwC','pp','liao_xinqing@yahoo.ca','','','4169649052','S','user001','2019-11-04 02:57:02',NULL,'111',NULL),(32,'sigfox_admin','$2a$10$y9CN6YFaSdfU1EFlRFzpFezS5zzOzgIOTI3nReFG70LX2v5HQRPk6','pp','matt.davis@sigfoxcanada.com','','','1112223333','A','user001','2019-11-04 13:49:17',NULL,'111',NULL),(33,'yan','$2a$10$UfxH3ppoW4EFrxD8g7.eCOUYuuaBUOEBtHmeuDZjqYFMixiV.uvni','pp','yan.dong@itsp-inc.com','','','6479283699','S','sigfox_admin','2019-11-04 16:43:34',NULL,'123',NULL),(34,'Matt-demo','$2a$10$6X7Cz1gLBE9z/nFgTN3kbeYGvIId43NXAW7oASaADigwgwzPqmNFa','pp',NULL,'','',NULL,'S','sigfox_admin','2019-11-04 19:57:14',NULL,'111',NULL),(36,'yyz','$2a$10$HurEQJOPAfP5AogHu4tpIOa3E5tXIJJlm.otVzJvKym.VNQmWhs3S','pp','yyy@yyy.zzz','','','3520789170','S','yyy','2019-11-04 23:22:21',NULL,'111',NULL),(37,'yyy','$2a$10$DQ0GANqjND4FdK7Nygs4Mu.dW61lsuOTWWAm3ldFHfGYopZyxHBN.','pp','cpu133@qq.com','','','6479283699','S','sigfox_admin','2019-11-05 00:10:07',NULL,'111',NULL),(38,'normal_user','$2a$10$j0yt3WekEbSYyG645TYj8..reKB0uRMhRhJCEnOyRBdl6gSMzW6pW','pp',NULL,'','',NULL,'S','user001','2019-11-05 02:38:03',NULL,'111',NULL),(39,'DFI_admin0408','$2a$10$kA/8l7WUNRaAoBIcZCp4KeY637kTy0/VAxuMv31GGQlc9Hv1lgIgy','pp',NULL,'','',NULL,'S','user001','2019-11-05 14:56:40',NULL,'101',NULL),(40,'alps_admin','$2a$10$O.QKw0W0D30A0s3KUU9k.OERa6KGKitx7S0lJIHcXSFR/632FL/t6','pp',NULL,'','',NULL,'S','user001','2019-11-05 15:07:06',NULL,'122',NULL),(41,'uat_test','$2a$10$x1p5h2Luwq1RBnrr9tPLuOmkILygYmkqQtmA4/rSfoUX8TtCEUgim','pp',NULL,'','',NULL,'S','sigfox_admin','2019-11-05 18:42:54',NULL,'111',NULL),(42,'user10','$2a$10$Xdk4Q4/3KCrIXyAqbUZf1eA3aJ8hrM0S.nawNLBquULY0Rf/VTPbW','pp','varinder.chen@itsp-inc.com','','','1234567899','S','sigfox_admin','2019-11-08 14:23:28',NULL,'111',NULL),(44,'user01','user@1234','pp',NULL,'','',NULL,'S','sigfox_admin','2019-11-08 14:31:07',NULL,'111',NULL),(45,'Test','test@123','pp',NULL,'','',NULL,'S','user01','2019-11-11 21:51:17',NULL,'111',NULL),(46,'sigfoxx','$2a$10$NTi0rqn.ihklv4mF7LNU1eKpMpd0vIAOoRu.HWPeSnM/G4fEgn.NS','pp','rre4@jojtor.com','','','1234567899','S','user01','2019-11-11 22:17:21',NULL,'111',NULL),(47,'thamiso','$2a$10$DRixE6qmhvPidnhcQa0Bw.KF.ak14ErIiWaSzmI/UBba8Rj.MjWVq','pp',NULL,'','',NULL,'S','user001','2019-11-12 16:46:41',NULL,'111',NULL),(48,'sigfox_super_admin','$2a$10$NunszqcB4j7xvcsIu/vKueOt1Ym6nVt93EDxUzoMsYhQQmRD51FgG','pp','lZxEVxAoC5E0vdf/iA9QZYttIqRCgbti5x1rGYZG9L0=','','','jHmLVteA07JamJhqvpPMYQ==','A','user001','2019-12-03 18:24:57',NULL,'111',NULL),(49,'test_011','$2a$10$bNU41n0mw0Ja3WWYL2pyveQsmfyMqg47OTyERbI3Q1VE9iBV4Wthq','pp','PVB8fB0q1a22QfAxCLmAtYttIqRCgbti5x1rGYZG9L0=','','','SDVT/ws7S2KFUVWSEXoqIQ==','S','user001','2019-12-03 18:53:07',NULL,'123',NULL),(50,'Greg Taylor','$2a$10$dXSLQ1VYI8gdErYMINJJpOMg5mup9RlZ80i80KIwxv4g6A3nmqsBG','pp','sZGfTlC2TjSCVMKe+98LC+KErvuPCUkzNyaPXPUI7UE=','','','kzgADEguyX1niyP+pMhflw==','A','sigfox_super_admin','2019-12-04 13:38:47',NULL,'123',NULL),(51,'Simon Brunet','$2a$10$tCC6dZY7rJ4/RMffmHDQ/.DEjiiseE/hgmiZYDwy562.tMuhvay9O','pp','snq3W1elwpufpQ/dQKi3xFL9HcHHtfi1hRNXghECWcOLbSKkQoG7YucdaxmGRvS9','','','ExhlyJk9UkFpQdKbrB8JTg==','A','Greg Taylor','2020-01-11 15:25:27',NULL,'123',NULL),(52,'mytag_admin','$2a$10$OIekUzM3kN9o5eLP9/tJZuyPvgSEV/ngZXtJ644Tov.MbXxeTb9Yy','pp','j510eqVwi96zF+RHbYjT+9Xg5XS+45j/bXBslrhw3Sw=','','','rDYPypQZJI7fFjZKkgHX0A==','A','admin','2020-02-01 02:26:33',NULL,'200',NULL),(53,'weather_admin','$2a$10$4i3raRyRfAZ0EKKXms1eiuQQ2F5bO6zb/M4IumyPrAeRX2PImJc/q','pp','AG5YkySD/YFAjnez1Re4Cczx1jC8wct8tp0BK8DGp+w=','','','gB6Yz9SK3W6qftYskCj1qw==','A','admin','2020-03-06 13:48:40',NULL,'111',NULL),(54,'weather_user','$2a$10$uUh16XpnmY8Zw0UJcsjeMeoAni.qZonyn/uxJE8i6lbexeY3katAC','pp','mfPU+iCJ8S0oWr3m0itBszMgfxesvB+bosEEg8GV/7A=','','','gB6Yz9SK3W6qftYskCj1qw==','A','weather_admin','2020-04-02 12:10:43',NULL,'111',NULL),(55,'metro_admin','$2a$10$/qbu7XB6cz4U8ZCsxnHpF.S5JGe4B5WXzcDAoAbSVaHU7Q2BXsuwO','pp','2jKfDNGWebj7qPtZaG0pIGMtgn7cJnNka+1BOnMuRFM=','','','1ifrXltTQU96qR/6VQlpAg==','S','admin','2020-04-02 12:42:22',NULL,'METRO',NULL),(56,'metro_user','$2a$10$A59Kh0NeI6nheGBlNPEmkeqehhna0onI5k2cwak66JQVrpNGw0IOC','pp','/QVY0M8lfgvg+RczxMn9nGMtgn7cJnNka+1BOnMuRFM=','','','1ifrXltTQU96qR/6VQlpAg==','A','metro_admin','2020-04-02 14:31:45',NULL,'METRO',NULL),(57,'aburns','$2a$10$V9SBILLEz9IT/yQ2AuAMFeV74pAQp21QNVM2vXswIn7oYEFWrv3Y2','pp','O4gv7rgizmUJq8owK1xpBbLRSd30o4A+wxLWVqTTNWs=','','','U5TxCfkRF62ynEdQGPJ9IQ==','A','metro_admin','2020-04-06 14:07:00',NULL,'METRO',NULL),(58,'rskleryk','$2a$10$McyXA1ZrE/oqKYmj4XKzDeFWG8F4FWeZYZo8EEIgLIzLme9/lM/gq','pp','W6Ip/7oq+4cF1Y/NoIIVrkyrjfO03eo6Hn5SLYrQXVg=','','','XFdqmtxmnIIjj7zW33yzCQ==','A','metro_admin','2020-04-06 14:09:34',NULL,'METRO',NULL),(59,'aconopio','$2a$10$Na7TZZxcsT/GEd8JHWfiu.DACijyYPNNkSwz1EGV1Ed19Jbg9cE4i','pp','dFIRnToRd/xHn3Z4U564gVQ6w3QTRNqcUUTJib3m4q0=','','','0tIvptPl6DSP9RWWosci5Q==','A','metro_admin','2020-04-06 14:10:27',NULL,'METRO',NULL),(61,'dfi_admin','$2a$10$5rUhPkawEpPWnCKuh1C7W.FxaIKevO/.Il6nIZLIxKz...Fk1GGAu','pp','acI4yvn6A9ch7DTAWTdJpA==','','','SDVT/ws7S2KFUVWSEXoqIQ==','A','admin','2020-04-09 01:23:26',NULL,'DFI',NULL),(62,'dfi_user','$2a$10$n6OtVkHw2C79KSPABMw8XOwWVUCX0HHTAH6kqR.gI06sJXJrpFfYK','pp','4e/BAvomZHVatEUBQk1RHA==','','','SDVT/ws7S2KFUVWSEXoqIQ==','A','admin','2020-04-09 01:24:31',NULL,'DFI',NULL),(63,'swilly','$2a$10$oHxGTADFmJ1sNFxlPt3zE.FndVFWZa2y/ZeQJAevnIkqw0ovICadC','pp','sVF+/+wS2khkIN6Qwxjl4I2Zto9zC1toPOqPb9d/ucM=','','','96rEj7iMBWhgjakg4i/SAg==','S','metro_admin','2020-08-05 17:19:06',NULL,'METRO',NULL),(64,'dtuira','$2a$10$0z8UUhUTmUO07rW9FtloIecWAxAxZLVw2tBThYXXjvECRgiC5kZ9W','pp','4ZB6lSqyHwWqKX0tqehTbI2Zto9zC1toPOqPb9d/ucM=','','','96rEj7iMBWhgjakg4i/SAg==','A','metro_admin','2020-08-05 17:19:53',NULL,'METRO',NULL),(65,'ljing','$2a$10$OOJUSxSX5yNgjl10.zveHu1fQYBVvZEjaqnh36BHRY7zEFEGKRqOa','pp','pzJMlpcO9MDFxrcxfNiQtqR5hS4weOOI8vrASBkoX4k=','','','96rEj7iMBWhgjakg4i/SAg==','A','metro_admin','2020-08-05 17:20:54',NULL,'METRO',NULL),(66,'swillie','$2a$10$2xwOaDZ5Es1yr4AmabMHNuSY0JTZlHwLb9/QYLzLrwjWOSI2z2CTS','pp','HscjPNnIXoNpCNtDHCFn/0SAaJ29U3XxqnZmgZ4z3J4=','','','96rEj7iMBWhgjakg4i/SAg==','S','metro_admin','2020-08-06 18:31:17',NULL,'METRO',NULL),(67,'Simon','$2a$10$D.imgAyw/lwnigD999gBqOV8SRc5gsu9H3C/NiySSO5tFDZOaRJVO','pp','snq3W1elwpufpQ/dQKi3xFL9HcHHtfi1hRNXghECWcOLbSKkQoG7YucdaxmGRvS9','','','ExhlyJk9UkFpQdKbrB8JTg==','A','sigfox_super_admin','2020-08-13 12:39:34',NULL,'123',NULL),(68,'sigfox_test','$2a$10$Du/iWIf3sbGPlT.1reSJuehwR9jRa3z2VGO128Vyler2znOhA7DOS','pp','+QC1Z0IoUWUrWV9eVuMbm8zx1jC8wct8tp0BK8DGp+w=','','','DssSkwBoS/VHy9OLhMWANw==','A','admin','2020-08-13 12:58:00',NULL,'DFI',NULL),(69,'itsp_test','$2a$10$zOdOxmX08uzWphTUNY2N3uO3AsCim/M44rQbZ7AmgG1XWSYtZJ9dS','pp','+QC1Z0IoUWUrWV9eVuMbm8zx1jC8wct8tp0BK8DGp+w=','','','DssSkwBoS/VHy9OLhMWANw==','A','admin','2020-08-13 13:00:25',NULL,'123',NULL),(70,'dstelatos','$2a$10$MQHNTxCE5dbE1XtK3BRbeepYv5RBYVIjsM3CZR9au.kwlkOAPe.T6','pp','U6p/lrNXijl6Nk3895QejQ4hUW5fGRnkt6+xzWloyEBfQxBdgFgajxhX67h56KrB','','','UTlkWcsbZn1GkXqAasVA1Q==','A','metro_admin','2020-09-04 12:54:57',NULL,'METRO',NULL),(71,'dfi_demo','$2a$10$AtyijF1GjpuX.ujS.p0VO.Vs4JhzNFDyPKBJamkJQiFeUTl5tv1MW','pp','xqIdu5eKkQyXdWxi2E+6fg==','','','SDVT/ws7S2KFUVWSEXoqIQ==','A','admin','2020-09-24 14:47:56',NULL,'DFI',NULL),(72,'dfi_super','$2a$10$SzvxEqw43KHHtqSF2sG2F.85gWZQGoigRwqgI1Maisn/OAuukIfjS','pp','IMHCBvXWeOIlE5MVysGYoQ==','','','vcvzT7+lULaBSTwsE8zDWQ==','A','admin','2020-09-24 16:42:48',NULL,'DFI',NULL),(73,'enduser01','$2a$10$JFt56Rc9Zj2cFyZtcZ9F4udMLO646aqYqWHLtT6xkWGEajJTColgC','pp',NULL,NULL,NULL,NULL,'A','admin','2020-09-24 16:42:48',NULL,'DFI',NULL),(74,'mcameron','$2a$10$CAkZiYaKUUWYeumOZ6fhTOuAy86uKEJWERXjNFu5KrzjAKoqjl8.O','pp','5JJms1H/hiTRvORCuwX2LkyrjfO03eo6Hn5SLYrQXVg=','','','UTlkWcsbZn1GkXqAasVA1Q==','A','metro_admin','2020-10-22 12:33:57',NULL,'METRO',NULL),(75,'NOC','$2a$10$gBthN5Y8HhU1Ch4nLdFsROi87vR2Tyrl9V0nntgt0zwfFjz79gdge','pp','gzJH13rfmfUKNI6Z+IhgkDMgfxesvB+bosEEg8GV/7A=','','','0uCDDmdCu9J5W58NtaxgfQ==','A','metro_admin','2020-11-01 13:25:59',NULL,'METRO',NULL),(76,'TTR','$2a$10$vyFgjejfFsct96IcfyQ78eLmcyYyZPdZYzuSbpmQpHxsHMNmBdcA.','pp','sSoYnuC3nPOr+Q/TjXrDGA==','','','eHFfPqsHiFFvvrUCnnFrjw==','A','metro_admin','2020-11-01 13:44:43',NULL,'METRO',NULL),(77,'opg_admin','$2a$10$cxX1BIg/bC3JW5YW5rwzW.Ya1flPw.KF3Nnq15osfWQ7.4pT1N2na','pp','4+rMf0pCHF0z+UjieJEGQzMgfxesvB+bosEEg8GV/7A=','','','OjEt9dKeudx7wuG5Gz/C7A==','A','admin','2020-11-02 23:04:52',NULL,'OPG',NULL),(78,'test_user','$2a$10$32.NLe1ouHATqDP0tb.su.MYYGH7ImBRVWOdHep/Gy2n52Fuq19F2','pp','/c11ro10WsRK7P88h4xOd14iieQ2y8a2WCs8e7uULgY=','','','Y5t+YeDvFV47Q2LYPQnabw==','A','dfi_demo','2020-12-30 14:54:10',NULL,'DFI',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role` (
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKh8ciramu9cc9q3qcqiv4ue8a6` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,2),(2,1),(4,1),(4,3),(7,1),(12,1),(14,1),(15,1),(16,1),(17,2),(19,2),(20,1),(21,1),(22,2),(23,2),(24,2),(25,2),(26,3),(27,2),(28,3),(29,2),(30,2),(31,2),(32,2),(33,1),(34,2),(35,2),(36,2),(37,3),(38,3),(39,2),(40,2),(41,2),(42,2),(44,2),(45,3),(46,3),(47,2),(48,1),(49,2),(50,2),(51,2),(52,2),(53,2),(54,3),(55,2),(56,3),(57,2),(58,2),(59,3),(61,2),(62,3),(63,3),(64,3),(65,3),(66,3),(67,2),(68,2),(69,2),(70,3),(71,2),(72,1),(73,4),(74,3),(75,3),(76,3),(77,2),(78,3);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-27 11:56:38
