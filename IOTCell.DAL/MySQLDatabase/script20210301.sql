﻿ALTER TABLE `iot_cell1`.`role`
ADD COLUMN `value` INT NOT NULL AFTER `description`;

ALTER TABLE `iot_cell1`.`inventory`
DROP FOREIGN KEY `inventory_customer_id`;
ALTER TABLE `iot_cell1`.`inventory`
CHANGE COLUMN `CUSTOMER_ID` `CUSTOMER_ID` BIGINT(20) NULL ;
ALTER TABLE `iot_cell1`.`inventory`
ADD CONSTRAINT `inventory_customer_id`
  FOREIGN KEY (`CUSTOMER_ID`)
  REFERENCES `iot_cell1`.`customer` (`customer_id`);

  ALTER TABLE `iot_cell1`.`item` 
DROP COLUMN `UNIT_PRICE_FOR_RESELLER`,
ADD COLUMN `Company_ID` INT NULL AFTER `ITEM_ID`,
ADD COLUMN `PlanDataVolume` VARCHAR(20) NULL DEFAULT NULL AFTER `BILLCYCLE`,
CHANGE COLUMN `Customer_ID` `Customer_ID` BIGINT NULL DEFAULT NULL AFTER `RESELLER_ID`;

ALTER TABLE `iot_cell1`.`item` 
CHANGE COLUMN `PlanDataVolume` `PlanDataVolume` INT NULL DEFAULT NULL ;
