﻿ALTER TABLE `iot_cell1`.`invoice` 
ADD COLUMN `Amount_Due` DECIMAL(20,2) NOT NULL DEFAULT '0.00' AFTER `OT_ITEMS_FLAG`,
ADD COLUMN `Amount_Paid` DECIMAL(20,2) NOT NULL DEFAULT '0.00' AFTER `Amount_Due`;

ALTER TABLE `iot_cell1`.`invoice` 
CHANGE COLUMN `Amount_Due` `Amount_Due` DECIMAL(20,2) NOT NULL DEFAULT '0.00' AFTER `INV_AMT`,
CHANGE COLUMN `Amount_Paid` `Amount_Paid` DECIMAL(20,2) NOT NULL DEFAULT '0.00' AFTER `Amount_Due`;

 

ALTER TABLE `iot_cell1`.`invoice` 
CHANGE COLUMN `STATUS_ID` `STATUS_ID` INT(11) NOT NULL ;

ALTER TABLE `iot_cell1`.`carrier` 
ADD COLUMN `APIUser` VARCHAR(255) NULL AFTER `CarrierAssembly`,
ADD COLUMN `APIPwd` VARCHAR(255) NULL AFTER `APIUser`;

UPDATE `iot_cell1`.`carrier` SET `APIUser` = 'aGVsaXhfY2VsbHVsYXI=', `APIPwd` = 'aDNpeGMzbEBy' WHERE (`CarrierID` = '1');

ALTER TABLE `iot_cell1`.`carrier` 
DROP COLUMN `APIPwd`,
DROP COLUMN `APIUser`;
