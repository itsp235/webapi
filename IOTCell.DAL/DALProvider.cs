﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Linq;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;

using IOTCell.DAL.DBContext;
using IOTCell.DAL.Models;
using IOTCell.Framework.Diagnostics;

namespace IOTCell.DAL
{
    public class DALProvider : IDALProvider
    {
        #region Members
        private readonly IConfiguration configuration;
        private readonly IOTDBContext dbContext;
        private IDbContextTransaction dbContextTransaction;
        #endregion

        #region Constructor
        public DALProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
            dbContext = new IOTDBContext(IOTDBContextHelper.GetDbContextOptions(this.configuration));
        }
        #endregion

        #region Methods
        #region BeginTransaction
        async Task IDALProvider.BeginTransaction()
        {
            dbContextTransaction = await dbContext.Database.BeginTransactionAsync();
        }
        #endregion

        #region CommitTransaction
        async Task IDALProvider.CommitTransaction()
        {
            await dbContextTransaction.CommitAsync();
        }
        #endregion

        #region RollBackTransaction
        async Task IDALProvider.RollBackTransaction()
        {
            await dbContextTransaction.RollbackAsync();
        }
        #endregion

        #region InsertActivityEntry
        async Task IDALProvider.InsertActivityEntry(ActivityEntry activityEntry)
        {
            ActivityEntryLog activityEntryLog = new ActivityEntryLog
            {
                CorrelationId = activityEntry.CorrelationID,
                EntryType = activityEntry.EntryType,
                ActvityText = activityEntry.ActivityText,
                ActivityDate = activityEntry.ActivityDate
            };

            await dbContext.ActivityEntryLog.AddAsync(activityEntryLog);
            await Save();
        }
        #endregion

        #region InsertAPIActivityLog
        async Task IDALProvider.InsertAPIActivityLog(APIActivity apiActivity)
        {
            ApiActivityLog apiActivityLog = new ApiActivityLog
            {
                CorrelationId = apiActivity.CorrelationID,
                RequestTime = apiActivity.RequestTime,
                ResponseMilliseconds = apiActivity.ResponseMilliseconds,
                StatusCode = apiActivity.StatusCode,
                Url = apiActivity.Url,
                Headers = apiActivity.HeadersText,
                Method = apiActivity.Method,
                Path = apiActivity.Path,
                QueryString = apiActivity.QueryString,
                RequestBody = apiActivity.RequestBody,
                ResponseBody = apiActivity.ResponseBody
            };

            await dbContext.ApiActivityLog.AddAsync(apiActivityLog);
            await Save();
        }
        #endregion

        #region InsertErrorLog
        async Task IDALProvider.InsertErrorLog(APIError apiError)
        {
            ErrorLog errorLog = new ErrorLog
            {
                CorrelationId = apiError.CorrelationID,
                ErrorDateTime = apiError.ErrorDateTime,
                MethodName = apiError.MethodName,
                Message = apiError.Message,
                StackTrace = apiError.StackTrace,
                LineNumber = apiError.LineNumber,
                ColumnNumber = apiError.ColumnNumber,
                FileName = apiError.FileName
            };

            await dbContext.ErrorLog.AddAsync(errorLog);
            await Save();
        }
        #endregion

        #region GetCompanies
        async Task<IEnumerable<Company>> IDALProvider.GetCompanies()
        {
            return await dbContext.Company.ToListAsync();
        }
        #endregion

        #region GetCompanyByID
        async Task<Company> IDALProvider.GetCompanyByID(int companyId)
        {
            return await dbContext.Company.FindAsync(companyId);
        }
        #endregion

        #region InsertCompany
        async Task IDALProvider.InsertCompany(Company company)
        {
            dbContext.Add(company);

            var validationContext = new ValidationContext(company);
            Validator.ValidateObject(company, validationContext);

            await Save();
        }
        #endregion

        #region UpdateCompany
        async Task IDALProvider.UpdateCompany(Company company)
        {
            dbContext.Entry(company).State = EntityState.Modified;

            var validationContext = new ValidationContext(company);
            Validator.ValidateObject(company, validationContext);

            await Save();
        }
        #endregion

        #region DeleteCompany
        async Task IDALProvider.DeleteCompany(int companyId)
        {
            var company = dbContext.Company.Find(companyId);
            dbContext.Company.Remove(company);

            var validationContext = new ValidationContext(company);
            Validator.ValidateObject(company, validationContext);

            await Save();
        }
        #endregion

        #region GetResellerByResellerCode
        async Task<Reseller> IDALProvider.GetResellerByResellerCode(string resellerCode)
        {          
            return await dbContext.Reseller.SingleOrDefaultAsync(b => b.ResellerCode == resellerCode);
        }
        #endregion

        #region GetResellerExtensionByResellerCode
        async Task<ResellerExtension> IDALProvider.GetResellerExtensionByResellerCode(string resellerCode)
        {
            return await (from r in dbContext.Reseller
                                        join re in dbContext.ResellerExtension on r.ResellerId equals re.ResellerId
                                        where r.ResellerCode == resellerCode
                                        select re).FirstOrDefaultAsync();
        }
        #endregion


        #region GetResellerExtension
        async Task<ResellerExtension> IDALProvider.GetResellerExtension(int resellerID)
        {
            return await dbContext.ResellerExtension.SingleOrDefaultAsync(b => b.ResellerId == resellerID);
        }
        #endregion


        #region GetPlanByPlanName
        async Task<Item> IDALProvider.GetPlanByPlanName(string planName, int resellerID, int carrierID)
        {
            //return await dbContext.Item.SingleOrDefaultAsync(b => ((b.ResellerId == resellerID) && (b.ItemName == planName) && (b.ItemTypeId == 1)));
            
            if (dbContext.Item.Count(b => ((b.ResellerId == resellerID) && (b.CarrierId == carrierID) && (b.ItemName == planName) && (b.ItemTypeId == 1))) > 0)
                return await dbContext.Item.FirstOrDefaultAsync(b => ((b.ResellerId == resellerID) && (b.ItemName == planName) && (b.ItemTypeId == 1)));
            else
                return null;
        }
        #endregion

        #region GetSIMType
        async Task<Item> IDALProvider.GetSIMType(long itemID)
        {
                return await dbContext.Item.SingleOrDefaultAsync(b => (b.ItemId == itemID));
        }
        #endregion

        #region GetSIMFromInventory
        async Task<Inventory> IDALProvider.GetSIMFromInventory(string simIdentifier, string simIdentifierValue)
        {
            return await dbContext.Inventory.SingleOrDefaultAsync(b => ((b.IdentifierType == simIdentifier) && (b.IdentifierValue == simIdentifierValue)));
        }
        #endregion

        //#region GetCarrierFromInventory
        //async Task<Carrier> IDALProvider.GetCarrierFromInventory(string simIdentifier, string simIdentifierValue)
        //{
        //    Inventory inventory = await dbContext.Inventory.SingleOrDefaultAsync(b => ((b.IdentifierType == simIdentifier) && (b.IdentifierValue == simIdentifierValue)));
        //    if (inventory != null)
        //        return await dbContext.Carrier.SingleOrDefaultAsync(b => (b.CarrierId == inventory.CarrierId));
        //    else
        //        return null;
        //}
        //#endregion

        #region GetCarrierFromReseller
        async Task<Carrier> IDALProvider.GetCarrierFromReseller(string resellerCode)
        {
            Reseller reseller = await dbContext.Reseller.SingleOrDefaultAsync(b => (b.ResellerCode == resellerCode));
            return await dbContext.Carrier.SingleOrDefaultAsync(b => (b.CarrierId == reseller.CarrierId));
        }
        #endregion


        #region GetCarrierFromInventory
        async Task<Carrier> IDALProvider.GetCarrierFromInventory(int carrierID)
        {
            return await dbContext.Carrier.SingleOrDefaultAsync(b => (b.CarrierId == carrierID));
        }
        #endregion


        #region AddNewCustomer
        async Task<Customer> IDALProvider.AddNewCustomer(Customer customer)
        {
            await dbContext.Customer.AddAsync(customer);
            await Save();
            return customer;
        }
        #endregion

        #region GetLastCustomerForReseller
        long? IDALProvider.GetLastCustomerForReseller(int resellerID)
        {
            return dbContext.Customer.Where(c => c.ResellerId == resellerID).Select(c => c.Resellercount).Max();
        }
        #endregion

        #region AddNewOrder
        async Task<Order> IDALProvider.AddNewOrder(Order order)
        {
            await dbContext.Order.AddAsync(order);
            await Save();
            return order;
        }
        #endregion

        #region AddNewAccount
        async Task<Account> IDALProvider.AddNewAccount(Account account)
        {
            await dbContext.Account.AddAsync(account);
            await Save();
            return account;
        }
        #endregion


        #region AddNewAddress
        async Task<Address> IDALProvider.AddNewAddress(Address address)
        {
            await dbContext.Address.AddAsync(address);
            await Save();
            return address;
        }
        #endregion


        #region AddNewOrderItem
        async Task<OrderItem> IDALProvider.AddNewOrderItem(OrderItem orderItem)
        {
            await dbContext.OrderItem.AddAsync(orderItem);
            await Save();
            return orderItem;
        }
        #endregion

        #region UpdateInventory
        async Task<Inventory> IDALProvider.UpdateInventory(Inventory inventorySIM)
        {
            dbContext.Inventory.Update(inventorySIM);
            await Save();
            return inventorySIM;
        }
        #endregion

        #region UpdateOrder
        async Task<Order> IDALProvider.UpdateOrder(Order order)
        {
            dbContext.Order.Update(order);
            await Save();
            return order;
        }
        #endregion

        #region UpdateOrderItem
        async Task<OrderItem> IDALProvider.UpdateOrderItem(OrderItem orderItem)
        {
            dbContext.OrderItem.Update(orderItem);
            await Save();
            return orderItem;
        }
        #endregion

        #region GetCustomerByAccountNumber
        async Task<Customer> IDALProvider.GetCustomerByAccountNumber(string accountNumber)
        {
            return await dbContext.Customer.SingleOrDefaultAsync(b => b.Accountnumber == accountNumber);
        }
        #endregion

        #region GetAccountByCustomerID
        async Task<Account> IDALProvider.GetAccountByCustomerID(long customerID)
        {
            return await dbContext.Account.SingleOrDefaultAsync(b => b.CustomerId == customerID);
        }
        #endregion

        #region GetAccountByResellerID
        async Task<Account> IDALProvider.GetAccountByResellerID(long resellerID)
        {
            return await dbContext.Account.FirstOrDefaultAsync(b => ((b.ResellerId == resellerID) && (b.Status == 1)));
        }
        #endregion


        #region GetOrderItemByInventoryID
        async Task<OrderItem> IDALProvider.GetOrderItemByInventoryID(long inventoryID)
        {
            return await dbContext.OrderItem.OrderByDescending(y => y.OrderitemId).FirstOrDefaultAsync(b => b.InventoryId == inventoryID);
        }
        #endregion

        #region GetOrderItemPlanByOrderID
        async Task<OrderItem> IDALProvider.GetOrderItemPlanByOrderID(long orderID)
        {
            return await (from o in dbContext.Order
                          join oi in dbContext.OrderItem on o.OrderId equals oi.OrderId
                          join i in dbContext.Item on oi.ItemId equals i.ItemId
                          join it in dbContext.ItemType on i.ItemTypeId equals it.ItemTypeId
                          where it.ItemTypeDescription.ToUpper() == "PLAN"
                          select oi).FirstOrDefaultAsync();

            //return await dbContext.OrderItem.FirstOrDefaultAsync(b => ((b.OrderId == orderID) && (b.InventoryId == null)));
        }
        #endregion

        #region GetOrderByOrderID
        async Task<Order> IDALProvider.GetOrderByOrderID(long orderID)
        {
            return await dbContext.Order.SingleOrDefaultAsync(b => b.OrderId == orderID);
        }
        #endregion


        #region Save
        async Task Save()
        {
            await dbContext.SaveChangesAsync();
        }
        #endregion
        #endregion
    }
}