﻿using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.Diagnostics;
using IOTCell.DAL;
using IOTCell.Framework.Diagnostics;
using IOTCell.Framework.Configuration;

namespace IOTCell.BL
{
    public class BLProviderBase : IBLProviderBase
    {
        #region Members
        public readonly IConfiguration configuration;
        public readonly IAppSettings appSettings;
        public readonly IDALProvider dalProvider;
        public readonly IActivityEntryLogger logger;
        #endregion

        #region Constructor
        public BLProviderBase(IConfiguration configuration, string correlationID)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
            this.dalProvider = new DALProvider(this.configuration);
            this.logger = this.GetActivityEntryLogger(correlationID);
        }

        public BLProviderBase(IConfiguration configuration, IActivityEntryLogger logger)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
            this.logger = logger;
            this.dalProvider = new DALProvider(this.configuration);
        }

        public BLProviderBase(IConfiguration configuration, IActivityEntryLogger logger, IDALProvider dalProvider)
        {
            this.configuration = configuration;
            this.appSettings = new AppSettings(this.configuration);
            this.logger = logger;
            this.dalProvider = dalProvider;
        }
        #endregion

        #region Methods
        #region GetActivityEntryLogger
        public IActivityEntryLogger GetActivityEntryLogger(string correlationID)
        {
            return new ActivityEntryLogger(this.configuration, this.dalProvider, correlationID);
        }
        #endregion

        #region LogAPIActivity
        async Task IBLProviderBase.LogAPIActivity(APIActivity apiActivity)
        {
            await this.logger.AddEntryWithFlush("Log API Activity", "Start Logging API Activity");
            await this.dalProvider.InsertAPIActivityLog(apiActivity);
            await this.logger.AddEntryWithFlush("Log API Activity", "End Logging API Activity");
        }
        #endregion

        #region LogError
        async Task IBLProviderBase.LogError(APIError apiError)
        {
            await this.dalProvider.InsertErrorLog(apiError);
        }
        #endregion
        #endregion
    }
}