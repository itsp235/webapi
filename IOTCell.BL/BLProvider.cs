﻿using System;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.Diagnostics;
using IOTCell.BL.ViewModels;
using IOTCell.DAL;
using IOTCell.DAL.Models;
using IOTCell.Framework.Enums;

namespace IOTCell.BL
{
    public class BLProvider : BLProviderBase, IBLProvider
    {
        #region Members
        #endregion

        #region Constructor
        public BLProvider(IConfiguration configuration, string correlationID)
            : base(configuration, correlationID)
        {
        }

        public BLProvider(IConfiguration configuration, IActivityEntryLogger logger)
            : base(configuration, logger)
        {
        }

        public BLProvider(IConfiguration configuration, IActivityEntryLogger logger, IDALProvider dalProvider)
            : base(configuration, logger, dalProvider)
        {
        }
        #endregion

        #region Methods

        #region GetNewCustomer
        public Customer GetNewCustomer(CustomerViewModel customerViewModel, Reseller reseller)
        {
            Customer customer = new Customer()
            {
                ResellerId = reseller.ResellerId,
                FirstName = customerViewModel.FirstName,
                MiddleName = customerViewModel.MiddleName,
                LastName = customerViewModel.LastName,
                //BirthDate = customerViewModel.BirthDate,
                Language = customerViewModel.Language,
                //Phone = customerViewModel.Phone,
                //Email = customerViewModel.Email
            };

            return customer;
        }
        #endregion


        #region GetNewAddress
        public Address GetNewAddress(CustomerViewModel customerViewModel, Reseller reseller)
        {
            Address address = new Address()
            {
                AddressLine1 = customerViewModel.AddressLine1,
                AddressLine2 = customerViewModel.AddressLine2,
                City = customerViewModel.City,
                Zipcode = customerViewModel.Zipcode,
                State = customerViewModel.State,
                Country = customerViewModel.Country,
                AddressType = "B",
                IsStillUsed = 1
            };

            return address;
        }
        #endregion


        #region GetNewAccountNumber
        public string GetNewAccountNumber(Reseller reseller, Customer customer, Carrier carrier)
        {
            //account number 
            //ResellerCode (2 letter) + first letter from the Customer name + counter for that reseller (5 digits) + Carrier code/Telco code (2 digits)

            string newAccountSequence = (this.dalProvider.GetLastCustomerForReseller(reseller.ResellerId) + 1).ToString().PadLeft(7, '0');

            string firstLetterCustomerName;
            if ((customer.BusinessName != null) && (customer.BusinessName.ToString().Trim() != String.Empty))
                firstLetterCustomerName = customer.BusinessName.Substring(0, 1);
            else
                firstLetterCustomerName = customer.LastName.Substring(0, 1);

            string newAccountNumber = reseller.ResellerAccountPrefix + firstLetterCustomerName + newAccountSequence + carrier.CarrierCode; 


            return newAccountNumber;
        }
        #endregion

        #region GetNewOrder
        public Order GetNewOrder(CustomerViewModel customerViewModel, Item item)
        {
            Order newOrder = new Order()
            {
                OrderDate = DateTime.Now,
//                Billcycle = item.Billcycle,
                Status = (int)OrderStatus.Inactive, //inactive until we activate the SIM and populate the rest of the fields (activation date, billing start date,etc) and then it will become 1 (active) 
                PayFrequencyId = item.ItemPayFrequencyId,
                OrdrPayType = item.ItemPayType,
                //Devicename = customerViewModel.DeviceName,
                //Devicetype = customerViewModel.DeviceType,
                //DeviceImei = customerViewModel.DeviceIMEI,
            };

            return newOrder;
        }
        #endregion

        #region GetNewAccount
        public Account GetNewAccount()
        {
            Account newAccount = new Account()
            {
                Status = (int)OrderStatus.Inactive //inactive - this is an online activation, not really a payment method
            };

            return newAccount;
        }
        #endregion

        #region GetNewOrderItem
        public OrderItem GetNewOrderItem(Item item)
        {
            OrderItem newOrderItem = new OrderItem()
            {
                ItemId = item.ItemId,
                ItemQty = 1,
                Billcycle = item.Billcycle,
                ItemPrice = item.UnitPrice
            };

            return newOrderItem;
        }
        #endregion
        #endregion
    }
}