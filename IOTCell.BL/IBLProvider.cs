﻿using System.Threading.Tasks;

using IOTCell.BL.ViewModels;
using IOTCell.DAL.Models;

namespace IOTCell.BL
{
    public interface IBLProvider : IBLProviderBase
    {
        public Customer GetNewCustomer(CustomerViewModel customerViewModel, Reseller reseller);
        public string GetNewAccountNumber(Reseller reseller, Customer customer, Carrier carrier);
        public Order GetNewOrder(CustomerViewModel customerViewModel, Item item);
        public Account GetNewAccount();
        public OrderItem GetNewOrderItem(Item item);
    }
}