﻿using System.Threading.Tasks;

using IOTCell.Framework.Diagnostics;

namespace IOTCell.BL
{
    public interface IBLProviderBase
    {
        Task LogAPIActivity(APIActivity apiActivity);
        Task LogError(APIError apiError);
    }
}