﻿using Newtonsoft.Json;
using System;

namespace IOTCell.BL.ViewModels
{
    public class CustomerViewModel
    {
        public string CompanyCode { get; set; } //mandatory
        public int CarrierID { get; set; } //mandatory

        //        public string ResellerKey { get; set; } //mandatory
        public string CompanyMVNO { get; set; } //mandatory

        public string IB_UserName { get; set; } //mandatory
        public string IB_Password { get; set; } //mandatory

        public string SimIdentifier { get; set; } //mandatory
        public string SimIdentifierValue { get; set; } //mandatory

        //Device
        public string DeviceName { get; set; } //mandatory
        public string DeviceType { get; set; }
        public string DeviceIMEI { get; set; }

        //Plan
        public string PlanName { get; set; } //mandatory + validate in the database

        public string PlanCode { get; set; } //mandatory 

        //Activation date
        //public DateTime activationDate { get; set; }  //??? will probably get it from iBasis = current date

        //Customer
        public string FirstName { get; set; }  //mandatory
        public string MiddleName { get; set; }
        public string LastName { get; set; } //mandatory
        //public DateTime BirthDate { get; set; }
        public string Language { get; set; }  //mandatory - if blank default is En
        //public string Phone { get; set; }
        //public string Email { get; set; }

        //Customer
        public string AccountNumber { get; set; }  //mandatory

        //Adress
        public string AddressLine1 { get; set; }  //mandatory
        public string AddressLine2 { get; set; }
        public string City { get; set; } //mandatory
        public string Zipcode { get; set; } //mandatory
        public string State { get; set; } //mandatory
        public string Country { get; set; } //mandatory - if blank default is USA



        //Get usage period
        public string FromDate { get; set; }  //mandatory
        public string ToDate { get; set; }  //mandatory

        //Carrier
        public string ApiUser { get; set; }  //mandatory
        public string ApiPwd { get; set; }  //mandatory


        #region Methods
        #region ConvertViewModelToCustomerViewModel
        public CustomerViewModel ConvertViewModelToCustomerViewModel(CustomerSIMPlanViewModel customerSIMPlanViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(customerSIMPlanViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }

        public CustomerViewModel ConvertViewModelToCustomerViewModel(CustomerAccountViewModel customerAccountViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(customerAccountViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }

        public CustomerViewModel ConvertViewModelToCustomerViewModel(SIMUsageViewModel simUsageViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(simUsageViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }

        public CustomerViewModel ConvertViewModelToCustomerViewModel(SIMUsageTotalViewModel simUsageTotalViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(simUsageTotalViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }


        public CustomerViewModel ConvertViewModelToCustomerViewModel(ResellerUsageViewModel resellerUsageViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(resellerUsageViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }

        public CustomerViewModel ConvertViewModelToCustomerViewModel(ResellerCarrierViewModel resellerCarrierViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(resellerCarrierViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }


        public CustomerViewModel ConvertViewModelToCustomerViewModel(SIMViewModel simViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(simViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }
        public CustomerViewModel ConvertViewModelToCustomerViewModel(SIMPlanChangeViewModel simPlanChangeViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(simPlanChangeViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }

        public CustomerViewModel ConvertViewModelToCustomerViewModel(ResellerViewModel resellerViewModel)
        {
            string jsonString = JsonConvert.SerializeObject(resellerViewModel);
            return JsonConvert.DeserializeObject<CustomerViewModel>(jsonString);
        }


        #endregion
        #endregion
    }
}