﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOTCell.BL.ViewModels
{
    public class ResellerCarrierViewModel
    {
        //Reseller

        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        [Required]
        public string CompanyCode { get; set; } //mandatory

        //CarrierID

        /// <summary>
        /// Carrier ID
        /// </summary>
        /// <example>1</example>
        [Required]
        public int CarrierID { get; set; } //mandatory

    }
}