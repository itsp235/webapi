﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOTCell.BL.ViewModels
{
    public class CustomerSIMPlanViewModel
    {
        //Reseller

        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        [Required]
        public string CompanyCode { get; set; } //mandatory

        ///// <summary>
        ///// Reseller key
        ///// </summary>
        ///// <example>exampleResellerKey</example>
        //[Required]
        //public string ResellerKey { get; set; } //mandatory

        //SIM

        /// <summary>
        /// SIM identifier (ICCID or EID)
        /// </summary>
        /// <example>ICCID</example>
        [Required]
        public string SimIdentifier { get; set; } //mandatory

        /// <summary>
        /// The actual ICCID or EID
        /// </summary>
        /// <example>8931080219101218961F</example>
        [Required]
        public string SimIdentifierValue { get; set; } //mandatory


        //Device

        /// <summary>
        /// Device name
        /// </summary>
        /// <example>IPHONE</example>
        [Required]
        public string DeviceName { get; set; } //mandatory

        /// <summary>
        /// Device Type
        /// </summary>
        /// <example>PHONE</example>        
        public string DeviceType { get; set; }
        
        
        /// <summary>
        /// Device IMEI
        /// </summary>
        /// <example>123456789123456789</example>        
        public string DeviceIMEI { get; set; }

 
        //Plan

        /// <summary>
        /// Plan name
        /// </summary>
        /// <example>2GB</example>    
        [Required]
        public string PlanName { get; set; } //mandatory + validate in the database


        //Customer
        
        /// <summary>
        /// First name
        /// </summary>
        /// <example>Christopher</example>    
        [Required]
        public string FirstName { get; set; }  //mandatory

        /// <summary>
        /// Middle name
        /// </summary>
        /// <example>M.</example>    
        public string MiddleName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        /// <example>Columbus</example>    
        [Required]
        public string LastName { get; set; } //mandatory

        /// <summary>
        /// Language - default en
        /// </summary>
        /// <example>en</example> 
        [DefaultValue("en")]
        public string Language { get; set; }  //mandatory - if blank default is En

        /// <summary>
        /// Address Line 1
        /// </summary>
        /// <example>First St. 123</example> 
        [Required]
        public string AddressLine1 { get; set; }  //mandatory

        /// <summary>
        /// Address Line 2
        /// </summary>
        /// <example>apt. 12</example> 
        public string AddressLine2 { get; set; }


        /// <summary>
        /// City
        /// </summary>
        /// <example>New York</example> 
        [Required]
        public string City { get; set; } //mandatory


        /// <summary>
        /// Zip code
        /// </summary>
        /// <example>10016</example> 
        [Required]
        public string Zipcode { get; set; } //mandatory

        /// <summary>
        /// State
        /// </summary>
        /// <example>NY</example> 
        [Required]
        public string State { get; set; } //mandatory
                
        /// <summary>
        /// Country
        /// </summary>
        /// <example>USA</example> 
        [DefaultValue("USA")]
        public string Country { get; set; } //mandatory - if blank default is USA
    }
}