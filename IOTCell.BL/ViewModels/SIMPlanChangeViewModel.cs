﻿using System.ComponentModel.DataAnnotations;

namespace IOTCell.BL.ViewModels
{
    public class SIMPlanChangeViewModel
    {
        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        [Required]
        public string CompanyCode { get; set; } //mandatory


        ///// <summary>
        ///// Reseller key
        ///// </summary>
        ///// <example>exampleResellerKey</example>
        //[Required]
        //public string ResellerKey { get; set; } //mandatory

        //SIM

        /// <summary>
        /// SIM identifier (ICCID or EID)
        /// </summary>
        /// <example>ICCID</example>
        [Required]
        public string SimIdentifier { get; set; } //mandatory

        /// <summary>
        /// The actual ICCID or EID
        /// </summary>
        /// <example>8931080219101218961F</example>
        [Required]
        public string SimIdentifierValue { get; set; } //mandatory

        /// <summary>
        /// Plan name
        /// </summary>
        /// <example>2GB</example>    
        [Required]
        public string PlanName { get; set; } //mandatory + validate in the database


    }
}