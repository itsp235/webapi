﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOTCell.BL.ViewModels
{
    public class ResellerUsageViewModel
    {
        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        [Required]
        public string CompanyCode { get; set; } //mandatory


        //Period of time
        /// <summary>
        /// Start Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-01</example>
        [Required]
        public string FromDate { get; set; } //mandatory

        //Period of time
        /// <summary>
        /// End Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-31</example>
        [Required]
        public string ToDate { get; set; } //mandatory
    }
}