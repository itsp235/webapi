﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOTCell.BL.ViewModels
{
    public class CustomerAccountViewModel
    {
        //Reseller

        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        [Required]
        public string CompanyCode { get; set; } //mandatory

        ///// <summary>
        ///// Reseller key
        ///// </summary>
        ///// <example>exampleResellerKey</example>
        //[Required]
        //public string ResellerKey { get; set; } //mandatory

        //SIM

        /// <summary>
        /// SIM identifier (ICCID or EID)
        /// </summary>
        /// <example>ICCID</example>
        [Required]
        public string SimIdentifier { get; set; } //mandatory

        /// <summary>
        /// The actual ICCID or EID
        /// </summary>
        /// <example>8931080219101218961F</example>
        [Required]
        public string SimIdentifierValue { get; set; } //mandatory

        //Device

        /// <summary>
        /// Device name
        /// </summary>
        /// <example>IPHONE</example>
        [Required]
        public string DeviceName { get; set; } //mandatory

        /// <summary>
        /// Device Type
        /// </summary>
        /// <example>PHONE</example>        
        public string DeviceType { get; set; }

        /// <summary>
        /// Device IMEI
        /// </summary>
        /// <example>123456789123456789</example>        
        public string DeviceIMEI { get; set; }

        //Plan

        /// <summary>
        /// Plan name
        /// </summary>
        /// <example>2GB</example>    
        [Required]
        public string PlanName { get; set; } //mandatory + validate in the database

        ////Customer

        ///// <summary>
        ///// Account number
        ///// </summary>
        ///// <example>AAA00001001</example>    
        //[Required]
        //public string AccountNumber { get; set; }  //mandatory
    }
}