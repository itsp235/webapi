﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IOTCell.BL.ViewModels
{
    public class SIMUsageViewModel
    {
        /// <summary>
        /// Reseller code
        /// </summary>
        /// <example>Helix</example>
        [Required]
        public string CompanyCode { get; set; } //mandatory


        ///// <summary>
        ///// Reseller key
        ///// </summary>
        ///// <example>exampleResellerKey</example>
        //[Required]
        //public string ResellerKey { get; set; } //mandatory

        //SIM

        /// <summary>
        /// SIM identifier (ICCID or EID)
        /// </summary>
        /// <example>ICCID</example>
        [Required]
        public string SimIdentifier { get; set; } //mandatory

        /// <summary>
        /// The actual ICCID or EID
        /// </summary>
        /// <example>8931080219101218961F</example>
        [Required]
        public string SimIdentifierValue { get; set; } //mandatory

        //Period of time
        /// <summary>
        /// Start Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-01</example>
        [Required]
        public string FromDate { get; set; } //mandatory

        //Period of time
        /// <summary>
        /// End Date  (format yyyy-mm-dd )
        /// </summary>
        /// <example>2021-01-31</example>
        [Required]
        public string ToDate { get; set; } //mandatory
    }
}