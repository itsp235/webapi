using System.Threading.Tasks;

using IOTCell.Framework.Diagnostics;

namespace IOTCell.BL.Diagnostics
{
	public interface IActivityEntryLogger
	{
		void AddEntry(string entryType, string activityText);
		void AddEntry(ActivityEntry entry);
		Task AddEntryWithFlush(string entryType, string activityText);
		Task AddEntryWithFlush(ActivityEntry entry);
		Task Flush();
	}
}