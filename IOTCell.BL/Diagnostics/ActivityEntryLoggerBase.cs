using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using IOTCell.Framework.Diagnostics;

namespace IOTCell.BL.Diagnostics
{
	[Serializable]
	public abstract class ActivityEntryLoggerBase : IActivityEntryLogger
	{
		#region Properties
		#region CorrelationID
		private readonly string correlationID;
		public string CorrelationID
		{
			get
			{
				return this.correlationID;
			}
		}
		#endregion

		#region InnerLogger
		private readonly IActivityEntryLogger innerLogger;
		public IActivityEntryLogger InnerLogger
		{
			get
			{
				return this.innerLogger;
			}
		}
		#endregion

		#region ActivityEntries
		private List<ActivityEntry> activityEntries;
		public List<ActivityEntry> ActivityEntries
		{
			get
			{
				if (this.activityEntries == null)
				{
					this.activityEntries = new List<ActivityEntry>();
				}
				return this.activityEntries;
			}
		}
		#endregion
		#endregion

		#region Constructor
		public ActivityEntryLoggerBase(IActivityEntryLogger innerLogger, string correlationID)
		{
			this.innerLogger = innerLogger;
			this.correlationID = correlationID;		}
		#endregion

		#region Methods
		#region AddEntry
		public void AddEntry(string entryType, string activityText)
		{
			ActivityEntry entry = new ActivityEntry()
			{
				CorrelationID = this.CorrelationID,
				EntryType = entryType,
				ActivityText = activityText,
				ActivityDate = DateTime.Now
			};

			this.AddEntry(entry);
		}

		public void AddEntry(ActivityEntry entry)
		{
			this.ActivityEntries.Add(entry);
			if (this.InnerLogger != null)
			{
				this.InnerLogger.AddEntry(entry);
			}
		}
		#endregion

		#region AddEntryWithFlush
		public async Task AddEntryWithFlush(string entryType, string activityText)
		{
			ActivityEntry entry = new ActivityEntry()
			{
				CorrelationID = this.CorrelationID,
				EntryType = entryType,
				ActivityText = activityText,
				ActivityDate = DateTime.Now
			};

			this.AddEntry(entry);
			await this.Flush();
		}

		public async Task AddEntryWithFlush(ActivityEntry entry)
		{
			this.ActivityEntries.Add(entry);
			if (this.InnerLogger != null)
			{
				this.InnerLogger.AddEntry(entry);
			}

			await this.Flush();
		}
		#endregion

		#region Flush
		public async Task Flush()
		{
			await this.PerformFlush();
			this.ActivityEntries.Clear();

			if (this.InnerLogger != null)
			{
				await this.InnerLogger.Flush();
			}
		}
		#endregion

		#region PerformFlush
		protected abstract Task PerformFlush();
		#endregion
		#endregion
	}
}