using System;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.DAL;
using IOTCell.Framework.Configuration;
using IOTCell.Framework.Diagnostics;

namespace IOTCell.BL.Diagnostics
{
	[Serializable]
	public class ActivityEntryLogger
		: ActivityEntryLoggerBase,
		IActivityEntryLogger
	{
        #region Members
        private readonly IConfiguration configuration;
		private readonly IAppSettings appSettings;
        #endregion

        #region Properties
        #region DALProvider
        private readonly IDALProvider dalProvider;
		public IDALProvider DALProvider
		{
			get
			{
				return this.dalProvider;
			}
		}
		#endregion
		#endregion

		#region Constructor
		public ActivityEntryLogger(IConfiguration configuration, string correlationID)
			: base(null, correlationID)
		{
			this.configuration = configuration;
			this.appSettings = new AppSettings(this.configuration);
			this.dalProvider = new DALProvider(this.configuration);
		}

		public ActivityEntryLogger(IConfiguration configuration, IDALProvider dalProvider, string correlationID)
			: base(null, correlationID)
		{
			this.configuration = configuration;
			this.appSettings = new AppSettings(this.configuration);
			this.dalProvider = dalProvider;
		}
		#endregion

		#region Methods
		#region PerformFlush
		protected async override Task PerformFlush()
		{
			if (this.appSettings.LogActivity)
			{
				foreach (ActivityEntry activityEntry in this.ActivityEntries)
				{
					await this.DALProvider.InsertActivityEntry(activityEntry);
				}
			}
		}
		#endregion
		#endregion
	}
}