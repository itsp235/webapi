﻿using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.BusinessRules.Context;
using IOTCell.BL.BusinessRules;
using IOTCell.BL.ClientControllerProvider;
using IOTCell.BL.Diagnostics;
using IOTCell.BL.ViewModels;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;
using IOTCell.Framework.Enums;

namespace IOTCell.BL.Providers
{
    public class ActivationProvider : BLProvider, IActivationProvider
    {
        #region Constructor
        public ActivationProvider(IConfiguration configuration, IActivityEntryLogger logger)
            : base(configuration, logger)
        {
        }
        #endregion

        #region Methods

        //#region ActivateNewCustomer
        //async Task<ResponseDetails> IActivationProvider.ActivateNewCustomer(CustomerViewModel customerViewModel)
        //{
        //    IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
        //    IBusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
        //    ResponseDetails responseDetails = await business.PerformNewCustomerActivationValidation();
        //    if (responseDetails.StatusCode != "200")
        //    {
        //        return responseDetails;
        //    }

        //    Reseller reseller = businessRuleContext.Reseller;
        //    Customer newCustomer = businessRuleContext.Customer;
        //    Item itemPlan = businessRuleContext.ItemPlan;
        //    Item itemSIM = businessRuleContext.ItemSIM;
        //    Inventory inventorySIM = businessRuleContext.InventorySIM;
        //    Carrier carrier = businessRuleContext.Carrier;
        //    Address address = businessRuleContext.Address;

        //    Account newAccount = this.GetNewAccount();
        //    Order newOrder = this.GetNewOrder(customerViewModel, itemPlan);
        //    OrderItem newOrderItemPlan = this.GetNewOrderItem(itemPlan);
        //    OrderItem newOrderItemSIM = this.GetNewOrderItem(itemSIM);

        //    customerViewModel.CompanyMVNO = reseller.Mvno;

        //    await this.dalProvider.BeginTransaction();

        //    //Assign Account number
        //    newCustomer.Accountnumber = this.GetNewAccountNumber(reseller, newCustomer, carrier);

        //    newCustomer.Resellercount = Convert.ToInt64(newCustomer.Accountnumber[6..]);

        //    //Insert record in Customer table
        //    newCustomer = await this.dalProvider.AddNewCustomer(newCustomer);

        //    //Insert record in Account table
        //    newAccount.CustomerId = newCustomer.CustomerId;
        //    newAccount = await this.dalProvider.AddNewAccount(newAccount);

        //    //Insert record in Address table
        //    address.CustomerId = newCustomer.CustomerId;
        //    address = await this.dalProvider.AddNewAddress(address);

        //    //Insert record in Order table
        //    newOrder.AccountId = newAccount.AccountId;
        //    newOrder = await this.dalProvider.AddNewOrder(newOrder);

        //    //Insert record in OrderItem - for plan
        //    newOrderItemPlan.OrderId = newOrder.OrderId;
        //    newOrderItemPlan.ItemId = itemPlan.ItemId;

        //    await this.dalProvider.AddNewOrderItem(newOrderItemPlan);

        //    //Insert record in OrderItem - for SIM
        //    newOrderItemSIM.OrderId = newOrder.OrderId;
        //    newOrderItemSIM.ItemId = itemSIM.ItemId;
        //    await this.dalProvider.AddNewOrderItem(newOrderItemSIM);

        //    NameValueCollection settings = new NameValueCollection()
        //    {
        //        {"ClientName", reseller.ResellerName}
        //        //{"ClientApiKey", clientApiKey.ToString()}
        //    };

        //    //Activate service in iBasis/carrier
        //    ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
        //    IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
        //    provider.Init(this.configuration, this.logger, settings);

        //    //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
        //    //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


        //    ResponseDetails clientData = await provider.ActivateService(customerViewModel);


        //    ActivationResultBody clientDataActivationResultBody = (ActivationResultBody)clientData.ResultResponse;

        //    Result clientDataResult = (Result)clientDataActivationResultBody.Result;

        //    IOTActivationResult iotActivationResult = new IOTActivationResult();

        //    if (clientData.StatusCode == "OK")
        //    {

        //        if (!DateTime.TryParse(clientDataResult.EffectiveDate, out DateTime activeDate))
        //        {
        //            throw new Exception(string.Format("Invalid activation date {0} received from API", clientDataResult.EffectiveDate));
        //        }


        //        //Update Inventory table if any

        //        if (inventorySIM != null)
        //        {
        //            inventorySIM.CustomerId = newCustomer.CustomerId;
        //            inventorySIM.Iccid = clientDataResult.ICCID;
        //            inventorySIM.Status = (int)SIMStatus.Active; //active

        //            inventorySIM.Devicename = customerViewModel.DeviceName;
        //            inventorySIM.Devicetype = customerViewModel.DeviceType;
        //            inventorySIM.DeviceImei = customerViewModel.DeviceIMEI;

        //            inventorySIM.ActiveDate = activeDate;

        //            inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
        //        }


        //        //Update Order with Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1 - active, INventoryID

        //        newOrder.ActiveDate = activeDate;

        //        newOrder.Status = (int)OrderStatus.Active; //active


        //        newOrderItemPlan.BillingStartDt = activeDate.Date;

        //        if (newOrderItemPlan.Billcycle == 30)
        //        {
        //            //monthly billing cycle

        //            newOrderItemPlan.BillingEndDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).AddDays(-1).Date;
        //            newOrderItemPlan.NextBillingDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).Date;
        //        }
        //        else if (newOrderItemPlan.Billcycle == 365)
        //        {
        //            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).AddDays(-1).Date;
        //            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).Date;
        //        }
        //        else
        //            throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));

        //        await this.dalProvider.UpdateOrderItem(newOrderItemPlan);


        //        if (inventorySIM != null)
        //        {
        //            newOrderItemSIM.InventoryId = inventorySIM.InventoryId;
        //        }

        //        await this.dalProvider.UpdateOrderItem(newOrderItemSIM);

        //        await this.dalProvider.UpdateOrder(newOrder);

        //        await this.dalProvider.CommitTransaction();

        //        iotActivationResult.AccountNumber = newCustomer.Accountnumber;
        //        iotActivationResult.EffectiveDate = activeDate.ToString("G");
        //        if (clientDataResult != null)
        //            iotActivationResult.ICCID = clientDataResult.ICCID;
        //        if (clientDataResult != null)
        //            iotActivationResult.Plan = clientDataResult.Plan;
        //        iotActivationResult.Message = "Successful activation";
        //        return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResult);
        //    }
        //    else
        //    {
        //        await this.dalProvider.RollBackTransaction();

        //        iotActivationResult.Message = "Issue in API - " + clientData.Message;
        //        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult);
        //    }
        //}
        //#endregion

        #region ActivateDevice
        async Task<ResponseDetails> IActivationProvider.ActivateDevice(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformExistingCustomerActivationValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Customer customer = businessRuleContext.Customer;
            Item itemPlan = businessRuleContext.ItemPlan;
            Item itemSIM = businessRuleContext.ItemSIM;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            customerViewModel.PlanCode = itemPlan.ItemCode;

            //            Account account = await this.dalProvider.GetAccountByCustomerID(customer.CustomerId);
            //Account account = await this.dalProvider.GetAccountByResellerID(reseller.ResellerId);

            Account account = businessRuleContext.Account;

            //ResponseDetails responseDetails1 = await business.PerformAccountValidation();
            //if (responseDetails.StatusCode != "200")
            //{
            //    return responseDetails1;
            //}


            Order newOrder = this.GetNewOrder(customerViewModel, itemPlan);
            OrderItem newOrderItemPlan = this.GetNewOrderItem(itemPlan);
            OrderItem newOrderItemSIM = this.GetNewOrderItem(itemSIM);

            customerViewModel.CompanyMVNO = reseller.Mvno; //this will change to get it from reseller extension
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;

            await this.dalProvider.BeginTransaction();

            //Insert record in Order table
            newOrder.AccountId = account.AccountId;
            newOrder = await this.dalProvider.AddNewOrder(newOrder);

            //Insert record in OrderItem - for plan
            newOrderItemPlan.OrderId = newOrder.OrderId;
            newOrderItemPlan.ItemId = itemPlan.ItemId;
            await this.dalProvider.AddNewOrderItem(newOrderItemPlan);



            ////Insert record in OrderItem - for SIM
            //newOrderItemSIM.OrderId = newOrder.OrderId;
            //newOrderItemSIM.ItemId = itemSIM.ItemId;
            //await this.dalProvider.AddNewOrderItem(newOrderItemSIM);



            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };

            //Activate service in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.ActivateService(customerViewModel);


            
            if (!this.appSettings.MultiCarrier)
            {



                ActivationResultBody clientDataActivationResultBody = (ActivationResultBody)clientData.ResultResponse;

                Result clientDataResult = (Result)clientDataActivationResultBody.Result;

                IOTActivationResult iotActivationResult = new IOTActivationResult();

                if (clientData.StatusCode == "OK")
                {

                    if (!DateTime.TryParse(clientDataResult.EffectiveDate, out DateTime activeDate))
                    {
                        throw new Exception(string.Format("Invalid activation date {0} received from API", clientDataResult.EffectiveDate));
                    }


                    //Update Inventory table if any
                    if (inventorySIM != null)
                    {
                        //inventorySIM.CustomerId = customer.CustomerId;
                        inventorySIM.ResellerId = reseller.ResellerId;
                        inventorySIM.Iccid = clientDataResult.ICCID;
                        inventorySIM.Status = (int)SIMStatus.Active; //active

                        inventorySIM.Devicename = customerViewModel.DeviceName;
                        inventorySIM.Devicetype = customerViewModel.DeviceType;
                        inventorySIM.DeviceImei = customerViewModel.DeviceIMEI;

                        inventorySIM.ActiveDate = activeDate;

                        inventorySIM.ItemPlanId = (int?)itemPlan.ItemId;

                        inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
                    }


                    //Update Order and OrderItem with Active_Date, Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1, INventoryID
                    newOrder.ActiveDate = activeDate;

                    newOrderItemPlan.InventoryId = inventorySIM.InventoryId;


                    newOrderItemPlan.BillingStartDt = activeDate.Date;

                    if (newOrderItemPlan.Billcycle == 30)
                    {
                        //monthly billing cycle

                        newOrderItemPlan.BillingEndDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).AddDays(-1).Date;
                        newOrderItemPlan.NextBillingDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).Date;
                    }
                    else if (newOrderItemPlan.Billcycle == 365)
                    {
                        newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).AddDays(-1).Date;
                        newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).Date;
                    }
                    else
                        throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));

                    await this.dalProvider.UpdateOrderItem(newOrderItemPlan);

                    newOrder.Status = (int)OrderStatus.Active; //active

                    if (inventorySIM != null)
                    {
                        newOrderItemSIM.InventoryId = inventorySIM.InventoryId;
                    }


//                    await this.dalProvider.UpdateOrderItem(newOrderItemSIM);
                    await this.dalProvider.UpdateOrder(newOrder);
                    await this.dalProvider.CommitTransaction();

                    //iotActivationResult.AccountNumber = customer.Accountnumber;
                    iotActivationResult.AccountNumber = reseller.AccountNumber;
                    iotActivationResult.EffectiveDate = activeDate.ToString("G");

                    if (clientDataResult != null)
                    {
                        iotActivationResult.ICCID = clientDataResult.ICCID;
                    }

                    if (clientDataResult != null)
                    {
                        iotActivationResult.Plan = clientDataResult.Plan;
                    }

                    iotActivationResult.Message = "Successful activation";
                    return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResult);
                }
                else
                {
                    await this.dalProvider.RollBackTransaction();

                    iotActivationResult.Message = "Issue in API - " + clientData.Message;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult);
                }
            }
            else 
            {
                if (carrier.CarrierName == "iBasis")
                {
                    ActivationResultBody clientDataActivationResultBody = (ActivationResultBody)clientData.ResultResponse;

                    Result clientDataResult = (Result)clientDataActivationResultBody.Result;

                    IOTActivationResult1 iotActivationResult = new IOTActivationResult1();

                    if (clientData.StatusCode == "OK")
                    {

                        if (!DateTime.TryParse(clientDataResult.EffectiveDate, out DateTime activeDate))
                        {
                            throw new Exception(string.Format("Invalid activation date {0} received from API", clientDataResult.EffectiveDate));
                        }


                        //Update Inventory table if any
                        if (inventorySIM != null)
                        {
                            //inventorySIM.CustomerId = customer.CustomerId;
                            inventorySIM.ResellerId = reseller.ResellerId;
                            inventorySIM.Iccid = clientDataResult.ICCID;
                            inventorySIM.Status = (int)SIMStatus.Active; //active

                            inventorySIM.Devicename = customerViewModel.DeviceName;
                            inventorySIM.Devicetype = customerViewModel.DeviceType;
                            inventorySIM.DeviceImei = customerViewModel.DeviceIMEI;

                            inventorySIM.ActiveDate = activeDate;

                            inventorySIM.ItemPlanId = (int?)itemPlan.ItemId;

                            inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
                        }


                        //Update Order and OrderItem with Active_Date, Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1, INventoryID
                        newOrder.ActiveDate = activeDate;

                        newOrderItemPlan.InventoryId = inventorySIM.InventoryId;

                        newOrderItemPlan.BillingStartDt = activeDate.Date;

                        if (newOrderItemPlan.Billcycle == 30)
                        {
                            //monthly billing cycle

                            newOrderItemPlan.BillingEndDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).AddDays(-1).Date;
                            newOrderItemPlan.NextBillingDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).Date;
                        }
                        else if (newOrderItemPlan.Billcycle == 365)
                        {
                            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).AddDays(-1).Date;
                            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).Date;
                        }
                        else
                            throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));

                        await this.dalProvider.UpdateOrderItem(newOrderItemPlan);

                        newOrder.Status = (int)OrderStatus.Active; //active

                        if (inventorySIM != null)
                        {
                            newOrderItemSIM.InventoryId = inventorySIM.InventoryId;
                        }


//                        await this.dalProvider.UpdateOrderItem(newOrderItemSIM);
                        await this.dalProvider.UpdateOrder(newOrder);
                        await this.dalProvider.CommitTransaction();

                        //iotActivationResult.AccountNumber = customer.Accountnumber;
                        iotActivationResult.AccountNumber = reseller.AccountNumber;
                        iotActivationResult.EffectiveDate = activeDate.ToString("G");

                        if (clientDataResult != null)
                        {
                            iotActivationResult.ICCID = clientDataResult.ICCID;
                        }

                        if (clientDataResult != null)
                        {
                            iotActivationResult.Plan = clientDataResult.Plan;
                        }

                        iotActivationResult.Message = "Successful activation";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResult);
                    }
                    else
                    {
                        await this.dalProvider.RollBackTransaction();

                        iotActivationResult.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                {
                    ResultFloLiveBody clientDataActivationResultBody = (ResultFloLiveBody)clientData.ResultResponse;
                    IOTActivationResult1 iotActivationResult1 = new IOTActivationResult1();

                    DateTime activeDate = DateTime.Now;

                    if (clientData.StatusCode == "OK")
                    {

                        //Update Inventory table if any
                        if (inventorySIM != null)
                        {
                            //inventorySIM.CustomerId = customer.CustomerId;
                            inventorySIM.ResellerId = reseller.ResellerId;
                            inventorySIM.Iccid = customerViewModel.SimIdentifierValue;
                            inventorySIM.Status = (int)SIMStatus.Active; //active


                            inventorySIM.Devicename = customerViewModel.DeviceName;
                            inventorySIM.Devicetype = customerViewModel.DeviceType;
                            inventorySIM.DeviceImei = customerViewModel.DeviceIMEI;

                            inventorySIM.ActiveDate = activeDate;

                            inventorySIM.ItemPlanId = (int?)itemPlan.ItemId;

                            inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
                        }


                        //Update Order and OrderItem with Active_Date, Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1, INventoryID
                        newOrder.ActiveDate = activeDate;

                        newOrderItemPlan.InventoryId = inventorySIM.InventoryId;

                        newOrderItemPlan.BillingStartDt = activeDate.Date;

                        if (newOrderItemPlan.Billcycle == 30)
                        {
                            //monthly billing cycle

                            newOrderItemPlan.BillingEndDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).AddDays(-1).Date;
                            newOrderItemPlan.NextBillingDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).Date;
                        }
                        else if (newOrderItemPlan.Billcycle == 365)
                        {
                            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).AddDays(-1).Date;
                            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).Date;
                        }
                        else
                            throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));

                        await this.dalProvider.UpdateOrderItem(newOrderItemPlan);

                        newOrder.Status = (int)OrderStatus.Active; //active

                        if (inventorySIM != null)
                        {
                            newOrderItemSIM.InventoryId = inventorySIM.InventoryId;
                        }


 //                       await this.dalProvider.UpdateOrderItem(newOrderItemSIM);
                        await this.dalProvider.UpdateOrder(newOrder);
                        await this.dalProvider.CommitTransaction();

                        //iotActivationResult.AccountNumber = customer.Accountnumber;
                        iotActivationResult1.AccountNumber = reseller.AccountNumber;
                        iotActivationResult1.EffectiveDate = activeDate.ToString("G");

                        iotActivationResult1.ICCID = customerViewModel.SimIdentifierValue;


                        iotActivationResult1.Plan = customerViewModel.PlanName;
                        iotActivationResult1.RequestID = clientDataActivationResultBody.Content[0].RequestId;


                        iotActivationResult1.Message = "Successful activation";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResult1);
                    }
                    else
                    {
                        await this.dalProvider.RollBackTransaction();

                        iotActivationResult1.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                    }
                }
                else
                {
                    IOTActivationResult1 iotActivationResult1 = new IOTActivationResult1();
                    await this.dalProvider.RollBackTransaction();
                    iotActivationResult1.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                }
            }
        }
        #endregion

        #region ActivateServiceNoDB
        async Task<ResponseDetails> IActivationProvider.ActivateServiceNoDB(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMChangePlanValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Item itemPlan = businessRuleContext.ItemPlan;
            Item itemSIM = businessRuleContext.ItemSIM;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            customerViewModel.PlanCode = itemPlan.ItemCode;
            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };

            //Activate service in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.ActivateService(customerViewModel);

            if (!this.appSettings.MultiCarrier)
            {

                ActivationResultBody clientDataActivationResultBody = (ActivationResultBody)clientData.ResultResponse;

                Result clientDataResult = (Result)clientDataActivationResultBody.Result;

                IOTActivationResultNoDB iotActivationResultNoDB = new IOTActivationResultNoDB();

                if (clientData.StatusCode == "OK")
                {
                    if (!DateTime.TryParse(clientDataResult.EffectiveDate, out DateTime activeDate))
                    {
                        throw new Exception(string.Format("Invalid activation date {0} received from API", clientDataResult.EffectiveDate));
                    }

                    iotActivationResultNoDB.EffectiveDate = activeDate.ToString("G");

                    if (clientDataResult != null)
                    {
                        iotActivationResultNoDB.ICCID = clientDataResult.ICCID;
                    }

                    if (clientDataResult != null)
                    {
                        iotActivationResultNoDB.Plan = clientDataResult.Plan;
                    }

                    iotActivationResultNoDB.Message = "Successful activation";
                    return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResultNoDB);
                }
                else
                {

                    iotActivationResultNoDB.Message = "Issue in API - " + clientData.Message;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResultNoDB);
                }
            }
            else
            {

                if (carrier.CarrierName == "iBasis")
                {
                    ActivationResultBody clientDataActivationResultBody = (ActivationResultBody)clientData.ResultResponse;

                    Result clientDataResult = (Result)clientDataActivationResultBody.Result;

                    IOTActivationResult1 iotActivationResultNoDB = new IOTActivationResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        if (!DateTime.TryParse(clientDataResult.EffectiveDate, out DateTime activeDate))
                        {
                            throw new Exception(string.Format("Invalid activation date {0} received from API", clientDataResult.EffectiveDate));
                        }

                        iotActivationResultNoDB.EffectiveDate = activeDate.ToString("G");

                        if (clientDataResult != null)
                        {
                            iotActivationResultNoDB.ICCID = clientDataResult.ICCID;
                        }

                        if (clientDataResult != null)
                        {
                            iotActivationResultNoDB.Plan = clientDataResult.Plan;
                        }

                        iotActivationResultNoDB.Message = "Successful activation";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResultNoDB);
                    }
                    else
                    {

                        iotActivationResultNoDB.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResultNoDB);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                { 
                    ResultFloLiveBody clientDataActivationResultBody = (ResultFloLiveBody)clientData.ResultResponse;

                    //                Result clientDataResult = (Result)clientDataActivationResultBody. .Result;

                    IOTActivationResult1 iotActivationResult1 = new IOTActivationResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        iotActivationResult1.RequestID = clientDataActivationResultBody.Content[0].RequestId;

                        iotActivationResult1.Message = "Successful activation";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotActivationResult1);
                    }
                    else
                    {

                        iotActivationResult1.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                    }
                }
                else
                {
                    IOTActivationResult1 iotActivationResult1 = new IOTActivationResult1();
                    iotActivationResult1.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                }
            }
        }
        #endregion

        #endregion
    }
}