﻿using System.Threading.Tasks;

using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;

namespace IOTCell.BL.Providers
{
    public interface IActivationProvider : IBLProvider
    {
        //public Task<ResponseDetails> ActivateNewCustomer(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> ActivateDevice(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> ActivateServiceNoDB(CustomerViewModel customerViewModel);
    }
}