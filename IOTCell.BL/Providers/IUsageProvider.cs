﻿using System.Threading.Tasks;

using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;

namespace IOTCell.BL.Providers
{
    public interface IUsageProvider : IBLProviderBase
    {
        public Task<ResponseDetails> GetUsage(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetTotalUsage(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetCalculatedUsage(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetAvailableServicePlans(CustomerViewModel customerViewModel);
    }
}