﻿using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.Diagnostics;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;
using IOTCell.Framework.Security;

namespace IOTCell.BL.Providers
{
    public class TokenProvider : BLProvider, ITokenProvider
    {
        #region Constructor
        public TokenProvider(IConfiguration configuration, IActivityEntryLogger logger)
            : base(configuration, logger)
        {
        }
        #endregion

        #region Methods
        #region Authenticate
        async Task<ResponseDetails> ITokenProvider.Authenticate(AuthenticationDetails authenticationDetails)
        {
            Reseller reseller = await this.dalProvider.GetResellerByResellerCode(authenticationDetails.CompanyCode);
            if (reseller != null)
            {
                ResellerExtension resellerExtension = await this.dalProvider.GetResellerExtensionByResellerCode(authenticationDetails.CompanyCode);
                if (resellerExtension != null)
                {
                    //string databaseUser = this.appSettings.GetDecryptedValue("dVNjVE9QTlZ3ck9pNFM1TG1VK2hwZz09");
                    //string password = this.appSettings.GetDecryptedValue("dVNjVE9QTlZ3ck9pNFM1TG1VK2hwZz09");

                    //string encryptedDatabaseUser = this.appSettings.GetEncryptedValue("waivout");
                    //string encryptedDatabasePassword = this.appSettings.GetEncryptedValue("X5Pfd3NeL9");

                    string userName = this.appSettings.GetDecodedValue(resellerExtension.ResellerUserName);
                    string password = this.appSettings.GetDecodedValue(resellerExtension.ResellerPassword);


                    // Validate User Name and Password
                    if (userName != authenticationDetails.UserName ||
                        password != authenticationDetails.Password)
                    {
                        return CommonHelper.GetResponseDetails("401", "Unauthorized", null);
                    }

                    IJWTAuthenticationManager jwtAuthenticationManager = new JWTAuthenticationManager(this.configuration, this.appSettings.TokenSecretKey);
                    TokenDetails tokenDetails = jwtAuthenticationManager.Authenticate(authenticationDetails);

                    if (tokenDetails.AccessToken == null)
                    {
                        return CommonHelper.GetResponseDetails("401", "Unauthorized", tokenDetails);
                    }

                    return CommonHelper.GetResponseDetails("200", "Ok", tokenDetails);
                }
                else
                {
                    return CommonHelper.GetResponseDetails("401", "Unauthorized", null);
                }
            }
            else
            {
                return CommonHelper.GetResponseDetails("401", "Unauthorized", null);
            }
        }
        #endregion
        #endregion
    }
}