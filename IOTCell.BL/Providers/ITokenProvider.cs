﻿using System.Threading.Tasks;

using IOTCell.Framework.Models;

namespace IOTCell.BL.Providers
{
    public interface ITokenProvider : IBLProviderBase
    {
        public Task<ResponseDetails> Authenticate(AuthenticationDetails authenticationDetails);
    }
}