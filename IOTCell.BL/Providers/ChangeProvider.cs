﻿using System;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Linq;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.BusinessRules.Context;
using IOTCell.BL.BusinessRules;
using IOTCell.BL.ClientControllerProvider;
using IOTCell.BL.Diagnostics;
using IOTCell.BL.ViewModels;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;
using IOTCell.Framework.Enums;

namespace IOTCell.BL.Providers
{
    public class ChangeProvider : BLProvider, IChangeProvider
    {
        #region Constructor
        public ChangeProvider(IConfiguration configuration, IActivityEntryLogger logger)
            : base(configuration, logger)
        {
        }
        #endregion

        #region Methods

        #region TerminateService
        async Task<ResponseDetails> IChangeProvider.TerminateService(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMChangeValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            _ = businessRuleContext.ItemPlan;
            _ = businessRuleContext.ItemSIM;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            //if (carrier.UsesTerminateServiceMethod != 1)
            //    return CommonHelper.GetResponseDetails("404", "Method not available for this carrier - " + carrier.CarrierName);


            OrderItem orderItemSIM = await this.dalProvider.GetOrderItemByInventoryID(inventorySIM.InventoryId);
            Order order = await this.dalProvider.GetOrderByOrderID(orderItemSIM.OrderId);
            //OrderItem orderItemPlan = await this.dalProvider.GetOrderItemPlanByOrderID(order.OrderId);
            OrderItem orderItemPlan = orderItemSIM;

            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };


            //Terminate service in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);

            ResponseDetails clientData = await provider.TerminateService(customerViewModel);

            if (!this.appSettings.MultiCarrier)
            {

                ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                IOTTerminateServiceResult iotTerminateServiceResult = new IOTTerminateServiceResult();

                if (clientData.StatusCode == "OK")
                {

                    await this.dalProvider.BeginTransaction();
                    //update inventory table
                    if (inventorySIM != null)
                    {
                        inventorySIM.Status = (int)SIMStatus.Cancelled_Inactive;
                        _ = await this.dalProvider.UpdateInventory(inventorySIM);
                    }

                    //update orderItem table
                    orderItemPlan.Status = (int)OrderStatus.Inactive;
                    orderItemPlan.NextBillingDt = null;    //JW request by Zhao
                    _ = await this.dalProvider.UpdateOrderItem(orderItemPlan);

                    orderItemSIM.Status = (int)SIMStatus.Cancelled_Inactive;
                    _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);


                    //update order table
                    order.Status = (int)OrderStatus.Inactive;
                    _ = await this.dalProvider.UpdateOrder(order);

                    //end transaction
                    await this.dalProvider.CommitTransaction();

                    iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotTerminateServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                    iotTerminateServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                    iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                    iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                    return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);
                }
                else
                {
                    iotTerminateServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                }
            }
            else
            {
                if (carrier.CarrierName == "iBasis")
                {
                    ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                    IOTSuspendServiceResult1 iotTerminateServiceResult = new IOTSuspendServiceResult1();

                    if (clientData.StatusCode == "OK")
                    {

                        await this.dalProvider.BeginTransaction();
                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Cancelled_Inactive;
                            _ = await this.dalProvider.UpdateInventory(inventorySIM);
                        }

                        //update orderItem table
                        orderItemPlan.Status = (int)OrderStatus.Inactive;
                        orderItemPlan.NextBillingDt = null;    //JW request by Zhao
                        _ = await this.dalProvider.UpdateOrderItem(orderItemPlan);

                        orderItemSIM.Status = (int)SIMStatus.Cancelled_Inactive;
                        _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);


                        //update order table
                        order.Status = (int)OrderStatus.Inactive;
                        _ = await this.dalProvider.UpdateOrder(order);

                        //end transaction
                        await this.dalProvider.CommitTransaction();

                        iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                        iotTerminateServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                        iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                        iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                        return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);
                    }
                    else
                    {
                        iotTerminateServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                {


                    ResultFloLiveBody clientDataActivationResultBody = (ResultFloLiveBody)clientData.ResultResponse;

                    IOTSuspendServiceResult1 iotSuspendServiceResult1 = new IOTSuspendServiceResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        await this.dalProvider.BeginTransaction();
                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Cancelled_Inactive;
                            _ = await this.dalProvider.UpdateInventory(inventorySIM);
                        }

                        //update orderItem table 
                        orderItemSIM.Status = (int)SIMStatus.Cancelled_Inactive;
                        _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                        //update order table - not needed

                        //end transaction
                        await this.dalProvider.CommitTransaction();


                        iotSuspendServiceResult1.RequestID = clientDataActivationResultBody.Content[0].RequestId;

                        iotSuspendServiceResult1.Message = "SIM was cancelled (plan was detached)";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotSuspendServiceResult1);
                    }
                    else
                    {

                        iotSuspendServiceResult1.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotSuspendServiceResult1);
                    }
                }
                else
                {
                    IOTSuspendServiceResult1 iotSuspendServiceResult1 = new IOTSuspendServiceResult1();
                    iotSuspendServiceResult1.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotSuspendServiceResult1);
                }

            }
        }
        #endregion


        #region SuspendService
        async Task<ResponseDetails> IChangeProvider.SuspendService(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMChangeValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            _ = businessRuleContext.ItemPlan;
            _ = businessRuleContext.ItemSIM;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            OrderItem orderItemSIM = await this.dalProvider.GetOrderItemByInventoryID(inventorySIM.InventoryId);
            _ = await this.dalProvider.GetOrderByOrderID(orderItemSIM.OrderId);

            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };


            //Suspend service in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.SuspendService(customerViewModel);

            if (!this.appSettings.MultiCarrier)
            {


                ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                IOTSuspendServiceResult iotSuspendServiceResult = new IOTSuspendServiceResult();

                if (clientData.StatusCode == "OK")
                {

                    await this.dalProvider.BeginTransaction();
                    //update inventory table
                    if (inventorySIM != null)
                    {
                        inventorySIM.Status = (int)SIMStatus.Suspended_Inactive;
                        _ = await this.dalProvider.UpdateInventory(inventorySIM);
                    }

                    //update orderItem table 
                    orderItemSIM.Status = (int)SIMStatus.Suspended_Inactive;
                    _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                    //update order table - not needed

                    //end transaction
                    await this.dalProvider.CommitTransaction();

                    iotSuspendServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotSuspendServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotSuspendServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                    iotSuspendServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                    iotSuspendServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                    iotSuspendServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                    return CommonHelper.GetResponseDetails("200", "Ok", iotSuspendServiceResult);
                }
                else
                {
                    iotSuspendServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotSuspendServiceResult);
                }
            }
            else 
            {
                if (carrier.CarrierName == "iBasis")
                {
                    ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                    IOTSuspendServiceResult1 iotSuspendServiceResult = new IOTSuspendServiceResult1();

                    if (clientData.StatusCode == "OK")
                    {

                        await this.dalProvider.BeginTransaction();
                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Suspended_Inactive;
                            _ = await this.dalProvider.UpdateInventory(inventorySIM);
                        }

                        //update orderItem table 
                        orderItemSIM.Status = (int)SIMStatus.Suspended_Inactive;
                        _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                        //update order table - not needed

                        //end transaction
                        await this.dalProvider.CommitTransaction();

                        iotSuspendServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotSuspendServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotSuspendServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                        iotSuspendServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                        iotSuspendServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                        iotSuspendServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                        return CommonHelper.GetResponseDetails("200", "Ok", iotSuspendServiceResult);
                    }
                    else
                    {
                        iotSuspendServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotSuspendServiceResult);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                {

                    ResultFloLiveBody clientDataActivationResultBody = (ResultFloLiveBody)clientData.ResultResponse;

                    //                Result clientDataResult = (Result)clientDataActivationResultBody. .Result;

                    IOTSuspendServiceResult1 iotSuspendServiceResult1 = new IOTSuspendServiceResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        await this.dalProvider.BeginTransaction();
                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Suspended_Inactive;
                            _ = await this.dalProvider.UpdateInventory(inventorySIM);
                        }

                        //update orderItem table 
                        orderItemSIM.Status = (int)SIMStatus.Suspended_Inactive;
                        _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                        //update order table - not needed

                        //end transaction
                        await this.dalProvider.CommitTransaction();


                        iotSuspendServiceResult1.RequestID = clientDataActivationResultBody.Content[0].RequestId;

                        iotSuspendServiceResult1.Message = "SIM was suspended";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotSuspendServiceResult1);
                    }
                    else
                    {

                        iotSuspendServiceResult1.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotSuspendServiceResult1);
                    }
                }
                else
                {
                    IOTSuspendServiceResult1 iotActivationResult1 = new IOTSuspendServiceResult1();
                    iotActivationResult1.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                }

            }
        }
        #endregion


        #region ResumeService
        async Task<ResponseDetails> IChangeProvider.ResumeService(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMChangeValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            _ = businessRuleContext.ItemPlan;
            _ = businessRuleContext.ItemSIM;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            OrderItem orderItemSIM = await this.dalProvider.GetOrderItemByInventoryID(inventorySIM.InventoryId);
            _ = await this.dalProvider.GetOrderByOrderID(orderItemSIM.OrderId);

            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };


            //Resume service in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.ResumeService(customerViewModel);

            if (!this.appSettings.MultiCarrier)
            {
                ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                IOTTerminateServiceResult iotTerminateServiceResult = new IOTTerminateServiceResult();

                if (clientData.StatusCode == "OK")
                {

                    await this.dalProvider.BeginTransaction();
                    //update inventory table
                    if (inventorySIM != null)
                    {
                        inventorySIM.Status = (int)SIMStatus.Active;
                        _ = await this.dalProvider.UpdateInventory(inventorySIM);
                    }

                    //update orderItem table
                    orderItemSIM.Status = (int)SIMStatus.Active;
                    _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                    ////update order table - not needed
                    //order.Status = (int)ServiceStatus.Inactive;
                    //order = await this.dalProvider.UpdateOrder(order);

                    //end transaction
                    await this.dalProvider.CommitTransaction();

                    iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotTerminateServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                    iotTerminateServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                    iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                    iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                    return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);
                }
                else
                {
                    iotTerminateServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                }
            }
            else
            {
                if (carrier.CarrierName == "iBasis")
                {
                    ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                    IOTSuspendServiceResult1 iotTerminateServiceResult = new IOTSuspendServiceResult1();

                    if (clientData.StatusCode == "OK")
                    {

                        await this.dalProvider.BeginTransaction();
                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Active;
                            _ = await this.dalProvider.UpdateInventory(inventorySIM);
                        }

                        //update orderItem table
                        orderItemSIM.Status = (int)SIMStatus.Active;
                        _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                        ////update order table - not needed
                        //order.Status = (int)ServiceStatus.Inactive;
                        //order = await this.dalProvider.UpdateOrder(order);

                        //end transaction
                        await this.dalProvider.CommitTransaction();

                        iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                        iotTerminateServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                        iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                        iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                        return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);
                    }
                    else
                    {
                        iotTerminateServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                {
                    ResultFloLiveBody clientDataActivationResultBody = (ResultFloLiveBody)clientData.ResultResponse;

                    //                Result clientDataResult = (Result)clientDataActivationResultBody. .Result;

                    IOTSuspendServiceResult1 iotSuspendServiceResult1 = new IOTSuspendServiceResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        await this.dalProvider.BeginTransaction();
                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Active;
                            _ = await this.dalProvider.UpdateInventory(inventorySIM);
                        }

                        //update orderItem table
                        orderItemSIM.Status = (int)SIMStatus.Active;
                        _ = await this.dalProvider.UpdateOrderItem(orderItemSIM);

                        ////update order table - not needed
                        //order.Status = (int)ServiceStatus.Inactive;
                        //order = await this.dalProvider.UpdateOrder(order);

                        //end transaction
                        await this.dalProvider.CommitTransaction();




                        iotSuspendServiceResult1.RequestID = clientDataActivationResultBody.Content[0].RequestId;

                        iotSuspendServiceResult1.Message = "SIM is active";
                        return CommonHelper.GetResponseDetails("200", "Ok", iotSuspendServiceResult1);
                    }
                    else
                    {

                        iotSuspendServiceResult1.Message = "Issue in API - " + clientData.Message;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotSuspendServiceResult1);
                    }
                }
                else
                {
                    IOTSuspendServiceResult1 iotActivationResult1 = new IOTSuspendServiceResult1();
                    iotActivationResult1.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                }

            }
        }
        #endregion


        #region ChangeServicePlan
        async Task<ResponseDetails> IChangeProvider.ChangeServicePlan(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMChangePlanValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Item itemPlan = businessRuleContext.ItemPlan;
            Item itemSIM = businessRuleContext.ItemSIM;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Customer customer = businessRuleContext.Customer;
            Carrier carrier = businessRuleContext.Carrier;

            OrderItem orderItemSIM = await this.dalProvider.GetOrderItemByInventoryID(inventorySIM.InventoryId);
            Order order = await this.dalProvider.GetOrderByOrderID(orderItemSIM.OrderId);
//            OrderItem orderItemPlan = await this.dalProvider.GetOrderItemPlanByOrderID(order.OrderId);


            OrderItem orderItemPlan = orderItemSIM;

            customerViewModel.PlanCode = itemPlan.ItemCode;
            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };


            customerViewModel.FromDate = DateTime.Parse(orderItemPlan.BillingStartDt.ToString()).ToString("yyyy-MM-dd");
            customerViewModel.ToDate = DateTime.Now.ToString("yyyy-MM-dd");

            //get usage unbilled up to now in Carrier's APIs

            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientDataUsage = await provider.GetUsage(customerViewModel);


            if (!this.appSettings.MultiCarrier)
            {
                UsageResultBody clientDataUsageResultBody = (UsageResultBody)clientDataUsage.ResultResponse;

                IOTTerminateServiceResult iotTerminateServiceResult = new IOTTerminateServiceResult();

                if (clientDataUsage.StatusCode != "OK")
                {
                    iotTerminateServiceResult.Message = "Issue in API - " + clientDataUsageResultBody.Messages[0];
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                }



                await this.dalProvider.BeginTransaction();
                //record usage in database and flag old order as PlanChanged

                //update order table for old order record with status and usage up to date
                order.Status = (int)OrderStatus.PlanChanged;

                order = await this.dalProvider.UpdateOrder(order);

                //change service in carrier

                ResponseDetails clientData = await provider.ChangeServicePlan(customerViewModel);

                ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;


                if (clientData.StatusCode == "OK")
                {

                    if (!DateTime.TryParse(clientDataTerminateServiceResultBody.EffectiveDate, out DateTime effectiveDate))
                    {
                        throw new Exception(string.Format("Invalid change service effective date {0} received from API", clientDataTerminateServiceResultBody.EffectiveDate));
                    }


                    //update inventory table
                    if (inventorySIM != null)
                    {
                        inventorySIM.Status = (int)SIMStatus.Active;
                        inventorySIM.ActiveDate = effectiveDate;
                        inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
                    }


                    //update orderItem table for old orderItems 
                    orderItemSIM.Status = (int)SIMStatus.PlanChanged;
                    orderItemSIM = await this.dalProvider.UpdateOrderItem(orderItemSIM);
                    orderItemPlan.Status = (int)OrderStatus.PlanChanged;
                    orderItemPlan.DataUsageWhenPlanChanged = clientDataUsageResultBody.Usages.Sum(item => item.Volume);
                    orderItemPlan.BillingEndDt = DateTime.Now.AddDays(-1);      //JW added. Request by Zhao
                    orderItemPlan = await this.dalProvider.UpdateOrderItem(orderItemPlan);



                    //add new order for new plan
                    //Account account = await this.dalProvider.GetAccountByCustomerID(customer.CustomerId);
                    Account account = await this.dalProvider.GetAccountByResellerID(reseller.ResellerId);

                    Order newOrder = this.GetNewOrder(customerViewModel, itemPlan);
                    OrderItem newOrderItemPlan = this.GetNewOrderItem(itemPlan);
                    OrderItem newOrderItemSIM = this.GetNewOrderItem(itemSIM);

                    //Insert new record in Order table
                    newOrder.AccountId = account.AccountId;
                    newOrder = await this.dalProvider.AddNewOrder(newOrder);

                    //Insert new record in OrderItem - for plan
                    newOrderItemPlan.OrderId = newOrder.OrderId;
                    newOrderItemPlan.ItemId = itemPlan.ItemId;

                    newOrderItemPlan.BillingStartDt = effectiveDate.Date;

                    if (newOrderItemPlan.Billcycle == 30)
                    {
                        //monthly billing cycle

                        newOrderItemPlan.BillingEndDt = new DateTime(effectiveDate.Year, effectiveDate.Month, 1).AddMonths(1).AddDays(-1).Date;
                        newOrderItemPlan.NextBillingDt = new DateTime(effectiveDate.Year, effectiveDate.Month, 1).AddMonths(1).Date;
                    }
                    else if (newOrderItemPlan.Billcycle == 365)
                    {
                        newOrderItemPlan.BillingEndDt = effectiveDate.AddYears(1).AddDays(-1).Date;
                        newOrderItemPlan.BillingEndDt = effectiveDate.AddYears(1).Date;
                    }
                    else
                        throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));



                    await this.dalProvider.AddNewOrderItem(newOrderItemPlan);

                    //Insert record in OrderItem - for SIM
                    newOrderItemSIM.OrderId = newOrder.OrderId;
                    newOrderItemSIM.ItemId = itemSIM.ItemId;
 //                   await this.dalProvider.AddNewOrderItem(newOrderItemSIM);




                    //Update Order with Active_Date, Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1, INventoryID

                    newOrder.ActiveDate = effectiveDate;

                    newOrder.Status = (int)OrderStatus.Active; //active




                    //end transaction
                    await this.dalProvider.CommitTransaction();

                    iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                    iotTerminateServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                    iotTerminateServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                    iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                    iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                    return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);
                }
                else
                {
                    await this.dalProvider.RollBackTransaction();

                    iotTerminateServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                }

            }
            else
            {

                if (carrier.CarrierName == "iBasis")
                {
                    UsageResultBody clientDataUsageResultBody = (UsageResultBody)clientDataUsage.ResultResponse;

                    IOTTerminateServiceResult1 iotTerminateServiceResult = new IOTTerminateServiceResult1();

                    if (clientDataUsage.StatusCode != "OK")
                    {
                        iotTerminateServiceResult.Message = "Issue in API - " + clientDataUsageResultBody.Messages[0];
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                    }



                    await this.dalProvider.BeginTransaction();
                    //record usage in database and flag old order as PlanChanged

                    //update order table for old order record with status and usage up to date
                    order.Status = (int)OrderStatus.PlanChanged;

                    order = await this.dalProvider.UpdateOrder(order);

                    //change service in carrier

                    ResponseDetails clientData = await provider.ChangeServicePlan(customerViewModel);

                    ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;


                    if (clientData.StatusCode == "OK")
                    {

                        if (!DateTime.TryParse(clientDataTerminateServiceResultBody.EffectiveDate, out DateTime effectiveDate))
                        {
                            throw new Exception(string.Format("Invalid change service effective date {0} received from API", clientDataTerminateServiceResultBody.EffectiveDate));
                        }


                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Active;
                            inventorySIM.ActiveDate = effectiveDate;
                            inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
                        }


                        //update orderItem table for old orderItems 
                        orderItemSIM.Status = (int)SIMStatus.PlanChanged;
                        orderItemSIM = await this.dalProvider.UpdateOrderItem(orderItemSIM);
                        orderItemPlan.Status = (int)OrderStatus.PlanChanged;
                        orderItemPlan.DataUsageWhenPlanChanged = clientDataUsageResultBody.Usages.Sum(item => item.Volume);
                        orderItemPlan.BillingEndDt = DateTime.Now.AddDays(-1);      //JW added. Request by Zhao
                        orderItemPlan = await this.dalProvider.UpdateOrderItem(orderItemPlan);



                        //add new order for new plan
                        //Account account = await this.dalProvider.GetAccountByCustomerID(customer.CustomerId);
                        Account account = await this.dalProvider.GetAccountByResellerID(reseller.ResellerId);

                        Order newOrder = this.GetNewOrder(customerViewModel, itemPlan);
                        OrderItem newOrderItemPlan = this.GetNewOrderItem(itemPlan);
                        OrderItem newOrderItemSIM = this.GetNewOrderItem(itemSIM);

                        //Insert new record in Order table
                        newOrder.AccountId = account.AccountId;
                        newOrder = await this.dalProvider.AddNewOrder(newOrder);

                        //Insert new record in OrderItem - for plan
                        newOrderItemPlan.OrderId = newOrder.OrderId;
                        newOrderItemPlan.ItemId = itemPlan.ItemId;

                        newOrderItemPlan.BillingStartDt = effectiveDate.Date;

                        if (newOrderItemPlan.Billcycle == 30)
                        {
                            //monthly billing cycle

                            newOrderItemPlan.BillingEndDt = new DateTime(effectiveDate.Year, effectiveDate.Month, 1).AddMonths(1).AddDays(-1).Date;
                            newOrderItemPlan.NextBillingDt = new DateTime(effectiveDate.Year, effectiveDate.Month, 1).AddMonths(1).Date;
                        }
                        else if (newOrderItemPlan.Billcycle == 365)
                        {
                            newOrderItemPlan.BillingEndDt = effectiveDate.AddYears(1).AddDays(-1).Date;
                            newOrderItemPlan.BillingEndDt = effectiveDate.AddYears(1).Date;
                        }
                        else
                            throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));



                        await this.dalProvider.AddNewOrderItem(newOrderItemPlan);

                        //Insert record in OrderItem - for SIM
                        newOrderItemSIM.OrderId = newOrder.OrderId;
                        newOrderItemSIM.ItemId = itemSIM.ItemId;
                        //                   await this.dalProvider.AddNewOrderItem(newOrderItemSIM);




                        //Update Order with Active_Date, Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1, INventoryID

                        newOrder.ActiveDate = effectiveDate;

                        newOrder.Status = (int)OrderStatus.Active; //active




                        //end transaction
                        await this.dalProvider.CommitTransaction();

                        iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.Plan = clientDataTerminateServiceResultBody.Plan;
                        iotTerminateServiceResult.EffectiveDate = clientDataTerminateServiceResultBody.EffectiveDate;
                        iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                        iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];

                        return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);
                    }
                    else
                    {
                        await this.dalProvider.RollBackTransaction();

                        iotTerminateServiceResult.Message = "Issue in API - " + clientDataTerminateServiceResultBody.Messages[0];
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                {

                    UsageResultFloLiveBody clientDataUsageResultBody = (UsageResultFloLiveBody)clientDataUsage.ResultResponse;

                    IOTTerminateServiceResult1 iotTerminateServiceResult = new IOTTerminateServiceResult1();

                    if (clientDataUsage.StatusCode != "OK")
                    {
                        iotTerminateServiceResult.Message = "Issue in API - " + clientDataUsageResultBody.ErrorMessage;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                    }

                    await this.dalProvider.BeginTransaction();
                    //record usage in database and flag old order as PlanChanged

                    //update order table for old order record with status and usage up to date
                    order.Status = (int)OrderStatus.PlanChanged;

                    order = await this.dalProvider.UpdateOrder(order);

                    //change service in carrier

                    ResponseDetails clientData = await provider.ChangeServicePlan(customerViewModel);

                    ResultFloLiveBody clientDataActivationResultBody = (ResultFloLiveBody)clientData.ResultResponse;
                    //                ActivationResultBody clientDataTerminateServiceResultBody = (ActivationResultBody)clientData.ResultResponse;

                    DateTime activeDate = DateTime.Now;

                    if (clientData.StatusCode == "OK")
                    {

                        //update inventory table
                        if (inventorySIM != null)
                        {
                            inventorySIM.Status = (int)SIMStatus.Active;
                            inventorySIM.ActiveDate = activeDate;
                            inventorySIM = await this.dalProvider.UpdateInventory(inventorySIM);
                        }


                        //update orderItem table for old orderItems 
                        orderItemSIM.Status = (int)SIMStatus.PlanChanged;
                        //                    orderItemSIM = await this.dalProvider.UpdateOrderItem(orderItemSIM);
                        orderItemPlan.Status = (int)OrderStatus.PlanChanged;
                        orderItemPlan.DataUsageWhenPlanChanged = clientDataUsageResultBody.Content.Where(b => b.Type.ToUpper() == "DATA").Sum(item => item.Quantity);
                        orderItemPlan.SmsusageWhenPlanChanged = clientDataUsageResultBody.Content.Where(b => b.Type.ToUpper() == "SMS").Sum(item => item.Quantity);
                        orderItemPlan.BillingEndDt = DateTime.Now.AddDays(-1);      //JW added. Request by Zhao

                        orderItemPlan = await this.dalProvider.UpdateOrderItem(orderItemPlan);



                        //add new order for new plan
                        //Account account = await this.dalProvider.GetAccountByCustomerID(customer.CustomerId);
                        Account account = await this.dalProvider.GetAccountByResellerID(reseller.ResellerId);

                        Order newOrder = this.GetNewOrder(customerViewModel, itemPlan);
                        OrderItem newOrderItemPlan = this.GetNewOrderItem(itemPlan);
                        OrderItem newOrderItemSIM = this.GetNewOrderItem(itemSIM);

                        //Insert new record in Order table
                        newOrder.AccountId = account.AccountId;
                        newOrder = await this.dalProvider.AddNewOrder(newOrder);

                        //Insert new record in OrderItem - for plan
                        newOrderItemPlan.OrderId = newOrder.OrderId;
                        newOrderItemPlan.ItemId = itemPlan.ItemId;

                        newOrderItemPlan.BillingStartDt = activeDate.Date;

                        if (newOrderItemPlan.Billcycle == 30)
                        {
                            //monthly billing cycle

                            newOrderItemPlan.BillingEndDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).AddDays(-1).Date;
                            newOrderItemPlan.NextBillingDt = new DateTime(activeDate.Year, activeDate.Month, 1).AddMonths(1).Date;
                        }
                        else if (newOrderItemPlan.Billcycle == 365)
                        {
                            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).AddDays(-1).Date;
                            newOrderItemPlan.BillingEndDt = activeDate.AddYears(1).Date;
                        }
                        else
                            throw new Exception(string.Format("Invalid billing cycle {0}", newOrderItemPlan.Billcycle));



                        await this.dalProvider.AddNewOrderItem(newOrderItemPlan);

                        //Insert record in OrderItem - for SIM
                        newOrderItemSIM.OrderId = newOrder.OrderId;
                        newOrderItemSIM.ItemId = itemSIM.ItemId;
                        //                    await this.dalProvider.AddNewOrderItem(newOrderItemSIM);




                        //Update Order with Active_Date, Billing_Start_Dt, Billing_End_DT, Next_Billing_DT, STATUS = 1, INventoryID

                        newOrder.ActiveDate = activeDate;

                        newOrder.Status = (int)OrderStatus.Active; //active




                        //end transaction
                        await this.dalProvider.CommitTransaction();

                        iotTerminateServiceResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotTerminateServiceResult.Plan = customerViewModel.PlanName;
                        iotTerminateServiceResult.EffectiveDate = activeDate.ToString("yyyy-MM-dd");
                        //iotTerminateServiceResult.Status = clientDataTerminateServiceResultBody.Status;
                        //iotTerminateServiceResult.Message = clientDataTerminateServiceResultBody.Messages[0];
                        iotTerminateServiceResult.RequestID = clientDataActivationResultBody.Content[0].RequestId;

                        return CommonHelper.GetResponseDetails("200", "Ok", iotTerminateServiceResult);

                    }
                    else
                    {
                        await this.dalProvider.RollBackTransaction();

                        iotTerminateServiceResult.Message = "Issue in API - " + clientDataActivationResultBody.ErrorMessage;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTerminateServiceResult);
                    }
                }
                else
                {
                    IOTTerminateServiceResult1 iotActivationResult1 = new IOTTerminateServiceResult1();
                    iotActivationResult1.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotActivationResult1);
                }

            }

        }
        #endregion


        #endregion
    }
}