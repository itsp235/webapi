﻿using System.Threading.Tasks;

using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;

namespace IOTCell.BL.Providers
{
    public interface IChangeProvider : IBLProviderBase
    {
        public Task<ResponseDetails> TerminateService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> SuspendService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> ResumeService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> ChangeServicePlan(CustomerViewModel customerViewModel);
    }
}
