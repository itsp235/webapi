﻿using System;
using System.Collections.Specialized;
using System.Threading.Tasks;
using System.Linq;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.BusinessRules.Context;
using IOTCell.BL.BusinessRules;
using IOTCell.BL.ClientControllerProvider;
using IOTCell.BL.Diagnostics;
using IOTCell.BL.ViewModels;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;
using IOTCell.Framework.Enums;

namespace IOTCell.BL.Providers
{
    public class UsageProvider : BLProvider, IUsageProvider
    {
        #region Constructor
        public UsageProvider(IConfiguration configuration, IActivityEntryLogger logger)
            : base(configuration, logger)
        {
        }
        #endregion

        #region Methods

        #region GetUsage
        async Task<ResponseDetails> IUsageProvider.GetUsage(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMUsageValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };



            //Get usage in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.GetUsage(customerViewModel);


            if (!this.appSettings.MultiCarrier)
            {
                UsageResultBody clientDataUsageResultBody = (UsageResultBody)clientData.ResultResponse;

                //Result clientDataResult = (Result)clientDataUsageResultBody. .Result;

                IOTUsageResult iotUsageResult = new IOTUsageResult();

                if (clientData.StatusCode == "OK")
                {
                    if (clientDataUsageResultBody.Messages[0].ToLower() != "usage not available")
                    {

                        iotUsageResult.FromDate = DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd");
                        iotUsageResult.ToDate = DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd");
                        iotUsageResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotUsageResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotUsageResult.Plan = clientDataUsageResultBody.Usages[0].Plan;
                        iotUsageResult.TotalVolume = clientDataUsageResultBody.Usages.Sum(item => item.Volume);
                        iotUsageResult.Usages = clientDataUsageResultBody.Usages;

                        iotUsageResult.Message = clientDataUsageResultBody.Messages[0];

                        return CommonHelper.GetResponseDetails("200", "Ok", iotUsageResult);
                    }
                    else
                    {
                        iotUsageResult.FromDate = DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd");
                        iotUsageResult.ToDate = DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd");
                        iotUsageResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotUsageResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                        iotUsageResult.Plan = String.Empty;
                        iotUsageResult.TotalVolume = 0;
                        iotUsageResult.Usages = clientDataUsageResultBody.Usages;
                        iotUsageResult.Message = clientDataUsageResultBody.Messages[0];

                        return CommonHelper.GetResponseDetails("200", "Ok", iotUsageResult);

                    }
                }
                else
                {
                    iotUsageResult.Message = "Issue in API - " + clientDataUsageResultBody.Messages[0];
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotUsageResult);
                }
            }
            else 
            {
                if (carrier.CarrierName == "iBasis")
                {
                    UsageResultBody clientDataUsageResultBody = (UsageResultBody)clientData.ResultResponse;

                    //Result clientDataResult = (Result)clientDataUsageResultBody. .Result;

                    IOTUsageResult1 iotUsageResult = new IOTUsageResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        if (clientDataUsageResultBody.Messages[0].ToLower() != "usage not available")
                        {

                            iotUsageResult.FromDate = DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd");
                            iotUsageResult.ToDate = DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd");
                            iotUsageResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                            iotUsageResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                            iotUsageResult.Plan = clientDataUsageResultBody.Usages[0].Plan;
                            iotUsageResult.TotalDataVolume = clientDataUsageResultBody.Usages.Sum(item => item.Volume);
                            iotUsageResult.Usages = clientDataUsageResultBody.Usages;

                            iotUsageResult.Message = clientDataUsageResultBody.Messages[0];

                            return CommonHelper.GetResponseDetails("200", "Ok", iotUsageResult);
                        }
                        else
                        {
                            iotUsageResult.FromDate = DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd");
                            iotUsageResult.ToDate = DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd");
                            iotUsageResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                            iotUsageResult.EID = ((customerViewModel.SimIdentifier == SIMIdentifiers.EID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);
                            iotUsageResult.Plan = String.Empty;
                            iotUsageResult.TotalDataVolume = 0;
                            iotUsageResult.Usages = clientDataUsageResultBody.Usages;
                            iotUsageResult.Message = clientDataUsageResultBody.Messages[0];

                            return CommonHelper.GetResponseDetails("200", "Ok", iotUsageResult);

                        }
                    }
                    else
                    {
                        iotUsageResult.Message = "Issue in API - " + clientDataUsageResultBody.Messages[0];
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotUsageResult);
                    }

                }
                else if (carrier.CarrierName == "floLIVE")
                {
                    UsageResultFloLiveBody clientDataUsageResultBody = (UsageResultFloLiveBody)clientData.ResultResponse;

                    IOTUsageResult1 iotUsageResult = new IOTUsageResult1();

                    if (clientData.StatusCode == "OK")
                    {
                        iotUsageResult.FromDate = DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd");
                        iotUsageResult.ToDate = DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd");
                        iotUsageResult.ICCID = ((customerViewModel.SimIdentifier == SIMIdentifiers.ICCID.ToString()) ? customerViewModel.SimIdentifierValue : String.Empty);

                        iotUsageResult.TotalDataVolume = clientDataUsageResultBody.Content.Where(b => b.Type.ToUpper() == "DATA").Sum(item => item.Quantity);
                        iotUsageResult.TotalSMSCount = (int)clientDataUsageResultBody.Content.Where(b => b.Type.ToUpper() == "SMS").Sum(item => item.Quantity);

                        return CommonHelper.GetResponseDetails("200", "Ok", iotUsageResult);
                    }
                    else
                    {
                        iotUsageResult.Message = "Issue in API - " + clientDataUsageResultBody.ErrorCode + " - " + clientDataUsageResultBody.ErrorMessage;
                        return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotUsageResult);
                    }
                }
                else
                {
                    IOTUsageResult1 iotUsageResult = new IOTUsageResult1();
                    iotUsageResult.Message = "Issue in API - Carrier unknown " + carrier.CarrierName;
                    return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotUsageResult);

                }

            }
        }
        #endregion


        #region GetCalculatedUsage
        async Task<ResponseDetails> IUsageProvider.GetCalculatedUsage(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformSIMUsageNoPeriodValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Inventory inventorySIM = businessRuleContext.InventorySIM;
            Carrier carrier = businessRuleContext.Carrier;

            if (carrier.UsesGetCalculatedUsageMethod != 1)
                return CommonHelper.GetResponseDetails("404", "Method not available for this carrier - " + carrier.CarrierName);


            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };



            //Get usage in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.GetCalculatedUsage(customerViewModel);


            UsageMonthResultBody clientDataUsageMonthResultBody = (UsageMonthResultBody)clientData.ResultResponse;


            IOTCalculatedUsageResult iotCalculatedUsageResult = new IOTCalculatedUsageResult();

            if (clientData.StatusCode == "OK")
            {
                //calculates all the data volumes (curent month, 3, 6, 12 months)
                var firstDayOfCurrentMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var firstDayOfMonth3Ago = firstDayOfCurrentMonth.AddMonths(-3);
                var firstDayOfMonth6Ago = firstDayOfCurrentMonth.AddMonths(-6);
                var firstDayOfMonth12Ago = firstDayOfCurrentMonth.AddMonths(-12);
                iotCalculatedUsageResult.TotalVolumeCurrentMonth = clientDataUsageMonthResultBody.Usages.Where(x => x.FromDate == firstDayOfCurrentMonth.ToString("yyyy-MM-dd")).Select(x => x.Volume).Sum();

                iotCalculatedUsageResult.TotalVolumeLast3Months = clientDataUsageMonthResultBody.Usages.Where(x => DateTime.Parse(x.FromDate) < firstDayOfCurrentMonth).Where(x => DateTime.Parse(x.FromDate) >= firstDayOfMonth3Ago).Select(x => x.Volume).Sum();
                iotCalculatedUsageResult.TotalVolumeLast6Months = clientDataUsageMonthResultBody.Usages.Where(x => DateTime.Parse(x.FromDate) < firstDayOfCurrentMonth).Where(x => DateTime.Parse(x.FromDate) >= firstDayOfMonth6Ago).Select(x => x.Volume).Sum();
                iotCalculatedUsageResult.TotalVolumeLast12Months = clientDataUsageMonthResultBody.Usages.Where(x => DateTime.Parse(x.FromDate) < firstDayOfCurrentMonth).Where(x => DateTime.Parse(x.FromDate) >= firstDayOfMonth12Ago).Select(x => x.Volume).Sum();

                iotCalculatedUsageResult.TotalVolumeLifetime = clientDataUsageMonthResultBody.Usages.Sum(item => item.Volume);
                iotCalculatedUsageResult.Usages = clientDataUsageMonthResultBody.Usages;

                iotCalculatedUsageResult.Message = clientDataUsageMonthResultBody.Messages[0];

                return CommonHelper.GetResponseDetails("200", "Ok", iotCalculatedUsageResult);
            }
            else
            {
                iotCalculatedUsageResult.Message = "Issue in API - " + clientDataUsageMonthResultBody.Messages[0];
                return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotCalculatedUsageResult);
            }
        }
        #endregion



        #region GetTotalUsage
        async Task<ResponseDetails> IUsageProvider.GetTotalUsage(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformTotalUsageValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Carrier carrier = businessRuleContext.Carrier;

            if (carrier.UsesGetTotalUsageForResellerMethod != 1)
                return CommonHelper.GetResponseDetails("404", "Method not available for this carrier - " + carrier.CarrierName);


            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;


            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };



            //Get total usage in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            ResponseDetails clientData = await provider.GetTotalUsage(customerViewModel);


            TotalUsageResultBody clientDataTotalUsageResultBody = (TotalUsageResultBody)clientData.ResultResponse;


            IOTTotalUsageResult iotTotalUsageResult = new IOTTotalUsageResult();

            if (clientData.StatusCode == "OK")
            {
                iotTotalUsageResult.FromDate = DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd");
                iotTotalUsageResult.ToDate = DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd");
                iotTotalUsageResult.TotalVolumePerPeriod = clientDataTotalUsageResultBody.Usages.Sum(item => item.TotalVolume);
                iotTotalUsageResult.TotalUsages = clientDataTotalUsageResultBody.Usages;

                iotTotalUsageResult.Message = clientDataTotalUsageResultBody.Messages[0];

                return CommonHelper.GetResponseDetails("200", "Ok", iotTotalUsageResult);
            }
            else
            {
                iotTotalUsageResult.Message = "Issue in API - " + clientDataTotalUsageResultBody.Messages[0];
                return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotTotalUsageResult);
            }
        }
        #endregion


        #region GetAvailableServicePlans
        async Task<ResponseDetails> IUsageProvider.GetAvailableServicePlans(CustomerViewModel customerViewModel)
        {
            IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);
            BusinessRuleLogic business = new BusinessRuleLogic(this.configuration, this.logger, this.dalProvider, businessRuleContext);
            ResponseDetails responseDetails = await business.PerformResellerValidation();
            if (responseDetails.StatusCode != "200")
            {
                return responseDetails;
            }

            Reseller reseller = businessRuleContext.Reseller;
            ResellerExtension resellerExtension = businessRuleContext.ResellerExtension;
            Carrier carrier = businessRuleContext.Carrier;

            if (carrier.UsesGetCalculatedUsageMethod != 1)
                return CommonHelper.GetResponseDetails("404", "Method not available for this carrier - " + carrier.CarrierName);

            customerViewModel.CompanyMVNO = reseller.Mvno;
            customerViewModel.IB_UserName = resellerExtension.IbUserName;
            customerViewModel.IB_Password = resellerExtension.IbPassword;

            NameValueCollection settings = new NameValueCollection()
            {
                {"ClientName", reseller.ResellerName}
                //{"ClientApiKey", clientApiKey.ToString()}
            };



            //Get available serviceplans in iBasis/carrier
            ClientControllerProviderFactory providerFactory = new ClientControllerProviderFactory();
            IClientControllerProvider provider = providerFactory.GetClientControllerProvider(carrier.CarrierAssembly);
            provider.Init(this.configuration, this.logger, settings);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);

            //customerViewModel.ApiUser = this.appSettings.GetDecodedValue(carrier.Apiuser);
            //customerViewModel.ApiPwd = this.appSettings.GetDecodedValue(carrier.Apipwd);


            //string dbUserName = this.appSettings.GetEncryptedValue("waivuser");
            //string dbPwd = this.appSettings.GetEncryptedValue("w@iv4Hel!x");


            ResponseDetails clientData = await provider.GetAvailableServicePlans(customerViewModel);


            ServicePlanResultBody clientDataServicePlanResultBody = (ServicePlanResultBody)clientData.ResultResponse;


            IOTServicePlanResult iotServicePlanResult = new IOTServicePlanResult();

            if (clientData.StatusCode == "OK")
            {
                iotServicePlanResult.ServicePlans = clientDataServicePlanResultBody.Results;

                iotServicePlanResult.Message = clientDataServicePlanResultBody.Messages[0];

                return CommonHelper.GetResponseDetails("200", "Ok", iotServicePlanResult);
            }
            else
            {
                iotServicePlanResult.Message = "Issue in API - " + clientDataServicePlanResultBody.Messages[0];
                return CommonHelper.GetResponseDetails("500", "Internal Server Error", iotServicePlanResult);
            }
        }
        #endregion


        #endregion
    }
}