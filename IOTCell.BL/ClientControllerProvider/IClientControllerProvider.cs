﻿using System.Collections.Specialized;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.Diagnostics;
using IOTCell.BL.ViewModels;
using IOTCell.Framework.Models;

namespace IOTCell.BL.ClientControllerProvider
{
    public interface IClientControllerProvider
    {
        public void Init(IConfiguration configuration, IActivityEntryLogger logger, NameValueCollection settings);
       
        public Task<ResponseDetails> ActivateService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetUsage(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetTotalUsage(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetCalculatedUsage(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> TerminateService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> SuspendService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> ResumeService(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> ChangeServicePlan(CustomerViewModel customerViewModel);
        public Task<ResponseDetails> GetAvailableServicePlans(CustomerViewModel customerViewModel);
    }
}