﻿using System;
using System.IO;
using System.Reflection;

namespace IOTCell.BL.ClientControllerProvider
{
    public class ClientControllerProviderFactory
    {
        #region Methods
        #region GetClientControllerProvider
        public IClientControllerProvider GetClientControllerProvider(string assemblyType)
        {
            string[] assemblyArray = assemblyType.Split(',');

            if (assemblyArray.Length <= 1)
            {
                throw new Exception(string.Format("Invalid assembly {0} specified", assemblyType));
            }

            string assemblyFileName = assemblyArray[1].Trim() + ".dll";
            string assemblyPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory ?? AppDomain.CurrentDomain.RelativeSearchPath) + assemblyFileName;

            Assembly.LoadFrom(assemblyPath);

            Type type = Type.GetType(assemblyType);
            if (type == null)
            {
                throw new Exception(string.Format("Not able to get type for assembly {0}", assemblyType));
            }

            IClientControllerProvider provider = (IClientControllerProvider)Activator.CreateInstance(type);
 
            return provider;
        }
        #endregion
        #endregion
    }
}