using System;

using IOTCell.BL.ViewModels;
using IOTCell.DAL.Models;

namespace IOTCell.BL.BusinessRules.Context
{
	public class BusinessRuleContext
		: IDisposable, IBusinessRuleContext
	{
		#region Properties
		#region BLProvider
		private readonly IBLProvider blProvider;
		IBLProvider IBusinessRuleContext.BLProvider
		{
			get
			{
				return blProvider;
			}
		}
		#endregion

		#region CustomerViewModel
		private readonly CustomerViewModel customerViewModel;
		CustomerViewModel IBusinessRuleContext.CustomerViewModel
		{
			get
			{
				return customerViewModel;
			}
		}
		#endregion

		#region Reseller
		private Reseller reseller;
		Reseller IBusinessRuleContext.Reseller
		{
			get
			{
				return reseller;
			}
            set
            {
                this.reseller = value;
			}
		}
		#endregion

		#region ResellerExtension
		private ResellerExtension resellerExtension;
		ResellerExtension IBusinessRuleContext.ResellerExtension
		{
			get
			{
				return resellerExtension;
			}
			set
			{
				this.resellerExtension = value;
			}
		}
		#endregion

		#region Customer
		private Customer customer;
		Customer IBusinessRuleContext.Customer
		{
			get
			{
				return customer;
			}
			set
			{
				this.customer = value;
			}
		}
		#endregion

		#region Carrier
		private Carrier carrier;
		Carrier IBusinessRuleContext.Carrier
		{
			get
			{
				return carrier;
			}
			set
			{
				this.carrier = value;
			}
		}
		#endregion


		#region Address
		private Address address;
		Address IBusinessRuleContext.Address
		{
			get
			{
				return address;
			}
			set
			{
				this.address = value;
			}
		}
		#endregion




		#region ItemPlan
		private Item itemPlan;
		Item IBusinessRuleContext.ItemPlan
		{
			get
			{
				return itemPlan;
			}
			set
			{
				this.itemPlan = value;
			}
		}
		#endregion

		#region ItemSIM
		private Item itemSIM;
		Item IBusinessRuleContext.ItemSIM
		{
			get
			{
				return itemSIM;
			}
			set
			{
				this.itemSIM = value;
			}
		}
		#endregion

		#region InventorySIM
		private Inventory inventorySIM;
		Inventory IBusinessRuleContext.InventorySIM
		{
			get
			{
				return inventorySIM;
			}
			set
			{
				this.inventorySIM = value;
			}
		}
		#endregion

		#region Account
		private Account account;
		Account IBusinessRuleContext.Account
		{
			get
			{
				return account;
			}
			set
			{
				this.account = value;
			}
		}
		#endregion

		#endregion

		#region Constructor
		public BusinessRuleContext(IBLProvider blProvider,
			CustomerViewModel customerViewModel)
		{
			this.blProvider = blProvider;
			this.customerViewModel = customerViewModel;
		}
		#endregion
		
		#region Methods
		#region Dispose
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		#endregion
		#endregion
	}
}