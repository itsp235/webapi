using IOTCell.BL.ViewModels;
using IOTCell.DAL.Models;

namespace IOTCell.BL.BusinessRules.Context
{
	public interface IBusinessRuleContext
	{
		IBLProvider BLProvider { get; }
		CustomerViewModel CustomerViewModel { get; }
		Reseller Reseller { get; set; }
		ResellerExtension ResellerExtension { get; set; }
		Carrier Carrier { get; set; }
		Address Address { get; set; }
		Customer Customer { get; set; }
		Item ItemPlan { get; set; }
		Item ItemSIM { get; set; }
		Inventory InventorySIM { get; set; }
		Account Account { get; set; }
	}
}