﻿
using IOTCell.DAL.Models;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface IChangeLogic
	{
		ResponseDetails ValidateChange(Inventory inventorySIMModel, Carrier carrier);
		ResponseDetails ValidateChangePlan(Item itemPlan, Inventory inventorySIMModel, Carrier carrier);
	}
}