using System.Threading.Tasks;

using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface IBusinessRuleLogic
	{
		Task<ResponseDetails> PerformNewCustomerActivationValidation();
		Task<ResponseDetails> PerformExistingCustomerActivationValidation();
	}
}