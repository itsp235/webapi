﻿using System
;
using System.Collections.Generic;

using IOTCell.BL.BusinessRules.Context;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Enums;
using IOTCell.Framework.Models;


namespace IOTCell.BL.BusinessRules
{
	public class UsageLogic : IUsageLogic
	{
		#region Properties
		#region BusinessRulesContext
		private readonly IBusinessRuleContext businessRuleContext;
		public IBusinessRuleContext BusinessRuleContext
		{
			get
			{
				return businessRuleContext;
			}
		}
		#endregion
		#endregion

		#region Constructors
		public UsageLogic(IBusinessRuleContext businessRuleContext)
		{
			this.businessRuleContext = businessRuleContext;
		}
		#endregion

		#region Methods		
		#region ValidateTimePeriod
		ResponseDetails IUsageLogic.ValidateTimePeriod()
		{
            //Validate fromDate
            if (!DateTime.TryParse(this.businessRuleContext.CustomerViewModel.FromDate, out _))
            {
                return CommonHelper.GetResponseDetails("400", "Wrong parameter From Date " + this.businessRuleContext.CustomerViewModel.FromDate);
            }

            //Validate toDate
            if (!DateTime.TryParse(this.businessRuleContext.CustomerViewModel.ToDate, out _))
            {
                return CommonHelper.GetResponseDetails("400", "Wrong parameter To Date " + this.businessRuleContext.CustomerViewModel.ToDate);
            }

            return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion

		#region ValidateSIM
		ResponseDetails IUsageLogic.ValidateSIM(Inventory inventorySIMModel, Carrier carrier)
		{
			List<string> simIdentifiersList = new List<string>()
			{
				SIMIdentifiers.ICCID.ToString(),
				SIMIdentifiers.EID.ToString()
			};

			//Validate sim identifier
			if (!simIdentifiersList.Contains(this.businessRuleContext.CustomerViewModel.SimIdentifier))
			{
				return CommonHelper.GetResponseDetails("400", "Wrong SIM identifier (valid values EID or ICCID)");
			}

			//Validate sim identifier value
			if (this.businessRuleContext.CustomerViewModel.SimIdentifierValue == string.Empty)
			{
				return CommonHelper.GetResponseDetails("400", "SIM identifier (EID or ICCID) is empty");
			}
						

			//Validate SIM in Inventory
//			if (this.businessRuleContext.CustomerViewModel.SimIdentifier == "ICCID")
//			{
			if (inventorySIMModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "This SIM was not found in the inventory database");
			}

			//            }

			if (carrier == null)
			{
				return CommonHelper.GetResponseDetails("404", "The carrier for this SIM was not found in the database");
			}



			return CommonHelper.GetResponseDetails("200", string.Empty);

		}
		#endregion
		#endregion
	}
}