using IOTCell.DAL.Models;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface IActivationLogic
	{
		ResponseDetails ValidateActivation(Item itemPlanModel, Inventory inventorySIMModel, Carrier carrier);
		ResponseDetails ValidateInventorySIM(Inventory inventoryModel);
	}
}