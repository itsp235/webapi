﻿using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using IOTCell.BL.BusinessRules.Context;
using IOTCell.BL.Diagnostics;
using IOTCell.BL.ViewModels;
using IOTCell.DAL;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public class BusinessRuleLogic : BLProvider, IBusinessRuleLogic
	{
		#region Properties
		#region BusinessRulesContext
		private readonly IBusinessRuleContext businessRuleContext;
		public IBusinessRuleContext BusinessRuleContext
		{
			get
			{
				return businessRuleContext;
			}
		}
		#endregion
		#endregion

		#region Constructors
		public BusinessRuleLogic(IConfiguration configuration, IActivityEntryLogger logger,
			IDALProvider dalProvider, IBusinessRuleContext businessRuleContext)
			: base(configuration, logger, dalProvider)
		{
			this.businessRuleContext = businessRuleContext;
		}
		#endregion

		#region Methods
		#region PerformNewCustomerActivationValidation
		public async Task<ResponseDetails> PerformNewCustomerActivationValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);

			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}


			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			//this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);
			//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}


			this.businessRuleContext.Customer = this.GetNewCustomer(customerViewModel, this.businessRuleContext.Reseller);
			this.businessRuleContext.Address = this.GetNewAddress(customerViewModel, this.businessRuleContext.Reseller);

			//Customer Business Rule Logic
			ICustomerLogic customerLogic = new CustomerLogic(businessRuleContext);
			ResponseDetails customerResponseDetails = customerLogic.ValidateNewCustomer(this.businessRuleContext.Customer);
			if (customerResponseDetails.StatusCode != "200")
			{
				return customerResponseDetails;
			}

			//Address Business Rule Logic
			IAddressLogic addressLogic = new AddressLogic(businessRuleContext);
			ResponseDetails addressResponseDetails = addressLogic.ValidateAddress(this.businessRuleContext.Address);
			if (addressResponseDetails.StatusCode != "200")
			{
				return addressResponseDetails;
			}


			this.businessRuleContext.ItemPlan = await this.dalProvider.GetPlanByPlanName(customerViewModel.PlanName, this.businessRuleContext.Reseller.ResellerId, this.businessRuleContext.Carrier.CarrierId);
			
			this.businessRuleContext.ItemSIM = await this.dalProvider.GetSIMType(this.businessRuleContext.InventorySIM.ItemId);


			//Activation Business Rule Logic
			IActivationLogic activationLogic1 = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails1 = activationLogic1.ValidateActivation(this.businessRuleContext.ItemPlan, this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (activationResponseDetails1.StatusCode != "200")
			{
				return activationResponseDetails1;
			}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion

		#region PerformExistingCustomerActivationValidation
		public async Task<ResponseDetails> PerformExistingCustomerActivationValidation()
        {
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);
			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}


			this.businessRuleContext.Account = await this.dalProvider.GetAccountByResellerID(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails2 = resellerLogic.ValidateAccount(this.businessRuleContext.Account);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails2;
			}

			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);


			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}



			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);
//			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}


			//this.businessRuleContext.Customer = await this.dalProvider.GetCustomerByAccountNumber(customerViewModel.AccountNumber);

			////Customer Business Rule Logic
			//ICustomerLogic customerLogic = new CustomerLogic(businessRuleContext);
			//ResponseDetails customerResponseDetails = customerLogic.ValidateExistingCustomer(this.businessRuleContext.Customer);
			//if (customerResponseDetails.StatusCode != "200")
			//{
			//	return customerResponseDetails;
			//}

			this.businessRuleContext.ItemPlan = await this.dalProvider.GetPlanByPlanName(customerViewModel.PlanName, this.businessRuleContext.Reseller.ResellerId, this.businessRuleContext.Carrier.CarrierId);
			
			this.businessRuleContext.ItemSIM = await this.dalProvider.GetSIMType(this.businessRuleContext.InventorySIM.ItemId);


			//Activation Business Rule Logic
			IActivationLogic activationLogic1 = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails1 = activationLogic1.ValidateActivation(this.businessRuleContext.ItemPlan, this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (activationResponseDetails1.StatusCode != "200")
			{
				return activationResponseDetails1;
			}
			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		//#region PerformAccountValidation
		//public async Task<ResponseDetails> PerformAccountValidation()
		//{
		//	CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

		//	// Get required database data
		//	this.businessRuleContext.Account = await this.dalProvider.GetAccountByResellerID(this.businessRuleContext.Reseller.ResellerId);

		//	//Context to be used by Business rule logic classes
		//	IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

		//	//Reseller Business Rule Logic
		//	IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
		//	ResponseDetails resellerResponseDetails = resellerLogic.ValidateAccount(this.businessRuleContext.Account);
		//	if (resellerResponseDetails.StatusCode != "200")
		//	{
		//		return resellerResponseDetails;
		//	}

			
		//	return CommonHelper.GetResponseDetails("200", string.Empty);
		//}
		//#endregion


		#region PerformSIMChangeValidation
		public async Task<ResponseDetails> PerformSIMChangeValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}



			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);

			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}


			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);

			//			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}




			//this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);
			this.businessRuleContext.ItemSIM = await this.dalProvider.GetSIMType(this.businessRuleContext.InventorySIM.ItemId);
			

			//ChangeSIM Business Rule Logic
			IChangeLogic changeLogic = new ChangeLogic(businessRuleContext);
			ResponseDetails changeResponseDetails = changeLogic.ValidateChange(this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (changeResponseDetails.StatusCode != "200")
			{
				return changeResponseDetails;
			}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#region PerformSIMChangePlanValidation
		public async Task<ResponseDetails> PerformSIMChangePlanValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}



			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);

			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}



			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);



			//this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}




			//this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);
			this.businessRuleContext.ItemSIM = await this.dalProvider.GetSIMType(this.businessRuleContext.InventorySIM.ItemId);
			this.businessRuleContext.ItemPlan = await this.dalProvider.GetPlanByPlanName(customerViewModel.PlanName, this.businessRuleContext.Reseller.ResellerId, this.businessRuleContext.Carrier.CarrierId);
			
			

			//ChangeSIM Business Rule Logic
			IChangeLogic changeLogic = new ChangeLogic(businessRuleContext);
			ResponseDetails changeResponseDetails = changeLogic.ValidateChangePlan(this.businessRuleContext.ItemPlan, this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (changeResponseDetails.StatusCode != "200")
			{
				return changeResponseDetails;
			}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#region PerformSIMPlanValidation
		public async Task<ResponseDetails> PerformSIMPlanValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			//// Get required database data
			//this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.ResellerCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			////Reseller Business Rule Logic
			//IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			//ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			//if (resellerResponseDetails.StatusCode != "200")
			//{
			//	return resellerResponseDetails;
			//}

			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);
			this.businessRuleContext.ItemSIM = await this.dalProvider.GetSIMType(this.businessRuleContext.InventorySIM.ItemId);
			this.businessRuleContext.ItemPlan = await this.dalProvider.GetPlanByPlanName(customerViewModel.PlanName, this.businessRuleContext.Reseller.ResellerId, this.businessRuleContext.Carrier.CarrierId);

			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}



			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);



			//this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);

			//ChangeSIM Business Rule Logic
			IChangeLogic changeLogic = new ChangeLogic(businessRuleContext);
			ResponseDetails changeResponseDetails = changeLogic.ValidateChangePlan(this.businessRuleContext.ItemPlan, this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (changeResponseDetails.StatusCode != "200")
			{
				return changeResponseDetails;
			}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion



		#region PerformSIMUsageValidation
		public async Task<ResponseDetails> PerformSIMUsageValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}



			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);

			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}


			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);

//			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}




			//this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);
			

			//Usage Business Rule Logic
			IUsageLogic usageLogic = new UsageLogic(businessRuleContext);
			ResponseDetails usageResponseDetails = usageLogic.ValidateSIM(this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (usageResponseDetails.StatusCode != "200")
			{
				return usageResponseDetails;
			}
			ResponseDetails usageResponseDetails1 = usageLogic.ValidateTimePeriod();
			if (usageResponseDetails1.StatusCode != "200")
			{
				return usageResponseDetails1;
			}


			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#region PerformSIMUsageNoPeriodValidation
		public async Task<ResponseDetails> PerformSIMUsageNoPeriodValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}



			this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);

			IActivationLogic activationLogic = new ActivationLogic(businessRuleContext);
			ResponseDetails activationResponseDetails = activationLogic.ValidateInventorySIM(this.businessRuleContext.InventorySIM);
			if (activationResponseDetails.StatusCode != "200")
			{
				return activationResponseDetails;
			}


			int tempCarrierID;
			if (this.businessRuleContext.InventorySIM == null)
				tempCarrierID = 0;
			else
				tempCarrierID = this.businessRuleContext.InventorySIM.CarrierId;


			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(tempCarrierID);



//			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}



			//this.businessRuleContext.InventorySIM = await this.dalProvider.GetSIMFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);
			

			//Usage Business Rule Logic
			IUsageLogic usageLogic = new UsageLogic(businessRuleContext);
			ResponseDetails usageResponseDetails = usageLogic.ValidateSIM(this.businessRuleContext.InventorySIM, this.businessRuleContext.Carrier);
			if (usageResponseDetails.StatusCode != "200")
			{
				return usageResponseDetails;
			}
			//ResponseDetails usageResponseDetails1 = usageLogic.ValidateTimePeriod();
			//if (usageResponseDetails1.StatusCode != "200")
			//{
			//	return usageResponseDetails1;
			//}


			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion



		#region PerformTotalUsageValidation
		public async Task<ResponseDetails> PerformTotalUsageValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}



			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			//Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}


			//Usage Business Rule Logic
			IUsageLogic usageLogic = new UsageLogic(businessRuleContext);
			ResponseDetails usageResponseDetails1 = usageLogic.ValidateTimePeriod();
			if (usageResponseDetails1.StatusCode != "200")
			{
				return usageResponseDetails1;
			}


			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#region PerformResellerValidation
		public async Task<ResponseDetails> PerformResellerValidation()
		{
			CustomerViewModel customerViewModel = this.businessRuleContext.CustomerViewModel;

			// Get required database data
			this.businessRuleContext.Reseller = await this.dalProvider.GetResellerByResellerCode(customerViewModel.CompanyCode);

			//Context to be used by Business rule logic classes
			IBusinessRuleContext businessRuleContext = new BusinessRuleContext(this, customerViewModel);

			//Reseller Business Rule Logic
			IResellerLogic resellerLogic = new ResellerLogic(businessRuleContext);
			ResponseDetails resellerResponseDetails = resellerLogic.ValidateReseller(this.businessRuleContext.Reseller);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails;
			}

			this.businessRuleContext.ResellerExtension = await this.dalProvider.GetResellerExtension(this.businessRuleContext.Reseller.ResellerId);
			ResponseDetails resellerResponseDetails1 = resellerLogic.ValidateResellerExtension(this.businessRuleContext.ResellerExtension);
			if (resellerResponseDetails.StatusCode != "200")
			{
				return resellerResponseDetails1;
			}


			//this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.CompanyCode);
			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(customerViewModel.CarrierID);
			////Carrier Business Rule Logic

			ResponseDetails carrierResponseDetails = resellerLogic.ValidateCarrier(this.businessRuleContext.Carrier);
			if (carrierResponseDetails.StatusCode != "200")
			{
				return carrierResponseDetails;
			}



			//			this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromInventory(customerViewModel.SimIdentifier, customerViewModel.SimIdentifierValue);
			//this.businessRuleContext.Carrier = await this.dalProvider.GetCarrierFromReseller(customerViewModel.ResellerCode);

			

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion



		#endregion
	}
}