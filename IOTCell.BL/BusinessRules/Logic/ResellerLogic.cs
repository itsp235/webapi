using IOTCell.BL.BusinessRules.Context;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public class ResellerLogic : IResellerLogic
	{
		#region Properties
		#region BusinessRulesContext
		private readonly IBusinessRuleContext businessRuleContext;
		public IBusinessRuleContext BusinessRuleContext
		{
			get
			{
				return businessRuleContext;
			}
		}
        #endregion
        #endregion

        #region Constructors
        public ResellerLogic(IBusinessRuleContext businessRuleContext)
		{
			this.businessRuleContext = businessRuleContext;
		}
		#endregion

		#region Methods
		#region ValidateReseller
		ResponseDetails IResellerLogic.ValidateReseller(Reseller resellerModel)
		{
			if (resellerModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "Reseller not found or wrong reseller code");
			}

            //if (resellerModel.Resellerkey != this.businessRuleContext.CustomerViewModel.ResellerKey)
            //{
            //	return CommonHelper.GetResponseDetails("400", "Wrong reseller API key");
            //}

            return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion

		#region ValidateResellerExtension
		ResponseDetails IResellerLogic.ValidateResellerExtension(ResellerExtension resellerExtensionModel)
		{
			if (resellerExtensionModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "Reseller extension not found");
			}

			//if (resellerModel.Resellerkey != this.businessRuleContext.CustomerViewModel.ResellerKey)
			//{
			//	return CommonHelper.GetResponseDetails("400", "Wrong reseller API key");
			//}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#region ValidateCarrier
		ResponseDetails IResellerLogic.ValidateCarrier(Carrier carrierModel)
		{
			if (carrierModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "Carrier not found for this reseller");
			}

			//if (resellerModel.Resellerkey != this.businessRuleContext.CustomerViewModel.ResellerKey)
			//{
			//	return CommonHelper.GetResponseDetails("400", "Wrong reseller API key");
			//}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#region ValidateAccount
		ResponseDetails IResellerLogic.ValidateAccount(Account accountModel)
		{
			if (accountModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "Pay method (account) not found for this reseller");
			}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#endregion
	}
}