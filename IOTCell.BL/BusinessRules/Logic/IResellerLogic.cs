using IOTCell.DAL.Models;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface IResellerLogic
	{
		ResponseDetails ValidateReseller(Reseller resellerModel);
		ResponseDetails ValidateResellerExtension(ResellerExtension resellerExtensionModel);
		ResponseDetails ValidateCarrier(Carrier carrierModel);
		ResponseDetails ValidateAccount(Account accountModel);
	}
}