﻿using IOTCell.DAL.Models;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface IUsageLogic
	{
		ResponseDetails ValidateSIM(Inventory inventorySIMModel, Carrier carrier);
		ResponseDetails ValidateTimePeriod();
	}
}