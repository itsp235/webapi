﻿using System.Collections.Generic;

using IOTCell.BL.BusinessRules.Context;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Enums;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public class ChangeLogic : IChangeLogic
	{
		#region Properties
		#region BusinessRulesContext
		private readonly IBusinessRuleContext businessRuleContext;
		public IBusinessRuleContext BusinessRuleContext
		{
			get
			{
				return businessRuleContext;
			}
		}
		#endregion
		#endregion

		#region Constructors
		public ChangeLogic(IBusinessRuleContext businessRuleContext)
		{
			this.businessRuleContext = businessRuleContext;
		}
		#endregion

		#region Methods
		#region ValidateChange
		ResponseDetails IChangeLogic.ValidateChange(Inventory inventorySIMModel, Carrier carrier)
		{
			List<string> simIdentifiersList = new List<string>()
			{
				SIMIdentifiers.ICCID.ToString(),
				SIMIdentifiers.EID.ToString()
			};

			//Validate sim identifier
			if (!simIdentifiersList.Contains(this.businessRuleContext.CustomerViewModel.SimIdentifier))
			{
				return CommonHelper.GetResponseDetails("400", "Wrong SIM identifier (valid values EID or ICCID)");
			}

			//Validate sim identifier value
			if (this.businessRuleContext.CustomerViewModel.SimIdentifierValue == string.Empty)
			{
				return CommonHelper.GetResponseDetails("400", "SIM identifier (EID or ICCID) is empty");
			}

			//Validate SIM in Inventory
//			if (this.businessRuleContext.CustomerViewModel.SimIdentifier == "ICCID")
//			{
			if (inventorySIMModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "This SIM was not found in the inventory database");
			}
			//			}

			if (carrier == null)
			{
				return CommonHelper.GetResponseDetails("404", "Cannot find the carrier for this SIM in the database");
			}


			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion

		#region ValidateChangePlan
		ResponseDetails IChangeLogic.ValidateChangePlan(Item itemPlan, Inventory inventorySIMModel, Carrier carrier)
		{
			List<string> simIdentifiersList = new List<string>()
			{
				SIMIdentifiers.ICCID.ToString(),
				SIMIdentifiers.EID.ToString()
			};

			//Validate sim identifier
			if (!simIdentifiersList.Contains(this.businessRuleContext.CustomerViewModel.SimIdentifier))
			{
				return CommonHelper.GetResponseDetails("400", "Wrong SIM identifier (valid values EID or ICCID)");
			}

			//Validate sim identifier value
			if (this.businessRuleContext.CustomerViewModel.SimIdentifierValue == string.Empty)
			{
				return CommonHelper.GetResponseDetails("400", "SIM identifier (EID or ICCID) is empty");
			}

			//Validate SIM in Inventory
			//			if (this.businessRuleContext.CustomerViewModel.SimIdentifier == "ICCID")
			//			{
			if (inventorySIMModel == null)
			{
				return CommonHelper.GetResponseDetails("404", "This SIM was not found in the inventory database");
			}
			//			}

			if (carrier == null)
			{
				return CommonHelper.GetResponseDetails("404", "Cannot find the carrier for this SIM in the database");
			}

			if (itemPlan == null)
			{
				return CommonHelper.GetResponseDetails("404", "Plan was not found in the database");
			}

			return CommonHelper.GetResponseDetails("200", string.Empty);
		}
		#endregion


		#endregion
	}
}