using IOTCell.BL.BusinessRules.Context;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public class AddressLogic : IAddressLogic
	{
		#region Properties
		#region BusinessRulesContext
		private readonly IBusinessRuleContext businessRuleContext;
		public IBusinessRuleContext BusinessRuleContext
		{
			get
			{
				return businessRuleContext;
			}
		}
		#endregion
		#endregion

		#region Constructors
		public AddressLogic(IBusinessRuleContext businessRuleContext)
		{
			this.businessRuleContext = businessRuleContext;
		}
		#endregion

		#region Methods
		#region ValidateAddress
		ResponseDetails IAddressLogic.ValidateAddress(Address adddressModel)
		{

            if ((adddressModel.AddressLine1 == string.Empty) && 
                (adddressModel.AddressLine2 == string.Empty))
            {
                return CommonHelper.GetResponseDetails("400", "Customer's address is empty");
            }

            if (adddressModel.City == string.Empty)
            {
                return CommonHelper.GetResponseDetails("400", "Customer's city address is empty");
            }

            if (adddressModel.Zipcode == string.Empty)
            {
                return CommonHelper.GetResponseDetails("400", "Customer's zip code is empty");
            }

            if (adddressModel.State == string.Empty)
            {
                return CommonHelper.GetResponseDetails("400", "Customer's state is empty");
            }

            if (adddressModel.Country == string.Empty)
            {
                adddressModel.Country = "US";
            }

            return CommonHelper.GetResponseDetails("200", string.Empty);
        }
        #endregion

        #endregion
    }
}