using IOTCell.BL.BusinessRules.Context;
using IOTCell.DAL.Models;
using IOTCell.Framework.Common;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public class CustomerLogic : ICustomerLogic
	{
		#region Properties
		#region BusinessRulesContext
		private readonly IBusinessRuleContext businessRuleContext;
		public IBusinessRuleContext BusinessRuleContext
		{
			get
			{
				return businessRuleContext;
			}
		}
		#endregion
		#endregion

		#region Constructors
		public CustomerLogic(IBusinessRuleContext businessRuleContext)
		{
			this.businessRuleContext = businessRuleContext;
		}
		#endregion

		#region Methods
		#region ValidateNewCustomer
		ResponseDetails ICustomerLogic.ValidateNewCustomer(Customer customerModel)
		{
            if (customerModel.FirstName == string.Empty)
            {
                return CommonHelper.GetResponseDetails("400", "Customer's first name is empty");
            }

            if (customerModel.LastName == string.Empty)
            {
                return CommonHelper.GetResponseDetails("400", "Customer's last name is empty");
            }

            if (customerModel.Language == string.Empty)
            {
                customerModel.Language = "en";
            }

            return CommonHelper.GetResponseDetails("200", string.Empty);
        }
        #endregion

        #region ValidateExistingCustomer
        ResponseDetails ICustomerLogic.ValidateExistingCustomer(Customer customerModel)
        {
            if (customerModel == null)
            {
                return CommonHelper.GetResponseDetails("404", "Customer not found or wrong customer account number");
            }

            return CommonHelper.GetResponseDetails("200", string.Empty);
        }
        #endregion
        #endregion
    }
}