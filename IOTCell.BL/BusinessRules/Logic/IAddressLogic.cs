using IOTCell.DAL.Models;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface IAddressLogic
	{
		ResponseDetails ValidateAddress(Address addressModel);
	}
}