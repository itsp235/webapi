using IOTCell.DAL.Models;
using IOTCell.Framework.Models;

namespace IOTCell.BL.BusinessRules
{
	public interface ICustomerLogic
	{
		ResponseDetails ValidateNewCustomer(Customer customerModel);
		ResponseDetails ValidateExistingCustomer(Customer customerModel);
	}
}