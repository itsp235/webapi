﻿using Newtonsoft.Json;

namespace IOTCell.Custom.IBasis.BL
{
    public class RootChangePlanOld
    {
        [JsonProperty("Plan")]
        public string Plan { get; set; }
    }
}