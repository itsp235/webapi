﻿using Newtonsoft.Json;

namespace IOTCell.Custom.IBasis.BL
{
    public class Activation
    {
        [JsonProperty("service")]
        public Service Service { get; set; }
    }
}