﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace IOTCell.Custom.IBasis.BL
{
    public class Service
    {
        [JsonProperty("characteristics")]
        public List<Characteristic> Characteristics { get; set; }
    }
}