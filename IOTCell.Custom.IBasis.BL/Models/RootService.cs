﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace IOTCell.Custom.IBasis.BL
{
    public class RootService
    {
        [JsonProperty("mode")]
        public string Mode { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

    }

 
}
