﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.WebUtilities;

using IOTCell.BL.ClientControllerProvider;
using IOTCell.BL.Diagnostics;
using IOTCell.Framework.Models;
using IOTCell.BL.ViewModels;
using IOTCell.Framework.Enums;
using IOTCell.Framework.Configuration;
using IOTCell.Custom.IBasis.BL.Configuration;

//using Newtonsoft.Json;

namespace IOTCell.Custom.IBasis.BL
{
    public class CustomBLProvider : IClientControllerProvider
    {
        #region Members
        private IConfiguration configuration;
        private IActivityEntryLogger logger;
        private IClientAppSettings clientAppSettings;
        #endregion

        #region Methods
        #region Init
        void IClientControllerProvider.Init(IConfiguration configuration, IActivityEntryLogger logger, NameValueCollection settings)
        {
            this.configuration = configuration;
            this.clientAppSettings = new ClientAppSettings(this.configuration);
            this.logger = logger;
        }
        #endregion

        #region ActivateService
        async Task<ResponseDetails> IClientControllerProvider.ActivateService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIActivationURL = this.clientAppSettings.IBasisAPIActivation;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;
            string planType = this.clientAppSettings.IBasisAPIPlanType;

            Characteristic characteristic = new Characteristic
            {
                Name = customerViewModel.SimIdentifier,
                Value = customerViewModel.SimIdentifierValue
            };

            Characteristic characteristic1 = new Characteristic
            {
                Name = planType,
                Value = customerViewModel .PlanCode
            };

            List<Characteristic> list = new List<Characteristic>
            {
                characteristic,
                characteristic1
            };

            Service service = new Service
            {
                Characteristics = list
            };

            Activation activation = new Activation
            {
                Service = service
            };

            List<Activation> activations = new List<Activation>
            {
                activation
            };

            Root root = new Root
            {
                Activations = activations
            };

            string jsonString = root.ToString();

            var parameters = new Dictionary<string, string>() { { "MVNO", clientAPIMVNO } };
            var requestUri = QueryHelpers.AddQueryString(clientAPIActivationURL, parameters);

            using (var client = new HttpClient())
            {


                //r authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Post, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Activation Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Activation Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ActivationResultBody activationResultBody = JsonSerializer.Deserialize<ActivationResultBody>(resultBody, options);                       
                if (activationResultBody.Result != null)
                {
                    activationResultBody.ICCID = activationResultBody.Result.ICCID;
                    activationResultBody.Status = activationResultBody.Result.Status;
                    activationResultBody.EID = activationResultBody.Result.EID;
                    activationResultBody.Plan = activationResultBody.Result.Plan;
                    activationResultBody.EffectiveDate = activationResultBody.Result.EffectiveDate;
                }
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = activationResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion

        #region GetUsage
        async Task<ResponseDetails> IClientControllerProvider.GetUsage(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIGetUsageURL = this.clientAppSettings.IBasisAPIGetUsage + "/" + customerViewModel.SimIdentifierValue;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            //var parameters = new Dictionary<string, string>
            //{
            //    { customerViewModel.SimIdentifier.ToLower(), customerViewModel.SimIdentifierValue },
            //    { "fromDate", DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd") },
            //    { "toDate", DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd") },
            //    { "MVNO", clientAPIMVNO }
            //};




            var parameters = new Dictionary<string, string>
            {
//                { customerViewModel.SimIdentifier.ToLower(), customerViewModel.SimIdentifierValue },
                { "fromDate", DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd") },
                { "toDate", DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd") },
                { "filter", "day" },
                { "type", customerViewModel.SimIdentifier.ToUpper() },
                { "MVNO", clientAPIMVNO }
            };



            var requestUri = QueryHelpers.AddQueryString(clientAPIGetUsageURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                UsageResultBody usageResultBody = JsonSerializer.Deserialize<UsageResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = usageResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = usageResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region GetCalculatedUsage
        async Task<ResponseDetails> IClientControllerProvider.GetCalculatedUsage(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIGetUsageURL = this.clientAppSettings.IBasisAPIGetUsage + "/" + customerViewModel.SimIdentifierValue;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
                { "fromDate", DateTime.MinValue.ToString("yyyy-MM-dd") },
                { "toDate", DateTime.Now.ToString("yyyy-MM-dd") },
                { "filter", "Month"},
                { "type", customerViewModel.SimIdentifier},
                { "MVNO", clientAPIMVNO }
            };

            var requestUri = QueryHelpers.AddQueryString(clientAPIGetUsageURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Usage Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                UsageMonthResultBody usageMonthResultBody = JsonSerializer.Deserialize<UsageMonthResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = usageMonthResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = usageMonthResultBody;
            }

            return responseDetails;
        }
        #endregion



        #region GetTotalUsage
        async Task<ResponseDetails> IClientControllerProvider.GetTotalUsage(CustomerViewModel customerViewModel)
        {

            //string userName = this.clientAppSettings.GetEncryptedValue("");
            //string pwd = this.clientAppSettings.GetEncryptedValue("");
            //string uatUser = this.clientAppSettings.GetEncryptedValue("");

            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIGetTotalUsageURL = this.clientAppSettings.IBasisAPIGetTotalUsage;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
                { "fromDate", DateTime.Parse(customerViewModel.FromDate).ToString("yyyy-MM-dd") },
                { "toDate", DateTime.Parse(customerViewModel.ToDate).ToString("yyyy-MM-dd") },
                { "filter", "Day"},
                { "MVNO", clientAPIMVNO }
            };

            var requestUri = QueryHelpers.AddQueryString(clientAPIGetTotalUsageURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Total Usage Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Total Usage Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                TotalUsageResultBody totalUsageResultBody = JsonSerializer.Deserialize<TotalUsageResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = totalUsageResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = totalUsageResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region TerminateService
        async Task<ResponseDetails> IClientControllerProvider.TerminateService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIServicesURL = this.clientAppSettings.IBasisAPIServices;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
//                { customerViewModel.SimIdentifierValue,customerViewModel.SimIdentifier.ToLower()},
                { "MVNO", clientAPIMVNO }
            };

            RootService root = new RootService
            {
                Mode = "Cancel"
            };

            string jsonString = root.ToString();

            clientAPIServicesURL = clientAPIServicesURL + "/" + customerViewModel.SimIdentifierValue;

            var requestUri = QueryHelpers.AddQueryString(clientAPIServicesURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Patch, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Terminate service Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Terminate service Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options2 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ActivationResultBody activationResultBody = JsonSerializer.Deserialize<ActivationResultBody>(resultBody, options2);
                if (activationResultBody.Result != null)
                {
                    activationResultBody.Status = activationResultBody.Result.Status;
                    activationResultBody.ICCID = activationResultBody.Result.ICCID;
                    activationResultBody.EID = activationResultBody.Result.EID;
                    activationResultBody.Plan = activationResultBody.Result.Plan;
                    activationResultBody.EffectiveDate = activationResultBody.Result.EffectiveDate;
                }
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = activationResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region SuspendService
        async Task<ResponseDetails> IClientControllerProvider.SuspendService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIServicesURL = this.clientAppSettings.IBasisAPIServices;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
//                { customerViewModel.SimIdentifierValue,customerViewModel.SimIdentifier.ToLower()},
                { "MVNO", clientAPIMVNO }
            };

            RootService root = new RootService
            {
                Mode = "Suspend"
            };

            string jsonString = root.ToString();

            clientAPIServicesURL = clientAPIServicesURL + "/" + customerViewModel.SimIdentifierValue;

            var requestUri = QueryHelpers.AddQueryString(clientAPIServicesURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Patch, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Suspend service Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Suspend service Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options2 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ActivationResultBody activationResultBody = JsonSerializer.Deserialize<ActivationResultBody>(resultBody, options2);
                if (activationResultBody.Result != null)
                {
                    activationResultBody.Status = activationResultBody.Result.Status;
                    activationResultBody.ICCID = activationResultBody.Result.ICCID;
                    activationResultBody.EID = activationResultBody.Result.EID;
                    activationResultBody.Plan = activationResultBody.Result.Plan;
                    activationResultBody.EffectiveDate = activationResultBody.Result.EffectiveDate;
                }
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = activationResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region ResumeService
        async Task<ResponseDetails> IClientControllerProvider.ResumeService(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIServicesURL = this.clientAppSettings.IBasisAPIServices;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
//                { customerViewModel.SimIdentifierValue,customerViewModel.SimIdentifier.ToLower()},
                { "MVNO", clientAPIMVNO }
            };

            RootService root = new RootService
            {
                Mode = "Resume"
            };

            string jsonString = root.ToString();


            clientAPIServicesURL = clientAPIServicesURL + "/" + customerViewModel.SimIdentifierValue;

            var requestUri = QueryHelpers.AddQueryString(clientAPIServicesURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Patch, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Resume service Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Resume service Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options2 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ActivationResultBody activationResultBody = JsonSerializer.Deserialize<ActivationResultBody>(resultBody, options2);
                if (activationResultBody.Result != null)
                {
                    activationResultBody.Status = activationResultBody.Result.Status;
                    activationResultBody.ICCID = activationResultBody.Result.ICCID;
                    activationResultBody.EID = activationResultBody.Result.EID;
                    activationResultBody.Plan = activationResultBody.Result.Plan;
                    activationResultBody.EffectiveDate = activationResultBody.Result.EffectiveDate;
                }
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = activationResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion


        #region ChangeServicePlan
        async Task<ResponseDetails> IClientControllerProvider.ChangeServicePlan(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIServicesURL = this.clientAppSettings.IBasisAPIServices;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
//                { customerViewModel.SimIdentifierValue,customerViewModel.SimIdentifier.ToLower()},
                { "MVNO", clientAPIMVNO }
            };


            string planType = this.clientAppSettings.IBasisAPIPlanType;


            Characteristic characteristic = new Characteristic
            {
                Name = planType,
                Value = customerViewModel .PlanCode
            };

            List<Characteristic> charactheristics = new List<Characteristic>
            {
                characteristic
            };


            Service service = new Service
            {
                Characteristics = charactheristics
            };

            RootServiceChangePlan rootServiceChangePlan = new RootServiceChangePlan
            {
                Service = service
            };

            string jsonString = rootServiceChangePlan.ToString();


            clientAPIServicesURL = clientAPIServicesURL + "/" + customerViewModel.SimIdentifierValue;

            var requestUri = QueryHelpers.AddQueryString(clientAPIServicesURL, parameters);

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Patch, requestUri);
                using var stringContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);
                request.Content = stringContent;

                await this.logger.AddEntryWithFlush("Change service plan Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Change service plan Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options2 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ActivationResultBody activationResultBody = JsonSerializer.Deserialize<ActivationResultBody>(resultBody, options2);
                if (activationResultBody.Result != null)
                {
                    activationResultBody.Status = activationResultBody.Result.Status;
                    activationResultBody.ICCID = activationResultBody.Result.ICCID;
                    activationResultBody.EID = activationResultBody.Result.EID;
                    activationResultBody.Plan = activationResultBody.Result.Plan;
                    activationResultBody.EffectiveDate = activationResultBody.Result.EffectiveDate;
                }
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = activationResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = activationResultBody;
            }

            return responseDetails;
        }
        #endregion



        #region GetAvailableServicePlans
        async Task<ResponseDetails> IClientControllerProvider.GetAvailableServicePlans(CustomerViewModel customerViewModel)
        {
            ResponseDetails responseDetails = new ResponseDetails();

            string clientAPIProductsURL = this.clientAppSettings.IBasisAPIProducts;
            string clientAPIMVNO = customerViewModel.CompanyMVNO;

            var parameters = new Dictionary<string, string>
            {
                { "MVNO", clientAPIMVNO }
            };

            var requestUri = QueryHelpers.AddQueryString(clientAPIProductsURL, parameters);
            

            using (var client = new HttpClient())
            {
                //var authenticationString = this.clientAppSettings.IBasisIBSUsr + ":" + this.clientAppSettings.IBasisIBSPwd;
                var authenticationString = clientAppSettings.GetDecodedValue(customerViewModel.IB_UserName) + ":" + clientAppSettings.GetDecodedValue(customerViewModel.IB_Password);

                var base64EncodedAuthenticationString = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(authenticationString));

                using var request = new HttpRequestMessage(HttpMethod.Get, requestUri);

                request.Headers.Authorization = new AuthenticationHeaderValue("Basic", base64EncodedAuthenticationString);

                await this.logger.AddEntryWithFlush("Get Available Service Plans Client API", "Send API Request");

                using var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead)
                    .ConfigureAwait(true);

                await this.logger.AddEntryWithFlush("Get Available Service Plans Client API", "Receive API Response");

                string resultBody = string.Empty;

                using (HttpContent content = response.Content)
                {
                    resultBody = content.ReadAsStringAsync().Result;
                }

                var options1 = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                };

                ServicePlanResultBody servicePlanResultBody = JsonSerializer.Deserialize<ServicePlanResultBody>(resultBody, options1);
                responseDetails.StatusCode = response.StatusCode.ToString();
                responseDetails.Message = servicePlanResultBody.Messages[0].ToString();
                responseDetails.ResultResponse = servicePlanResultBody;
            }

            return responseDetails;
        }
        #endregion



        //#region GetToken
        //async Task<ResponseDetails> GetToken(CustomerViewModel customerViewModel)
        //{
        //    ResponseDetails responseDetails = new ResponseDetails();


        //    return responseDetails;
        //}
        //#endregion



        #endregion
    }
}