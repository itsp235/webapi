﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace IOTCell.Custom.IBasis.BL.Configuration
{
    public interface IClientAppSettings
    {
        string IBasisAPIActivation { get; }
        string IBasisAPIGetUsage { get; }
        string IBasisAPIGetTotalUsage { get; }
        string IBasisAPIServices { get; }
        string IBasisAPIProducts { get; }
        string IBasisAPIPlanType { get; }
        string IBasisIBSUsr { get; }
        string IBasisIBSPwd { get; }

        string GetEncryptedValue(string value);
        string GetDecryptedValue(string value);

        string GetDecodedValue(string value);
        string GetEncodedValue(string value);
    }
}