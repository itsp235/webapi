﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using IOTCell.Framework.Common;

namespace IOTCell.Custom.IBasis.BL.Configuration
{
    public class ClientAppSettings : IClientAppSettings
    {
        #region Members
        private readonly IConfiguration configuration;
        #endregion

        #region Properties
        #region IBasisAPIActivation
        string IClientAppSettings.IBasisAPIActivation
        {
            get { return this.GetSettingAsString(this.configuration["IBasisAppSettings:IBasisAPIActivation"]); }
        }
        #endregion

        #region IBasisAPIGetUsage
        string IClientAppSettings.IBasisAPIGetUsage
        {
            get { return this.GetSettingAsString(this.configuration["IBasisAppSettings:IBasisAPIGetUsage"]); }
        }
        #endregion


        #region IBasisAPIGetTotalUsage
        string IClientAppSettings.IBasisAPIGetTotalUsage
        {
            get { return this.GetSettingAsString(this.configuration["IBasisAppSettings:IBasisAPIGetTotalUsage"]); }
        }
        #endregion

        #region IBasisAPIServices
        string IClientAppSettings.IBasisAPIServices
        {
            get { return this.GetSettingAsString(this.configuration["IBasisAppSettings:IBasisAPIServices"]); }
        }

        #region IBasisAPIProducts
        string IClientAppSettings.IBasisAPIProducts
        {
            get { return this.GetSettingAsString(this.configuration["IBasisAppSettings:IBasisAPIProducts"]); }
        }
        #endregion



        #endregion



        #region IBasisAPIPlanType
        string IClientAppSettings.IBasisAPIPlanType
        {
            get { return this.GetSettingAsString(this.configuration["IBasisAppSettings:IBasisAPIPlanType"]); }
        }
        #endregion

        #region IBasisIBSUsr
        string IClientAppSettings.IBasisIBSUsr
        {
            get { return this.GetSettingAsString(this.PrivateGetDecryptedValue(this.configuration["IBasisAppSettings:IBasisIBSUsr"])); }
        }
        #endregion

        #region IBasisIBSPwd
        string IClientAppSettings.IBasisIBSPwd
        {
            get { return this.GetSettingAsString(this.PrivateGetDecryptedValue(this.configuration["IBasisAppSettings:IBasisIBSPwd"])); }
        }
        #endregion


        #endregion

        #region Constructor
        public ClientAppSettings(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        #endregion

        #region Methods
        #region GetSetting
        private object GetSetting(string value, Type type, object defaultValue)
        {
            bool exists = (value != null && value.Length > 0);
            if (exists)
            {
                object result = Converter.ConvertToType(type, value, string.Empty, defaultValue);
                return result;
            }
            else
            {
                return defaultValue;
            }
        }
        #endregion

        #region GetSettingAsString
        private string GetSettingAsString(string value)
        {
            return (string)this.GetSetting(value, typeof(string), string.Empty);
        }
        #endregion

        #region GetSettingAsBool
        private bool GetSettingAsBool(string key)
        {
            return (bool)this.GetSetting(key, typeof(bool), false);
        }

        private bool GetSettingAsBool(string key, bool defaultValue)
        {
            return (bool)this.GetSetting(key, typeof(bool), defaultValue);
        }
        #endregion

        #region GetSettingAsDouble
        private double GetSettingAsDouble(string key)
        {
            return (double)this.GetSetting(key, typeof(double), 0F);
        }
        #endregion

        #region GetSettingAsInt32
        private int GetSettingAsInt32(string key)
        {
            return (int)this.GetSetting(key, typeof(int), 0);
        }
        #endregion

        #region GetSettingAsLong
        private long GetSettingAsLong(string key)
        {
            return (long)this.GetSetting(key, typeof(long), 0);
        }
        #endregion

        #region GetSettingAsTimeSpan
        private TimeSpan GetSettingAsTimeSpan(string key)
        {
            string valueAsString = (string)this.GetSetting(key, typeof(string), TimeSpan.Zero.ToString());
            return TimeSpan.Parse(valueAsString);
        }
        #endregion

        #region GetEncryptedValue
        string IClientAppSettings.GetEncryptedValue(string value)
        {
            return EncryptionHelper.Encrypt(value,
                "rESThELWebaPiForioT",
                "ITSPhEl02032022IoT%ReSt-!^#%Web",
                "SHA512",
                2,
                "ItSp2021IoT*<@>Z",
                256);
        }
        #endregion

        #region PrivateGetDecryptedValue
        private string PrivateGetDecryptedValue(string value)
        {
            return EncryptionHelper.Decrypt(value,
                "rESThELWebaPiForioT",
                "ITSPhEl02032022IoT%ReSt-!^#%Web",
                "SHA512",
                2,
                "ItSp2021IoT*<@>Z",
                256);
        }
        #endregion

        #region GetDecryptedValue
        string IClientAppSettings.GetDecryptedValue(string value)
        {
            return this.PrivateGetDecryptedValue(value);
        }
        #endregion



        #region GetDecodedValue
        string IClientAppSettings.GetDecodedValue(string value)
        {
            if (value == null)
                return null;
            else
            {
                var base64EncodedBytes = System.Convert.FromBase64String(value);
                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
        }
        #endregion

        #region GetEncodedValue
        string IClientAppSettings.GetEncodedValue(string value)
        {
            if (value == null)
                return null;
            else
            {
                var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(value);
                return System.Convert.ToBase64String(plainTextBytes);
            }
        }

        #endregion



        #endregion
    }
}